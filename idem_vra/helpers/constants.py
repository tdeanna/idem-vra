"""
vRA sub constants module.

.. data:: NAME

    Key used to set or access the name of a state in the StateReturn.

.. data:: CHANGES

    Key used to set or access the changes to an object that happend
    after the execution of a state.

.. data:: RESULT

    Key used to set or access the result of a state or exec.

.. data:: COMMENT

    Key used to set or access the comment of a state or exec.

.. data:: RET

    Key used to set or access the return object from an exec.
"""

NAME = "name"
CHANGES = "changes"
RESULT = "result"
COMMENT = "comment"
RET = "ret"
OLD_STATE = "old_state"
NEW_STATE = "new_state"

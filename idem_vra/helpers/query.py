from typing import Any

from jsonpath_ng.ext import parse


def query(path: str, obj: Any):
    jsonpath_expression = parse(path)
    return [match.value for match in jsonpath_expression.find(obj)]

from idem_vra.helpers.mapper import add_properties
from idem_vra.helpers.mapper import omit_properties
from idem_vra.helpers.models import StateReturn


__contracts__ = ["resource"]

TREQ = {"present": {"require": ["vra.iaas.project.present"]}, "absent": {"require": []}}


async def present(hub, ctx, name: str, **kwargs):

    """

    :param string cloudAccountSelfLinkId: (optional in body)

    :param integer createdMillis: (optional in body)

    :param object definition: (optional in body) The definition varies depending on the type of cluster plan. Example
      shown below is for cluster plans of type TANZU_CLUSTER_PLAN. In that
      case the definition is equivalent to the spec of a Tanzu Kubernetes
      cluster in JSON format. Here is a documentation
      https://docs.vmware.com/en/VMware-vSphere/7.0/vmware-vsphere-with-
      tanzu/GUID-B1034373-8C38-4FE2-9517-345BF7271A1E.html
      example: {"spec":{"distribution":{"version":"1.20"},"topology":{"contr
      olPlane":{"count":1,"class":"best-effort-xsmall","storageClass":"vsan-
      default-storage-policy"},"workers":{"count":1,"class":"best-effort-
      xsmall","storageClass":"vsan-default-storage-
      policy"}},"settings":{"storage":{"defaultClass":"","classes":[]}}}}

    :param string description: (optional in body)

    :param string id: (optional in body)

    :param string name: (optional in body)

    :param string orgId: (optional in body)

    :param string type: (optional in body)

    :param integer updatedMillis: (optional in body)

    """

    try:
        state = ClusterplansState(hub, ctx)
        return await state.present(hub, ctx, name, **kwargs)
    except Exception as error:
        hub.log.error("Error during enforcing present state: clusterplans")
        hub.log.error(str(error))
        raise error


async def absent(hub, ctx, name: str, **kwargs):

    """

    :param string p_id: (required in path) id

    """

    """
    :param string name: (required) name of the resource
    """
    try:
        state = ClusterplansState(hub, ctx)
        return await state.absent(hub, ctx, name, **kwargs)
    except Exception as error:
        hub.log.error("Error during enforcing absent state: clusterplans")
        hub.log.error(str(error))
        raise error


async def describe(hub, ctx):
    try:
        state = ClusterplansState(hub, ctx)
        return await state.describe(hub, ctx)
    except Exception as error:
        hub.log.error("Error during describe: clusterplans")
        hub.log.error(str(error))
        raise error


def is_pending(hub, ret: dict, state: str = None, **pending_kwargs):
    try:
        state = ClusterplansState(hub, None)
        return state.is_pending(hub, ret, state, **pending_kwargs)
    except Exception as error:
        hub.log.error("Error during is_pending: clusterplans")
        hub.log.error(str(error))
        raise error


class ClusterplansState:
    def __init__(self, hub, ctx):
        self.hub = hub
        self.ctx = ctx

    async def present(self, hub, ctx, name: str, **kwargs):

        search_result = (await self.paginate_find(hub, ctx))["ret"]

        for s in search_result.content:
            if name == s["name"] and True:
                hub.log.info(
                    f'Returning resource clusterplans "{s["name"]}" due to existing resource "{name}"'
                )
                s = await self.remap_resource_structure(hub, ctx, s)
                return StateReturn(
                    result=True,
                    comment=f"Resource clusterplans {name} already exists.",
                    old=s,
                    new=s,
                )
        kwargs["name"] = name
        res = (await hub.exec.vra.cmx.clusterplans.create_using_post(ctx, **kwargs))[
            "ret"
        ]
        res = await self.remap_resource_structure(hub, ctx, res)

        return StateReturn(
            result=True,
            comment=f"Creation of clusterplans {name} success.",
            old=None,
            new=res,
        )

    async def absent(self, hub, ctx, name: str, **kwargs):

        search_result = (await self.paginate_find(hub, ctx))["ret"]

        resource = None
        for s in search_result.content:
            if name == s["name"] and True:
                hub.log.info(
                    f'Found resource clusterplans "{s["name"]}" due to existing resource "{name}"'
                )
                s = await self.remap_resource_structure(hub, ctx, s)
                resource = s

        if resource:
            # it exists!

            delete_kwargs = {}
            delete_kwargs["p_id"] = resource.get("id")

            hub.log.debug(
                f"clusterplans with name = {resource.get('name')} already exists"
            )
            await hub.exec.vra.cmx.clusterplans.delete_using_delete(
                ctx, **delete_kwargs
            )

            return StateReturn(
                result=True,
                comment=f"Resource with name = {resource.get('name')} deleted.",
                old=resource,
                new=None,
            )

        return StateReturn(
            result=True,
            comment=f"Resource with name = {name} is already absent.",
            old=None,
            new=None,
        )

    async def describe(self, hub, ctx):

        result = {}
        res = await self.paginate_find(hub, ctx)

        for obj in res.get("ret", {}).get("content", []):

            # Keep track of name and id properties as they may get remapped
            obj_name = obj.get("name", "unknown")
            obj_id = obj.get("id", "unknown")

            obj = await self.remap_resource_structure(hub, ctx, obj)

            # Define props
            props = [{key: value} for key, value in obj.items()]

            # Build result
            result[f"{obj_name}-{obj_id.split('-')[-1]}"] = {
                "vra.cmx.clusterplans.present": props
            }

        return result

    async def paginate_find(self, hub, ctx, **kwargs):
        """
        Paginate through all resources using their 'find' method.
        """
        res = await hub.exec.vra.cmx.clusterplans.search_cluster_plans_using_get(
            ctx, **kwargs
        )

        numberOfElements = res.get("ret", {}).get("numberOfElements", 0)
        totalElements = res.get("ret", {}).get("totalElements", 0)
        initialElements = numberOfElements
        if numberOfElements != totalElements and totalElements != 0:
            while initialElements < totalElements:
                hub.log.debug(
                    f"Requesting clusterplans with offset={initialElements} out of {totalElements}"
                )
                pres = (
                    await hub.exec.vra.cmx.clusterplans.search_cluster_plans_using_get(
                        ctx, skip=initialElements
                    )
                )
                initialElements += pres.get("ret", {}).get("numberOfElements", 0)
                aggO = res.get("ret", {}).get("content", [])
                aggN = pres.get("ret", {}).get("content", [])
                res["ret"]["content"] = [*aggO, *aggN]
                res["ret"]["numberOfElements"] = initialElements

        return res

    def is_pending(self, hub, ret: dict, state: str = None, **pending_kwargs):
        """
        State reconciliation
        """
        hub.log.debug(f'Running is_pending for resource: {ret.get("__id__", None)}...')
        is_pending_result = False
        hub.log.debug(
            f'is_pending_result for resource "{ret.get("__id__", None)}": {is_pending_result}'
        )
        return is_pending_result

    async def remap_resource_structure(self, hub, ctx, obj: dict) -> dict:
        schema_mapper = None

        # Perform resource mapping by adding properties and omitting properties.
        # Property renaming is addition followed by omission.
        if schema_mapper:
            resource_name = "clusterplans"
            hub.log.debug(f"Remapping resource {resource_name}...")
            obj = await add_properties(obj, schema_mapper.get("add", []))
            obj = omit_properties(obj, schema_mapper.get("omit", []))

        return obj

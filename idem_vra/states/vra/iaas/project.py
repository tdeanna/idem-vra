from idem_vra.helpers.mapper import add_properties
from idem_vra.helpers.mapper import omit_properties
from idem_vra.helpers.models import StateReturn


__contracts__ = ["resource"]

TREQ = {
    "present": {"require": ["vra.iaas.location.present"]},
    "absent": {"require": ["vra.blueprint.blueprint.absent"]},
}


async def present(hub, ctx, name: str, **kwargs):

    """

    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option.

    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about

    :param array viewers: (optional in body) List of viewer users associated with the project.

    :param string machineNamingTemplate: (optional in body) The naming template to be used for machines provisioned in this
      project

    :param object customProperties: (optional in body) The project custom properties which are added to all requests in this
      project

    :param boolean sharedResources: (optional in body) Specifies whether the resources in this projects are shared or not. If
      not set default will be used.

    :param integer operationTimeout: (optional in body) The timeout that should be used for Blueprint operations and
      Provisioning tasks. The timeout is in seconds

    :param array members: (optional in body) List of member users associated with the project.

    :param array zoneAssignmentConfigurations: (optional in body) List of configurations for zone assignment to a project.

    :param string description: (optional in body) A human-friendly description.

    :param string placementPolicy: (optional in body) Placement policy for the project. Determines how a zone will be
      selected for provisioning. DEFAULT, SPREAD or SPREAD_MEMORY.

    :param object constraints: (optional in body) List of storage, network and extensibility constraints to be applied
      when provisioning through this project.

    :param array administrators: (optional in body) List of administrator users associated with the project. Only
      administrators can manage project's configuration.

    """

    try:
        state = ProjectStateImpl(hub, ctx)
        return await state.present(hub, ctx, name, **kwargs)
    except Exception as error:
        hub.log.error("Error during enforcing present state: project")
        hub.log.error(str(error))
        raise error


async def absent(hub, ctx, name: str, **kwargs):

    """

    :param string p_id: (required in path) The ID of the project.

    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about

    """

    """
    :param string name: (required) name of the resource
    """
    try:
        state = ProjectStateImpl(hub, ctx)
        return await state.absent(hub, ctx, name, **kwargs)
    except Exception as error:
        hub.log.error("Error during enforcing absent state: project")
        hub.log.error(str(error))
        raise error


async def describe(hub, ctx):
    try:
        state = ProjectStateImpl(hub, ctx)
        return await state.describe(hub, ctx)
    except Exception as error:
        hub.log.error("Error during describe: project")
        hub.log.error(str(error))
        raise error


def is_pending(hub, ret: dict, state: str = None, **pending_kwargs):
    try:
        state = ProjectStateImpl(hub, None)
        return state.is_pending(hub, ret, state, **pending_kwargs)
    except Exception as error:
        hub.log.error("Error during is_pending: project")
        hub.log.error(str(error))
        raise error


class ProjectState:
    def __init__(self, hub, ctx):
        self.hub = hub
        self.ctx = ctx

    async def present(self, hub, ctx, name: str, **kwargs):

        search_result = (await self.paginate_find(hub, ctx))["ret"]

        for s in search_result.content:
            if name == s["name"] and True:
                hub.log.info(
                    f'Returning resource project "{s["name"]}" due to existing resource "{name}"'
                )
                s = await self.remap_resource_structure(hub, ctx, s)
                return StateReturn(
                    result=True,
                    comment=f"Resource project {name} already exists.",
                    old=s,
                    new=s,
                )
        res = (await hub.exec.vra.iaas.project.create_project(ctx, name, **kwargs))[
            "ret"
        ]
        res = await self.remap_resource_structure(hub, ctx, res)

        return StateReturn(
            result=True,
            comment=f"Creation of project {name} success.",
            old=None,
            new=res,
        )

    async def absent(self, hub, ctx, name: str, **kwargs):

        search_result = (await self.paginate_find(hub, ctx))["ret"]

        resource = None
        for s in search_result.content:
            if name == s["name"] and True:
                hub.log.info(
                    f'Found resource project "{s["name"]}" due to existing resource "{name}"'
                )
                s = await self.remap_resource_structure(hub, ctx, s)
                resource = s

        if resource:
            # it exists!

            delete_kwargs = {}
            delete_kwargs["p_id"] = resource.get("id")

            hub.log.debug(f"project with name = {resource.get('name')} already exists")
            await hub.exec.vra.iaas.project.delete_project(ctx, **delete_kwargs)

            return StateReturn(
                result=True,
                comment=f"Resource with name = {resource.get('name')} deleted.",
                old=resource,
                new=None,
            )

        return StateReturn(
            result=True,
            comment=f"Resource with name = {name} is already absent.",
            old=None,
            new=None,
        )

    async def describe(self, hub, ctx):

        result = {}
        res = await self.paginate_find(hub, ctx)

        for obj in res.get("ret", {}).get("content", []):

            # Keep track of name and id properties as they may get remapped
            obj_name = obj.get("name", "unknown")
            obj_id = obj.get("id", "unknown")

            obj = await self.remap_resource_structure(hub, ctx, obj)

            # Define props
            props = [{key: value} for key, value in obj.items()]

            # Build result
            result[f"{obj_name}-{obj_id.split('-')[-1]}"] = {
                "vra.iaas.project.present": props
            }

        return result

    async def paginate_find(self, hub, ctx, **kwargs):
        """
        Paginate through all resources using their 'find' method.
        """
        res = await hub.exec.vra.iaas.project.get_projects(ctx, **kwargs)

        numberOfElements = res.get("ret", {}).get("numberOfElements", 0)
        totalElements = res.get("ret", {}).get("totalElements", 0)
        initialElements = numberOfElements
        if numberOfElements != totalElements and totalElements != 0:
            while initialElements < totalElements:
                hub.log.debug(
                    f"Requesting project with offset={initialElements} out of {totalElements}"
                )
                pres = await hub.exec.vra.iaas.project.get_projects(
                    ctx, skip=initialElements
                )
                initialElements += pres.get("ret", {}).get("numberOfElements", 0)
                aggO = res.get("ret", {}).get("content", [])
                aggN = pres.get("ret", {}).get("content", [])
                res["ret"]["content"] = [*aggO, *aggN]
                res["ret"]["numberOfElements"] = initialElements

        return res

    def is_pending(self, hub, ret: dict, state: str = None, **pending_kwargs):
        """
        State reconciliation
        """
        hub.log.debug(f'Running is_pending for resource: {ret.get("__id__", None)}...')
        is_pending_result = False
        hub.log.debug(
            f'is_pending_result for resource "{ret.get("__id__", None)}": {is_pending_result}'
        )
        return is_pending_result

    async def remap_resource_structure(self, hub, ctx, obj: dict) -> dict:
        schema_mapper = {
            "add": [
                {"key": "zoneAssignmentConfigurations", "value": "jsonpath:zones"},
            ],
            "omit": ["orgId", "createdAt", "updatedAt", "owner", "_links", "zones"],
        }

        # Perform resource mapping by adding properties and omitting properties.
        # Property renaming is addition followed by omission.
        if schema_mapper:
            resource_name = "project"
            hub.log.debug(f"Remapping resource {resource_name}...")
            obj = await add_properties(obj, schema_mapper.get("add", []))
            obj = omit_properties(obj, schema_mapper.get("omit", []))

        return obj


# ====================================
# State override
# ====================================
class ProjectStateImpl(ProjectState):
    async def absent(self, hub, ctx, name: str, **kwargs):

        search_result = (await self.paginate_find(hub, ctx))["ret"]

        resource = None
        for s in search_result.content:
            if name == s["name"] and True:
                hub.log.info(
                    f'Found resource project "{s["name"]}" due to existing resource "{name}"'
                )
                s = await self.remap_resource_structure(hub, ctx, s)
                resource = s

        if resource:
            # it exists!
            hub.log.debug(f"project with name = {resource.get('name')} already exists")

            hub.log.debug(f"removing cloud zones from project {resource.get('name')}")
            await hub.exec.vra.iaas.project.update_project(
                ctx, resource["id"], resource["name"], zoneAssignmentConfigurations=[]
            )

            await hub.exec.vra.iaas.project.delete_project(
                ctx,
                resource["id"],
            )

            return StateReturn(
                result=True,
                comment=f"Resource with name = {resource.get('name')} deleted.",
                old=resource,
                new=None,
            )

        return StateReturn(
            result=True,
            comment=f"Resource with name = {name} is already absent.",
            old=None,
            new=None,
        )

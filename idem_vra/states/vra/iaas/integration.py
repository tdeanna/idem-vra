from typing import Any

from idem_vra.helpers.mapper import add_properties
from idem_vra.helpers.mapper import omit_properties
from idem_vra.helpers.models import StateReturn


__contracts__ = ["resource"]

TREQ = {"present": {"require": []}, "absent": {"require": []}}


async def present(
    hub, ctx, name: str, integrationType: Any, integrationProperties: Any, **kwargs
):

    """

    :param string integrationType: (required in body) Integration type

    :param object integrationProperties: (required in body) Integration specific properties supplied in as name value pairs

    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option.

    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about

    :param string validateOnly: (optional in query) Only validate provided Integration Specification. Integration will not
      be created.

    :param string privateKey: (optional in body) Secret access key or password to be used to authenticate with the
      integration

    :param array associatedCloudAccountIds: (optional in body) Cloud accounts to associate with this integration

    :param object customProperties: (optional in body) Additional custom properties that may be used to extend the
      Integration.

    :param string description: (optional in body) A human-friendly description.

    :param Any certificateInfo: (optional in body)

    :param string privateKeyId: (optional in body) Access key id or username to be used to authenticate with the
      integration

    :param array tags: (optional in body) A set of tag keys and optional values to set on the Integration

    """

    try:
        state = IntegrationState(hub, ctx)
        return await state.present(
            hub, ctx, name, integrationType, integrationProperties, **kwargs
        )
    except Exception as error:
        hub.log.error("Error during enforcing present state: integration")
        hub.log.error(str(error))
        raise error


async def absent(hub, ctx, name: str, **kwargs):

    """

    :param string p_id: (required in path) The ID of the Integration

    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about

    """

    """
    :param string name: (required) name of the resource
    """
    try:
        state = IntegrationState(hub, ctx)
        return await state.absent(hub, ctx, name, **kwargs)
    except Exception as error:
        hub.log.error("Error during enforcing absent state: integration")
        hub.log.error(str(error))
        raise error


async def describe(hub, ctx):
    try:
        state = IntegrationState(hub, ctx)
        return await state.describe(hub, ctx)
    except Exception as error:
        hub.log.error("Error during describe: integration")
        hub.log.error(str(error))
        raise error


def is_pending(hub, ret: dict, state: str = None, **pending_kwargs):
    try:
        state = IntegrationState(hub, None)
        return state.is_pending(hub, ret, state, **pending_kwargs)
    except Exception as error:
        hub.log.error("Error during is_pending: integration")
        hub.log.error(str(error))
        raise error


class IntegrationState:
    def __init__(self, hub, ctx):
        self.hub = hub
        self.ctx = ctx

    async def present(
        self,
        hub,
        ctx,
        name: str,
        integrationType: Any,
        integrationProperties: Any,
        **kwargs,
    ):

        search_result = (await self.paginate_find(hub, ctx))["ret"]

        for s in search_result.content:
            if name == s["name"] and True:
                hub.log.info(
                    f'Returning resource integration "{s["name"]}" due to existing resource "{name}"'
                )
                s = await self.remap_resource_structure(hub, ctx, s)
                return StateReturn(
                    result=True,
                    comment=f"Resource integration {name} already exists.",
                    old=s,
                    new=s,
                )
        res = (
            await hub.exec.vra.iaas.integration.create_integration_async(
                ctx, integrationType, integrationProperties, name, **kwargs
            )
        )["ret"]
        res = await self.remap_resource_structure(hub, ctx, res)

        return StateReturn(
            result=True,
            comment=f"Creation of integration {name} success.",
            old=None,
            new=res,
        )

    async def absent(self, hub, ctx, name: str, **kwargs):

        search_result = (await self.paginate_find(hub, ctx))["ret"]

        resource = None
        for s in search_result.content:
            if name == s["name"] and True:
                hub.log.info(
                    f'Found resource integration "{s["name"]}" due to existing resource "{name}"'
                )
                s = await self.remap_resource_structure(hub, ctx, s)
                resource = s

        if resource:
            # it exists!

            delete_kwargs = {}
            delete_kwargs["p_id"] = resource.get("id")

            hub.log.debug(
                f"integration with name = {resource.get('name')} already exists"
            )
            await hub.exec.vra.iaas.integration.delete_integration(ctx, **delete_kwargs)

            return StateReturn(
                result=True,
                comment=f"Resource with name = {resource.get('name')} deleted.",
                old=resource,
                new=None,
            )

        return StateReturn(
            result=True,
            comment=f"Resource with name = {name} is already absent.",
            old=None,
            new=None,
        )

    async def describe(self, hub, ctx):

        result = {}
        res = await self.paginate_find(hub, ctx)

        for obj in res.get("ret", {}).get("content", []):

            # Keep track of name and id properties as they may get remapped
            obj_name = obj.get("name", "unknown")
            obj_id = obj.get("id", "unknown")

            obj = await self.remap_resource_structure(hub, ctx, obj)

            # Define props
            props = [{key: value} for key, value in obj.items()]

            # Build result
            result[f"{obj_name}-{obj_id.split('-')[-1]}"] = {
                "vra.iaas.integration.present": props
            }

        return result

    async def paginate_find(self, hub, ctx, **kwargs):
        """
        Paginate through all resources using their 'find' method.
        """
        res = await hub.exec.vra.iaas.integration.get_integrations(ctx, **kwargs)

        numberOfElements = res.get("ret", {}).get("numberOfElements", 0)
        totalElements = res.get("ret", {}).get("totalElements", 0)
        initialElements = numberOfElements
        if numberOfElements != totalElements and totalElements != 0:
            while initialElements < totalElements:
                hub.log.debug(
                    f"Requesting integration with offset={initialElements} out of {totalElements}"
                )
                pres = await hub.exec.vra.iaas.integration.get_integrations(
                    ctx, skip=initialElements
                )
                initialElements += pres.get("ret", {}).get("numberOfElements", 0)
                aggO = res.get("ret", {}).get("content", [])
                aggN = pres.get("ret", {}).get("content", [])
                res["ret"]["content"] = [*aggO, *aggN]
                res["ret"]["numberOfElements"] = initialElements

        return res

    def is_pending(self, hub, ret: dict, state: str = None, **pending_kwargs):
        """
        State reconciliation
        """
        hub.log.debug(f'Running is_pending for resource: {ret.get("__id__", None)}...')
        is_pending_result = False
        hub.log.debug(
            f'is_pending_result for resource "{ret.get("__id__", None)}": {is_pending_result}'
        )
        return is_pending_result

    async def remap_resource_structure(self, hub, ctx, obj: dict) -> dict:
        schema_mapper = {
            "add": [],
            "omit": [
                "orgId",
                "_links",
                "owner",
                "createdAt",
                "updatedAt",
                'customProperties["lastEnumerationStartTimeInMillis"]',
                'customProperties["lastEnumerationTaskState"]',
                'customProperties["lastEnumerationTimestampMicro"]',
                'customProperties["lastOptimizableResourceEnumerationStartTimeInMillis"]',
                'customProperties["lastRunningPriceSyncTime"]',
                'customProperties["lastSuccessfulEnumerationTimestampMicro"]',
                'customProperties["enumerationTaskState"]',
                'customProperties["isCloud"]',
                'customProperties["isExternal"]',
                'customProperties["orgId"]',
                'customProperties["vcIds"]',
                'customProperties["hostName"]',
                'customProperties["privateKeyId"]',
                'customProperties["dcId"]',
                'integrationProperties["dcId"]',
                'integrationProperties["endpointId"]',
            ],
        }

        # Perform resource mapping by adding properties and omitting properties.
        # Property renaming is addition followed by omission.
        if schema_mapper:
            resource_name = "integration"
            hub.log.debug(f"Remapping resource {resource_name}...")
            obj = await add_properties(obj, schema_mapper.get("add", []))
            obj = omit_properties(obj, schema_mapper.get("omit", []))

        return obj

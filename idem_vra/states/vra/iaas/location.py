from typing import Any

from idem_vra.helpers.mapper import add_properties
from idem_vra.helpers.mapper import omit_properties
from idem_vra.helpers.models import StateReturn


__contracts__ = ["resource"]

TREQ = {
    "present": {"require": ["vra.iaas.cloudaccount.present"]},
    "absent": {"require": ["vra.iaas.project.absent"]},
}


async def present(hub, ctx, name: str, regionId: Any, **kwargs):

    """

    :param string regionId: (required in body) The id of the region for which this profile is created

    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option.

    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about

    :param object customProperties: (optional in body) A list of key value pair of properties that will  be used

    :param string folder: (optional in body) The folder relative path to the datacenter where resources are
      deployed to. (only applicable for vSphere cloud zones)

    :param array computeIds: (optional in body) The ids of the compute resources that will be explicitly assigned to
      this zone

    :param array tagsToMatch: (optional in body) A set of tag keys and optional values that will be used

    :param string description: (optional in body) A human-friendly description.

    :param string placementPolicy: (optional in body) Placement policy for the zone. One of DEFAULT, SPREAD, BINPACK or
      SPREAD_MEMORY.

    :param array tags: (optional in body) A set of tag keys and optional values that are effectively applied to
      all compute resources in this zone, but only in the context of this
      zone.

    """

    try:
        state = LocationState(hub, ctx)
        return await state.present(hub, ctx, name, regionId, **kwargs)
    except Exception as error:
        hub.log.error("Error during enforcing present state: location")
        hub.log.error(str(error))
        raise error


async def absent(hub, ctx, name: str, **kwargs):

    """

    :param string p_id: (required in path) The ID of the zone.

    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about

    """

    """
    :param string name: (required) name of the resource
    """
    try:
        state = LocationState(hub, ctx)
        return await state.absent(hub, ctx, name, **kwargs)
    except Exception as error:
        hub.log.error("Error during enforcing absent state: location")
        hub.log.error(str(error))
        raise error


async def describe(hub, ctx):
    try:
        state = LocationState(hub, ctx)
        return await state.describe(hub, ctx)
    except Exception as error:
        hub.log.error("Error during describe: location")
        hub.log.error(str(error))
        raise error


def is_pending(hub, ret: dict, state: str = None, **pending_kwargs):
    try:
        state = LocationState(hub, None)
        return state.is_pending(hub, ret, state, **pending_kwargs)
    except Exception as error:
        hub.log.error("Error during is_pending: location")
        hub.log.error(str(error))
        raise error


class LocationState:
    def __init__(self, hub, ctx):
        self.hub = hub
        self.ctx = ctx

    async def present(self, hub, ctx, name: str, regionId: Any, **kwargs):

        search_result = (await self.paginate_find(hub, ctx))["ret"]

        for s in search_result.content:
            if name == s["name"] and True:
                hub.log.info(
                    f'Returning resource location "{s["name"]}" due to existing resource "{name}"'
                )
                s = await self.remap_resource_structure(hub, ctx, s)
                return StateReturn(
                    result=True,
                    comment=f"Resource location {name} already exists.",
                    old=s,
                    new=s,
                )
        res = (
            await hub.exec.vra.iaas.location.create_zone(ctx, regionId, name, **kwargs)
        )["ret"]
        res = await self.remap_resource_structure(hub, ctx, res)

        return StateReturn(
            result=True,
            comment=f"Creation of location {name} success.",
            old=None,
            new=res,
        )

    async def absent(self, hub, ctx, name: str, **kwargs):

        search_result = (await self.paginate_find(hub, ctx))["ret"]

        resource = None
        for s in search_result.content:
            if name == s["name"] and True:
                hub.log.info(
                    f'Found resource location "{s["name"]}" due to existing resource "{name}"'
                )
                s = await self.remap_resource_structure(hub, ctx, s)
                resource = s

        if resource:
            # it exists!

            delete_kwargs = {}
            delete_kwargs["p_id"] = resource.get("id")

            hub.log.debug(f"location with name = {resource.get('name')} already exists")
            await hub.exec.vra.iaas.location.delete_zone(ctx, **delete_kwargs)

            return StateReturn(
                result=True,
                comment=f"Resource with name = {resource.get('name')} deleted.",
                old=resource,
                new=None,
            )

        return StateReturn(
            result=True,
            comment=f"Resource with name = {name} is already absent.",
            old=None,
            new=None,
        )

    async def describe(self, hub, ctx):

        result = {}
        res = await self.paginate_find(hub, ctx)

        for obj in res.get("ret", {}).get("content", []):

            # Keep track of name and id properties as they may get remapped
            obj_name = obj.get("name", "unknown")
            obj_id = obj.get("id", "unknown")

            obj = await self.remap_resource_structure(hub, ctx, obj)

            # Define props
            props = [{key: value} for key, value in obj.items()]

            # Build result
            result[f"{obj_name}-{obj_id.split('-')[-1]}"] = {
                "vra.iaas.location.present": props
            }

        return result

    async def paginate_find(self, hub, ctx, **kwargs):
        """
        Paginate through all resources using their 'find' method.
        """
        res = await hub.exec.vra.iaas.location.get_zones(ctx, **kwargs)

        numberOfElements = res.get("ret", {}).get("numberOfElements", 0)
        totalElements = res.get("ret", {}).get("totalElements", 0)
        initialElements = numberOfElements
        if numberOfElements != totalElements and totalElements != 0:
            while initialElements < totalElements:
                hub.log.debug(
                    f"Requesting location with offset={initialElements} out of {totalElements}"
                )
                pres = await hub.exec.vra.iaas.location.get_zones(
                    ctx, skip=initialElements
                )
                initialElements += pres.get("ret", {}).get("numberOfElements", 0)
                aggO = res.get("ret", {}).get("content", [])
                aggN = pres.get("ret", {}).get("content", [])
                res["ret"]["content"] = [*aggO, *aggN]
                res["ret"]["numberOfElements"] = initialElements

        return res

    def is_pending(self, hub, ret: dict, state: str = None, **pending_kwargs):
        """
        State reconciliation
        """
        hub.log.debug(f'Running is_pending for resource: {ret.get("__id__", None)}...')
        is_pending_result = False
        hub.log.debug(
            f'is_pending_result for resource "{ret.get("__id__", None)}": {is_pending_result}'
        )
        return is_pending_result

    async def remap_resource_structure(self, hub, ctx, obj: dict) -> dict:
        schema_mapper = {
            "add": [
                {
                    "key": "regionId",
                    "value": 'jsonpath:enabledRegions[?externalRegionId=="${jsonpath:externalRegionId}"].id',
                    "source": hub.exec.vra.iaas.cloudaccount.get_cloud_account,
                    "kwargs": {"p_id": "jsonpath:cloudAccountId"},
                    "hub": hub,
                    "ctx": ctx,
                },
                {"key": "customProperties.__isDefaultPlacementZone", "value": True},
            ],
            "omit": [
                "orgId",
                "createdAt",
                "updatedAt",
                "owner",
                "_links",
                "cloudAccountId",
                "externalRegionId",
                "customProperties.zone_overlapping_migrated",
            ],
        }

        # Perform resource mapping by adding properties and omitting properties.
        # Property renaming is addition followed by omission.
        if schema_mapper:
            resource_name = "location"
            hub.log.debug(f"Remapping resource {resource_name}...")
            obj = await add_properties(obj, schema_mapper.get("add", []))
            obj = omit_properties(obj, schema_mapper.get("omit", []))

        return obj

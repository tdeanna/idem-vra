# Auto-generated virtuale state module

__contracts__ = ["resource"]

TREQ = {
    "present": {"require": ["vra.iaas.cloudaccount.present"]},
    "absent": {"require": []},
}

# ====================================
# State implementation
# ====================================
from idem_vra.helpers.models import StateReturn


async def present(hub, ctx, name: str, **kwargs):
    try:
        return StateReturn(
            result=True,
            comment=f"Resource with name = {name} is already present.",
            old=None,
            new=None,
        )
    except Exception as error:
        hub.log.error("Error during enforcing present state: networkfabricstate")
        hub.log.error(str(error))
        raise error


async def absent(hub, ctx, name: str, **kwargs):
    try:
        return StateReturn(
            result=True,
            comment=f"Resource with name = {name} is already absent.",
            old=None,
            new=None,
        )
    except Exception as error:
        hub.log.error("Error during enforcing absent state: networkfabricstate")
        hub.log.error(str(error))
        raise error


async def describe(hub, ctx):
    try:
        return {}
    except Exception as error:
        hub.log.error("Error during describe: networkfabricstate")
        hub.log.error(str(error))
        raise error


def is_pending(hub, ret: dict, state: str = None, **pending_kwargs):
    return False

from typing import Any

from idem_vra.helpers.mapper import add_properties
from idem_vra.helpers.mapper import omit_properties
from idem_vra.helpers.models import StateReturn


__contracts__ = ["resource"]

TREQ = {"present": {"require": ["vra.iaas.project.present"]}, "absent": {"require": []}}


async def present(hub, ctx, name: str, q_dryRun: Any, typeId: Any, **kwargs):

    """

    :param string q_dryRun: (required in query)

    :param string typeId: (required in body) The policy type ID.

    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.

    :param string createdAt: (optional in body) Policy creation timestamp.

    :param string createdBy: (optional in body) Policy author.

    :param Any criteria: (optional in body)

    :param object definition: (optional in body) Policy-type-specific settings such as lease limits for lease policies.

    :param object definitionLegend: (optional in body)

    :param string description: (optional in body) The policy description.

    :param string enforcementType: (optional in body) Defines enforcement type for policy. Default enforcement type is HARD.

    :param string id: (optional in body) The policy ID.

    :param string lastUpdatedAt: (optional in body) Most recent policy update timestamp.

    :param string lastUpdatedBy: (optional in body) Most recent policy editor.

    :param string name: (optional in body) The policy name.

    :param string orgId: (optional in body) The ID of the organization to which the policy belongs.

    :param string projectId: (optional in body) For project-scoped policies, the ID of the project to which the policy
      belongs.

    :param Any scopeCriteria: (optional in body)

    :param Any statistics: (optional in body)

    """

    try:
        state = PoliciesState(hub, ctx)
        return await state.present(hub, ctx, name, q_dryRun, typeId, **kwargs)
    except Exception as error:
        hub.log.error("Error during enforcing present state: policies")
        hub.log.error(str(error))
        raise error


async def absent(hub, ctx, name: str, **kwargs):

    """

    :param string p_id: (required in path) Policy ID

    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.

    """

    """
    :param string name: (required) name of the resource
    """
    try:
        state = PoliciesState(hub, ctx)
        return await state.absent(hub, ctx, name, **kwargs)
    except Exception as error:
        hub.log.error("Error during enforcing absent state: policies")
        hub.log.error(str(error))
        raise error


async def describe(hub, ctx):
    try:
        state = PoliciesState(hub, ctx)
        return await state.describe(hub, ctx)
    except Exception as error:
        hub.log.error("Error during describe: policies")
        hub.log.error(str(error))
        raise error


def is_pending(hub, ret: dict, state: str = None, **pending_kwargs):
    try:
        state = PoliciesState(hub, None)
        return state.is_pending(hub, ret, state, **pending_kwargs)
    except Exception as error:
        hub.log.error("Error during is_pending: policies")
        hub.log.error(str(error))
        raise error


class PoliciesState:
    def __init__(self, hub, ctx):
        self.hub = hub
        self.ctx = ctx

    async def present(self, hub, ctx, name: str, q_dryRun: Any, typeId: Any, **kwargs):

        search_result = (await self.paginate_find(hub, ctx))["ret"]

        for s in search_result.content:
            if name == s["name"] and True:
                hub.log.info(
                    f'Returning resource policies "{s["name"]}" due to existing resource "{name}"'
                )
                s = await self.remap_resource_structure(hub, ctx, s)
                return StateReturn(
                    result=True,
                    comment=f"Resource policies {name} already exists.",
                    old=s,
                    new=s,
                )
        kwargs["name"] = name
        res = (
            await hub.exec.vra.catalog.policies.dry_run_policy_using_post2(
                ctx, q_dryRun, typeId, **kwargs
            )
        )["ret"]
        res = await self.remap_resource_structure(hub, ctx, res)

        return StateReturn(
            result=True,
            comment=f"Creation of policies {name} success.",
            old=None,
            new=res,
        )

    async def absent(self, hub, ctx, name: str, **kwargs):

        search_result = (await self.paginate_find(hub, ctx))["ret"]

        resource = None
        for s in search_result.content:
            if name == s["name"] and True:
                hub.log.info(
                    f'Found resource policies "{s["name"]}" due to existing resource "{name}"'
                )
                s = await self.remap_resource_structure(hub, ctx, s)
                resource = s

        if resource:
            # it exists!

            delete_kwargs = {}
            delete_kwargs["p_id"] = resource.get("id")

            hub.log.debug(f"policies with name = {resource.get('name')} already exists")
            await hub.exec.vra.catalog.policies.delete_policy_using_delete5(
                ctx, **delete_kwargs
            )

            return StateReturn(
                result=True,
                comment=f"Resource with name = {resource.get('name')} deleted.",
                old=resource,
                new=None,
            )

        return StateReturn(
            result=True,
            comment=f"Resource with name = {name} is already absent.",
            old=None,
            new=None,
        )

    async def describe(self, hub, ctx):

        result = {}
        res = await self.paginate_find(hub, ctx)

        for obj in res.get("ret", {}).get("content", []):

            # Keep track of name and id properties as they may get remapped
            obj_name = obj.get("name", "unknown")
            obj_id = obj.get("id", "unknown")

            obj = await self.remap_resource_structure(hub, ctx, obj)

            # Define props
            props = [{key: value} for key, value in obj.items()]

            # Build result
            result[f"{obj_name}-{obj_id.split('-')[-1]}"] = {
                "vra.catalog.policies.present": props
            }

        return result

    async def paginate_find(self, hub, ctx, **kwargs):
        """
        Paginate through all resources using their 'find' method.
        """
        res = await hub.exec.vra.catalog.policies.get_policies_using_get5(ctx, **kwargs)

        numberOfElements = res.get("ret", {}).get("numberOfElements", 0)
        totalElements = res.get("ret", {}).get("totalElements", 0)
        initialElements = numberOfElements
        if numberOfElements != totalElements and totalElements != 0:
            while initialElements < totalElements:
                hub.log.debug(
                    f"Requesting policies with offset={initialElements} out of {totalElements}"
                )
                pres = await hub.exec.vra.catalog.policies.get_policies_using_get5(
                    ctx, skip=initialElements
                )
                initialElements += pres.get("ret", {}).get("numberOfElements", 0)
                aggO = res.get("ret", {}).get("content", [])
                aggN = pres.get("ret", {}).get("content", [])
                res["ret"]["content"] = [*aggO, *aggN]
                res["ret"]["numberOfElements"] = initialElements

        return res

    def is_pending(self, hub, ret: dict, state: str = None, **pending_kwargs):
        """
        State reconciliation
        """
        hub.log.debug(f'Running is_pending for resource: {ret.get("__id__", None)}...')
        is_pending_result = False
        hub.log.debug(
            f'is_pending_result for resource "{ret.get("__id__", None)}": {is_pending_result}'
        )
        return is_pending_result

    async def remap_resource_structure(self, hub, ctx, obj: dict) -> dict:
        schema_mapper = {
            "add": [],
            "omit": [
                "createdBy",
                "createdAt",
                "lastUpdatedAt",
                "lastUpdatedBy",
                "orgId",
            ],
        }

        # Perform resource mapping by adding properties and omitting properties.
        # Property renaming is addition followed by omission.
        if schema_mapper:
            resource_name = "policies"
            hub.log.debug(f"Remapping resource {resource_name}...")
            obj = await add_properties(obj, schema_mapper.get("add", []))
            obj = omit_properties(obj, schema_mapper.get("omit", []))

        return obj

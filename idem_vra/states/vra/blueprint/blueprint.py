from typing import Any

from idem_vra.helpers.mapper import add_properties
from idem_vra.helpers.mapper import omit_properties
from idem_vra.helpers.models import StateReturn


__contracts__ = ["resource"]

TREQ = {"present": {"require": ["vra.iaas.project.present"]}, "absent": {"require": []}}


async def present(hub, ctx, name: str, projectId: Any, **kwargs):

    """

    :param string name: (required in body) Blueprint name

    :param string projectId: (required in body) Project ID

    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about

    :param string content: (optional in body) Blueprint YAML content

    :param string contentSourceId: (optional in body) Content source id

    :param string contentSourcePath: (optional in body) Content source path

    :param string contentSourceSyncAt: (optional in body) Content source last sync time

    :param array contentSourceSyncMessages: (optional in body) Content source last sync messages

    :param string contentSourceSyncStatus: (optional in body) Content source last sync status

    :param string contentSourceType: (optional in body) Content source type

    :param string createdAt: (optional in body) Created time

    :param string createdBy: (optional in body) Created by

    :param string description: (optional in body) Blueprint description

    :param string id: (optional in body) Object ID

    :param string orgId: (optional in body) Org ID

    :param string projectName: (optional in body) Project Name

    :param boolean requestScopeOrg: (optional in body) Flag to indicate blueprint can be requested from any project in org

    :param string selfLink: (optional in body) Blueprint self link

    :param string status: (optional in body) Blueprint status

    :param integer totalReleasedVersions: (optional in body) Total released versions

    :param integer totalVersions: (optional in body) Total versions

    :param string updatedAt: (optional in body) Updated time

    :param string updatedBy: (optional in body) Updated by

    :param boolean valid: (optional in body) Validation result on update

    :param array validationMessages: (optional in body) Validation messages

    """

    try:
        state = BlueprintState(hub, ctx)
        return await state.present(hub, ctx, name, projectId, **kwargs)
    except Exception as error:
        hub.log.error("Error during enforcing present state: blueprint")
        hub.log.error(str(error))
        raise error


async def absent(hub, ctx, name: str, **kwargs):

    """

    :param string p_blueprintId: (required in path) blueprintId

    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about

    """

    """
    :param string name: (required) name of the resource
    """
    try:
        state = BlueprintState(hub, ctx)
        return await state.absent(hub, ctx, name, **kwargs)
    except Exception as error:
        hub.log.error("Error during enforcing absent state: blueprint")
        hub.log.error(str(error))
        raise error


async def describe(hub, ctx):
    try:
        state = BlueprintState(hub, ctx)
        return await state.describe(hub, ctx)
    except Exception as error:
        hub.log.error("Error during describe: blueprint")
        hub.log.error(str(error))
        raise error


def is_pending(hub, ret: dict, state: str = None, **pending_kwargs):
    try:
        state = BlueprintState(hub, None)
        return state.is_pending(hub, ret, state, **pending_kwargs)
    except Exception as error:
        hub.log.error("Error during is_pending: blueprint")
        hub.log.error(str(error))
        raise error


class BlueprintState:
    def __init__(self, hub, ctx):
        self.hub = hub
        self.ctx = ctx

    async def present(self, hub, ctx, name: str, projectId: Any, **kwargs):

        search_result = (await self.paginate_find(hub, ctx))["ret"]

        for s in search_result.content:
            if name == s["name"] and True:
                hub.log.info(
                    f'Returning resource blueprint "{s["name"]}" due to existing resource "{name}"'
                )
                s = await self.remap_resource_structure(hub, ctx, s)
                return StateReturn(
                    result=True,
                    comment=f"Resource blueprint {name} already exists.",
                    old=s,
                    new=s,
                )
        res = (
            await hub.exec.vra.blueprint.blueprint.create_blueprint_using_post1(
                ctx, name, projectId, **kwargs
            )
        )["ret"]
        res = await self.remap_resource_structure(hub, ctx, res)

        return StateReturn(
            result=True,
            comment=f"Creation of blueprint {name} success.",
            old=None,
            new=res,
        )

    async def absent(self, hub, ctx, name: str, **kwargs):

        search_result = (await self.paginate_find(hub, ctx))["ret"]

        resource = None
        for s in search_result.content:
            if name == s["name"] and True:
                hub.log.info(
                    f'Found resource blueprint "{s["name"]}" due to existing resource "{name}"'
                )
                s = await self.remap_resource_structure(hub, ctx, s)
                resource = s

        if resource:
            # it exists!

            delete_kwargs = {}
            delete_kwargs["p_blueprintId"] = resource.get("id")

            hub.log.debug(
                f"blueprint with name = {resource.get('name')} already exists"
            )
            await hub.exec.vra.blueprint.blueprint.delete_blueprint_using_delete1(
                ctx, **delete_kwargs
            )

            return StateReturn(
                result=True,
                comment=f"Resource with name = {resource.get('name')} deleted.",
                old=resource,
                new=None,
            )

        return StateReturn(
            result=True,
            comment=f"Resource with name = {name} is already absent.",
            old=None,
            new=None,
        )

    async def describe(self, hub, ctx):

        result = {}
        res = await self.paginate_find(hub, ctx)

        for obj in res.get("ret", {}).get("content", []):

            # Keep track of name and id properties as they may get remapped
            obj_name = obj.get("name", "unknown")
            obj_id = obj.get("id", "unknown")

            obj = await self.remap_resource_structure(hub, ctx, obj)

            # Define props
            props = [{key: value} for key, value in obj.items()]

            # Build result
            result[f"{obj_name}-{obj_id.split('-')[-1]}"] = {
                "vra.blueprint.blueprint.present": props
            }

        return result

    async def paginate_find(self, hub, ctx, **kwargs):
        """
        Paginate through all resources using their 'find' method.
        """
        res = await hub.exec.vra.blueprint.blueprint.list_blueprints_using_get1(
            ctx, **kwargs
        )

        numberOfElements = res.get("ret", {}).get("numberOfElements", 0)
        totalElements = res.get("ret", {}).get("totalElements", 0)
        initialElements = numberOfElements
        if numberOfElements != totalElements and totalElements != 0:
            while initialElements < totalElements:
                hub.log.debug(
                    f"Requesting blueprint with offset={initialElements} out of {totalElements}"
                )
                pres = (
                    await hub.exec.vra.blueprint.blueprint.list_blueprints_using_get1(
                        ctx, skip=initialElements
                    )
                )
                initialElements += pres.get("ret", {}).get("numberOfElements", 0)
                aggO = res.get("ret", {}).get("content", [])
                aggN = pres.get("ret", {}).get("content", [])
                res["ret"]["content"] = [*aggO, *aggN]
                res["ret"]["numberOfElements"] = initialElements

        return res

    def is_pending(self, hub, ret: dict, state: str = None, **pending_kwargs):
        """
        State reconciliation
        """
        hub.log.debug(f'Running is_pending for resource: {ret.get("__id__", None)}...')
        is_pending_result = False
        hub.log.debug(
            f'is_pending_result for resource "{ret.get("__id__", None)}": {is_pending_result}'
        )
        return is_pending_result

    async def remap_resource_structure(self, hub, ctx, obj: dict) -> dict:
        schema_mapper = None

        # Perform resource mapping by adding properties and omitting properties.
        # Property renaming is addition followed by omission.
        if schema_mapper:
            resource_name = "blueprint"
            hub.log.debug(f"Remapping resource {resource_name}...")
            obj = await add_properties(obj, schema_mapper.get("add", []))
            obj = omit_properties(obj, schema_mapper.get("omit", []))

        return obj

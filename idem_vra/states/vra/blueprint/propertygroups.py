from idem_vra.helpers.mapper import add_properties
from idem_vra.helpers.mapper import omit_properties
from idem_vra.helpers.models import StateReturn


__contracts__ = ["resource"]

TREQ = {"present": {"require": []}, "absent": {"require": []}}


async def present(hub, ctx, name: str, **kwargs):

    """

    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about

    :param string createdAt: (optional in body) Created time

    :param string createdBy: (optional in body) Created by

    :param string description: (optional in body) Property group description

    :param string displayName: (optional in body) Property group display name

    :param string id: (optional in body) Object ID

    :param string name: (optional in body) Property group name

    :param string orgId: (optional in body) Org ID

    :param string projectId: (optional in body) Project ID

    :param string projectName: (optional in body) Project Name

    :param object properties: (optional in body) Properties

    :param string type: (optional in body) Property group type

    :param string updatedAt: (optional in body) Updated time

    :param string updatedBy: (optional in body) Updated by

    """

    try:
        state = PropertygroupsState(hub, ctx)
        return await state.present(hub, ctx, name, **kwargs)
    except Exception as error:
        hub.log.error("Error during enforcing present state: propertygroups")
        hub.log.error(str(error))
        raise error


async def absent(hub, ctx, name: str, **kwargs):

    """

    :param string p_propertyGroupId: (required in path) propertyGroupId

    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about

    """

    """
    :param string name: (required) name of the resource
    """
    try:
        state = PropertygroupsState(hub, ctx)
        return await state.absent(hub, ctx, name, **kwargs)
    except Exception as error:
        hub.log.error("Error during enforcing absent state: propertygroups")
        hub.log.error(str(error))
        raise error


async def describe(hub, ctx):
    try:
        state = PropertygroupsState(hub, ctx)
        return await state.describe(hub, ctx)
    except Exception as error:
        hub.log.error("Error during describe: propertygroups")
        hub.log.error(str(error))
        raise error


def is_pending(hub, ret: dict, state: str = None, **pending_kwargs):
    try:
        state = PropertygroupsState(hub, None)
        return state.is_pending(hub, ret, state, **pending_kwargs)
    except Exception as error:
        hub.log.error("Error during is_pending: propertygroups")
        hub.log.error(str(error))
        raise error


class PropertygroupsState:
    def __init__(self, hub, ctx):
        self.hub = hub
        self.ctx = ctx

    async def present(self, hub, ctx, name: str, **kwargs):

        search_result = (await self.paginate_find(hub, ctx))["ret"]

        for s in search_result.content:
            if name == s["name"] and True:
                hub.log.info(
                    f'Returning resource propertygroups "{s["name"]}" due to existing resource "{name}"'
                )
                s = await self.remap_resource_structure(hub, ctx, s)
                return StateReturn(
                    result=True,
                    comment=f"Resource propertygroups {name} already exists.",
                    old=s,
                    new=s,
                )
        kwargs["name"] = name
        res = (
            await hub.exec.vra.blueprint.propertygroups.create_property_group_using_post(
                ctx, **kwargs
            )
        )["ret"]
        res = await self.remap_resource_structure(hub, ctx, res)

        return StateReturn(
            result=True,
            comment=f"Creation of propertygroups {name} success.",
            old=None,
            new=res,
        )

    async def absent(self, hub, ctx, name: str, **kwargs):

        search_result = (await self.paginate_find(hub, ctx))["ret"]

        resource = None
        for s in search_result.content:
            if name == s["name"] and True:
                hub.log.info(
                    f'Found resource propertygroups "{s["name"]}" due to existing resource "{name}"'
                )
                s = await self.remap_resource_structure(hub, ctx, s)
                resource = s

        if resource:
            # it exists!

            delete_kwargs = {}
            delete_kwargs["p_propertyGroupId"] = resource.get("id")

            hub.log.debug(
                f"propertygroups with name = {resource.get('name')} already exists"
            )
            await hub.exec.vra.blueprint.propertygroups.delete_property_group_using_delete(
                ctx, **delete_kwargs
            )

            return StateReturn(
                result=True,
                comment=f"Resource with name = {resource.get('name')} deleted.",
                old=resource,
                new=None,
            )

        return StateReturn(
            result=True,
            comment=f"Resource with name = {name} is already absent.",
            old=None,
            new=None,
        )

    async def describe(self, hub, ctx):

        result = {}
        res = await self.paginate_find(hub, ctx)

        for obj in res.get("ret", {}).get("content", []):

            # Keep track of name and id properties as they may get remapped
            obj_name = obj.get("name", "unknown")
            obj_id = obj.get("id", "unknown")

            obj = await self.remap_resource_structure(hub, ctx, obj)

            # Define props
            props = [{key: value} for key, value in obj.items()]

            # Build result
            result[f"{obj_name}-{obj_id.split('-')[-1]}"] = {
                "vra.blueprint.propertygroups.present": props
            }

        return result

    async def paginate_find(self, hub, ctx, **kwargs):
        """
        Paginate through all resources using their 'find' method.
        """
        res = (
            await hub.exec.vra.blueprint.propertygroups.list_property_groups_using_get(
                ctx, **kwargs
            )
        )

        numberOfElements = res.get("ret", {}).get("numberOfElements", 0)
        totalElements = res.get("ret", {}).get("totalElements", 0)
        initialElements = numberOfElements
        if numberOfElements != totalElements and totalElements != 0:
            while initialElements < totalElements:
                hub.log.debug(
                    f"Requesting propertygroups with offset={initialElements} out of {totalElements}"
                )
                pres = await hub.exec.vra.blueprint.propertygroups.list_property_groups_using_get(
                    ctx, skip=initialElements
                )
                initialElements += pres.get("ret", {}).get("numberOfElements", 0)
                aggO = res.get("ret", {}).get("content", [])
                aggN = pres.get("ret", {}).get("content", [])
                res["ret"]["content"] = [*aggO, *aggN]
                res["ret"]["numberOfElements"] = initialElements

        return res

    def is_pending(self, hub, ret: dict, state: str = None, **pending_kwargs):
        """
        State reconciliation
        """
        hub.log.debug(f'Running is_pending for resource: {ret.get("__id__", None)}...')
        is_pending_result = False
        hub.log.debug(
            f'is_pending_result for resource "{ret.get("__id__", None)}": {is_pending_result}'
        )
        return is_pending_result

    async def remap_resource_structure(self, hub, ctx, obj: dict) -> dict:
        schema_mapper = {
            "add": [],
            "omit": ["orgId", "createdAt", "updatedAt", "createdBy", "updatedBy"],
        }

        # Perform resource mapping by adding properties and omitting properties.
        # Property renaming is addition followed by omission.
        if schema_mapper:
            resource_name = "propertygroups"
            hub.log.debug(f"Remapping resource {resource_name}...")
            obj = await add_properties(obj, schema_mapper.get("add", []))
            obj = omit_properties(obj, schema_mapper.get("omit", []))

        return obj

from typing import Any

from idem_vra.helpers.mapper import add_properties
from idem_vra.helpers.mapper import omit_properties
from idem_vra.helpers.models import StateReturn


__contracts__ = ["resource"]

TREQ = {"present": {"require": ["vra.iaas.project.present"]}, "absent": {"require": []}}


async def present(hub, ctx, name: str, properties: Any, type: Any, **kwargs):

    """

    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option

    :param object properties: (required in body) Endpoint specific properties

    :param string type: (required in body) The type of this Endpoint instance.

    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about

    :param string Authorization: (optional in header) Bearer token

    :param string description: (optional in body) A human-friendly description.

    :param boolean isRestricted: (optional in body) This type of Endpoint can be created, updated or deleted by admin
      only. If a restricted Endpoint is consumed in a pipeline, and that
      pipeline is executed by a non-admin user, then the execution will fail
      at the task which is consuming this restricted Endpoint. Only admin
      can then resume this pipeline to make it progress.

    :param string project: (optional in body) The project this entity belongs to.

    """

    try:
        state = EndpointsStateImpl(hub, ctx)
        return await state.present(hub, ctx, name, properties, type, **kwargs)
    except Exception as error:
        hub.log.error("Error during enforcing present state: endpoints")
        hub.log.error(str(error))
        raise error


async def absent(hub, ctx, name: str, **kwargs):

    """

    :param string p_id: (required in path) The ID of the Endpoint

    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about

    :param string Authorization: (optional in header) Bearer token

    """

    """
    :param string name: (required) name of the resource
    """
    try:
        state = EndpointsStateImpl(hub, ctx)
        return await state.absent(hub, ctx, name, **kwargs)
    except Exception as error:
        hub.log.error("Error during enforcing absent state: endpoints")
        hub.log.error(str(error))
        raise error


async def describe(hub, ctx):
    try:
        state = EndpointsStateImpl(hub, ctx)
        return await state.describe(hub, ctx)
    except Exception as error:
        hub.log.error("Error during describe: endpoints")
        hub.log.error(str(error))
        raise error


def is_pending(hub, ret: dict, state: str = None, **pending_kwargs):
    try:
        state = EndpointsStateImpl(hub, None)
        return state.is_pending(hub, ret, state, **pending_kwargs)
    except Exception as error:
        hub.log.error("Error during is_pending: endpoints")
        hub.log.error(str(error))
        raise error


class EndpointsState:
    def __init__(self, hub, ctx):
        self.hub = hub
        self.ctx = ctx

    async def present(self, hub, ctx, name: str, properties: Any, type: Any, **kwargs):

        search_result = (await self.paginate_find(hub, ctx))["ret"]

        for s in search_result.content:
            if name == s["name"] and True:
                hub.log.info(
                    f'Returning resource endpoints "{s["name"]}" due to existing resource "{name}"'
                )
                s = await self.remap_resource_structure(hub, ctx, s)
                return StateReturn(
                    result=True,
                    comment=f"Resource endpoints {name} already exists.",
                    old=s,
                    new=s,
                )
        res = (
            await hub.exec.vra.pipeline.endpoints.create_endpoint_using_post(
                ctx, name, properties, type, **kwargs
            )
        )["ret"]
        res = await self.remap_resource_structure(hub, ctx, res)

        return StateReturn(
            result=True,
            comment=f"Creation of endpoints {name} success.",
            old=None,
            new=res,
        )

    async def absent(self, hub, ctx, name: str, **kwargs):

        search_result = (await self.paginate_find(hub, ctx))["ret"]

        resource = None
        for s in search_result.content:
            if name == s["name"] and True:
                hub.log.info(
                    f'Found resource endpoints "{s["name"]}" due to existing resource "{name}"'
                )
                s = await self.remap_resource_structure(hub, ctx, s)
                resource = s

        if resource:
            # it exists!

            delete_kwargs = {}
            delete_kwargs["p_id"] = resource.get("id")

            hub.log.debug(
                f"endpoints with name = {resource.get('name')} already exists"
            )
            await hub.exec.vra.pipeline.endpoints.delete_endpoint_by_id_using_delete(
                ctx, **delete_kwargs
            )

            return StateReturn(
                result=True,
                comment=f"Resource with name = {resource.get('name')} deleted.",
                old=resource,
                new=None,
            )

        return StateReturn(
            result=True,
            comment=f"Resource with name = {name} is already absent.",
            old=None,
            new=None,
        )

    async def describe(self, hub, ctx):

        result = {}
        res = await self.paginate_find(hub, ctx)

        for obj in res.get("ret", {}).get("content", []):

            # Keep track of name and id properties as they may get remapped
            obj_name = obj.get("name", "unknown")
            obj_id = obj.get("id", "unknown")

            obj = await self.remap_resource_structure(hub, ctx, obj)

            # Define props
            props = [{key: value} for key, value in obj.items()]

            # Build result
            result[f"{obj_name}-{obj_id.split('-')[-1]}"] = {
                "vra.pipeline.endpoints.present": props
            }

        return result

    async def paginate_find(self, hub, ctx, **kwargs):
        """
        Paginate through all resources using their 'find' method.
        """
        res = await hub.exec.vra.pipeline.endpoints.get_all_endpoints_using_get(
            ctx, **kwargs
        )

        numberOfElements = res.get("ret", {}).get("numberOfElements", 0)
        totalElements = res.get("ret", {}).get("totalElements", 0)
        initialElements = numberOfElements
        if numberOfElements != totalElements and totalElements != 0:
            while initialElements < totalElements:
                hub.log.debug(
                    f"Requesting endpoints with offset={initialElements} out of {totalElements}"
                )
                pres = (
                    await hub.exec.vra.pipeline.endpoints.get_all_endpoints_using_get(
                        ctx, skip=initialElements
                    )
                )
                initialElements += pres.get("ret", {}).get("numberOfElements", 0)
                aggO = res.get("ret", {}).get("content", [])
                aggN = pres.get("ret", {}).get("content", [])
                res["ret"]["content"] = [*aggO, *aggN]
                res["ret"]["numberOfElements"] = initialElements

        return res

    def is_pending(self, hub, ret: dict, state: str = None, **pending_kwargs):
        """
        State reconciliation
        """
        hub.log.debug(f'Running is_pending for resource: {ret.get("__id__", None)}...')
        is_pending_result = False
        hub.log.debug(
            f'is_pending_result for resource "{ret.get("__id__", None)}": {is_pending_result}'
        )
        return is_pending_result

    async def remap_resource_structure(self, hub, ctx, obj: dict) -> dict:
        schema_mapper = {
            "add": [],
            "omit": [
                "createdAt",
                "_createTimeInMicros",
                "_updateTimeInMicros",
                "_createdBy",
                "updatedAt",
                "updatedBy",
                "createdBy",
                "_link",
                "_projectId",
                "version",
            ],
        }

        # Perform resource mapping by adding properties and omitting properties.
        # Property renaming is addition followed by omission.
        if schema_mapper:
            resource_name = "endpoints"
            hub.log.debug(f"Remapping resource {resource_name}...")
            obj = await add_properties(obj, schema_mapper.get("add", []))
            obj = omit_properties(obj, schema_mapper.get("omit", []))

        return obj


# ====================================
# State override
# ====================================
class EndpointsStateImpl(EndpointsState):
    async def present(self, hub, ctx, name: str, properties: Any, type: Any, **kwargs):
        search_result = (await self.paginate_find(hub, ctx))["ret"]

        for itm in search_result.get("links", []):
            s = search_result.documents.get(itm)
            if name == s.get("name", "") and True:
                hub.log.info(
                    f'Returning resource endpoints "{s["name"]}" due to existing resource "{name}"'
                )
                s = await self.remap_resource_structure(hub, ctx, s)
                return StateReturn(
                    result=True,
                    comment=f"Resource endpoints {name} already exists.",
                    old=s,
                    new=s,
                )
        res = (
            await hub.exec.vra.pipeline.endpoints.create_endpoint_using_post(
                ctx, name, properties, type, **kwargs
            )
        )["ret"]
        res = await self.remap_resource_structure(hub, ctx, res)

        return StateReturn(
            result=True,
            comment=f"Creation of endpoints {name} success.",
            old=None,
            new=res,
        )

    async def absent(self, hub, ctx, name: str, **kwargs):
        search_result = (await self.paginate_find(hub, ctx))["ret"]

        resource = None
        for itm in search_result.get("links", []):
            s = search_result.documents.get(itm)
            if name == s.get("name", "") and True:
                hub.log.info(
                    f'Found resource endpoints "{s["name"]}" due to existing resource "{name}"'
                )
                s = await self.remap_resource_structure(hub, ctx, s)
                resource = s

        if resource:
            # it exists!

            delete_kwargs = {}
            delete_kwargs["p_id"] = resource.get("id")

            hub.log.debug(
                f"endpoints with name = {resource.get('name')} already exists"
            )
            await hub.exec.vra.pipeline.endpoints.delete_endpoint_by_id_using_delete(
                ctx, **delete_kwargs
            )

            return StateReturn(
                result=True,
                comment=f"Resource with name = {resource.get('name')} deleted.",
                old=resource,
                new=None,
            )

        return StateReturn(
            result=True,
            comment=f"Resource with name = {name} is already absent.",
            old=None,
            new=None,
        )

    async def describe(self, hub, ctx):

        result = {}
        res = await self.paginate_find(hub, ctx)
        for itm in res.get("ret", {}).get("links", []):

            # Keep track of name and id properties as they may get remapped
            obj = res.get("ret", {}).get("documents", {}).get(itm)
            obj_name = obj.get("name", "unknown")
            obj_id = obj.get("id", "unknown")

            obj = await self.remap_resource_structure(hub, ctx, obj)

            # Define props
            props = [{key: value} for key, value in obj.items()]

            # Build result
            result[f"{obj_name}-{obj_id.split('-')[-1]}"] = {
                "vra.pipeline.endpoints.present": props
            }

        return result

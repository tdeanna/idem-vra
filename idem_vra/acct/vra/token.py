import base64
import re
from typing import Any
from typing import Dict

import idem_vra.client.vra_abx_lib as vra_abx_lib
import idem_vra.client.vra_blueprint_lib as vra_blueprint_lib
import idem_vra.client.vra_catalog_lib as vra_catalog_lib
import idem_vra.client.vra_cmx_lib as vra_cmx_lib
import idem_vra.client.vra_iaas_lib as vra_iaas_lib
import idem_vra.client.vra_pipeline_lib as vra_pipeline_lib
import idem_vra.client.vra_rbac_lib as vra_rbac_lib

PUBLIC_VRA_URL = "https://api.mgmt.cloud.vmware.com"


async def gather(hub, profiles) -> Dict[str, Any]:
    """
    Given a refresh token, retrieve an access token.
    Any extra parameters will be saved as part of the profile.

    Example:

    .. code-block:: yaml
        vra:
          profile_name:
            refresh_token: <refresh token or API key>
            # optional configuration
            vra_url: https://api.mgmt.cloud.vmware.com
    """

    sub_profiles = {}

    for profile, ctx in profiles.get("vra", {}).items():
        try:
            # Fall back on to the public vRA url
            ctx["vra_url"] = ctx.get("vra_url", PUBLIC_VRA_URL)

            extras = hub.tool.vra.config.get_extras(ctx)

            hub.log.debug(f"connecting to vra with profile: {profile}")
            iaas_client = ClientFactory(ctx, vra_iaas_lib, extras)

            # Login
            # Use the iaas_client to get the access token

            loginApi = vra_iaas_lib.LoginApi(iaas_client.get_client())
            auth_response = loginApi.retrieve_auth_token(
                {
                    "refreshToken": iaas_client.get_config().get_api_key_with_prefix(
                        "refreshToken"
                    )
                }
            )
            ctx["access_token"] = auth_response.token

            if ctx["vra_url"].endswith("cloud.vmware.com") == False:
                jsonVersion = await hub.tool.vra.session.request(
                    ctx=ctx, method="GET", path="config.json", headers={}
                )
                if jsonVersion and jsonVersion["applicationVersion"]:
                    ctx["vra_version"] = base64.b64decode(
                        jsonVersion["applicationVersion"]
                    ).decode("utf-8")
                    match = re.search(r"\d+\.\d+\.\d+", ctx["vra_version"])
                    if match:
                        ctx["vra_version"] = match.group()

                    hub.log.debug(f"Appliance version: {ctx.vra_version}")
                ctx["is_cloud"] = False
            else:
                ctx["is_cloud"] = True

            # store the token in the context
            # EOF Login

            iaas_client.update_config(ctx["access_token"])

            catalog_client = ClientFactory(ctx, vra_catalog_lib, extras)
            catalog_client.update_config(ctx["access_token"])

            blueprint_client = ClientFactory(ctx, vra_blueprint_lib, extras)
            blueprint_client.update_config(ctx["access_token"])

            cmx_client = ClientFactory(ctx, vra_cmx_lib, extras)
            cmx_client.update_config(ctx["access_token"])

            abx_client = ClientFactory(ctx, vra_abx_lib, extras)
            abx_client.update_config(ctx["access_token"])

            pipeline_client = ClientFactory(ctx, vra_pipeline_lib, extras)
            pipeline_client.update_config(ctx["access_token"])

            rbac_client = ClientFactory(ctx, vra_rbac_lib, extras)
            rbac_client.update_config(ctx["access_token"])

            hub.clients = {
                "idem_vra.client.vra_iaas_lib.api": iaas_client.get_client(),
                "idem_vra.client.vra_catalog_lib.api": catalog_client.get_client(),
                "idem_vra.client.vra_blueprint_lib.api": blueprint_client.get_client(),
                "idem_vra.client.vra_cmx_lib.api": cmx_client.get_client(),
                "idem_vra.client.vra_abx_lib.api": abx_client.get_client(),
                "idem_vra.client.vra_pipeline_lib.api": pipeline_client.get_client(),
                "idem_vra.client.vra_rbac_lib.api": rbac_client.get_client(),
            }

            sub_profiles[profile] = ctx
            hub.log.debug(f"connected to vra with profile: {profile}")

        except Exception as e:
            hub.log.error(f"{e.__class__.__name__}: {e}")
            continue

    return sub_profiles


class ClientFactory:
    def __init__(self, ctx, lib, extras):

        self.lib = lib
        self.ctx = ctx

        self.config = self.lib.Configuration()
        self.config.host = self.ctx["vra_url"]
        self.config.debug = extras.get("debug-clients", False)
        self.config.verify_ssl = False

        self.api_client = self.lib.ApiClient(self.config)
        self.config.api_key["refreshToken"] = self.ctx["refresh_token"]

    def get_config(self):
        return self.config

    def get_client(self):
        return self.api_client

    def update_config(self, access_token):
        self.config.api_key["Authorization"] = access_token
        self.config.api_key_prefix["Authorization"] = "Bearer"

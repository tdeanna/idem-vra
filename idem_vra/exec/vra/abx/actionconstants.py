from idem_vra.client.vra_abx_lib.api import ActionConstantsApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def create_using_post1(hub, ctx, **kwargs):
    """Create an action constant Creates a new action constant Performs POST /abx/api/resources/action-secrets


    :param integer createdMillis: (optional in body) Creation time in millis
    :param boolean encrypted: (optional in body) Flag to indicate if the value should be stored and transferred
      encrypted or not.
    :param string id: (optional in body) ID of the resource
    :param string name: (optional in body) Name of the resource
    :param string orgId: (optional in body) Org ID of the resource
    :param object value: (optional in body) Value of the corresponding key - could be a string or a JSON
    """

    hub.log.debug("POST /abx/api/resources/action-secrets")

    api = ActionConstantsApi(hub.clients["idem_vra.client.vra_abx_lib.api"])

    body = {}

    if "createdMillis" in kwargs:
        hub.log.debug(f"Got kwarg 'createdMillis' = {kwargs['createdMillis']}")
        body["createdMillis"] = kwargs.get("createdMillis")
        del kwargs["createdMillis"]
    if "encrypted" in kwargs:
        hub.log.debug(f"Got kwarg 'encrypted' = {kwargs['encrypted']}")
        body["encrypted"] = kwargs.get("encrypted")
        del kwargs["encrypted"]
    if "id" in kwargs:
        hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
        body["id"] = kwargs.get("id")
        del kwargs["id"]
    if "name" in kwargs:
        hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
        body["name"] = kwargs.get("name")
        del kwargs["name"]
    if "orgId" in kwargs:
        hub.log.debug(f"Got kwarg 'orgId' = {kwargs['orgId']}")
        body["orgId"] = kwargs.get("orgId")
        del kwargs["orgId"]
    if "value" in kwargs:
        hub.log.debug(f"Got kwarg 'value' = {kwargs['value']}")
        body["value"] = kwargs.get("value")
        del kwargs["value"]

    ret = api.create_using_post1(body, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def delete_using_delete4(hub, ctx, p_id, **kwargs):
    """Delete an action constant Deletes an action constant Performs DELETE /abx/api/resources/action-secrets/{id}


    :param string p_id: (required in path) id
    """

    hub.log.debug("DELETE /abx/api/resources/action-secrets/{id}")

    api = ActionConstantsApi(hub.clients["idem_vra.client.vra_abx_lib.api"])

    ret = api.delete_using_delete4(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_all_using_get2(hub, ctx, **kwargs):
    """Fetch all action constants Retrieves all action constants for given organization Performs GET /abx/api/resources/action-secrets"""

    hub.log.debug("GET /abx/api/resources/action-secrets")

    api = ActionConstantsApi(hub.clients["idem_vra.client.vra_abx_lib.api"])

    ret = api.get_all_using_get2(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_by_id_using_get2(hub, ctx, p_id, **kwargs):
    """Fetch action constant Retrieves action constant by given id Performs GET /abx/api/resources/action-secrets/{id}


    :param string p_id: (required in path) id
    """

    hub.log.debug("GET /abx/api/resources/action-secrets/{id}")

    api = ActionConstantsApi(hub.clients["idem_vra.client.vra_abx_lib.api"])

    ret = api.get_by_id_using_get2(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def update_using_put1(hub, ctx, p_id, **kwargs):
    """Update an action constant Updates an existing action constant Performs PUT /abx/api/resources/action-secrets/{id}


    :param string p_id: (required in path) id
    :param integer createdMillis: (optional in body) Creation time in millis
    :param boolean encrypted: (optional in body) Flag to indicate if the value should be stored and transferred
      encrypted or not.
    :param string id: (optional in body) ID of the resource
    :param string name: (optional in body) Name of the resource
    :param string orgId: (optional in body) Org ID of the resource
    :param object value: (optional in body) Value of the corresponding key - could be a string or a JSON
    """

    hub.log.debug("PUT /abx/api/resources/action-secrets/{id}")

    api = ActionConstantsApi(hub.clients["idem_vra.client.vra_abx_lib.api"])

    body = {}

    if "createdMillis" in kwargs:
        hub.log.debug(f"Got kwarg 'createdMillis' = {kwargs['createdMillis']}")
        body["createdMillis"] = kwargs.get("createdMillis")
        del kwargs["createdMillis"]
    if "encrypted" in kwargs:
        hub.log.debug(f"Got kwarg 'encrypted' = {kwargs['encrypted']}")
        body["encrypted"] = kwargs.get("encrypted")
        del kwargs["encrypted"]
    if "id" in kwargs:
        hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
        body["id"] = kwargs.get("id")
        del kwargs["id"]
    if "name" in kwargs:
        hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
        body["name"] = kwargs.get("name")
        del kwargs["name"]
    if "orgId" in kwargs:
        hub.log.debug(f"Got kwarg 'orgId' = {kwargs['orgId']}")
        body["orgId"] = kwargs.get("orgId")
        del kwargs["orgId"]
    if "value" in kwargs:
        hub.log.debug(f"Got kwarg 'value' = {kwargs['value']}")
        body["value"] = kwargs.get("value")
        del kwargs["value"]

    ret = api.update_using_put1(body, id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))

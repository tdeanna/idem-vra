from idem_vra.client.vra_abx_lib.api import DefaultApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get_all_using_get4(hub, ctx, **kwargs):
    """Proxy to fetch provisioning endpoints Retrieves provisioning endpoints for given organization Performs GET /abx/api/provisioning/endpoints"""

    hub.log.debug("GET /abx/api/provisioning/endpoints")

    api = DefaultApi(hub.clients["idem_vra.client.vra_abx_lib.api"])

    ret = api.get_all_using_get4(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_using_get(hub, ctx, p_id, **kwargs):
    """Proxy to fetch a provisioning endpoint by self link Retrieves provisioning endpoints for given organization Performs GET /abx/api/provisioning/endpoints/{id}


    :param string p_id: (required in path) id
    """

    hub.log.debug("GET /abx/api/provisioning/endpoints/{id}")

    api = DefaultApi(hub.clients["idem_vra.client.vra_abx_lib.api"])

    ret = api.get_using_get(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))

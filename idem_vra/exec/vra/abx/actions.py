from idem_vra.client.vra_abx_lib.api import ActionsApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def create_using_post(hub, ctx, actionType, name, orgId, **kwargs):
    """Create an action Creates a new action Performs POST /abx/api/resources/actions


    :param string actionType: (required in body) Type of the action
    :param string name: (required in body) Name of the action
    :param string orgId: (required in body) Organization ID of the action.
    :param boolean asyncDeployed: (optional in body)
    :param string compressedContent: (optional in body) base64encoded ZIP of action content (source & dependencies)
    :param object configuration: (optional in body) Configuration of the action for specific providers
    :param string contentId: (optional in body) ID of the actions saved compressed content
    :param string dependencies: (optional in body) A list of libraries to import, delimited by a comma
    :param string description: (optional in body) Description of the action
    :param string entrypoint: (optional in body) Name of the entry function of the action
    :param string id: (optional in body) ID of the action
    :param object inputs: (optional in body) Map defining the inputs of the action
    :param integer memoryInMB: (optional in body) Runtime RAM constraints in megabytes
    :param object metadata: (optional in body) Additional properties used by ABX
    :param integer prePolyglotMemoryLimitInMB: (optional in body)
    :param string projectId: (optional in body) Project Id of the action (required for non-system actions)
    :param string provider: (optional in body) Provider used for code execution
    :param string runtime: (optional in body) Runtime of the action (python, nodejs, etc...)
    :param boolean scalable: (optional in body)
    :param string selfLink: (optional in body)
    :param boolean shared: (optional in body) Flag indicating if the action can be shared across projects.
    :param boolean showMemoryAlert: (optional in body)
    :param string source: (optional in body) Source of the action as string
    :param boolean system: (optional in body) Flag indicating if the action is a system action.
    :param integer timeoutSeconds: (optional in body) Defines how long an action can run (default 600)
    """

    hub.log.debug("POST /abx/api/resources/actions")

    api = ActionsApi(hub.clients["idem_vra.client.vra_abx_lib.api"])

    body = {}
    body["actionType"] = actionType
    body["name"] = name
    body["orgId"] = orgId

    if "asyncDeployed" in kwargs:
        hub.log.debug(f"Got kwarg 'asyncDeployed' = {kwargs['asyncDeployed']}")
        body["asyncDeployed"] = kwargs.get("asyncDeployed")
        del kwargs["asyncDeployed"]
    if "compressedContent" in kwargs:
        hub.log.debug(f"Got kwarg 'compressedContent' = {kwargs['compressedContent']}")
        body["compressedContent"] = kwargs.get("compressedContent")
        del kwargs["compressedContent"]
    if "configuration" in kwargs:
        hub.log.debug(f"Got kwarg 'configuration' = {kwargs['configuration']}")
        body["configuration"] = kwargs.get("configuration")
        del kwargs["configuration"]
    if "contentId" in kwargs:
        hub.log.debug(f"Got kwarg 'contentId' = {kwargs['contentId']}")
        body["contentId"] = kwargs.get("contentId")
        del kwargs["contentId"]
    if "dependencies" in kwargs:
        hub.log.debug(f"Got kwarg 'dependencies' = {kwargs['dependencies']}")
        body["dependencies"] = kwargs.get("dependencies")
        del kwargs["dependencies"]
    if "description" in kwargs:
        hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
        body["description"] = kwargs.get("description")
        del kwargs["description"]
    if "entrypoint" in kwargs:
        hub.log.debug(f"Got kwarg 'entrypoint' = {kwargs['entrypoint']}")
        body["entrypoint"] = kwargs.get("entrypoint")
        del kwargs["entrypoint"]
    if "id" in kwargs:
        hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
        body["id"] = kwargs.get("id")
        del kwargs["id"]
    if "inputs" in kwargs:
        hub.log.debug(f"Got kwarg 'inputs' = {kwargs['inputs']}")
        body["inputs"] = kwargs.get("inputs")
        del kwargs["inputs"]
    if "memoryInMB" in kwargs:
        hub.log.debug(f"Got kwarg 'memoryInMB' = {kwargs['memoryInMB']}")
        body["memoryInMB"] = kwargs.get("memoryInMB")
        del kwargs["memoryInMB"]
    if "metadata" in kwargs:
        hub.log.debug(f"Got kwarg 'metadata' = {kwargs['metadata']}")
        body["metadata"] = kwargs.get("metadata")
        del kwargs["metadata"]
    if "prePolyglotMemoryLimitInMB" in kwargs:
        hub.log.debug(
            f"Got kwarg 'prePolyglotMemoryLimitInMB' = {kwargs['prePolyglotMemoryLimitInMB']}"
        )
        body["prePolyglotMemoryLimitInMB"] = kwargs.get("prePolyglotMemoryLimitInMB")
        del kwargs["prePolyglotMemoryLimitInMB"]
    if "projectId" in kwargs:
        hub.log.debug(f"Got kwarg 'projectId' = {kwargs['projectId']}")
        body["projectId"] = kwargs.get("projectId")
        del kwargs["projectId"]
    if "provider" in kwargs:
        hub.log.debug(f"Got kwarg 'provider' = {kwargs['provider']}")
        body["provider"] = kwargs.get("provider")
        del kwargs["provider"]
    if "runtime" in kwargs:
        hub.log.debug(f"Got kwarg 'runtime' = {kwargs['runtime']}")
        body["runtime"] = kwargs.get("runtime")
        del kwargs["runtime"]
    if "scalable" in kwargs:
        hub.log.debug(f"Got kwarg 'scalable' = {kwargs['scalable']}")
        body["scalable"] = kwargs.get("scalable")
        del kwargs["scalable"]
    if "selfLink" in kwargs:
        hub.log.debug(f"Got kwarg 'selfLink' = {kwargs['selfLink']}")
        body["selfLink"] = kwargs.get("selfLink")
        del kwargs["selfLink"]
    if "shared" in kwargs:
        hub.log.debug(f"Got kwarg 'shared' = {kwargs['shared']}")
        body["shared"] = kwargs.get("shared")
        del kwargs["shared"]
    if "showMemoryAlert" in kwargs:
        hub.log.debug(f"Got kwarg 'showMemoryAlert' = {kwargs['showMemoryAlert']}")
        body["showMemoryAlert"] = kwargs.get("showMemoryAlert")
        del kwargs["showMemoryAlert"]
    if "source" in kwargs:
        hub.log.debug(f"Got kwarg 'source' = {kwargs['source']}")
        body["source"] = kwargs.get("source")
        del kwargs["source"]
    if "system" in kwargs:
        hub.log.debug(f"Got kwarg 'system' = {kwargs['system']}")
        body["system"] = kwargs.get("system")
        del kwargs["system"]
    if "timeoutSeconds" in kwargs:
        hub.log.debug(f"Got kwarg 'timeoutSeconds' = {kwargs['timeoutSeconds']}")
        body["timeoutSeconds"] = kwargs.get("timeoutSeconds")
        del kwargs["timeoutSeconds"]

    ret = api.create_using_post(body, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def delete_using_delete1(hub, ctx, p_id, q_projectId, **kwargs):
    """Delete an action Deletes an action with a specific ID Performs DELETE /abx/api/resources/actions/{id}


    :param string p_id: (required in path) ID of the action
    :param string q_projectId: (required in query) Project ID of action (required for non-system actions)
    :param boolean force: (optional in query) force
    """

    hub.log.debug("DELETE /abx/api/resources/actions/{id}")

    api = ActionsApi(hub.clients["idem_vra.client.vra_abx_lib.api"])

    ret = api.delete_using_delete1(id=p_id, project_id=q_projectId, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def delete_using_delete(hub, ctx, **kwargs):
    """Delete multiple actions by their id and projectId Deletes actions with a specific ID and projectId Performs DELETE /abx/api/resources/actions"""

    hub.log.debug("DELETE /abx/api/resources/actions")

    api = ActionsApi(hub.clients["idem_vra.client.vra_abx_lib.api"])

    ret = api.delete_using_delete(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def export_using_post(hub, ctx, actions, **kwargs):
    """Export actions Exports the specified actions as a zip bundle Performs POST /abx/api/resources/actions/export


    :param array actions: (required in body) List of the identities of the actions to export
    """

    hub.log.debug("POST /abx/api/resources/actions/export")

    api = ActionsApi(hub.clients["idem_vra.client.vra_abx_lib.api"])

    body = {}
    body["actions"] = actions

    ret = api.export_using_post(body, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_all_flows_using_action_using_get(hub, ctx, p_id, **kwargs):
    """Fetch all action flows using an action Retrieves a list of all action flows which are using the specified action Performs GET /abx/api/resources/actions/{id}/flows


    :param string p_id: (required in path) ID of the action
    :param string projectId: (optional in query) Project ID of action (required for non-system actions)
    """

    hub.log.debug("GET /abx/api/resources/actions/{id}/flows")

    api = ActionsApi(hub.clients["idem_vra.client.vra_abx_lib.api"])

    ret = api.get_all_flows_using_action_using_get(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_all_using_get(hub, ctx, **kwargs):
    """Fetch all actions Retrieves all action entities Performs GET /abx/api/resources/actions


    :param string page: (optional in query) Page to fetch (starting from 0)
    :param string size: (optional in query) Amount of entities per page
    :param string id: (optional in query)
    :param boolean localAddress.address.MCGlobal: (optional in query)
    :param boolean localAddress.address.MCLinkLocal: (optional in query)
    :param boolean localAddress.address.MCNodeLocal: (optional in query)
    :param boolean localAddress.address.MCOrgLocal: (optional in query)
    :param boolean localAddress.address.MCSiteLocal: (optional in query)
    :param string localAddress.address.address: (optional in query)
    :param boolean localAddress.address.anyLocalAddress: (optional in query)
    :param string localAddress.address.canonicalHostName: (optional in query)
    :param string localAddress.address.hostAddress: (optional in query)
    :param string localAddress.address.hostName: (optional in query)
    :param boolean localAddress.address.linkLocalAddress: (optional in query)
    :param boolean localAddress.address.loopbackAddress: (optional in query)
    :param boolean localAddress.address.multicastAddress: (optional in query)
    :param boolean localAddress.address.siteLocalAddress: (optional in query)
    :param string localAddress.hostName: (optional in query)
    :param string localAddress.hostString: (optional in query)
    :param integer localAddress.port: (optional in query)
    :param boolean localAddress.unresolved: (optional in query)
    :param boolean remoteAddress.address.MCGlobal: (optional in query)
    :param boolean remoteAddress.address.MCLinkLocal: (optional in query)
    :param boolean remoteAddress.address.MCNodeLocal: (optional in query)
    :param boolean remoteAddress.address.MCOrgLocal: (optional in query)
    :param boolean remoteAddress.address.MCSiteLocal: (optional in query)
    :param string remoteAddress.address.address: (optional in query)
    :param boolean remoteAddress.address.anyLocalAddress: (optional in query)
    :param string remoteAddress.address.canonicalHostName: (optional in query)
    :param string remoteAddress.address.hostAddress: (optional in query)
    :param string remoteAddress.address.hostName: (optional in query)
    :param boolean remoteAddress.address.linkLocalAddress: (optional in query)
    :param boolean remoteAddress.address.loopbackAddress: (optional in query)
    :param boolean remoteAddress.address.multicastAddress: (optional in query)
    :param boolean remoteAddress.address.siteLocalAddress: (optional in query)
    :param string remoteAddress.hostName: (optional in query)
    :param string remoteAddress.hostString: (optional in query)
    :param integer remoteAddress.port: (optional in query)
    :param boolean remoteAddress.unresolved: (optional in query)
    :param string sslInfo.peerCertificates[0].TBSCertificate: (optional in query)
    :param integer sslInfo.peerCertificates[0].basicConstraints: (optional in query)
    :param string sslInfo.peerCertificates[0].encoded: (optional in query)
    :param array sslInfo.peerCertificates[0].extendedKeyUsage: (optional in query)
    :param string sslInfo.peerCertificates[0].issuerDN.name: (optional in query)
    :param array sslInfo.peerCertificates[0].issuerUniqueID: (optional in query)
    :param string sslInfo.peerCertificates[0].issuerX500Principal.encoded: (optional in query)
    :param string sslInfo.peerCertificates[0].issuerX500Principal.name: (optional in query)
    :param array sslInfo.peerCertificates[0].keyUsage: (optional in query)
    :param string sslInfo.peerCertificates[0].notAfter: (optional in query)
    :param string sslInfo.peerCertificates[0].notBefore: (optional in query)
    :param integer sslInfo.peerCertificates[0].serialNumber: (optional in query)
    :param string sslInfo.peerCertificates[0].sigAlgName: (optional in query)
    :param string sslInfo.peerCertificates[0].sigAlgOID: (optional in query)
    :param string sslInfo.peerCertificates[0].sigAlgParams: (optional in query)
    :param string sslInfo.peerCertificates[0].signature: (optional in query)
    :param string sslInfo.peerCertificates[0].subjectDN.name: (optional in query)
    :param array sslInfo.peerCertificates[0].subjectUniqueID: (optional in query)
    :param string sslInfo.peerCertificates[0].subjectX500Principal.encoded: (optional in query)
    :param string sslInfo.peerCertificates[0].subjectX500Principal.name: (optional in query)
    :param string sslInfo.peerCertificates[0].type: (optional in query)
    :param integer sslInfo.peerCertificates[0].version: (optional in query)
    :param string sslInfo.sessionId: (optional in query)
    """

    hub.log.debug("GET /abx/api/resources/actions")

    api = ActionsApi(hub.clients["idem_vra.client.vra_abx_lib.api"])

    ret = api.get_all_using_get(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_by_id_using_get(hub, ctx, p_id, **kwargs):
    """Fetch an action by its ID Retrieves an action entity with a specific ID Performs GET /abx/api/resources/actions/{id}


    :param string p_id: (required in path) ID of the action
    :param string projectId: (optional in query) Project ID of action (required for non-system actions)
    """

    hub.log.debug("GET /abx/api/resources/actions/{id}")

    api = ActionsApi(hub.clients["idem_vra.client.vra_abx_lib.api"])

    ret = api.get_by_id_using_get(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def import_actions_using_post(hub, ctx, **kwargs):
    """Import an action bundle Imports an action bundle and saves all actions Performs POST /abx/api/resources/actions/import


    :param string bundleId: (optional in body) ID of the saved bundle used for import conflict resolution
    :param string compressedBundle: (optional in body) base64encoded ZIP bundle of actions to import
    :param string option: (optional in body) Conflict option when importing (CREATE on initial import)
    :param string projectId: (optional in body) Project ID for where to create the actions
    :param boolean system: (optional in body) Flag indicating if the imported actions should be system actions
    """

    hub.log.debug("POST /abx/api/resources/actions/import")

    api = ActionsApi(hub.clients["idem_vra.client.vra_abx_lib.api"])

    body = {}

    if "bundleId" in kwargs:
        hub.log.debug(f"Got kwarg 'bundleId' = {kwargs['bundleId']}")
        body["bundleId"] = kwargs.get("bundleId")
        del kwargs["bundleId"]
    if "compressedBundle" in kwargs:
        hub.log.debug(f"Got kwarg 'compressedBundle' = {kwargs['compressedBundle']}")
        body["compressedBundle"] = kwargs.get("compressedBundle")
        del kwargs["compressedBundle"]
    if "option" in kwargs:
        hub.log.debug(f"Got kwarg 'option' = {kwargs['option']}")
        body["option"] = kwargs.get("option")
        del kwargs["option"]
    if "projectId" in kwargs:
        hub.log.debug(f"Got kwarg 'projectId' = {kwargs['projectId']}")
        body["projectId"] = kwargs.get("projectId")
        del kwargs["projectId"]
    if "system" in kwargs:
        hub.log.debug(f"Got kwarg 'system' = {kwargs['system']}")
        body["system"] = kwargs.get("system")
        del kwargs["system"]

    ret = api.import_actions_using_post(body, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def patch_using_patch(hub, ctx, p_id, actionType, name, orgId, **kwargs):
    """Patch an action Updates the state of the action based only on the supplied properties Performs PATCH /abx/api/resources/actions/{id}


    :param string p_id: (required in path) ID of the action
    :param string actionType: (required in body) Type of the action
    :param string name: (required in body) Name of the action
    :param string orgId: (required in body) Organization ID of the action.
    :param boolean asyncDeployed: (optional in body)
    :param string compressedContent: (optional in body) base64encoded ZIP of action content (source & dependencies)
    :param object configuration: (optional in body) Configuration of the action for specific providers
    :param string contentId: (optional in body) ID of the actions saved compressed content
    :param string dependencies: (optional in body) A list of libraries to import, delimited by a comma
    :param string description: (optional in body) Description of the action
    :param string entrypoint: (optional in body) Name of the entry function of the action
    :param string id: (optional in body) ID of the action
    :param object inputs: (optional in body) Map defining the inputs of the action
    :param integer memoryInMB: (optional in body) Runtime RAM constraints in megabytes
    :param object metadata: (optional in body) Additional properties used by ABX
    :param integer prePolyglotMemoryLimitInMB: (optional in body)
    :param string projectId: (optional in body) Project Id of the action (required for non-system actions)
    :param string provider: (optional in body) Provider used for code execution
    :param string runtime: (optional in body) Runtime of the action (python, nodejs, etc...)
    :param boolean scalable: (optional in body)
    :param string selfLink: (optional in body)
    :param boolean shared: (optional in body) Flag indicating if the action can be shared across projects.
    :param boolean showMemoryAlert: (optional in body)
    :param string source: (optional in body) Source of the action as string
    :param boolean system: (optional in body) Flag indicating if the action is a system action.
    :param integer timeoutSeconds: (optional in body) Defines how long an action can run (default 600)
    """

    hub.log.debug("PATCH /abx/api/resources/actions/{id}")

    api = ActionsApi(hub.clients["idem_vra.client.vra_abx_lib.api"])

    body = {}
    body["actionType"] = actionType
    body["name"] = name
    body["orgId"] = orgId

    if "asyncDeployed" in kwargs:
        hub.log.debug(f"Got kwarg 'asyncDeployed' = {kwargs['asyncDeployed']}")
        body["asyncDeployed"] = kwargs.get("asyncDeployed")
        del kwargs["asyncDeployed"]
    if "compressedContent" in kwargs:
        hub.log.debug(f"Got kwarg 'compressedContent' = {kwargs['compressedContent']}")
        body["compressedContent"] = kwargs.get("compressedContent")
        del kwargs["compressedContent"]
    if "configuration" in kwargs:
        hub.log.debug(f"Got kwarg 'configuration' = {kwargs['configuration']}")
        body["configuration"] = kwargs.get("configuration")
        del kwargs["configuration"]
    if "contentId" in kwargs:
        hub.log.debug(f"Got kwarg 'contentId' = {kwargs['contentId']}")
        body["contentId"] = kwargs.get("contentId")
        del kwargs["contentId"]
    if "dependencies" in kwargs:
        hub.log.debug(f"Got kwarg 'dependencies' = {kwargs['dependencies']}")
        body["dependencies"] = kwargs.get("dependencies")
        del kwargs["dependencies"]
    if "description" in kwargs:
        hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
        body["description"] = kwargs.get("description")
        del kwargs["description"]
    if "entrypoint" in kwargs:
        hub.log.debug(f"Got kwarg 'entrypoint' = {kwargs['entrypoint']}")
        body["entrypoint"] = kwargs.get("entrypoint")
        del kwargs["entrypoint"]
    if "id" in kwargs:
        hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
        body["id"] = kwargs.get("id")
        del kwargs["id"]
    if "inputs" in kwargs:
        hub.log.debug(f"Got kwarg 'inputs' = {kwargs['inputs']}")
        body["inputs"] = kwargs.get("inputs")
        del kwargs["inputs"]
    if "memoryInMB" in kwargs:
        hub.log.debug(f"Got kwarg 'memoryInMB' = {kwargs['memoryInMB']}")
        body["memoryInMB"] = kwargs.get("memoryInMB")
        del kwargs["memoryInMB"]
    if "metadata" in kwargs:
        hub.log.debug(f"Got kwarg 'metadata' = {kwargs['metadata']}")
        body["metadata"] = kwargs.get("metadata")
        del kwargs["metadata"]
    if "prePolyglotMemoryLimitInMB" in kwargs:
        hub.log.debug(
            f"Got kwarg 'prePolyglotMemoryLimitInMB' = {kwargs['prePolyglotMemoryLimitInMB']}"
        )
        body["prePolyglotMemoryLimitInMB"] = kwargs.get("prePolyglotMemoryLimitInMB")
        del kwargs["prePolyglotMemoryLimitInMB"]
    if "projectId" in kwargs:
        hub.log.debug(f"Got kwarg 'projectId' = {kwargs['projectId']}")
        body["projectId"] = kwargs.get("projectId")
        del kwargs["projectId"]
    if "provider" in kwargs:
        hub.log.debug(f"Got kwarg 'provider' = {kwargs['provider']}")
        body["provider"] = kwargs.get("provider")
        del kwargs["provider"]
    if "runtime" in kwargs:
        hub.log.debug(f"Got kwarg 'runtime' = {kwargs['runtime']}")
        body["runtime"] = kwargs.get("runtime")
        del kwargs["runtime"]
    if "scalable" in kwargs:
        hub.log.debug(f"Got kwarg 'scalable' = {kwargs['scalable']}")
        body["scalable"] = kwargs.get("scalable")
        del kwargs["scalable"]
    if "selfLink" in kwargs:
        hub.log.debug(f"Got kwarg 'selfLink' = {kwargs['selfLink']}")
        body["selfLink"] = kwargs.get("selfLink")
        del kwargs["selfLink"]
    if "shared" in kwargs:
        hub.log.debug(f"Got kwarg 'shared' = {kwargs['shared']}")
        body["shared"] = kwargs.get("shared")
        del kwargs["shared"]
    if "showMemoryAlert" in kwargs:
        hub.log.debug(f"Got kwarg 'showMemoryAlert' = {kwargs['showMemoryAlert']}")
        body["showMemoryAlert"] = kwargs.get("showMemoryAlert")
        del kwargs["showMemoryAlert"]
    if "source" in kwargs:
        hub.log.debug(f"Got kwarg 'source' = {kwargs['source']}")
        body["source"] = kwargs.get("source")
        del kwargs["source"]
    if "system" in kwargs:
        hub.log.debug(f"Got kwarg 'system' = {kwargs['system']}")
        body["system"] = kwargs.get("system")
        del kwargs["system"]
    if "timeoutSeconds" in kwargs:
        hub.log.debug(f"Got kwarg 'timeoutSeconds' = {kwargs['timeoutSeconds']}")
        body["timeoutSeconds"] = kwargs.get("timeoutSeconds")
        del kwargs["timeoutSeconds"]

    ret = api.patch_using_patch(body, id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def update_using_put(hub, ctx, p_id, actionType, name, orgId, **kwargs):
    """Update an action Updates the action with the new supplied state Performs PUT /abx/api/resources/actions/{id}


    :param string p_id: (required in path) ID of the action
    :param string actionType: (required in body) Type of the action
    :param string name: (required in body) Name of the action
    :param string orgId: (required in body) Organization ID of the action.
    :param boolean asyncDeployed: (optional in body)
    :param string compressedContent: (optional in body) base64encoded ZIP of action content (source & dependencies)
    :param object configuration: (optional in body) Configuration of the action for specific providers
    :param string contentId: (optional in body) ID of the actions saved compressed content
    :param string dependencies: (optional in body) A list of libraries to import, delimited by a comma
    :param string description: (optional in body) Description of the action
    :param string entrypoint: (optional in body) Name of the entry function of the action
    :param string id: (optional in body) ID of the action
    :param object inputs: (optional in body) Map defining the inputs of the action
    :param integer memoryInMB: (optional in body) Runtime RAM constraints in megabytes
    :param object metadata: (optional in body) Additional properties used by ABX
    :param integer prePolyglotMemoryLimitInMB: (optional in body)
    :param string projectId: (optional in body) Project Id of the action (required for non-system actions)
    :param string provider: (optional in body) Provider used for code execution
    :param string runtime: (optional in body) Runtime of the action (python, nodejs, etc...)
    :param boolean scalable: (optional in body)
    :param string selfLink: (optional in body)
    :param boolean shared: (optional in body) Flag indicating if the action can be shared across projects.
    :param boolean showMemoryAlert: (optional in body)
    :param string source: (optional in body) Source of the action as string
    :param boolean system: (optional in body) Flag indicating if the action is a system action.
    :param integer timeoutSeconds: (optional in body) Defines how long an action can run (default 600)
    """

    hub.log.debug("PUT /abx/api/resources/actions/{id}")

    api = ActionsApi(hub.clients["idem_vra.client.vra_abx_lib.api"])

    body = {}
    body["actionType"] = actionType
    body["name"] = name
    body["orgId"] = orgId

    if "asyncDeployed" in kwargs:
        hub.log.debug(f"Got kwarg 'asyncDeployed' = {kwargs['asyncDeployed']}")
        body["asyncDeployed"] = kwargs.get("asyncDeployed")
        del kwargs["asyncDeployed"]
    if "compressedContent" in kwargs:
        hub.log.debug(f"Got kwarg 'compressedContent' = {kwargs['compressedContent']}")
        body["compressedContent"] = kwargs.get("compressedContent")
        del kwargs["compressedContent"]
    if "configuration" in kwargs:
        hub.log.debug(f"Got kwarg 'configuration' = {kwargs['configuration']}")
        body["configuration"] = kwargs.get("configuration")
        del kwargs["configuration"]
    if "contentId" in kwargs:
        hub.log.debug(f"Got kwarg 'contentId' = {kwargs['contentId']}")
        body["contentId"] = kwargs.get("contentId")
        del kwargs["contentId"]
    if "dependencies" in kwargs:
        hub.log.debug(f"Got kwarg 'dependencies' = {kwargs['dependencies']}")
        body["dependencies"] = kwargs.get("dependencies")
        del kwargs["dependencies"]
    if "description" in kwargs:
        hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
        body["description"] = kwargs.get("description")
        del kwargs["description"]
    if "entrypoint" in kwargs:
        hub.log.debug(f"Got kwarg 'entrypoint' = {kwargs['entrypoint']}")
        body["entrypoint"] = kwargs.get("entrypoint")
        del kwargs["entrypoint"]
    if "id" in kwargs:
        hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
        body["id"] = kwargs.get("id")
        del kwargs["id"]
    if "inputs" in kwargs:
        hub.log.debug(f"Got kwarg 'inputs' = {kwargs['inputs']}")
        body["inputs"] = kwargs.get("inputs")
        del kwargs["inputs"]
    if "memoryInMB" in kwargs:
        hub.log.debug(f"Got kwarg 'memoryInMB' = {kwargs['memoryInMB']}")
        body["memoryInMB"] = kwargs.get("memoryInMB")
        del kwargs["memoryInMB"]
    if "metadata" in kwargs:
        hub.log.debug(f"Got kwarg 'metadata' = {kwargs['metadata']}")
        body["metadata"] = kwargs.get("metadata")
        del kwargs["metadata"]
    if "prePolyglotMemoryLimitInMB" in kwargs:
        hub.log.debug(
            f"Got kwarg 'prePolyglotMemoryLimitInMB' = {kwargs['prePolyglotMemoryLimitInMB']}"
        )
        body["prePolyglotMemoryLimitInMB"] = kwargs.get("prePolyglotMemoryLimitInMB")
        del kwargs["prePolyglotMemoryLimitInMB"]
    if "projectId" in kwargs:
        hub.log.debug(f"Got kwarg 'projectId' = {kwargs['projectId']}")
        body["projectId"] = kwargs.get("projectId")
        del kwargs["projectId"]
    if "provider" in kwargs:
        hub.log.debug(f"Got kwarg 'provider' = {kwargs['provider']}")
        body["provider"] = kwargs.get("provider")
        del kwargs["provider"]
    if "runtime" in kwargs:
        hub.log.debug(f"Got kwarg 'runtime' = {kwargs['runtime']}")
        body["runtime"] = kwargs.get("runtime")
        del kwargs["runtime"]
    if "scalable" in kwargs:
        hub.log.debug(f"Got kwarg 'scalable' = {kwargs['scalable']}")
        body["scalable"] = kwargs.get("scalable")
        del kwargs["scalable"]
    if "selfLink" in kwargs:
        hub.log.debug(f"Got kwarg 'selfLink' = {kwargs['selfLink']}")
        body["selfLink"] = kwargs.get("selfLink")
        del kwargs["selfLink"]
    if "shared" in kwargs:
        hub.log.debug(f"Got kwarg 'shared' = {kwargs['shared']}")
        body["shared"] = kwargs.get("shared")
        del kwargs["shared"]
    if "showMemoryAlert" in kwargs:
        hub.log.debug(f"Got kwarg 'showMemoryAlert' = {kwargs['showMemoryAlert']}")
        body["showMemoryAlert"] = kwargs.get("showMemoryAlert")
        del kwargs["showMemoryAlert"]
    if "source" in kwargs:
        hub.log.debug(f"Got kwarg 'source' = {kwargs['source']}")
        body["source"] = kwargs.get("source")
        del kwargs["source"]
    if "system" in kwargs:
        hub.log.debug(f"Got kwarg 'system' = {kwargs['system']}")
        body["system"] = kwargs.get("system")
        del kwargs["system"]
    if "timeoutSeconds" in kwargs:
        hub.log.debug(f"Got kwarg 'timeoutSeconds' = {kwargs['timeoutSeconds']}")
        body["timeoutSeconds"] = kwargs.get("timeoutSeconds")
        del kwargs["timeoutSeconds"]

    ret = api.update_using_put(body, id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))

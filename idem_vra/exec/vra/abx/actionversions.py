from idem_vra.client.vra_abx_lib.api import ActionVersionsApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def create_action_version_using_post(hub, ctx, p_id, **kwargs):
    """Create a version for an action Creates a new version for the specified action Performs POST /abx/api/resources/actions/{id}/versions


    :param string p_id: (required in path) ID of the action
    :param string projectId: (optional in query) Project ID of action (required for non-system actions)
    :param Any action: (optional in body)
    :param string actionId: (optional in body) ID of the action
    :param string contentId: (optional in body) ID of the actions saved compressed content
    :param string createdBy: (optional in body) Name of the version creator
    :param integer createdMillis: (optional in body) Creation time in millis
    :param string description: (optional in body) Description of the version
    :param string gitCommitId: (optional in body) Commit ID of the version if pushed to git
    :param string id: (optional in body) ID of the resource
    :param string name: (optional in body) Name of the resource
    :param string orgId: (optional in body) Org ID of the resource
    :param string projectId: (optional in body) Project Id of the action (required for non-system actions)
    :param boolean released: (optional in body) Flag indicating if the version is released (only 1 released version
      allowed per action)
    """

    hub.log.debug("POST /abx/api/resources/actions/{id}/versions")

    api = ActionVersionsApi(hub.clients["idem_vra.client.vra_abx_lib.api"])

    body = {}

    if "action" in kwargs:
        hub.log.debug(f"Got kwarg 'action' = {kwargs['action']}")
        body["action"] = kwargs.get("action")
        del kwargs["action"]
    if "actionId" in kwargs:
        hub.log.debug(f"Got kwarg 'actionId' = {kwargs['actionId']}")
        body["actionId"] = kwargs.get("actionId")
        del kwargs["actionId"]
    if "contentId" in kwargs:
        hub.log.debug(f"Got kwarg 'contentId' = {kwargs['contentId']}")
        body["contentId"] = kwargs.get("contentId")
        del kwargs["contentId"]
    if "createdBy" in kwargs:
        hub.log.debug(f"Got kwarg 'createdBy' = {kwargs['createdBy']}")
        body["createdBy"] = kwargs.get("createdBy")
        del kwargs["createdBy"]
    if "createdMillis" in kwargs:
        hub.log.debug(f"Got kwarg 'createdMillis' = {kwargs['createdMillis']}")
        body["createdMillis"] = kwargs.get("createdMillis")
        del kwargs["createdMillis"]
    if "description" in kwargs:
        hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
        body["description"] = kwargs.get("description")
        del kwargs["description"]
    if "gitCommitId" in kwargs:
        hub.log.debug(f"Got kwarg 'gitCommitId' = {kwargs['gitCommitId']}")
        body["gitCommitId"] = kwargs.get("gitCommitId")
        del kwargs["gitCommitId"]
    if "id" in kwargs:
        hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
        body["id"] = kwargs.get("id")
        del kwargs["id"]
    if "name" in kwargs:
        hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
        body["name"] = kwargs.get("name")
        del kwargs["name"]
    if "orgId" in kwargs:
        hub.log.debug(f"Got kwarg 'orgId' = {kwargs['orgId']}")
        body["orgId"] = kwargs.get("orgId")
        del kwargs["orgId"]
    if "projectId" in kwargs:
        hub.log.debug(f"Got kwarg 'projectId' = {kwargs['projectId']}")
        body["projectId"] = kwargs.get("projectId")
        del kwargs["projectId"]
    if "released" in kwargs:
        hub.log.debug(f"Got kwarg 'released' = {kwargs['released']}")
        body["released"] = kwargs.get("released")
        del kwargs["released"]

    ret = api.create_action_version_using_post(body, id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def delete_action_version_using_delete(hub, ctx, p_id, p_versionId, **kwargs):
    """Delete an action version Deletes an action version Performs DELETE /abx/api/resources/actions/{id}/versions/{versionId}


    :param string p_id: (required in path) ID of the action
    :param string p_versionId: (required in path) ID of the action version
    :param string projectId: (optional in query) Project ID of action (required for non-system actions)
    """

    hub.log.debug("DELETE /abx/api/resources/actions/{id}/versions/{versionId}")

    api = ActionVersionsApi(hub.clients["idem_vra.client.vra_abx_lib.api"])

    ret = api.delete_action_version_using_delete(
        id=p_id, version_id=p_versionId, **kwargs
    )

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_all_versions_using_get(hub, ctx, p_id, **kwargs):
    """Fetch all versions of an action Retrieves all created versions for an action Performs GET /abx/api/resources/actions/{id}/versions


    :param string p_id: (required in path) ID of the action
    :param string page: (optional in query) Page to fetch (starting from 0)
    :param string projectId: (optional in query) Project ID of action (required for non-system actions)
    :param string size: (optional in query) Amount of entities per page
    """

    hub.log.debug("GET /abx/api/resources/actions/{id}/versions")

    api = ActionVersionsApi(hub.clients["idem_vra.client.vra_abx_lib.api"])

    ret = api.get_all_versions_using_get(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def release_action_version_using_put(hub, ctx, p_id, version, **kwargs):
    """Mark an action version as released Marks an exisiting version of an action as released Performs PUT /abx/api/resources/actions/{id}/release


    :param string p_id: (required in path) ID of the action
    :param string version: (required in body) Name of the version to be released
    :param string projectId: (optional in query) Project ID of action (required for non-system actions)
    """

    hub.log.debug("PUT /abx/api/resources/actions/{id}/release")

    api = ActionVersionsApi(hub.clients["idem_vra.client.vra_abx_lib.api"])

    body = {}
    body["version"] = version

    ret = api.release_action_version_using_put(body, id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def restore_action_version_using_put(hub, ctx, p_id, p_versionId, **kwargs):
    """Restore the action state based on a specified version Change the current action state to the state specified in the version Performs PUT /abx/api/resources/actions/{id}/versions/{versionId}/restore


    :param string p_id: (required in path) ID of the action
    :param string p_versionId: (required in path) ID of the action version
    :param string projectId: (optional in query) Project ID of action (required for non-system actions)
    """

    hub.log.debug("PUT /abx/api/resources/actions/{id}/versions/{versionId}/restore")

    api = ActionVersionsApi(hub.clients["idem_vra.client.vra_abx_lib.api"])

    ret = api.restore_action_version_using_put(
        id=p_id, version_id=p_versionId, **kwargs
    )

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def unrelease_action_version_using_delete(hub, ctx, p_id, **kwargs):
    """Mark an action version as not released Marks an actions released version as not released Performs DELETE /abx/api/resources/actions/{id}/release


    :param string p_id: (required in path) ID of the action
    :param string projectId: (optional in query) Project ID of action (required for non-system actions)
    """

    hub.log.debug("DELETE /abx/api/resources/actions/{id}/release")

    api = ActionVersionsApi(hub.clients["idem_vra.client.vra_abx_lib.api"])

    ret = api.unrelease_action_version_using_delete(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))

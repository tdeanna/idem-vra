from idem_vra.client.vra_abx_lib.api import ActionTemplatesApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get_all_using_get3(hub, ctx, **kwargs):
    """Fetch all action templates Retrieves all action templates Performs GET /abx/api/resources/action-templates"""

    hub.log.debug("GET /abx/api/resources/action-templates")

    api = ActionTemplatesApi(hub.clients["idem_vra.client.vra_abx_lib.api"])

    ret = api.get_all_using_get3(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_by_id_using_get3(hub, ctx, p_id, **kwargs):
    """Fetch an action template by its ID Retrieves an action template entity with a specific ID Performs GET /abx/api/resources/action-templates/{id}


    :param string p_id: (required in path) ID of the action template
    """

    hub.log.debug("GET /abx/api/resources/action-templates/{id}")

    api = ActionTemplatesApi(hub.clients["idem_vra.client.vra_abx_lib.api"])

    ret = api.get_by_id_using_get3(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))

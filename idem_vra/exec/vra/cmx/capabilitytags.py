from idem_vra.client.vra_cmx_lib.api import CapabilityTagsApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def search_tags_using_get(hub, ctx, **kwargs):
    """Search for capability tags Search for capability tags using an OData filter. For example, use the
      following filter to retrieve the tag 'location:somewhere': $filter=(key eq
      '*location*') Performs GET /cmx/api/resources/tags


    :param integer offset: (optional in query)
    :param integer pageNumber: (optional in query)
    :param integer pageSize: (optional in query)
    :param boolean paged: (optional in query)
    :param boolean sort.sorted: (optional in query)
    :param boolean sort.unsorted: (optional in query)
    :param boolean unpaged: (optional in query)
    """

    hub.log.debug("GET /cmx/api/resources/tags")

    api = CapabilityTagsApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

    ret = api.search_tags_using_get(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))

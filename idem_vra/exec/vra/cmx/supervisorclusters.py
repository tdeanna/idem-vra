from idem_vra.client.vra_cmx_lib.api import SupervisorClustersApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def existing_tanzu_clusters_using_get(hub, ctx, p_selfLinkId, **kwargs):
    """Get all existing tanzu kubernetes clusters on a supervisor cluster. Get all existing tanzu kubernetes clusters on a supervisor cluster. Performs GET /cmx/api/resources/supervisor-clusters/{selfLinkId}/tanzu-clusters-namespace


    :param string p_selfLinkId: (required in path) selfLinkId
    :param string registered: (optional in query) registered
    :param string searchTerm: (optional in query) searchTerm
    """

    hub.log.debug(
        "GET /cmx/api/resources/supervisor-clusters/{selfLinkId}/tanzu-clusters-namespace"
    )

    api = SupervisorClustersApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

    ret = api.existing_tanzu_clusters_using_get(self_link_id=p_selfLinkId, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_cluster_using_get1(hub, ctx, p_endpointSelfLinkId, p_moref, **kwargs):
    """Find a Supervisor Cluster by vSphere moref and vSphere endpoint id Retrieve a Supervisor Cluster by vSphere moref and id from the endpoint self
      link of the vSphere endpoint this cluster is associated to Performs GET /cmx/api/resources/supervisor-clusters/endpoint/{endpointSelfLinkId}/cluster/{moref}


    :param string p_endpointSelfLinkId: (required in path) endpointSelfLinkId
    :param string p_moref: (required in path) moref
    """

    hub.log.debug(
        "GET /cmx/api/resources/supervisor-clusters/endpoint/{endpointSelfLinkId}/cluster/{moref}"
    )

    api = SupervisorClustersApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

    ret = api.get_cluster_using_get1(
        endpoint_self_link_id=p_endpointSelfLinkId, moref=p_moref, **kwargs
    )

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_cluster_using_get2(hub, ctx, p_clusterSelfLinkId, **kwargs):
    """Find a Supervisor Cluster by the id from documentSelfLink Retrieve a Supervisor Cluster by id from documentSelfLink Performs GET /cmx/api/resources/supervisor-clusters/{clusterSelfLinkId}


    :param string p_clusterSelfLinkId: (required in path) clusterSelfLinkId
    """

    hub.log.debug("GET /cmx/api/resources/supervisor-clusters/{clusterSelfLinkId}")

    api = SupervisorClustersApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

    ret = api.get_cluster_using_get2(cluster_self_link_id=p_clusterSelfLinkId, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def list_clusters_on_endpoint_using_get(hub, ctx, p_endpointSelfLinkId, **kwargs):
    """Get all Supervisor Clusters on a vSphere endpoint Get all Supervisor Clusters on a vSphere endpoint by provided id from the
      endpoint self link Performs GET /cmx/api/resources/supervisor-clusters/endpoint/{endpointSelfLinkId}


    :param string p_endpointSelfLinkId: (required in path) endpointSelfLinkId
    :param integer offset: (optional in query)
    :param integer pageNumber: (optional in query)
    :param integer pageSize: (optional in query)
    :param boolean paged: (optional in query)
    :param boolean sort.sorted: (optional in query)
    :param boolean sort.unsorted: (optional in query)
    :param boolean unpaged: (optional in query)
    :param string registered: (optional in query) registered
    """

    hub.log.debug(
        "GET /cmx/api/resources/supervisor-clusters/endpoint/{endpointSelfLinkId}"
    )

    api = SupervisorClustersApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

    ret = api.list_clusters_on_endpoint_using_get(
        endpoint_self_link_id=p_endpointSelfLinkId, **kwargs
    )

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def list_using_get4(hub, ctx, **kwargs):
    """Get all managed Supervisor Clusters Get all managed Supervisor Clusters Performs GET /cmx/api/resources/supervisor-clusters


    :param integer offset: (optional in query)
    :param integer pageNumber: (optional in query)
    :param integer pageSize: (optional in query)
    :param boolean paged: (optional in query)
    :param boolean sort.sorted: (optional in query)
    :param boolean sort.unsorted: (optional in query)
    :param boolean unpaged: (optional in query)
    """

    hub.log.debug("GET /cmx/api/resources/supervisor-clusters")

    api = SupervisorClustersApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

    ret = api.list_using_get4(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def register_using_put1(hub, ctx, p_clusterSelfLinkId, **kwargs):
    """Make a Supervisor Cluster a managed entity A valid document self link id shall be provided. Performs PUT /cmx/api/resources/supervisor-clusters/{clusterSelfLinkId}/register


    :param string p_clusterSelfLinkId: (required in path) clusterSelfLinkId
    """

    hub.log.debug(
        "PUT /cmx/api/resources/supervisor-clusters/{clusterSelfLinkId}/register"
    )

    api = SupervisorClustersApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

    ret = api.register_using_put1(cluster_self_link_id=p_clusterSelfLinkId, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def unregister_using_delete(hub, ctx, p_clusterSelfLinkId, **kwargs):
    """Make a Supervisor Cluster an unmanaged entity A valid document self link id shall be provided. Performs DELETE /cmx/api/resources/supervisor-clusters/{clusterSelfLinkId}


    :param string p_clusterSelfLinkId: (required in path) clusterSelfLinkId
    """

    hub.log.debug("DELETE /cmx/api/resources/supervisor-clusters/{clusterSelfLinkId}")

    api = SupervisorClustersApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

    ret = api.unregister_using_delete(
        cluster_self_link_id=p_clusterSelfLinkId, **kwargs
    )

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))

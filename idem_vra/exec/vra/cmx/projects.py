from idem_vra.client.vra_cmx_lib.api import ProjectsApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def count_of_entities_in_project_using_get(hub, ctx, p_projectId, **kwargs):
    """Retrieve the count of entities per project Get all entities for a project by providing a project id Performs GET /cmx/api/projects/{projectId}/entity-count


    :param string p_projectId: (required in path) projectId
    """

    hub.log.debug("GET /cmx/api/projects/{projectId}/entity-count")

    api = ProjectsApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

    ret = api.count_of_entities_in_project_using_get(project_id=p_projectId, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))

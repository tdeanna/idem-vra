from idem_vra.client.vra_cmx_lib.api import KubernetesClustersApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def assign_status_using_get(hub, ctx, p_requestId, **kwargs):
    """Retrieve assign status. Retrieve assign status. Performs GET /cmx/api/resources/k8s/clusters/assign/{requestId}


    :param string p_requestId: (required in path) requestId
    """

    hub.log.debug("GET /cmx/api/resources/k8s/clusters/assign/{requestId}")

    api = KubernetesClustersApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

    ret = api.assign_status_using_get(request_id=p_requestId, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def assign_using_post(hub, ctx, **kwargs):
    """Assign K8s cluster to cluster cluster group. Assign K8s cluster to cluster cluster group. Performs POST /cmx/api/resources/k8s/clusters/assign


    :param string clusterGroup: (optional in body)
    :param array clusterIds: (optional in body)
    :param string projectId: (optional in body)
    """

    hub.log.debug("POST /cmx/api/resources/k8s/clusters/assign")

    api = KubernetesClustersApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

    body = {}

    if "clusterGroup" in kwargs:
        hub.log.debug(f"Got kwarg 'clusterGroup' = {kwargs['clusterGroup']}")
        body["clusterGroup"] = kwargs.get("clusterGroup")
        del kwargs["clusterGroup"]
    if "clusterIds" in kwargs:
        hub.log.debug(f"Got kwarg 'clusterIds' = {kwargs['clusterIds']}")
        body["clusterIds"] = kwargs.get("clusterIds")
        del kwargs["clusterIds"]
    if "projectId" in kwargs:
        hub.log.debug(f"Got kwarg 'projectId' = {kwargs['projectId']}")
        body["projectId"] = kwargs.get("projectId")
        del kwargs["projectId"]

    ret = api.assign_using_post(body, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def delete_cluster_using_delete(hub, ctx, p_id, **kwargs):
    """Delete a K8S Cluster Delete a K8S Cluster by provided id Performs DELETE /cmx/api/resources/k8s/clusters/{id}


    :param string p_id: (required in path) id
    """

    hub.log.debug("DELETE /cmx/api/resources/k8s/clusters/{id}")

    api = KubernetesClustersApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

    ret = api.delete_cluster_using_delete(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def destroy_cluster_using_delete(hub, ctx, p_id, **kwargs):
    """Destroy a K8S Cluster Destroy a K8S Cluster by provided id Performs DELETE /cmx/api/resources/k8s/clusters/{id}/destroy


    :param string p_id: (required in path) id
    """

    hub.log.debug("DELETE /cmx/api/resources/k8s/clusters/{id}/destroy")

    api = KubernetesClustersApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

    ret = api.destroy_cluster_using_delete(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_cluster_using_get(hub, ctx, p_id, **kwargs):
    """Find a K8S Cluster by id Retrieve a K8S by id Performs GET /cmx/api/resources/k8s/clusters/{id}


    :param string p_id: (required in path) id
    """

    hub.log.debug("GET /cmx/api/resources/k8s/clusters/{id}")

    api = KubernetesClustersApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

    ret = api.get_cluster_using_get(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_kube_config_using_get(hub, ctx, p_id, **kwargs):
    """Get a KubeConfig for a K8S Cluster Get a KubeConfig for a K8S Cluster by provided id Performs GET /cmx/api/resources/k8s/clusters/{id}/kube-config


    :param string p_id: (required in path) id
    :param boolean ignoreTMC: (optional in query) ignoreTMC
    :param boolean includeCredentials: (optional in query) includeCredentials
    """

    hub.log.debug("GET /cmx/api/resources/k8s/clusters/{id}/kube-config")

    api = KubernetesClustersApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

    ret = api.get_kube_config_using_get(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_nodes_using_get(hub, ctx, p_id, **kwargs):
    """Get nodes of K8S Clusters Get nodes of K8S Clusters by provided id Performs GET /cmx/api/resources/k8s/clusters/{id}/nodes


    :param string p_id: (required in path) id
    """

    hub.log.debug("GET /cmx/api/resources/k8s/clusters/{id}/nodes")

    api = KubernetesClustersApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

    ret = api.get_nodes_using_get(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def list_using_get(hub, ctx, **kwargs):
    """Get all registered K8S Clusters Retrieve the managed K8S Clusters Performs GET /cmx/api/resources/k8s/clusters


    :param string id: (optional in query)
    :param boolean localAddress.address.MCGlobal: (optional in query)
    :param boolean localAddress.address.MCLinkLocal: (optional in query)
    :param boolean localAddress.address.MCNodeLocal: (optional in query)
    :param boolean localAddress.address.MCOrgLocal: (optional in query)
    :param boolean localAddress.address.MCSiteLocal: (optional in query)
    :param string localAddress.address.address: (optional in query)
    :param boolean localAddress.address.anyLocalAddress: (optional in query)
    :param string localAddress.address.canonicalHostName: (optional in query)
    :param string localAddress.address.hostAddress: (optional in query)
    :param string localAddress.address.hostName: (optional in query)
    :param boolean localAddress.address.linkLocalAddress: (optional in query)
    :param boolean localAddress.address.loopbackAddress: (optional in query)
    :param boolean localAddress.address.multicastAddress: (optional in query)
    :param boolean localAddress.address.siteLocalAddress: (optional in query)
    :param string localAddress.hostName: (optional in query)
    :param string localAddress.hostString: (optional in query)
    :param integer localAddress.port: (optional in query)
    :param boolean localAddress.unresolved: (optional in query)
    :param integer offset: (optional in query)
    :param integer pageNumber: (optional in query)
    :param integer pageSize: (optional in query)
    :param boolean paged: (optional in query)
    :param boolean remoteAddress.address.MCGlobal: (optional in query)
    :param boolean remoteAddress.address.MCLinkLocal: (optional in query)
    :param boolean remoteAddress.address.MCNodeLocal: (optional in query)
    :param boolean remoteAddress.address.MCOrgLocal: (optional in query)
    :param boolean remoteAddress.address.MCSiteLocal: (optional in query)
    :param string remoteAddress.address.address: (optional in query)
    :param boolean remoteAddress.address.anyLocalAddress: (optional in query)
    :param string remoteAddress.address.canonicalHostName: (optional in query)
    :param string remoteAddress.address.hostAddress: (optional in query)
    :param string remoteAddress.address.hostName: (optional in query)
    :param boolean remoteAddress.address.linkLocalAddress: (optional in query)
    :param boolean remoteAddress.address.loopbackAddress: (optional in query)
    :param boolean remoteAddress.address.multicastAddress: (optional in query)
    :param boolean remoteAddress.address.siteLocalAddress: (optional in query)
    :param string remoteAddress.hostName: (optional in query)
    :param string remoteAddress.hostString: (optional in query)
    :param integer remoteAddress.port: (optional in query)
    :param boolean remoteAddress.unresolved: (optional in query)
    :param boolean sort.sorted: (optional in query)
    :param boolean sort.unsorted: (optional in query)
    :param string sslInfo.peerCertificates[0].TBSCertificate: (optional in query)
    :param integer sslInfo.peerCertificates[0].basicConstraints: (optional in query)
    :param string sslInfo.peerCertificates[0].encoded: (optional in query)
    :param array sslInfo.peerCertificates[0].extendedKeyUsage: (optional in query)
    :param string sslInfo.peerCertificates[0].issuerDN.name: (optional in query)
    :param array sslInfo.peerCertificates[0].issuerUniqueID: (optional in query)
    :param string sslInfo.peerCertificates[0].issuerX500Principal.encoded: (optional in query)
    :param string sslInfo.peerCertificates[0].issuerX500Principal.name: (optional in query)
    :param array sslInfo.peerCertificates[0].keyUsage: (optional in query)
    :param string sslInfo.peerCertificates[0].notAfter: (optional in query)
    :param string sslInfo.peerCertificates[0].notBefore: (optional in query)
    :param integer sslInfo.peerCertificates[0].serialNumber: (optional in query)
    :param string sslInfo.peerCertificates[0].sigAlgName: (optional in query)
    :param string sslInfo.peerCertificates[0].sigAlgOID: (optional in query)
    :param string sslInfo.peerCertificates[0].sigAlgParams: (optional in query)
    :param string sslInfo.peerCertificates[0].signature: (optional in query)
    :param string sslInfo.peerCertificates[0].subjectDN.name: (optional in query)
    :param array sslInfo.peerCertificates[0].subjectUniqueID: (optional in query)
    :param string sslInfo.peerCertificates[0].subjectX500Principal.encoded: (optional in query)
    :param string sslInfo.peerCertificates[0].subjectX500Principal.name: (optional in query)
    :param string sslInfo.peerCertificates[0].type: (optional in query)
    :param integer sslInfo.peerCertificates[0].version: (optional in query)
    :param string sslInfo.sessionId: (optional in query)
    :param boolean unpaged: (optional in query)
    """

    hub.log.debug("GET /cmx/api/resources/k8s/clusters")

    api = KubernetesClustersApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

    ret = api.list_using_get(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def onboard_using_post(hub, ctx, **kwargs):
    """Onboard a K8S Cluster Onboard a K8S Cluster entity Performs POST /cmx/api/resources/k8s/clusters


    :param string address: (optional in body)
    :param string authCredentialsLink: (optional in body)
    :param string caCertificate: (optional in body)
    :param string clusterType: (optional in body)
    :param integer createdMillis: (optional in body)
    :param Any credentials: (optional in body)
    :param object customProperties: (optional in body)
    :param string description: (optional in body)
    :param boolean directConnection: (optional in body)
    :param string endpointId: (optional in body)
    :param string externalLink: (optional in body)
    :param boolean global: (optional in body)
    :param string id: (optional in body)
    :param string installerId: (optional in body)
    :param integer kubernetesWorkerInstances: (optional in body)
    :param string lastOperation: (optional in body)
    :param string lastOperationMessage: (optional in body)
    :param string lastOperationStatus: (optional in body)
    :param integer lastRefreshedMillis: (optional in body)
    :param string name: (optional in body)
    :param array nodes: (optional in body)
    :param string orgId: (optional in body)
    :param string owner: (optional in body)
    :param string projectId: (optional in body)
    :param integer remainingUnreachableRetryThreshold: (optional in body)
    :param boolean shared: (optional in body)
    :param string status: (optional in body)
    :param integer updatedMillis: (optional in body)
    :param string version: (optional in body)
    """

    hub.log.debug("POST /cmx/api/resources/k8s/clusters")

    api = KubernetesClustersApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

    body = {}

    if "address" in kwargs:
        hub.log.debug(f"Got kwarg 'address' = {kwargs['address']}")
        body["address"] = kwargs.get("address")
        del kwargs["address"]
    if "authCredentialsLink" in kwargs:
        hub.log.debug(
            f"Got kwarg 'authCredentialsLink' = {kwargs['authCredentialsLink']}"
        )
        body["authCredentialsLink"] = kwargs.get("authCredentialsLink")
        del kwargs["authCredentialsLink"]
    if "caCertificate" in kwargs:
        hub.log.debug(f"Got kwarg 'caCertificate' = {kwargs['caCertificate']}")
        body["caCertificate"] = kwargs.get("caCertificate")
        del kwargs["caCertificate"]
    if "clusterType" in kwargs:
        hub.log.debug(f"Got kwarg 'clusterType' = {kwargs['clusterType']}")
        body["clusterType"] = kwargs.get("clusterType")
        del kwargs["clusterType"]
    if "createdMillis" in kwargs:
        hub.log.debug(f"Got kwarg 'createdMillis' = {kwargs['createdMillis']}")
        body["createdMillis"] = kwargs.get("createdMillis")
        del kwargs["createdMillis"]
    if "credentials" in kwargs:
        hub.log.debug(f"Got kwarg 'credentials' = {kwargs['credentials']}")
        body["credentials"] = kwargs.get("credentials")
        del kwargs["credentials"]
    if "customProperties" in kwargs:
        hub.log.debug(f"Got kwarg 'customProperties' = {kwargs['customProperties']}")
        body["customProperties"] = kwargs.get("customProperties")
        del kwargs["customProperties"]
    if "description" in kwargs:
        hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
        body["description"] = kwargs.get("description")
        del kwargs["description"]
    if "directConnection" in kwargs:
        hub.log.debug(f"Got kwarg 'directConnection' = {kwargs['directConnection']}")
        body["directConnection"] = kwargs.get("directConnection")
        del kwargs["directConnection"]
    if "endpointId" in kwargs:
        hub.log.debug(f"Got kwarg 'endpointId' = {kwargs['endpointId']}")
        body["endpointId"] = kwargs.get("endpointId")
        del kwargs["endpointId"]
    if "externalLink" in kwargs:
        hub.log.debug(f"Got kwarg 'externalLink' = {kwargs['externalLink']}")
        body["externalLink"] = kwargs.get("externalLink")
        del kwargs["externalLink"]
    if "global" in kwargs:
        hub.log.debug(f"Got kwarg 'global' = {kwargs['global']}")
        body["global"] = kwargs.get("global")
        del kwargs["global"]
    if "id" in kwargs:
        hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
        body["id"] = kwargs.get("id")
        del kwargs["id"]
    if "installerId" in kwargs:
        hub.log.debug(f"Got kwarg 'installerId' = {kwargs['installerId']}")
        body["installerId"] = kwargs.get("installerId")
        del kwargs["installerId"]
    if "kubernetesWorkerInstances" in kwargs:
        hub.log.debug(
            f"Got kwarg 'kubernetesWorkerInstances' = {kwargs['kubernetesWorkerInstances']}"
        )
        body["kubernetesWorkerInstances"] = kwargs.get("kubernetesWorkerInstances")
        del kwargs["kubernetesWorkerInstances"]
    if "lastOperation" in kwargs:
        hub.log.debug(f"Got kwarg 'lastOperation' = {kwargs['lastOperation']}")
        body["lastOperation"] = kwargs.get("lastOperation")
        del kwargs["lastOperation"]
    if "lastOperationMessage" in kwargs:
        hub.log.debug(
            f"Got kwarg 'lastOperationMessage' = {kwargs['lastOperationMessage']}"
        )
        body["lastOperationMessage"] = kwargs.get("lastOperationMessage")
        del kwargs["lastOperationMessage"]
    if "lastOperationStatus" in kwargs:
        hub.log.debug(
            f"Got kwarg 'lastOperationStatus' = {kwargs['lastOperationStatus']}"
        )
        body["lastOperationStatus"] = kwargs.get("lastOperationStatus")
        del kwargs["lastOperationStatus"]
    if "lastRefreshedMillis" in kwargs:
        hub.log.debug(
            f"Got kwarg 'lastRefreshedMillis' = {kwargs['lastRefreshedMillis']}"
        )
        body["lastRefreshedMillis"] = kwargs.get("lastRefreshedMillis")
        del kwargs["lastRefreshedMillis"]
    if "name" in kwargs:
        hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
        body["name"] = kwargs.get("name")
        del kwargs["name"]
    if "nodes" in kwargs:
        hub.log.debug(f"Got kwarg 'nodes' = {kwargs['nodes']}")
        body["nodes"] = kwargs.get("nodes")
        del kwargs["nodes"]
    if "orgId" in kwargs:
        hub.log.debug(f"Got kwarg 'orgId' = {kwargs['orgId']}")
        body["orgId"] = kwargs.get("orgId")
        del kwargs["orgId"]
    if "owner" in kwargs:
        hub.log.debug(f"Got kwarg 'owner' = {kwargs['owner']}")
        body["owner"] = kwargs.get("owner")
        del kwargs["owner"]
    if "projectId" in kwargs:
        hub.log.debug(f"Got kwarg 'projectId' = {kwargs['projectId']}")
        body["projectId"] = kwargs.get("projectId")
        del kwargs["projectId"]
    if "remainingUnreachableRetryThreshold" in kwargs:
        hub.log.debug(
            f"Got kwarg 'remainingUnreachableRetryThreshold' = {kwargs['remainingUnreachableRetryThreshold']}"
        )
        body["remainingUnreachableRetryThreshold"] = kwargs.get(
            "remainingUnreachableRetryThreshold"
        )
        del kwargs["remainingUnreachableRetryThreshold"]
    if "shared" in kwargs:
        hub.log.debug(f"Got kwarg 'shared' = {kwargs['shared']}")
        body["shared"] = kwargs.get("shared")
        del kwargs["shared"]
    if "status" in kwargs:
        hub.log.debug(f"Got kwarg 'status' = {kwargs['status']}")
        body["status"] = kwargs.get("status")
        del kwargs["status"]
    if "updatedMillis" in kwargs:
        hub.log.debug(f"Got kwarg 'updatedMillis' = {kwargs['updatedMillis']}")
        body["updatedMillis"] = kwargs.get("updatedMillis")
        del kwargs["updatedMillis"]
    if "version" in kwargs:
        hub.log.debug(f"Got kwarg 'version' = {kwargs['version']}")
        body["version"] = kwargs.get("version")
        del kwargs["version"]

    ret = api.onboard_using_post(body, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def update_using_put1(hub, ctx, p_id, **kwargs):
    """Update a K8S Cluster Update K8S Cluster by id Performs PUT /cmx/api/resources/k8s/clusters/{id}


    :param string p_id: (required in path) id
    :param string address: (optional in body)
    :param string authCredentialsLink: (optional in body)
    :param string caCertificate: (optional in body)
    :param string clusterType: (optional in body)
    :param integer createdMillis: (optional in body)
    :param Any credentials: (optional in body)
    :param object customProperties: (optional in body)
    :param string description: (optional in body)
    :param boolean directConnection: (optional in body)
    :param string endpointId: (optional in body)
    :param string externalLink: (optional in body)
    :param boolean global: (optional in body)
    :param string id: (optional in body)
    :param string installerId: (optional in body)
    :param integer kubernetesWorkerInstances: (optional in body)
    :param string lastOperation: (optional in body)
    :param string lastOperationMessage: (optional in body)
    :param string lastOperationStatus: (optional in body)
    :param integer lastRefreshedMillis: (optional in body)
    :param string name: (optional in body)
    :param array nodes: (optional in body)
    :param string orgId: (optional in body)
    :param string owner: (optional in body)
    :param string projectId: (optional in body)
    :param integer remainingUnreachableRetryThreshold: (optional in body)
    :param boolean shared: (optional in body)
    :param string status: (optional in body)
    :param integer updatedMillis: (optional in body)
    :param string version: (optional in body)
    """

    hub.log.debug("PUT /cmx/api/resources/k8s/clusters/{id}")

    api = KubernetesClustersApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

    body = {}

    if "address" in kwargs:
        hub.log.debug(f"Got kwarg 'address' = {kwargs['address']}")
        body["address"] = kwargs.get("address")
        del kwargs["address"]
    if "authCredentialsLink" in kwargs:
        hub.log.debug(
            f"Got kwarg 'authCredentialsLink' = {kwargs['authCredentialsLink']}"
        )
        body["authCredentialsLink"] = kwargs.get("authCredentialsLink")
        del kwargs["authCredentialsLink"]
    if "caCertificate" in kwargs:
        hub.log.debug(f"Got kwarg 'caCertificate' = {kwargs['caCertificate']}")
        body["caCertificate"] = kwargs.get("caCertificate")
        del kwargs["caCertificate"]
    if "clusterType" in kwargs:
        hub.log.debug(f"Got kwarg 'clusterType' = {kwargs['clusterType']}")
        body["clusterType"] = kwargs.get("clusterType")
        del kwargs["clusterType"]
    if "createdMillis" in kwargs:
        hub.log.debug(f"Got kwarg 'createdMillis' = {kwargs['createdMillis']}")
        body["createdMillis"] = kwargs.get("createdMillis")
        del kwargs["createdMillis"]
    if "credentials" in kwargs:
        hub.log.debug(f"Got kwarg 'credentials' = {kwargs['credentials']}")
        body["credentials"] = kwargs.get("credentials")
        del kwargs["credentials"]
    if "customProperties" in kwargs:
        hub.log.debug(f"Got kwarg 'customProperties' = {kwargs['customProperties']}")
        body["customProperties"] = kwargs.get("customProperties")
        del kwargs["customProperties"]
    if "description" in kwargs:
        hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
        body["description"] = kwargs.get("description")
        del kwargs["description"]
    if "directConnection" in kwargs:
        hub.log.debug(f"Got kwarg 'directConnection' = {kwargs['directConnection']}")
        body["directConnection"] = kwargs.get("directConnection")
        del kwargs["directConnection"]
    if "endpointId" in kwargs:
        hub.log.debug(f"Got kwarg 'endpointId' = {kwargs['endpointId']}")
        body["endpointId"] = kwargs.get("endpointId")
        del kwargs["endpointId"]
    if "externalLink" in kwargs:
        hub.log.debug(f"Got kwarg 'externalLink' = {kwargs['externalLink']}")
        body["externalLink"] = kwargs.get("externalLink")
        del kwargs["externalLink"]
    if "global" in kwargs:
        hub.log.debug(f"Got kwarg 'global' = {kwargs['global']}")
        body["global"] = kwargs.get("global")
        del kwargs["global"]
    if "id" in kwargs:
        hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
        body["id"] = kwargs.get("id")
        del kwargs["id"]
    if "installerId" in kwargs:
        hub.log.debug(f"Got kwarg 'installerId' = {kwargs['installerId']}")
        body["installerId"] = kwargs.get("installerId")
        del kwargs["installerId"]
    if "kubernetesWorkerInstances" in kwargs:
        hub.log.debug(
            f"Got kwarg 'kubernetesWorkerInstances' = {kwargs['kubernetesWorkerInstances']}"
        )
        body["kubernetesWorkerInstances"] = kwargs.get("kubernetesWorkerInstances")
        del kwargs["kubernetesWorkerInstances"]
    if "lastOperation" in kwargs:
        hub.log.debug(f"Got kwarg 'lastOperation' = {kwargs['lastOperation']}")
        body["lastOperation"] = kwargs.get("lastOperation")
        del kwargs["lastOperation"]
    if "lastOperationMessage" in kwargs:
        hub.log.debug(
            f"Got kwarg 'lastOperationMessage' = {kwargs['lastOperationMessage']}"
        )
        body["lastOperationMessage"] = kwargs.get("lastOperationMessage")
        del kwargs["lastOperationMessage"]
    if "lastOperationStatus" in kwargs:
        hub.log.debug(
            f"Got kwarg 'lastOperationStatus' = {kwargs['lastOperationStatus']}"
        )
        body["lastOperationStatus"] = kwargs.get("lastOperationStatus")
        del kwargs["lastOperationStatus"]
    if "lastRefreshedMillis" in kwargs:
        hub.log.debug(
            f"Got kwarg 'lastRefreshedMillis' = {kwargs['lastRefreshedMillis']}"
        )
        body["lastRefreshedMillis"] = kwargs.get("lastRefreshedMillis")
        del kwargs["lastRefreshedMillis"]
    if "name" in kwargs:
        hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
        body["name"] = kwargs.get("name")
        del kwargs["name"]
    if "nodes" in kwargs:
        hub.log.debug(f"Got kwarg 'nodes' = {kwargs['nodes']}")
        body["nodes"] = kwargs.get("nodes")
        del kwargs["nodes"]
    if "orgId" in kwargs:
        hub.log.debug(f"Got kwarg 'orgId' = {kwargs['orgId']}")
        body["orgId"] = kwargs.get("orgId")
        del kwargs["orgId"]
    if "owner" in kwargs:
        hub.log.debug(f"Got kwarg 'owner' = {kwargs['owner']}")
        body["owner"] = kwargs.get("owner")
        del kwargs["owner"]
    if "projectId" in kwargs:
        hub.log.debug(f"Got kwarg 'projectId' = {kwargs['projectId']}")
        body["projectId"] = kwargs.get("projectId")
        del kwargs["projectId"]
    if "remainingUnreachableRetryThreshold" in kwargs:
        hub.log.debug(
            f"Got kwarg 'remainingUnreachableRetryThreshold' = {kwargs['remainingUnreachableRetryThreshold']}"
        )
        body["remainingUnreachableRetryThreshold"] = kwargs.get(
            "remainingUnreachableRetryThreshold"
        )
        del kwargs["remainingUnreachableRetryThreshold"]
    if "shared" in kwargs:
        hub.log.debug(f"Got kwarg 'shared' = {kwargs['shared']}")
        body["shared"] = kwargs.get("shared")
        del kwargs["shared"]
    if "status" in kwargs:
        hub.log.debug(f"Got kwarg 'status' = {kwargs['status']}")
        body["status"] = kwargs.get("status")
        del kwargs["status"]
    if "updatedMillis" in kwargs:
        hub.log.debug(f"Got kwarg 'updatedMillis' = {kwargs['updatedMillis']}")
        body["updatedMillis"] = kwargs.get("updatedMillis")
        del kwargs["updatedMillis"]
    if "version" in kwargs:
        hub.log.debug(f"Got kwarg 'version' = {kwargs['version']}")
        body["version"] = kwargs.get("version")
        del kwargs["version"]

    ret = api.update_using_put1(body, id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def validate_using_put(hub, ctx, **kwargs):
    """Validate provided K8S Cluster entity Validate provided K8S Cluster entity Performs PUT /cmx/api/resources/k8s/clusters/validate


    :param string address: (optional in body)
    :param string authCredentialsLink: (optional in body)
    :param string caCertificate: (optional in body)
    :param string clusterType: (optional in body)
    :param integer createdMillis: (optional in body)
    :param Any credentials: (optional in body)
    :param object customProperties: (optional in body)
    :param string description: (optional in body)
    :param boolean directConnection: (optional in body)
    :param string endpointId: (optional in body)
    :param string externalLink: (optional in body)
    :param boolean global: (optional in body)
    :param string id: (optional in body)
    :param string installerId: (optional in body)
    :param integer kubernetesWorkerInstances: (optional in body)
    :param string lastOperation: (optional in body)
    :param string lastOperationMessage: (optional in body)
    :param string lastOperationStatus: (optional in body)
    :param integer lastRefreshedMillis: (optional in body)
    :param string name: (optional in body)
    :param array nodes: (optional in body)
    :param string orgId: (optional in body)
    :param string owner: (optional in body)
    :param string projectId: (optional in body)
    :param integer remainingUnreachableRetryThreshold: (optional in body)
    :param boolean shared: (optional in body)
    :param string status: (optional in body)
    :param integer updatedMillis: (optional in body)
    :param string version: (optional in body)
    """

    hub.log.debug("PUT /cmx/api/resources/k8s/clusters/validate")

    api = KubernetesClustersApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

    body = {}

    if "address" in kwargs:
        hub.log.debug(f"Got kwarg 'address' = {kwargs['address']}")
        body["address"] = kwargs.get("address")
        del kwargs["address"]
    if "authCredentialsLink" in kwargs:
        hub.log.debug(
            f"Got kwarg 'authCredentialsLink' = {kwargs['authCredentialsLink']}"
        )
        body["authCredentialsLink"] = kwargs.get("authCredentialsLink")
        del kwargs["authCredentialsLink"]
    if "caCertificate" in kwargs:
        hub.log.debug(f"Got kwarg 'caCertificate' = {kwargs['caCertificate']}")
        body["caCertificate"] = kwargs.get("caCertificate")
        del kwargs["caCertificate"]
    if "clusterType" in kwargs:
        hub.log.debug(f"Got kwarg 'clusterType' = {kwargs['clusterType']}")
        body["clusterType"] = kwargs.get("clusterType")
        del kwargs["clusterType"]
    if "createdMillis" in kwargs:
        hub.log.debug(f"Got kwarg 'createdMillis' = {kwargs['createdMillis']}")
        body["createdMillis"] = kwargs.get("createdMillis")
        del kwargs["createdMillis"]
    if "credentials" in kwargs:
        hub.log.debug(f"Got kwarg 'credentials' = {kwargs['credentials']}")
        body["credentials"] = kwargs.get("credentials")
        del kwargs["credentials"]
    if "customProperties" in kwargs:
        hub.log.debug(f"Got kwarg 'customProperties' = {kwargs['customProperties']}")
        body["customProperties"] = kwargs.get("customProperties")
        del kwargs["customProperties"]
    if "description" in kwargs:
        hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
        body["description"] = kwargs.get("description")
        del kwargs["description"]
    if "directConnection" in kwargs:
        hub.log.debug(f"Got kwarg 'directConnection' = {kwargs['directConnection']}")
        body["directConnection"] = kwargs.get("directConnection")
        del kwargs["directConnection"]
    if "endpointId" in kwargs:
        hub.log.debug(f"Got kwarg 'endpointId' = {kwargs['endpointId']}")
        body["endpointId"] = kwargs.get("endpointId")
        del kwargs["endpointId"]
    if "externalLink" in kwargs:
        hub.log.debug(f"Got kwarg 'externalLink' = {kwargs['externalLink']}")
        body["externalLink"] = kwargs.get("externalLink")
        del kwargs["externalLink"]
    if "global" in kwargs:
        hub.log.debug(f"Got kwarg 'global' = {kwargs['global']}")
        body["global"] = kwargs.get("global")
        del kwargs["global"]
    if "id" in kwargs:
        hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
        body["id"] = kwargs.get("id")
        del kwargs["id"]
    if "installerId" in kwargs:
        hub.log.debug(f"Got kwarg 'installerId' = {kwargs['installerId']}")
        body["installerId"] = kwargs.get("installerId")
        del kwargs["installerId"]
    if "kubernetesWorkerInstances" in kwargs:
        hub.log.debug(
            f"Got kwarg 'kubernetesWorkerInstances' = {kwargs['kubernetesWorkerInstances']}"
        )
        body["kubernetesWorkerInstances"] = kwargs.get("kubernetesWorkerInstances")
        del kwargs["kubernetesWorkerInstances"]
    if "lastOperation" in kwargs:
        hub.log.debug(f"Got kwarg 'lastOperation' = {kwargs['lastOperation']}")
        body["lastOperation"] = kwargs.get("lastOperation")
        del kwargs["lastOperation"]
    if "lastOperationMessage" in kwargs:
        hub.log.debug(
            f"Got kwarg 'lastOperationMessage' = {kwargs['lastOperationMessage']}"
        )
        body["lastOperationMessage"] = kwargs.get("lastOperationMessage")
        del kwargs["lastOperationMessage"]
    if "lastOperationStatus" in kwargs:
        hub.log.debug(
            f"Got kwarg 'lastOperationStatus' = {kwargs['lastOperationStatus']}"
        )
        body["lastOperationStatus"] = kwargs.get("lastOperationStatus")
        del kwargs["lastOperationStatus"]
    if "lastRefreshedMillis" in kwargs:
        hub.log.debug(
            f"Got kwarg 'lastRefreshedMillis' = {kwargs['lastRefreshedMillis']}"
        )
        body["lastRefreshedMillis"] = kwargs.get("lastRefreshedMillis")
        del kwargs["lastRefreshedMillis"]
    if "name" in kwargs:
        hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
        body["name"] = kwargs.get("name")
        del kwargs["name"]
    if "nodes" in kwargs:
        hub.log.debug(f"Got kwarg 'nodes' = {kwargs['nodes']}")
        body["nodes"] = kwargs.get("nodes")
        del kwargs["nodes"]
    if "orgId" in kwargs:
        hub.log.debug(f"Got kwarg 'orgId' = {kwargs['orgId']}")
        body["orgId"] = kwargs.get("orgId")
        del kwargs["orgId"]
    if "owner" in kwargs:
        hub.log.debug(f"Got kwarg 'owner' = {kwargs['owner']}")
        body["owner"] = kwargs.get("owner")
        del kwargs["owner"]
    if "projectId" in kwargs:
        hub.log.debug(f"Got kwarg 'projectId' = {kwargs['projectId']}")
        body["projectId"] = kwargs.get("projectId")
        del kwargs["projectId"]
    if "remainingUnreachableRetryThreshold" in kwargs:
        hub.log.debug(
            f"Got kwarg 'remainingUnreachableRetryThreshold' = {kwargs['remainingUnreachableRetryThreshold']}"
        )
        body["remainingUnreachableRetryThreshold"] = kwargs.get(
            "remainingUnreachableRetryThreshold"
        )
        del kwargs["remainingUnreachableRetryThreshold"]
    if "shared" in kwargs:
        hub.log.debug(f"Got kwarg 'shared' = {kwargs['shared']}")
        body["shared"] = kwargs.get("shared")
        del kwargs["shared"]
    if "status" in kwargs:
        hub.log.debug(f"Got kwarg 'status' = {kwargs['status']}")
        body["status"] = kwargs.get("status")
        del kwargs["status"]
    if "updatedMillis" in kwargs:
        hub.log.debug(f"Got kwarg 'updatedMillis' = {kwargs['updatedMillis']}")
        body["updatedMillis"] = kwargs.get("updatedMillis")
        del kwargs["updatedMillis"]
    if "version" in kwargs:
        hub.log.debug(f"Got kwarg 'version' = {kwargs['version']}")
        body["version"] = kwargs.get("version")
        del kwargs["version"]

    ret = api.validate_using_put(body, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))

from idem_vra.client.vra_cmx_lib.api import VCenterEndpointsApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get_storage_classes_using_get(hub, ctx, p_endpointSelfLinkId, **kwargs):
    """Get all storage classes identifiers for a vCenter endpoint Get all storage classes defined in all managed supervisor clusters in a
      particular vCenter instance. vCenter instance is identified by endpoint
      SelfLink Id Performs GET /cmx/api/resources/vcenter/endpoints/{endpointSelfLinkId}/storage-classes


    :param string p_endpointSelfLinkId: (required in path) endpointSelfLinkId
    :param integer offset: (optional in query)
    :param integer pageNumber: (optional in query)
    :param integer pageSize: (optional in query)
    :param boolean paged: (optional in query)
    :param boolean sort.sorted: (optional in query)
    :param boolean sort.unsorted: (optional in query)
    :param boolean unpaged: (optional in query)
    """

    hub.log.debug(
        "GET /cmx/api/resources/vcenter/endpoints/{endpointSelfLinkId}/storage-classes"
    )

    api = VCenterEndpointsApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

    ret = api.get_storage_classes_using_get(
        endpoint_self_link_id=p_endpointSelfLinkId, **kwargs
    )

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))

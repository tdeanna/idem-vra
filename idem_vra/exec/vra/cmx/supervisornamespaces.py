from idem_vra.client.vra_cmx_lib.api import SupervisorNamespacesApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def create_using_post2(hub, ctx, **kwargs):
    """Create Supervisor Namespace Create Supervisor Namespace Performs POST /cmx/api/resources/supervisor-namespaces


    :param string cluster: (optional in body)
    :param string description: (optional in body)
    :param string endpointId: (optional in body)
    :param string name: (optional in body)
    :param string projectId: (optional in body)
    :param array storagePolicies: (optional in body)
    """

    hub.log.debug("POST /cmx/api/resources/supervisor-namespaces")

    api = SupervisorNamespacesApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

    body = {}

    if "cluster" in kwargs:
        hub.log.debug(f"Got kwarg 'cluster' = {kwargs['cluster']}")
        body["cluster"] = kwargs.get("cluster")
        del kwargs["cluster"]
    if "description" in kwargs:
        hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
        body["description"] = kwargs.get("description")
        del kwargs["description"]
    if "endpointId" in kwargs:
        hub.log.debug(f"Got kwarg 'endpointId' = {kwargs['endpointId']}")
        body["endpointId"] = kwargs.get("endpointId")
        del kwargs["endpointId"]
    if "name" in kwargs:
        hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
        body["name"] = kwargs.get("name")
        del kwargs["name"]
    if "projectId" in kwargs:
        hub.log.debug(f"Got kwarg 'projectId' = {kwargs['projectId']}")
        body["projectId"] = kwargs.get("projectId")
        del kwargs["projectId"]
    if "storagePolicies" in kwargs:
        hub.log.debug(f"Got kwarg 'storagePolicies' = {kwargs['storagePolicies']}")
        body["storagePolicies"] = kwargs.get("storagePolicies")
        del kwargs["storagePolicies"]

    ret = api.create_using_post2(body, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def delete_using_delete2(hub, ctx, p_selfLinkId, **kwargs):
    """Make not managed a Supervisor Namespace and optionally delete it Make not managed a Supervisor Namespace and delete it from cluster if destroy
      parameter is true Performs DELETE /cmx/api/resources/supervisor-namespaces/{selfLinkId}


    :param string p_selfLinkId: (required in path) selfLinkId
    :param boolean destroy: (optional in query) destroy
    """

    hub.log.debug("DELETE /cmx/api/resources/supervisor-namespaces/{selfLinkId}")

    api = SupervisorNamespacesApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

    ret = api.delete_using_delete2(self_link_id=p_selfLinkId, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_namespace_quotas_using_get(hub, ctx, p_selfLinkId, **kwargs):
    """Get Supervisor Namespace Quotas by the id from documentSelfLink Retrieve a Supervisor Namespace Quota by id from documentSelfLink Performs GET /cmx/api/resources/supervisor-namespaces/{selfLinkId}/resource-quotas


    :param string p_selfLinkId: (required in path) selfLinkId
    """

    hub.log.debug(
        "GET /cmx/api/resources/supervisor-namespaces/{selfLinkId}/resource-quotas"
    )

    api = SupervisorNamespacesApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

    ret = api.get_namespace_quotas_using_get(self_link_id=p_selfLinkId, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_namespace_using_get(hub, ctx, p_selfLinkId, **kwargs):
    """Find a Supervisor Namespace by the id from documentSelfLink Retrieve a Supervisor Namespace by id from documentSelfLink Performs GET /cmx/api/resources/supervisor-namespaces/{selfLinkId}


    :param string p_selfLinkId: (required in path) selfLinkId
    :param boolean expandStorage: (optional in query) expandStorage
    """

    hub.log.debug("GET /cmx/api/resources/supervisor-namespaces/{selfLinkId}")

    api = SupervisorNamespacesApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

    ret = api.get_namespace_using_get(self_link_id=p_selfLinkId, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def list_using_get5(hub, ctx, **kwargs):
    """Get all Supervisor Namespaces Get all Supervisor Namespaces Performs GET /cmx/api/resources/supervisor-namespaces


    :param string id: (optional in query)
    :param boolean localAddress.address.MCGlobal: (optional in query)
    :param boolean localAddress.address.MCLinkLocal: (optional in query)
    :param boolean localAddress.address.MCNodeLocal: (optional in query)
    :param boolean localAddress.address.MCOrgLocal: (optional in query)
    :param boolean localAddress.address.MCSiteLocal: (optional in query)
    :param string localAddress.address.address: (optional in query)
    :param boolean localAddress.address.anyLocalAddress: (optional in query)
    :param string localAddress.address.canonicalHostName: (optional in query)
    :param string localAddress.address.hostAddress: (optional in query)
    :param string localAddress.address.hostName: (optional in query)
    :param boolean localAddress.address.linkLocalAddress: (optional in query)
    :param boolean localAddress.address.loopbackAddress: (optional in query)
    :param boolean localAddress.address.multicastAddress: (optional in query)
    :param boolean localAddress.address.siteLocalAddress: (optional in query)
    :param string localAddress.hostName: (optional in query)
    :param string localAddress.hostString: (optional in query)
    :param integer localAddress.port: (optional in query)
    :param boolean localAddress.unresolved: (optional in query)
    :param integer offset: (optional in query)
    :param integer pageNumber: (optional in query)
    :param integer pageSize: (optional in query)
    :param boolean paged: (optional in query)
    :param boolean remoteAddress.address.MCGlobal: (optional in query)
    :param boolean remoteAddress.address.MCLinkLocal: (optional in query)
    :param boolean remoteAddress.address.MCNodeLocal: (optional in query)
    :param boolean remoteAddress.address.MCOrgLocal: (optional in query)
    :param boolean remoteAddress.address.MCSiteLocal: (optional in query)
    :param string remoteAddress.address.address: (optional in query)
    :param boolean remoteAddress.address.anyLocalAddress: (optional in query)
    :param string remoteAddress.address.canonicalHostName: (optional in query)
    :param string remoteAddress.address.hostAddress: (optional in query)
    :param string remoteAddress.address.hostName: (optional in query)
    :param boolean remoteAddress.address.linkLocalAddress: (optional in query)
    :param boolean remoteAddress.address.loopbackAddress: (optional in query)
    :param boolean remoteAddress.address.multicastAddress: (optional in query)
    :param boolean remoteAddress.address.siteLocalAddress: (optional in query)
    :param string remoteAddress.hostName: (optional in query)
    :param string remoteAddress.hostString: (optional in query)
    :param integer remoteAddress.port: (optional in query)
    :param boolean remoteAddress.unresolved: (optional in query)
    :param boolean sort.sorted: (optional in query)
    :param boolean sort.unsorted: (optional in query)
    :param string sslInfo.peerCertificates[0].TBSCertificate: (optional in query)
    :param integer sslInfo.peerCertificates[0].basicConstraints: (optional in query)
    :param string sslInfo.peerCertificates[0].encoded: (optional in query)
    :param array sslInfo.peerCertificates[0].extendedKeyUsage: (optional in query)
    :param string sslInfo.peerCertificates[0].issuerDN.name: (optional in query)
    :param array sslInfo.peerCertificates[0].issuerUniqueID: (optional in query)
    :param string sslInfo.peerCertificates[0].issuerX500Principal.encoded: (optional in query)
    :param string sslInfo.peerCertificates[0].issuerX500Principal.name: (optional in query)
    :param array sslInfo.peerCertificates[0].keyUsage: (optional in query)
    :param string sslInfo.peerCertificates[0].notAfter: (optional in query)
    :param string sslInfo.peerCertificates[0].notBefore: (optional in query)
    :param integer sslInfo.peerCertificates[0].serialNumber: (optional in query)
    :param string sslInfo.peerCertificates[0].sigAlgName: (optional in query)
    :param string sslInfo.peerCertificates[0].sigAlgOID: (optional in query)
    :param string sslInfo.peerCertificates[0].sigAlgParams: (optional in query)
    :param string sslInfo.peerCertificates[0].signature: (optional in query)
    :param string sslInfo.peerCertificates[0].subjectDN.name: (optional in query)
    :param array sslInfo.peerCertificates[0].subjectUniqueID: (optional in query)
    :param string sslInfo.peerCertificates[0].subjectX500Principal.encoded: (optional in query)
    :param string sslInfo.peerCertificates[0].subjectX500Principal.name: (optional in query)
    :param string sslInfo.peerCertificates[0].type: (optional in query)
    :param integer sslInfo.peerCertificates[0].version: (optional in query)
    :param string sslInfo.sessionId: (optional in query)
    :param boolean unpaged: (optional in query)
    :param string clusterSelfLinkId: (optional in query) clusterSelfLinkId
    :param string registered: (optional in query) registered
    """

    hub.log.debug("GET /cmx/api/resources/supervisor-namespaces")

    api = SupervisorNamespacesApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

    ret = api.list_using_get5(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def register_using_put2(hub, ctx, p_namespaceSelfLinkId, **kwargs):
    """Make a Supervisor Namespace a managed entity The body shall contain valid projectId, DocumentSelfLink and list of viewer and
      editor user and groups. Performs PUT /cmx/api/resources/supervisor-namespaces/{namespaceSelfLinkId}/register


    :param string p_namespaceSelfLinkId: (required in path) namespaceSelfLinkId
    :param string address: (optional in body)
    :param string cluster: (optional in body)
    :param integer cpuUsed: (optional in body)
    :param integer createdMillis: (optional in body)
    :param object customProperties: (optional in body)
    :param string description: (optional in body)
    :param string documentSelfLink: (optional in body)
    :param string editGroups: (optional in body)
    :param string editUsers: (optional in body)
    :param string endpointLink: (optional in body)
    :param string externalLink: (optional in body)
    :param string id: (optional in body)
    :param string installerId: (optional in body)
    :param integer memoryUsed: (optional in body)
    :param string name: (optional in body)
    :param string owner: (optional in body)
    :param string ownerGroups: (optional in body)
    :param string ownerUsers: (optional in body)
    :param string projectId: (optional in body)
    :param boolean registered: (optional in body)
    :param array resourceQuotas: (optional in body)
    :param string selfLinkId: (optional in body)
    :param string status: (optional in body)
    :param string statusMessage: (optional in body)
    :param array storagePolicies: (optional in body)
    :param integer storageUsed: (optional in body)
    :param string type: (optional in body)
    :param integer updatedMillis: (optional in body)
    :param string viewGroups: (optional in body)
    :param string viewUsers: (optional in body)
    """

    hub.log.debug(
        "PUT /cmx/api/resources/supervisor-namespaces/{namespaceSelfLinkId}/register"
    )

    api = SupervisorNamespacesApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

    body = {}

    if "address" in kwargs:
        hub.log.debug(f"Got kwarg 'address' = {kwargs['address']}")
        body["address"] = kwargs.get("address")
        del kwargs["address"]
    if "cluster" in kwargs:
        hub.log.debug(f"Got kwarg 'cluster' = {kwargs['cluster']}")
        body["cluster"] = kwargs.get("cluster")
        del kwargs["cluster"]
    if "cpuUsed" in kwargs:
        hub.log.debug(f"Got kwarg 'cpuUsed' = {kwargs['cpuUsed']}")
        body["cpuUsed"] = kwargs.get("cpuUsed")
        del kwargs["cpuUsed"]
    if "createdMillis" in kwargs:
        hub.log.debug(f"Got kwarg 'createdMillis' = {kwargs['createdMillis']}")
        body["createdMillis"] = kwargs.get("createdMillis")
        del kwargs["createdMillis"]
    if "customProperties" in kwargs:
        hub.log.debug(f"Got kwarg 'customProperties' = {kwargs['customProperties']}")
        body["customProperties"] = kwargs.get("customProperties")
        del kwargs["customProperties"]
    if "description" in kwargs:
        hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
        body["description"] = kwargs.get("description")
        del kwargs["description"]
    if "documentSelfLink" in kwargs:
        hub.log.debug(f"Got kwarg 'documentSelfLink' = {kwargs['documentSelfLink']}")
        body["documentSelfLink"] = kwargs.get("documentSelfLink")
        del kwargs["documentSelfLink"]
    if "editGroups" in kwargs:
        hub.log.debug(f"Got kwarg 'editGroups' = {kwargs['editGroups']}")
        body["editGroups"] = kwargs.get("editGroups")
        del kwargs["editGroups"]
    if "editUsers" in kwargs:
        hub.log.debug(f"Got kwarg 'editUsers' = {kwargs['editUsers']}")
        body["editUsers"] = kwargs.get("editUsers")
        del kwargs["editUsers"]
    if "endpointLink" in kwargs:
        hub.log.debug(f"Got kwarg 'endpointLink' = {kwargs['endpointLink']}")
        body["endpointLink"] = kwargs.get("endpointLink")
        del kwargs["endpointLink"]
    if "externalLink" in kwargs:
        hub.log.debug(f"Got kwarg 'externalLink' = {kwargs['externalLink']}")
        body["externalLink"] = kwargs.get("externalLink")
        del kwargs["externalLink"]
    if "id" in kwargs:
        hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
        body["id"] = kwargs.get("id")
        del kwargs["id"]
    if "installerId" in kwargs:
        hub.log.debug(f"Got kwarg 'installerId' = {kwargs['installerId']}")
        body["installerId"] = kwargs.get("installerId")
        del kwargs["installerId"]
    if "memoryUsed" in kwargs:
        hub.log.debug(f"Got kwarg 'memoryUsed' = {kwargs['memoryUsed']}")
        body["memoryUsed"] = kwargs.get("memoryUsed")
        del kwargs["memoryUsed"]
    if "name" in kwargs:
        hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
        body["name"] = kwargs.get("name")
        del kwargs["name"]
    if "owner" in kwargs:
        hub.log.debug(f"Got kwarg 'owner' = {kwargs['owner']}")
        body["owner"] = kwargs.get("owner")
        del kwargs["owner"]
    if "ownerGroups" in kwargs:
        hub.log.debug(f"Got kwarg 'ownerGroups' = {kwargs['ownerGroups']}")
        body["ownerGroups"] = kwargs.get("ownerGroups")
        del kwargs["ownerGroups"]
    if "ownerUsers" in kwargs:
        hub.log.debug(f"Got kwarg 'ownerUsers' = {kwargs['ownerUsers']}")
        body["ownerUsers"] = kwargs.get("ownerUsers")
        del kwargs["ownerUsers"]
    if "projectId" in kwargs:
        hub.log.debug(f"Got kwarg 'projectId' = {kwargs['projectId']}")
        body["projectId"] = kwargs.get("projectId")
        del kwargs["projectId"]
    if "registered" in kwargs:
        hub.log.debug(f"Got kwarg 'registered' = {kwargs['registered']}")
        body["registered"] = kwargs.get("registered")
        del kwargs["registered"]
    if "resourceQuotas" in kwargs:
        hub.log.debug(f"Got kwarg 'resourceQuotas' = {kwargs['resourceQuotas']}")
        body["resourceQuotas"] = kwargs.get("resourceQuotas")
        del kwargs["resourceQuotas"]
    if "selfLinkId" in kwargs:
        hub.log.debug(f"Got kwarg 'selfLinkId' = {kwargs['selfLinkId']}")
        body["selfLinkId"] = kwargs.get("selfLinkId")
        del kwargs["selfLinkId"]
    if "status" in kwargs:
        hub.log.debug(f"Got kwarg 'status' = {kwargs['status']}")
        body["status"] = kwargs.get("status")
        del kwargs["status"]
    if "statusMessage" in kwargs:
        hub.log.debug(f"Got kwarg 'statusMessage' = {kwargs['statusMessage']}")
        body["statusMessage"] = kwargs.get("statusMessage")
        del kwargs["statusMessage"]
    if "storagePolicies" in kwargs:
        hub.log.debug(f"Got kwarg 'storagePolicies' = {kwargs['storagePolicies']}")
        body["storagePolicies"] = kwargs.get("storagePolicies")
        del kwargs["storagePolicies"]
    if "storageUsed" in kwargs:
        hub.log.debug(f"Got kwarg 'storageUsed' = {kwargs['storageUsed']}")
        body["storageUsed"] = kwargs.get("storageUsed")
        del kwargs["storageUsed"]
    if "type" in kwargs:
        hub.log.debug(f"Got kwarg 'type' = {kwargs['type']}")
        body["type"] = kwargs.get("type")
        del kwargs["type"]
    if "updatedMillis" in kwargs:
        hub.log.debug(f"Got kwarg 'updatedMillis' = {kwargs['updatedMillis']}")
        body["updatedMillis"] = kwargs.get("updatedMillis")
        del kwargs["updatedMillis"]
    if "viewGroups" in kwargs:
        hub.log.debug(f"Got kwarg 'viewGroups' = {kwargs['viewGroups']}")
        body["viewGroups"] = kwargs.get("viewGroups")
        del kwargs["viewGroups"]
    if "viewUsers" in kwargs:
        hub.log.debug(f"Got kwarg 'viewUsers' = {kwargs['viewUsers']}")
        body["viewUsers"] = kwargs.get("viewUsers")
        del kwargs["viewUsers"]

    ret = api.register_using_put2(
        body, namespace_self_link_id=p_namespaceSelfLinkId, **kwargs
    )

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def set_namespace_quotas_using_put(hub, ctx, p_selfLinkId, **kwargs):
    """Set Supervisor Namespace Quotas by the id from documentSelfLink Set a Supervisor Namespace Quota by id from documentSelfLink Performs PUT /cmx/api/resources/supervisor-namespaces/{selfLinkId}/resource-quotas


    :param string p_selfLinkId: (required in path) selfLinkId
    """

    hub.log.debug(
        "PUT /cmx/api/resources/supervisor-namespaces/{selfLinkId}/resource-quotas"
    )

    api = SupervisorNamespacesApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

    ret = api.set_namespace_quotas_using_put(self_link_id=p_selfLinkId, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def sync_status_using_get(hub, ctx, p_namespaceSelfLinkId, p_requestId, **kwargs):
    """Retrieve sync status Retrieve sync status. Performs GET /cmx/api/resources/supervisor-namespaces/{namespaceSelfLinkId}/principals/{requestId}


    :param string p_namespaceSelfLinkId: (required in path) namespaceSelfLinkId
    :param string p_requestId: (required in path) requestId
    """

    hub.log.debug(
        "GET /cmx/api/resources/supervisor-namespaces/{namespaceSelfLinkId}/principals/{requestId}"
    )

    api = SupervisorNamespacesApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

    ret = api.sync_status_using_get(
        namespace_self_link_id=p_namespaceSelfLinkId, request_id=p_requestId, **kwargs
    )

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def sync_using_post(hub, ctx, p_namespaceSelfLinkId, **kwargs):
    """Sync Supervisor Namespace principals access list The body shall contain list of viewer and editor user and groups. Performs POST /cmx/api/resources/supervisor-namespaces/{namespaceSelfLinkId}/principals


    :param string p_namespaceSelfLinkId: (required in path) namespaceSelfLinkId
    :param string editGroups: (optional in body)
    :param string editUsers: (optional in body)
    :param string ownerGroups: (optional in body)
    :param string ownerUsers: (optional in body)
    :param string viewGroups: (optional in body)
    :param string viewUsers: (optional in body)
    """

    hub.log.debug(
        "POST /cmx/api/resources/supervisor-namespaces/{namespaceSelfLinkId}/principals"
    )

    api = SupervisorNamespacesApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

    body = {}

    if "editGroups" in kwargs:
        hub.log.debug(f"Got kwarg 'editGroups' = {kwargs['editGroups']}")
        body["editGroups"] = kwargs.get("editGroups")
        del kwargs["editGroups"]
    if "editUsers" in kwargs:
        hub.log.debug(f"Got kwarg 'editUsers' = {kwargs['editUsers']}")
        body["editUsers"] = kwargs.get("editUsers")
        del kwargs["editUsers"]
    if "ownerGroups" in kwargs:
        hub.log.debug(f"Got kwarg 'ownerGroups' = {kwargs['ownerGroups']}")
        body["ownerGroups"] = kwargs.get("ownerGroups")
        del kwargs["ownerGroups"]
    if "ownerUsers" in kwargs:
        hub.log.debug(f"Got kwarg 'ownerUsers' = {kwargs['ownerUsers']}")
        body["ownerUsers"] = kwargs.get("ownerUsers")
        del kwargs["ownerUsers"]
    if "viewGroups" in kwargs:
        hub.log.debug(f"Got kwarg 'viewGroups' = {kwargs['viewGroups']}")
        body["viewGroups"] = kwargs.get("viewGroups")
        del kwargs["viewGroups"]
    if "viewUsers" in kwargs:
        hub.log.debug(f"Got kwarg 'viewUsers' = {kwargs['viewUsers']}")
        body["viewUsers"] = kwargs.get("viewUsers")
        del kwargs["viewUsers"]

    ret = api.sync_using_post(
        body, namespace_self_link_id=p_namespaceSelfLinkId, **kwargs
    )

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def update_using_patch(hub, ctx, p_selfLinkId, **kwargs):
    """Update Supervisor Namespace Update Supervisor Namespace Performs PATCH /cmx/api/resources/supervisor-namespaces/{selfLinkId}


    :param string p_selfLinkId: (required in path) selfLinkId
    :param string description: (optional in body)
    :param array resourceQuotas: (optional in body)
    :param array storagePolicies: (optional in body)
    """

    hub.log.debug("PATCH /cmx/api/resources/supervisor-namespaces/{selfLinkId}")

    api = SupervisorNamespacesApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

    body = {}

    if "description" in kwargs:
        hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
        body["description"] = kwargs.get("description")
        del kwargs["description"]
    if "resourceQuotas" in kwargs:
        hub.log.debug(f"Got kwarg 'resourceQuotas' = {kwargs['resourceQuotas']}")
        body["resourceQuotas"] = kwargs.get("resourceQuotas")
        del kwargs["resourceQuotas"]
    if "storagePolicies" in kwargs:
        hub.log.debug(f"Got kwarg 'storagePolicies' = {kwargs['storagePolicies']}")
        body["storagePolicies"] = kwargs.get("storagePolicies")
        del kwargs["storagePolicies"]

    ret = api.update_using_patch(body, self_link_id=p_selfLinkId, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))

from idem_vra.client.vra_cmx_lib.api import ClusterPlansApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def create_using_post(hub, ctx, **kwargs):
    """Create a cluster plan The body shall contain a cluster plan entity. Performs POST /cmx/api/resources/cluster-plans


    :param string cloudAccountSelfLinkId: (optional in body)
    :param integer createdMillis: (optional in body)
    :param object definition: (optional in body) The definition varies depending on the type of cluster plan. Example
      shown below is for cluster plans of type TANZU_CLUSTER_PLAN. In that
      case the definition is equivalent to the spec of a Tanzu Kubernetes
      cluster in JSON format. Here is a documentation
      https://docs.vmware.com/en/VMware-vSphere/7.0/vmware-vsphere-with-
      tanzu/GUID-B1034373-8C38-4FE2-9517-345BF7271A1E.html
      example: {"spec":{"distribution":{"version":"1.20"},"topology":{"contr
      olPlane":{"count":1,"class":"best-effort-xsmall","storageClass":"vsan-
      default-storage-policy"},"workers":{"count":1,"class":"best-effort-
      xsmall","storageClass":"vsan-default-storage-
      policy"}},"settings":{"storage":{"defaultClass":"","classes":[]}}}}
    :param string description: (optional in body)
    :param string id: (optional in body)
    :param string name: (optional in body)
    :param string orgId: (optional in body)
    :param string type: (optional in body)
    :param integer updatedMillis: (optional in body)
    """

    hub.log.debug("POST /cmx/api/resources/cluster-plans")

    api = ClusterPlansApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

    body = {}

    if "cloudAccountSelfLinkId" in kwargs:
        hub.log.debug(
            f"Got kwarg 'cloudAccountSelfLinkId' = {kwargs['cloudAccountSelfLinkId']}"
        )
        body["cloudAccountSelfLinkId"] = kwargs.get("cloudAccountSelfLinkId")
        del kwargs["cloudAccountSelfLinkId"]
    if "createdMillis" in kwargs:
        hub.log.debug(f"Got kwarg 'createdMillis' = {kwargs['createdMillis']}")
        body["createdMillis"] = kwargs.get("createdMillis")
        del kwargs["createdMillis"]
    if "definition" in kwargs:
        hub.log.debug(f"Got kwarg 'definition' = {kwargs['definition']}")
        body["definition"] = kwargs.get("definition")
        del kwargs["definition"]
    if "description" in kwargs:
        hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
        body["description"] = kwargs.get("description")
        del kwargs["description"]
    if "id" in kwargs:
        hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
        body["id"] = kwargs.get("id")
        del kwargs["id"]
    if "name" in kwargs:
        hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
        body["name"] = kwargs.get("name")
        del kwargs["name"]
    if "orgId" in kwargs:
        hub.log.debug(f"Got kwarg 'orgId' = {kwargs['orgId']}")
        body["orgId"] = kwargs.get("orgId")
        del kwargs["orgId"]
    if "type" in kwargs:
        hub.log.debug(f"Got kwarg 'type' = {kwargs['type']}")
        body["type"] = kwargs.get("type")
        del kwargs["type"]
    if "updatedMillis" in kwargs:
        hub.log.debug(f"Got kwarg 'updatedMillis' = {kwargs['updatedMillis']}")
        body["updatedMillis"] = kwargs.get("updatedMillis")
        del kwargs["updatedMillis"]

    ret = api.create_using_post(body, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def delete_using_delete(hub, ctx, p_id, **kwargs):
    """Delete a Cluster Plan by Id Delete a Cluster Plan identified by id string Performs DELETE /cmx/api/resources/cluster-plans/{id}


    :param string p_id: (required in path) id
    """

    hub.log.debug("DELETE /cmx/api/resources/cluster-plans/{id}")

    api = ClusterPlansApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

    ret = api.delete_using_delete(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_using_get(hub, ctx, p_id, **kwargs):
    """Get a Cluster Plan by Id Get a Cluster Plan by Id Performs GET /cmx/api/resources/cluster-plans/{id}


    :param string p_id: (required in path) id
    """

    hub.log.debug("GET /cmx/api/resources/cluster-plans/{id}")

    api = ClusterPlansApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

    ret = api.get_using_get(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def search_cluster_plan_aggregations_using_get(hub, ctx, q_groupBy, **kwargs):
    """Search for cluster plan aggregations Search for cluster plan aggregations by name and cloud account document self
      link id. Performs GET /cmx/api/resources/cluster-plans/aggregation


    :param string q_groupBy: (required in query) groupBy
    :param integer offset: (optional in query)
    :param integer pageNumber: (optional in query)
    :param integer pageSize: (optional in query)
    :param boolean paged: (optional in query)
    :param boolean sort.sorted: (optional in query)
    :param boolean sort.unsorted: (optional in query)
    :param boolean unpaged: (optional in query)
    :param string cloudAccountSelfLinkId: (optional in query) cloudAccountSelfLinkId
    :param string name: (optional in query) name
    """

    hub.log.debug("GET /cmx/api/resources/cluster-plans/aggregation")

    api = ClusterPlansApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

    ret = api.search_cluster_plan_aggregations_using_get(group_by=q_groupBy, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def search_cluster_plans_using_get(hub, ctx, **kwargs):
    """Search a cluster plan instance Search a cluster plan by name and cloud account document self link id. Performs GET /cmx/api/resources/cluster-plans


    :param integer offset: (optional in query)
    :param integer pageNumber: (optional in query)
    :param integer pageSize: (optional in query)
    :param boolean paged: (optional in query)
    :param boolean sort.sorted: (optional in query)
    :param boolean sort.unsorted: (optional in query)
    :param boolean unpaged: (optional in query)
    :param string cloudAccountSelfLinkId: (optional in query) cloudAccountSelfLinkId
    :param string name: (optional in query) name
    """

    hub.log.debug("GET /cmx/api/resources/cluster-plans")

    api = ClusterPlansApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

    ret = api.search_cluster_plans_using_get(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def update_using_put(hub, ctx, p_id, **kwargs):
    """Update a cluster plan The body shall contains the cluster plan mutation entity. Performs PUT /cmx/api/resources/cluster-plans/{id}


    :param string p_id: (required in path) id
    :param string cloudAccountSelfLinkId: (optional in body)
    :param integer createdMillis: (optional in body)
    :param object definition: (optional in body) The definition varies depending on the type of cluster plan. Example
      shown below is for cluster plans of type TANZU_CLUSTER_PLAN. In that
      case the definition is equivalent to the spec of a Tanzu Kubernetes
      cluster in JSON format. Here is a documentation
      https://docs.vmware.com/en/VMware-vSphere/7.0/vmware-vsphere-with-
      tanzu/GUID-B1034373-8C38-4FE2-9517-345BF7271A1E.html
      example: {"spec":{"distribution":{"version":"1.20"},"topology":{"contr
      olPlane":{"count":1,"class":"best-effort-xsmall","storageClass":"vsan-
      default-storage-policy"},"workers":{"count":1,"class":"best-effort-
      xsmall","storageClass":"vsan-default-storage-
      policy"}},"settings":{"storage":{"defaultClass":"","classes":[]}}}}
    :param string description: (optional in body)
    :param string id: (optional in body)
    :param string name: (optional in body)
    :param string orgId: (optional in body)
    :param string type: (optional in body)
    :param integer updatedMillis: (optional in body)
    """

    hub.log.debug("PUT /cmx/api/resources/cluster-plans/{id}")

    api = ClusterPlansApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

    body = {}

    if "cloudAccountSelfLinkId" in kwargs:
        hub.log.debug(
            f"Got kwarg 'cloudAccountSelfLinkId' = {kwargs['cloudAccountSelfLinkId']}"
        )
        body["cloudAccountSelfLinkId"] = kwargs.get("cloudAccountSelfLinkId")
        del kwargs["cloudAccountSelfLinkId"]
    if "createdMillis" in kwargs:
        hub.log.debug(f"Got kwarg 'createdMillis' = {kwargs['createdMillis']}")
        body["createdMillis"] = kwargs.get("createdMillis")
        del kwargs["createdMillis"]
    if "definition" in kwargs:
        hub.log.debug(f"Got kwarg 'definition' = {kwargs['definition']}")
        body["definition"] = kwargs.get("definition")
        del kwargs["definition"]
    if "description" in kwargs:
        hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
        body["description"] = kwargs.get("description")
        del kwargs["description"]
    if "id" in kwargs:
        hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
        body["id"] = kwargs.get("id")
        del kwargs["id"]
    if "name" in kwargs:
        hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
        body["name"] = kwargs.get("name")
        del kwargs["name"]
    if "orgId" in kwargs:
        hub.log.debug(f"Got kwarg 'orgId' = {kwargs['orgId']}")
        body["orgId"] = kwargs.get("orgId")
        del kwargs["orgId"]
    if "type" in kwargs:
        hub.log.debug(f"Got kwarg 'type' = {kwargs['type']}")
        body["type"] = kwargs.get("type")
        del kwargs["type"]
    if "updatedMillis" in kwargs:
        hub.log.debug(f"Got kwarg 'updatedMillis' = {kwargs['updatedMillis']}")
        body["updatedMillis"] = kwargs.get("updatedMillis")
        del kwargs["updatedMillis"]

    ret = api.update_using_put(body, id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))

from idem_vra.client.vra_cmx_lib.api import NamespacesApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def create_using_post1(hub, ctx, **kwargs):
    """Create a K8S Namespace Create a K8S Namespace Performs POST /cmx/api/resources/k8s/namespaces


    :param string authCredentialsLink: (optional in body)
    :param string clusterId: (optional in body)
    :param integer createdMillis: (optional in body)
    :param object customProperties: (optional in body)
    :param string description: (optional in body)
    :param string id: (optional in body)
    :param string installerId: (optional in body)
    :param string name: (optional in body)
    :param string orgId: (optional in body)
    :param string owner: (optional in body)
    :param string projectId: (optional in body)
    :param boolean registered: (optional in body)
    :param boolean shared: (optional in body)
    :param string status: (optional in body)
    :param integer updatedMillis: (optional in body)
    :param string zoneProjectAssignmentId: (optional in body)
    """

    hub.log.debug("POST /cmx/api/resources/k8s/namespaces")

    api = NamespacesApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

    body = {}

    if "authCredentialsLink" in kwargs:
        hub.log.debug(
            f"Got kwarg 'authCredentialsLink' = {kwargs['authCredentialsLink']}"
        )
        body["authCredentialsLink"] = kwargs.get("authCredentialsLink")
        del kwargs["authCredentialsLink"]
    if "clusterId" in kwargs:
        hub.log.debug(f"Got kwarg 'clusterId' = {kwargs['clusterId']}")
        body["clusterId"] = kwargs.get("clusterId")
        del kwargs["clusterId"]
    if "createdMillis" in kwargs:
        hub.log.debug(f"Got kwarg 'createdMillis' = {kwargs['createdMillis']}")
        body["createdMillis"] = kwargs.get("createdMillis")
        del kwargs["createdMillis"]
    if "customProperties" in kwargs:
        hub.log.debug(f"Got kwarg 'customProperties' = {kwargs['customProperties']}")
        body["customProperties"] = kwargs.get("customProperties")
        del kwargs["customProperties"]
    if "description" in kwargs:
        hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
        body["description"] = kwargs.get("description")
        del kwargs["description"]
    if "id" in kwargs:
        hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
        body["id"] = kwargs.get("id")
        del kwargs["id"]
    if "installerId" in kwargs:
        hub.log.debug(f"Got kwarg 'installerId' = {kwargs['installerId']}")
        body["installerId"] = kwargs.get("installerId")
        del kwargs["installerId"]
    if "name" in kwargs:
        hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
        body["name"] = kwargs.get("name")
        del kwargs["name"]
    if "orgId" in kwargs:
        hub.log.debug(f"Got kwarg 'orgId' = {kwargs['orgId']}")
        body["orgId"] = kwargs.get("orgId")
        del kwargs["orgId"]
    if "owner" in kwargs:
        hub.log.debug(f"Got kwarg 'owner' = {kwargs['owner']}")
        body["owner"] = kwargs.get("owner")
        del kwargs["owner"]
    if "projectId" in kwargs:
        hub.log.debug(f"Got kwarg 'projectId' = {kwargs['projectId']}")
        body["projectId"] = kwargs.get("projectId")
        del kwargs["projectId"]
    if "registered" in kwargs:
        hub.log.debug(f"Got kwarg 'registered' = {kwargs['registered']}")
        body["registered"] = kwargs.get("registered")
        del kwargs["registered"]
    if "shared" in kwargs:
        hub.log.debug(f"Got kwarg 'shared' = {kwargs['shared']}")
        body["shared"] = kwargs.get("shared")
        del kwargs["shared"]
    if "status" in kwargs:
        hub.log.debug(f"Got kwarg 'status' = {kwargs['status']}")
        body["status"] = kwargs.get("status")
        del kwargs["status"]
    if "updatedMillis" in kwargs:
        hub.log.debug(f"Got kwarg 'updatedMillis' = {kwargs['updatedMillis']}")
        body["updatedMillis"] = kwargs.get("updatedMillis")
        del kwargs["updatedMillis"]
    if "zoneProjectAssignmentId" in kwargs:
        hub.log.debug(
            f"Got kwarg 'zoneProjectAssignmentId' = {kwargs['zoneProjectAssignmentId']}"
        )
        body["zoneProjectAssignmentId"] = kwargs.get("zoneProjectAssignmentId")
        del kwargs["zoneProjectAssignmentId"]

    ret = api.create_using_post1(body, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def delete_using_delete1(hub, ctx, p_id, **kwargs):
    """Delete a K8S Namespace Delete a K8S Namespace by id Performs DELETE /cmx/api/resources/k8s/namespaces/{id}


    :param string p_id: (required in path) id
    :param boolean destroy: (optional in query) destroy
    """

    hub.log.debug("DELETE /cmx/api/resources/k8s/namespaces/{id}")

    api = NamespacesApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

    ret = api.delete_using_delete1(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_kube_config_using_get1(hub, ctx, p_id, **kwargs):
    """Get KubeConfig for a K8S Namespace Get KubeConfig for a K8S Namespace by providing a K8S Namespace id Performs GET /cmx/api/resources/k8s/namespaces/{id}/kube-config


    :param string p_id: (required in path) id
    :param boolean ignoreTMC: (optional in query) ignoreTMC
    """

    hub.log.debug("GET /cmx/api/resources/k8s/namespaces/{id}/kube-config")

    api = NamespacesApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

    ret = api.get_kube_config_using_get1(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_using_get2(hub, ctx, p_id, **kwargs):
    """Get a K8S Namespace by id Get a K8S Namespace by id Performs GET /cmx/api/resources/k8s/namespaces/{id}


    :param string p_id: (required in path) id
    """

    hub.log.debug("GET /cmx/api/resources/k8s/namespaces/{id}")

    api = NamespacesApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

    ret = api.get_using_get2(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def list_using_get2(hub, ctx, **kwargs):
    """Get All K8S Namespaces Get a list of all K8S Namespaces Performs GET /cmx/api/resources/k8s/namespaces


    :param string id: (optional in query)
    :param boolean localAddress.address.MCGlobal: (optional in query)
    :param boolean localAddress.address.MCLinkLocal: (optional in query)
    :param boolean localAddress.address.MCNodeLocal: (optional in query)
    :param boolean localAddress.address.MCOrgLocal: (optional in query)
    :param boolean localAddress.address.MCSiteLocal: (optional in query)
    :param string localAddress.address.address: (optional in query)
    :param boolean localAddress.address.anyLocalAddress: (optional in query)
    :param string localAddress.address.canonicalHostName: (optional in query)
    :param string localAddress.address.hostAddress: (optional in query)
    :param string localAddress.address.hostName: (optional in query)
    :param boolean localAddress.address.linkLocalAddress: (optional in query)
    :param boolean localAddress.address.loopbackAddress: (optional in query)
    :param boolean localAddress.address.multicastAddress: (optional in query)
    :param boolean localAddress.address.siteLocalAddress: (optional in query)
    :param string localAddress.hostName: (optional in query)
    :param string localAddress.hostString: (optional in query)
    :param integer localAddress.port: (optional in query)
    :param boolean localAddress.unresolved: (optional in query)
    :param integer offset: (optional in query)
    :param integer pageNumber: (optional in query)
    :param integer pageSize: (optional in query)
    :param boolean paged: (optional in query)
    :param boolean remoteAddress.address.MCGlobal: (optional in query)
    :param boolean remoteAddress.address.MCLinkLocal: (optional in query)
    :param boolean remoteAddress.address.MCNodeLocal: (optional in query)
    :param boolean remoteAddress.address.MCOrgLocal: (optional in query)
    :param boolean remoteAddress.address.MCSiteLocal: (optional in query)
    :param string remoteAddress.address.address: (optional in query)
    :param boolean remoteAddress.address.anyLocalAddress: (optional in query)
    :param string remoteAddress.address.canonicalHostName: (optional in query)
    :param string remoteAddress.address.hostAddress: (optional in query)
    :param string remoteAddress.address.hostName: (optional in query)
    :param boolean remoteAddress.address.linkLocalAddress: (optional in query)
    :param boolean remoteAddress.address.loopbackAddress: (optional in query)
    :param boolean remoteAddress.address.multicastAddress: (optional in query)
    :param boolean remoteAddress.address.siteLocalAddress: (optional in query)
    :param string remoteAddress.hostName: (optional in query)
    :param string remoteAddress.hostString: (optional in query)
    :param integer remoteAddress.port: (optional in query)
    :param boolean remoteAddress.unresolved: (optional in query)
    :param boolean sort.sorted: (optional in query)
    :param boolean sort.unsorted: (optional in query)
    :param string sslInfo.peerCertificates[0].TBSCertificate: (optional in query)
    :param integer sslInfo.peerCertificates[0].basicConstraints: (optional in query)
    :param string sslInfo.peerCertificates[0].encoded: (optional in query)
    :param array sslInfo.peerCertificates[0].extendedKeyUsage: (optional in query)
    :param string sslInfo.peerCertificates[0].issuerDN.name: (optional in query)
    :param array sslInfo.peerCertificates[0].issuerUniqueID: (optional in query)
    :param string sslInfo.peerCertificates[0].issuerX500Principal.encoded: (optional in query)
    :param string sslInfo.peerCertificates[0].issuerX500Principal.name: (optional in query)
    :param array sslInfo.peerCertificates[0].keyUsage: (optional in query)
    :param string sslInfo.peerCertificates[0].notAfter: (optional in query)
    :param string sslInfo.peerCertificates[0].notBefore: (optional in query)
    :param integer sslInfo.peerCertificates[0].serialNumber: (optional in query)
    :param string sslInfo.peerCertificates[0].sigAlgName: (optional in query)
    :param string sslInfo.peerCertificates[0].sigAlgOID: (optional in query)
    :param string sslInfo.peerCertificates[0].sigAlgParams: (optional in query)
    :param string sslInfo.peerCertificates[0].signature: (optional in query)
    :param string sslInfo.peerCertificates[0].subjectDN.name: (optional in query)
    :param array sslInfo.peerCertificates[0].subjectUniqueID: (optional in query)
    :param string sslInfo.peerCertificates[0].subjectX500Principal.encoded: (optional in query)
    :param string sslInfo.peerCertificates[0].subjectX500Principal.name: (optional in query)
    :param string sslInfo.peerCertificates[0].type: (optional in query)
    :param integer sslInfo.peerCertificates[0].version: (optional in query)
    :param string sslInfo.sessionId: (optional in query)
    :param boolean unpaged: (optional in query)
    """

    hub.log.debug("GET /cmx/api/resources/k8s/namespaces")

    api = NamespacesApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

    ret = api.list_using_get2(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def register_using_put(hub, ctx, p_id, **kwargs):
    """Onboard existing K8S Namespace Onboard existing K8S Namespace by providing namespace id and entity Performs PUT /cmx/api/resources/k8s/namespaces/{id}/register


    :param string p_id: (required in path) id
    :param string authCredentialsLink: (optional in body)
    :param string clusterId: (optional in body)
    :param integer createdMillis: (optional in body)
    :param object customProperties: (optional in body)
    :param string description: (optional in body)
    :param string id: (optional in body)
    :param string installerId: (optional in body)
    :param string name: (optional in body)
    :param string orgId: (optional in body)
    :param string owner: (optional in body)
    :param string projectId: (optional in body)
    :param boolean registered: (optional in body)
    :param boolean shared: (optional in body)
    :param string status: (optional in body)
    :param integer updatedMillis: (optional in body)
    :param string zoneProjectAssignmentId: (optional in body)
    """

    hub.log.debug("PUT /cmx/api/resources/k8s/namespaces/{id}/register")

    api = NamespacesApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

    body = {}

    if "authCredentialsLink" in kwargs:
        hub.log.debug(
            f"Got kwarg 'authCredentialsLink' = {kwargs['authCredentialsLink']}"
        )
        body["authCredentialsLink"] = kwargs.get("authCredentialsLink")
        del kwargs["authCredentialsLink"]
    if "clusterId" in kwargs:
        hub.log.debug(f"Got kwarg 'clusterId' = {kwargs['clusterId']}")
        body["clusterId"] = kwargs.get("clusterId")
        del kwargs["clusterId"]
    if "createdMillis" in kwargs:
        hub.log.debug(f"Got kwarg 'createdMillis' = {kwargs['createdMillis']}")
        body["createdMillis"] = kwargs.get("createdMillis")
        del kwargs["createdMillis"]
    if "customProperties" in kwargs:
        hub.log.debug(f"Got kwarg 'customProperties' = {kwargs['customProperties']}")
        body["customProperties"] = kwargs.get("customProperties")
        del kwargs["customProperties"]
    if "description" in kwargs:
        hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
        body["description"] = kwargs.get("description")
        del kwargs["description"]
    if "id" in kwargs:
        hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
        body["id"] = kwargs.get("id")
        del kwargs["id"]
    if "installerId" in kwargs:
        hub.log.debug(f"Got kwarg 'installerId' = {kwargs['installerId']}")
        body["installerId"] = kwargs.get("installerId")
        del kwargs["installerId"]
    if "name" in kwargs:
        hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
        body["name"] = kwargs.get("name")
        del kwargs["name"]
    if "orgId" in kwargs:
        hub.log.debug(f"Got kwarg 'orgId' = {kwargs['orgId']}")
        body["orgId"] = kwargs.get("orgId")
        del kwargs["orgId"]
    if "owner" in kwargs:
        hub.log.debug(f"Got kwarg 'owner' = {kwargs['owner']}")
        body["owner"] = kwargs.get("owner")
        del kwargs["owner"]
    if "projectId" in kwargs:
        hub.log.debug(f"Got kwarg 'projectId' = {kwargs['projectId']}")
        body["projectId"] = kwargs.get("projectId")
        del kwargs["projectId"]
    if "registered" in kwargs:
        hub.log.debug(f"Got kwarg 'registered' = {kwargs['registered']}")
        body["registered"] = kwargs.get("registered")
        del kwargs["registered"]
    if "shared" in kwargs:
        hub.log.debug(f"Got kwarg 'shared' = {kwargs['shared']}")
        body["shared"] = kwargs.get("shared")
        del kwargs["shared"]
    if "status" in kwargs:
        hub.log.debug(f"Got kwarg 'status' = {kwargs['status']}")
        body["status"] = kwargs.get("status")
        del kwargs["status"]
    if "updatedMillis" in kwargs:
        hub.log.debug(f"Got kwarg 'updatedMillis' = {kwargs['updatedMillis']}")
        body["updatedMillis"] = kwargs.get("updatedMillis")
        del kwargs["updatedMillis"]
    if "zoneProjectAssignmentId" in kwargs:
        hub.log.debug(
            f"Got kwarg 'zoneProjectAssignmentId' = {kwargs['zoneProjectAssignmentId']}"
        )
        body["zoneProjectAssignmentId"] = kwargs.get("zoneProjectAssignmentId")
        del kwargs["zoneProjectAssignmentId"]

    ret = api.register_using_put(body, id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def update_using_put2(hub, ctx, p_id, **kwargs):
    """Update a K8S Namespace description or project Update a K8S Namespace description or project by providing namespace id and
      Namespace entity Performs PUT /cmx/api/resources/k8s/namespaces/{id}


    :param string p_id: (required in path) id
    :param string authCredentialsLink: (optional in body)
    :param string clusterId: (optional in body)
    :param integer createdMillis: (optional in body)
    :param object customProperties: (optional in body)
    :param string description: (optional in body)
    :param string id: (optional in body)
    :param string installerId: (optional in body)
    :param string name: (optional in body)
    :param string orgId: (optional in body)
    :param string owner: (optional in body)
    :param string projectId: (optional in body)
    :param boolean registered: (optional in body)
    :param boolean shared: (optional in body)
    :param string status: (optional in body)
    :param integer updatedMillis: (optional in body)
    :param string zoneProjectAssignmentId: (optional in body)
    """

    hub.log.debug("PUT /cmx/api/resources/k8s/namespaces/{id}")

    api = NamespacesApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

    body = {}

    if "authCredentialsLink" in kwargs:
        hub.log.debug(
            f"Got kwarg 'authCredentialsLink' = {kwargs['authCredentialsLink']}"
        )
        body["authCredentialsLink"] = kwargs.get("authCredentialsLink")
        del kwargs["authCredentialsLink"]
    if "clusterId" in kwargs:
        hub.log.debug(f"Got kwarg 'clusterId' = {kwargs['clusterId']}")
        body["clusterId"] = kwargs.get("clusterId")
        del kwargs["clusterId"]
    if "createdMillis" in kwargs:
        hub.log.debug(f"Got kwarg 'createdMillis' = {kwargs['createdMillis']}")
        body["createdMillis"] = kwargs.get("createdMillis")
        del kwargs["createdMillis"]
    if "customProperties" in kwargs:
        hub.log.debug(f"Got kwarg 'customProperties' = {kwargs['customProperties']}")
        body["customProperties"] = kwargs.get("customProperties")
        del kwargs["customProperties"]
    if "description" in kwargs:
        hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
        body["description"] = kwargs.get("description")
        del kwargs["description"]
    if "id" in kwargs:
        hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
        body["id"] = kwargs.get("id")
        del kwargs["id"]
    if "installerId" in kwargs:
        hub.log.debug(f"Got kwarg 'installerId' = {kwargs['installerId']}")
        body["installerId"] = kwargs.get("installerId")
        del kwargs["installerId"]
    if "name" in kwargs:
        hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
        body["name"] = kwargs.get("name")
        del kwargs["name"]
    if "orgId" in kwargs:
        hub.log.debug(f"Got kwarg 'orgId' = {kwargs['orgId']}")
        body["orgId"] = kwargs.get("orgId")
        del kwargs["orgId"]
    if "owner" in kwargs:
        hub.log.debug(f"Got kwarg 'owner' = {kwargs['owner']}")
        body["owner"] = kwargs.get("owner")
        del kwargs["owner"]
    if "projectId" in kwargs:
        hub.log.debug(f"Got kwarg 'projectId' = {kwargs['projectId']}")
        body["projectId"] = kwargs.get("projectId")
        del kwargs["projectId"]
    if "registered" in kwargs:
        hub.log.debug(f"Got kwarg 'registered' = {kwargs['registered']}")
        body["registered"] = kwargs.get("registered")
        del kwargs["registered"]
    if "shared" in kwargs:
        hub.log.debug(f"Got kwarg 'shared' = {kwargs['shared']}")
        body["shared"] = kwargs.get("shared")
        del kwargs["shared"]
    if "status" in kwargs:
        hub.log.debug(f"Got kwarg 'status' = {kwargs['status']}")
        body["status"] = kwargs.get("status")
        del kwargs["status"]
    if "updatedMillis" in kwargs:
        hub.log.debug(f"Got kwarg 'updatedMillis' = {kwargs['updatedMillis']}")
        body["updatedMillis"] = kwargs.get("updatedMillis")
        del kwargs["updatedMillis"]
    if "zoneProjectAssignmentId" in kwargs:
        hub.log.debug(
            f"Got kwarg 'zoneProjectAssignmentId' = {kwargs['zoneProjectAssignmentId']}"
        )
        body["zoneProjectAssignmentId"] = kwargs.get("zoneProjectAssignmentId")
        del kwargs["zoneProjectAssignmentId"]

    ret = api.update_using_put2(body, id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))

from idem_vra.client.vra_cmx_lib.api import ResourceQuotasApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get_using_get3(hub, ctx, p_id, **kwargs):
    """Get a K8S ResourceQuota by id Get a K8S ResourceQuota by id Performs GET /cmx/api/resources/resource-quotas/{id}


    :param string p_id: (required in path) id
    """

    hub.log.debug("GET /cmx/api/resources/resource-quotas/{id}")

    api = ResourceQuotasApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

    ret = api.get_using_get3(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def list_using_get3(hub, ctx, **kwargs):
    """Get All K8S ResourceQuotas Get a list of all K8S ResourceQuotas Performs GET /cmx/api/resources/resource-quotas


    :param integer offset: (optional in query)
    :param integer pageNumber: (optional in query)
    :param integer pageSize: (optional in query)
    :param boolean paged: (optional in query)
    :param boolean sort.sorted: (optional in query)
    :param boolean sort.unsorted: (optional in query)
    :param boolean unpaged: (optional in query)
    """

    hub.log.debug("GET /cmx/api/resources/resource-quotas")

    api = ResourceQuotasApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

    ret = api.list_using_get3(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))

from idem_vra.client.vra_cmx_lib.api import KubernetesZonesApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def create_zone_using_post(hub, ctx, **kwargs):
    """Create a K8S Zone Create a K8S Zone entity Performs POST /cmx/api/resources/k8s-zones


    :param array clusters: (optional in body)
    :param integer createdMillis: (optional in body)
    :param object customProperties: (optional in body)
    :param string description: (optional in body)
    :param string id: (optional in body)
    :param string name: (optional in body)
    :param string orgId: (optional in body)
    :param array projects: (optional in body)
    :param string providerId: (optional in body)
    :param string providerType: (optional in body)
    :param array resources: (optional in body)
    :param array supervisorClusters: (optional in body)
    :param array supervisorNamespaces: (optional in body)
    :param array tagLinks: (optional in body)
    :param array tags: (optional in body)
    :param integer updatedMillis: (optional in body)
    """

    hub.log.debug("POST /cmx/api/resources/k8s-zones")

    api = KubernetesZonesApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

    body = {}

    if "clusters" in kwargs:
        hub.log.debug(f"Got kwarg 'clusters' = {kwargs['clusters']}")
        body["clusters"] = kwargs.get("clusters")
        del kwargs["clusters"]
    if "createdMillis" in kwargs:
        hub.log.debug(f"Got kwarg 'createdMillis' = {kwargs['createdMillis']}")
        body["createdMillis"] = kwargs.get("createdMillis")
        del kwargs["createdMillis"]
    if "customProperties" in kwargs:
        hub.log.debug(f"Got kwarg 'customProperties' = {kwargs['customProperties']}")
        body["customProperties"] = kwargs.get("customProperties")
        del kwargs["customProperties"]
    if "description" in kwargs:
        hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
        body["description"] = kwargs.get("description")
        del kwargs["description"]
    if "id" in kwargs:
        hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
        body["id"] = kwargs.get("id")
        del kwargs["id"]
    if "name" in kwargs:
        hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
        body["name"] = kwargs.get("name")
        del kwargs["name"]
    if "orgId" in kwargs:
        hub.log.debug(f"Got kwarg 'orgId' = {kwargs['orgId']}")
        body["orgId"] = kwargs.get("orgId")
        del kwargs["orgId"]
    if "projects" in kwargs:
        hub.log.debug(f"Got kwarg 'projects' = {kwargs['projects']}")
        body["projects"] = kwargs.get("projects")
        del kwargs["projects"]
    if "providerId" in kwargs:
        hub.log.debug(f"Got kwarg 'providerId' = {kwargs['providerId']}")
        body["providerId"] = kwargs.get("providerId")
        del kwargs["providerId"]
    if "providerType" in kwargs:
        hub.log.debug(f"Got kwarg 'providerType' = {kwargs['providerType']}")
        body["providerType"] = kwargs.get("providerType")
        del kwargs["providerType"]
    if "resources" in kwargs:
        hub.log.debug(f"Got kwarg 'resources' = {kwargs['resources']}")
        body["resources"] = kwargs.get("resources")
        del kwargs["resources"]
    if "supervisorClusters" in kwargs:
        hub.log.debug(
            f"Got kwarg 'supervisorClusters' = {kwargs['supervisorClusters']}"
        )
        body["supervisorClusters"] = kwargs.get("supervisorClusters")
        del kwargs["supervisorClusters"]
    if "supervisorNamespaces" in kwargs:
        hub.log.debug(
            f"Got kwarg 'supervisorNamespaces' = {kwargs['supervisorNamespaces']}"
        )
        body["supervisorNamespaces"] = kwargs.get("supervisorNamespaces")
        del kwargs["supervisorNamespaces"]
    if "tagLinks" in kwargs:
        hub.log.debug(f"Got kwarg 'tagLinks' = {kwargs['tagLinks']}")
        body["tagLinks"] = kwargs.get("tagLinks")
        del kwargs["tagLinks"]
    if "tags" in kwargs:
        hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
        body["tags"] = kwargs.get("tags")
        del kwargs["tags"]
    if "updatedMillis" in kwargs:
        hub.log.debug(f"Got kwarg 'updatedMillis' = {kwargs['updatedMillis']}")
        body["updatedMillis"] = kwargs.get("updatedMillis")
        del kwargs["updatedMillis"]

    ret = api.create_zone_using_post(body, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def delete_zone_using_delete(hub, ctx, p_id, **kwargs):
    """Delete a K8S Zone Remove a K8S Zone Performs DELETE /cmx/api/resources/k8s-zones/{id}


    :param string p_id: (required in path) id
    """

    hub.log.debug("DELETE /cmx/api/resources/k8s-zones/{id}")

    api = KubernetesZonesApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

    ret = api.delete_zone_using_delete(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_zone_using_get(hub, ctx, p_id, **kwargs):
    """Get a K8S Zone Get a K8S Zone by Id Performs GET /cmx/api/resources/k8s-zones/{id}


    :param string p_id: (required in path) id
    :param boolean expandTags: (optional in query) expandTags
    """

    hub.log.debug("GET /cmx/api/resources/k8s-zones/{id}")

    api = KubernetesZonesApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

    ret = api.get_zone_using_get(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def list_zones_using_get(hub, ctx, **kwargs):
    """Get all K8S Zones List of all K8S Zones Performs GET /cmx/api/resources/k8s-zones


    :param integer offset: (optional in query)
    :param integer pageNumber: (optional in query)
    :param integer pageSize: (optional in query)
    :param boolean paged: (optional in query)
    :param boolean sort.sorted: (optional in query)
    :param boolean sort.unsorted: (optional in query)
    :param boolean unpaged: (optional in query)
    :param boolean expandTags: (optional in query) expandTags
    :param string projectId: (optional in query) projectId
    :param string providerId: (optional in query) providerId
    """

    hub.log.debug("GET /cmx/api/resources/k8s-zones")

    api = KubernetesZonesApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

    ret = api.list_zones_using_get(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def update_zone_projects_using_put(hub, ctx, p_id, **kwargs):
    """Update a K8S Zone project assignments Assignment of projects to K8S Zone Performs PUT /cmx/api/resources/k8s-zones/{id}/projects


    :param string p_id: (required in path) id
    """

    hub.log.debug("PUT /cmx/api/resources/k8s-zones/{id}/projects")

    api = KubernetesZonesApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

    ret = api.update_zone_projects_using_put(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def update_zone_using_put(hub, ctx, p_id, **kwargs):
    """Update a K8S Zone Modify a K8S Zone Performs PUT /cmx/api/resources/k8s-zones/{id}


    :param string p_id: (required in path) id
    :param array clusters: (optional in body)
    :param integer createdMillis: (optional in body)
    :param object customProperties: (optional in body)
    :param string description: (optional in body)
    :param string id: (optional in body)
    :param string name: (optional in body)
    :param string orgId: (optional in body)
    :param array projects: (optional in body)
    :param string providerId: (optional in body)
    :param string providerType: (optional in body)
    :param array resources: (optional in body)
    :param array supervisorClusters: (optional in body)
    :param array supervisorNamespaces: (optional in body)
    :param array tagLinks: (optional in body)
    :param array tags: (optional in body)
    :param integer updatedMillis: (optional in body)
    """

    hub.log.debug("PUT /cmx/api/resources/k8s-zones/{id}")

    api = KubernetesZonesApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

    body = {}

    if "clusters" in kwargs:
        hub.log.debug(f"Got kwarg 'clusters' = {kwargs['clusters']}")
        body["clusters"] = kwargs.get("clusters")
        del kwargs["clusters"]
    if "createdMillis" in kwargs:
        hub.log.debug(f"Got kwarg 'createdMillis' = {kwargs['createdMillis']}")
        body["createdMillis"] = kwargs.get("createdMillis")
        del kwargs["createdMillis"]
    if "customProperties" in kwargs:
        hub.log.debug(f"Got kwarg 'customProperties' = {kwargs['customProperties']}")
        body["customProperties"] = kwargs.get("customProperties")
        del kwargs["customProperties"]
    if "description" in kwargs:
        hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
        body["description"] = kwargs.get("description")
        del kwargs["description"]
    if "id" in kwargs:
        hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
        body["id"] = kwargs.get("id")
        del kwargs["id"]
    if "name" in kwargs:
        hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
        body["name"] = kwargs.get("name")
        del kwargs["name"]
    if "orgId" in kwargs:
        hub.log.debug(f"Got kwarg 'orgId' = {kwargs['orgId']}")
        body["orgId"] = kwargs.get("orgId")
        del kwargs["orgId"]
    if "projects" in kwargs:
        hub.log.debug(f"Got kwarg 'projects' = {kwargs['projects']}")
        body["projects"] = kwargs.get("projects")
        del kwargs["projects"]
    if "providerId" in kwargs:
        hub.log.debug(f"Got kwarg 'providerId' = {kwargs['providerId']}")
        body["providerId"] = kwargs.get("providerId")
        del kwargs["providerId"]
    if "providerType" in kwargs:
        hub.log.debug(f"Got kwarg 'providerType' = {kwargs['providerType']}")
        body["providerType"] = kwargs.get("providerType")
        del kwargs["providerType"]
    if "resources" in kwargs:
        hub.log.debug(f"Got kwarg 'resources' = {kwargs['resources']}")
        body["resources"] = kwargs.get("resources")
        del kwargs["resources"]
    if "supervisorClusters" in kwargs:
        hub.log.debug(
            f"Got kwarg 'supervisorClusters' = {kwargs['supervisorClusters']}"
        )
        body["supervisorClusters"] = kwargs.get("supervisorClusters")
        del kwargs["supervisorClusters"]
    if "supervisorNamespaces" in kwargs:
        hub.log.debug(
            f"Got kwarg 'supervisorNamespaces' = {kwargs['supervisorNamespaces']}"
        )
        body["supervisorNamespaces"] = kwargs.get("supervisorNamespaces")
        del kwargs["supervisorNamespaces"]
    if "tagLinks" in kwargs:
        hub.log.debug(f"Got kwarg 'tagLinks' = {kwargs['tagLinks']}")
        body["tagLinks"] = kwargs.get("tagLinks")
        del kwargs["tagLinks"]
    if "tags" in kwargs:
        hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
        body["tags"] = kwargs.get("tags")
        del kwargs["tags"]
    if "updatedMillis" in kwargs:
        hub.log.debug(f"Got kwarg 'updatedMillis' = {kwargs['updatedMillis']}")
        body["updatedMillis"] = kwargs.get("updatedMillis")
        del kwargs["updatedMillis"]

    ret = api.update_zone_using_put(body, id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))

from idem_vra.client.vra_cmx_lib.api import PKSEndpointsApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def create_cluster_using_post(hub, ctx, p_id, **kwargs):
    """Create a K8S cluster Create a K8S cluster on PKS endpoint specified by endpoint id Performs POST /cmx/api/resources/pks/endpoints/{id}/clusters


    :param string p_id: (required in path) id
    :param string hostnameAddress: (optional in body)
    :param string ipAddress: (optional in body)
    :param array kubernetes_master_ips: (optional in body)
    :param string last_action: (optional in body)
    :param string last_action_description: (optional in body)
    :param string last_action_state: (optional in body)
    :param string name: (optional in body)
    :param object parameters: (optional in body)
    :param string plan_name: (optional in body)
    :param string uuid: (optional in body)
    """

    hub.log.debug("POST /cmx/api/resources/pks/endpoints/{id}/clusters")

    api = PKSEndpointsApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

    body = {}

    if "hostnameAddress" in kwargs:
        hub.log.debug(f"Got kwarg 'hostnameAddress' = {kwargs['hostnameAddress']}")
        body["hostnameAddress"] = kwargs.get("hostnameAddress")
        del kwargs["hostnameAddress"]
    if "ipAddress" in kwargs:
        hub.log.debug(f"Got kwarg 'ipAddress' = {kwargs['ipAddress']}")
        body["ipAddress"] = kwargs.get("ipAddress")
        del kwargs["ipAddress"]
    if "kubernetes_master_ips" in kwargs:
        hub.log.debug(
            f"Got kwarg 'kubernetes_master_ips' = {kwargs['kubernetes_master_ips']}"
        )
        body["kubernetes_master_ips"] = kwargs.get("kubernetes_master_ips")
        del kwargs["kubernetes_master_ips"]
    if "last_action" in kwargs:
        hub.log.debug(f"Got kwarg 'last_action' = {kwargs['last_action']}")
        body["last_action"] = kwargs.get("last_action")
        del kwargs["last_action"]
    if "last_action_description" in kwargs:
        hub.log.debug(
            f"Got kwarg 'last_action_description' = {kwargs['last_action_description']}"
        )
        body["last_action_description"] = kwargs.get("last_action_description")
        del kwargs["last_action_description"]
    if "last_action_state" in kwargs:
        hub.log.debug(f"Got kwarg 'last_action_state' = {kwargs['last_action_state']}")
        body["last_action_state"] = kwargs.get("last_action_state")
        del kwargs["last_action_state"]
    if "name" in kwargs:
        hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
        body["name"] = kwargs.get("name")
        del kwargs["name"]
    if "parameters" in kwargs:
        hub.log.debug(f"Got kwarg 'parameters' = {kwargs['parameters']}")
        body["parameters"] = kwargs.get("parameters")
        del kwargs["parameters"]
    if "plan_name" in kwargs:
        hub.log.debug(f"Got kwarg 'plan_name' = {kwargs['plan_name']}")
        body["plan_name"] = kwargs.get("plan_name")
        del kwargs["plan_name"]
    if "uuid" in kwargs:
        hub.log.debug(f"Got kwarg 'uuid' = {kwargs['uuid']}")
        body["uuid"] = kwargs.get("uuid")
        del kwargs["uuid"]

    ret = api.create_cluster_using_post(body, id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def destroy_cluster_using_delete1(hub, ctx, p_clusterId, p_id, **kwargs):
    """Destroy a K8S cluster on a specific PKS endpoint Destroy and unregister a K8S cluster on PKS endpoint Performs DELETE /cmx/api/resources/pks/endpoints/{id}/clusters/{clusterId}


    :param string p_clusterId: (required in path) clusterId
    :param string p_id: (required in path) id
    """

    hub.log.debug("DELETE /cmx/api/resources/pks/endpoints/{id}/clusters/{clusterId}")

    api = PKSEndpointsApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

    ret = api.destroy_cluster_using_delete1(cluster_id=p_clusterId, id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_clusters_using_get(hub, ctx, p_id, **kwargs):
    """Get all K8S clusters for a PKS endpoint Get all K8S clusters for a PKS endpoint by provided PKS endpoint id Performs GET /cmx/api/resources/pks/endpoints/{id}/clusters


    :param string p_id: (required in path) id
    """

    hub.log.debug("GET /cmx/api/resources/pks/endpoints/{id}/clusters")

    api = PKSEndpointsApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

    ret = api.get_clusters_using_get(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_plans_using_get(hub, ctx, p_id, **kwargs):
    """Get supported plans for a PKS endpoint Get supported plans by providing PKS endpoint id Performs GET /cmx/api/resources/pks/endpoints/{id}/plans


    :param string p_id: (required in path) id
    """

    hub.log.debug("GET /cmx/api/resources/pks/endpoints/{id}/plans")

    api = PKSEndpointsApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

    ret = api.get_plans_using_get(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def update_cluster_using_put(hub, ctx, p_clusterId, p_id, **kwargs):
    """Update a K8S cluster on a specific endpoint id Update a K8S cluster on a PKS endpoint identified by id Performs PUT /cmx/api/resources/pks/endpoints/{id}/clusters/{clusterId}


    :param string p_clusterId: (required in path) clusterId
    :param string p_id: (required in path) id
    :param string hostnameAddress: (optional in body)
    :param string ipAddress: (optional in body)
    :param array kubernetes_master_ips: (optional in body)
    :param string last_action: (optional in body)
    :param string last_action_description: (optional in body)
    :param string last_action_state: (optional in body)
    :param string name: (optional in body)
    :param object parameters: (optional in body)
    :param string plan_name: (optional in body)
    :param string uuid: (optional in body)
    """

    hub.log.debug("PUT /cmx/api/resources/pks/endpoints/{id}/clusters/{clusterId}")

    api = PKSEndpointsApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

    body = {}

    if "hostnameAddress" in kwargs:
        hub.log.debug(f"Got kwarg 'hostnameAddress' = {kwargs['hostnameAddress']}")
        body["hostnameAddress"] = kwargs.get("hostnameAddress")
        del kwargs["hostnameAddress"]
    if "ipAddress" in kwargs:
        hub.log.debug(f"Got kwarg 'ipAddress' = {kwargs['ipAddress']}")
        body["ipAddress"] = kwargs.get("ipAddress")
        del kwargs["ipAddress"]
    if "kubernetes_master_ips" in kwargs:
        hub.log.debug(
            f"Got kwarg 'kubernetes_master_ips' = {kwargs['kubernetes_master_ips']}"
        )
        body["kubernetes_master_ips"] = kwargs.get("kubernetes_master_ips")
        del kwargs["kubernetes_master_ips"]
    if "last_action" in kwargs:
        hub.log.debug(f"Got kwarg 'last_action' = {kwargs['last_action']}")
        body["last_action"] = kwargs.get("last_action")
        del kwargs["last_action"]
    if "last_action_description" in kwargs:
        hub.log.debug(
            f"Got kwarg 'last_action_description' = {kwargs['last_action_description']}"
        )
        body["last_action_description"] = kwargs.get("last_action_description")
        del kwargs["last_action_description"]
    if "last_action_state" in kwargs:
        hub.log.debug(f"Got kwarg 'last_action_state' = {kwargs['last_action_state']}")
        body["last_action_state"] = kwargs.get("last_action_state")
        del kwargs["last_action_state"]
    if "name" in kwargs:
        hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
        body["name"] = kwargs.get("name")
        del kwargs["name"]
    if "parameters" in kwargs:
        hub.log.debug(f"Got kwarg 'parameters' = {kwargs['parameters']}")
        body["parameters"] = kwargs.get("parameters")
        del kwargs["parameters"]
    if "plan_name" in kwargs:
        hub.log.debug(f"Got kwarg 'plan_name' = {kwargs['plan_name']}")
        body["plan_name"] = kwargs.get("plan_name")
        del kwargs["plan_name"]
    if "uuid" in kwargs:
        hub.log.debug(f"Got kwarg 'uuid' = {kwargs['uuid']}")
        body["uuid"] = kwargs.get("uuid")
        del kwargs["uuid"]

    ret = api.update_cluster_using_put(body, cluster_id=p_clusterId, id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))

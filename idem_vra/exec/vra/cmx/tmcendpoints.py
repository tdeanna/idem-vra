from idem_vra.client.vra_cmx_lib.api import TMCEndpointsApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def create_request_tracker_using_post(hub, ctx, **kwargs):
    """Create a request tracker for endpoint creation Create a request tracker for a TMC endpoint by providing the endpoint
      configuration Performs POST /cmx/api/resources/tmc/endpoints/request-tracker


    :param Any customProperties: (optional in body)
    :param Any endpointProperties: (optional in body)
    :param string endpointType: (optional in body)
    :param string name: (optional in body)
    :param string requestType: (optional in body)
    :param string resourceReference: (optional in body)
    :param string taskReference: (optional in body)
    """

    hub.log.debug("POST /cmx/api/resources/tmc/endpoints/request-tracker")

    api = TMCEndpointsApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

    body = {}

    if "customProperties" in kwargs:
        hub.log.debug(f"Got kwarg 'customProperties' = {kwargs['customProperties']}")
        body["customProperties"] = kwargs.get("customProperties")
        del kwargs["customProperties"]
    if "endpointProperties" in kwargs:
        hub.log.debug(
            f"Got kwarg 'endpointProperties' = {kwargs['endpointProperties']}"
        )
        body["endpointProperties"] = kwargs.get("endpointProperties")
        del kwargs["endpointProperties"]
    if "endpointType" in kwargs:
        hub.log.debug(f"Got kwarg 'endpointType' = {kwargs['endpointType']}")
        body["endpointType"] = kwargs.get("endpointType")
        del kwargs["endpointType"]
    if "name" in kwargs:
        hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
        body["name"] = kwargs.get("name")
        del kwargs["name"]
    if "requestType" in kwargs:
        hub.log.debug(f"Got kwarg 'requestType' = {kwargs['requestType']}")
        body["requestType"] = kwargs.get("requestType")
        del kwargs["requestType"]
    if "resourceReference" in kwargs:
        hub.log.debug(f"Got kwarg 'resourceReference' = {kwargs['resourceReference']}")
        body["resourceReference"] = kwargs.get("resourceReference")
        del kwargs["resourceReference"]
    if "taskReference" in kwargs:
        hub.log.debug(f"Got kwarg 'taskReference' = {kwargs['taskReference']}")
        body["taskReference"] = kwargs.get("taskReference")
        del kwargs["taskReference"]

    ret = api.create_request_tracker_using_post(body, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_cluster_groups_by_endpoint_using_get(hub, ctx, p_id, **kwargs):
    """Get all cluster groups for TMC endpoint Get cluster groups for a TMC endpoint by provided TMC endpoint id and starting
      with the provided cluster group name. If cluster group name is not provided,
      then all cluster groups are returned Performs GET /cmx/api/resources/tmc/endpoints/{id}/clustergroups


    :param string p_id: (required in path) id
    :param boolean includeTotalCount: (optional in query) Include total count.
    :param string pagination.offset: (optional in query) Offset at which to start returning records.
    :param string pagination.size: (optional in query) Number of records to return.
    :param string query: (optional in query) TQL query string.
    :param string searchScope.name: (optional in query) Scope search to the specified name; supports globbing; default (*).
    :param string sortBy: (optional in query) Sort Order.
    """

    hub.log.debug("GET /cmx/api/resources/tmc/endpoints/{id}/clustergroups")

    api = TMCEndpointsApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

    ret = api.get_cluster_groups_by_endpoint_using_get(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_cluster_groups_by_request_tracker_using_get(
    hub, ctx, q_requestTrackerId, **kwargs
):
    """Get all cluster groups for TMC endpoint Get cluster groups for a TMC endpoint by provided TMC endpoint id and starting
      with the provided cluster group name. If cluster group name is not provided,
      then all cluster groups are returned Performs GET /cmx/api/resources/tmc/endpoints/clustergroups


    :param string q_requestTrackerId: (required in query)
    :param boolean includeTotalCount: (optional in query) Include total count.
    :param string pagination.offset: (optional in query) Offset at which to start returning records.
    :param string pagination.size: (optional in query) Number of records to return.
    :param string query: (optional in query) TQL query string.
    :param string searchScope.name: (optional in query) Scope search to the specified name; supports globbing; default (*).
    :param string sortBy: (optional in query) Sort Order.
    """

    hub.log.debug("GET /cmx/api/resources/tmc/endpoints/clustergroups")

    api = TMCEndpointsApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

    ret = api.get_cluster_groups_by_request_tracker_using_get(
        request_tracker_id=q_requestTrackerId, **kwargs
    )

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_tmc_cli_binaries_using_get(hub, ctx, **kwargs):
    """Get Tmc CLI binaries Get Tanzu CLI binaries for Mac, Linux, Windows Performs GET /cmx/api/resources/tmc/endpoints/tanzu-cli-binaries"""

    hub.log.debug("GET /cmx/api/resources/tmc/endpoints/tanzu-cli-binaries")

    api = TMCEndpointsApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

    ret = api.get_tmc_cli_binaries_using_get(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_workspaces_by_endpoint_using_get(hub, ctx, p_id, **kwargs):
    """Get all workspace names list for TMC endpoint Get workspace names list for a TMC endpoint by provided TMC endpoint id and
      starting with the default workspace name. If workspace name is not provided,
      then all workspaces are returned Performs GET /cmx/api/resources/tmc/endpoints/{id}/workspaces


    :param string p_id: (required in path) id
    :param boolean includeTotalCount: (optional in query) Include total count.
    :param string pagination.offset: (optional in query) Offset at which to start returning records.
    :param string pagination.size: (optional in query) Number of records to return.
    :param string query: (optional in query) TQL query string.
    :param string searchScope.name: (optional in query) Scope search to the specified name; supports globbing; default (*).
    :param string sortBy: (optional in query) Sort Order.
    """

    hub.log.debug("GET /cmx/api/resources/tmc/endpoints/{id}/workspaces")

    api = TMCEndpointsApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

    ret = api.get_workspaces_by_endpoint_using_get(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_workspaces_by_request_tracker_using_get(
    hub, ctx, q_requestTrackerId, **kwargs
):
    """Get all workspace names list for TMC endpoint Get workspace names list for a TMC endpoint by provided TMC endpoint id and
      starting with the default workspace name. If workspace name is not provided,
      then all workspaces are returned Performs GET /cmx/api/resources/tmc/endpoints/workspaces


    :param string q_requestTrackerId: (required in query)
    :param boolean includeTotalCount: (optional in query) Include total count.
    :param string pagination.offset: (optional in query) Offset at which to start returning records.
    :param string pagination.size: (optional in query) Number of records to return.
    :param string query: (optional in query) TQL query string.
    :param string searchScope.name: (optional in query) Scope search to the specified name; supports globbing; default (*).
    :param string sortBy: (optional in query) Sort Order.
    """

    hub.log.debug("GET /cmx/api/resources/tmc/endpoints/workspaces")

    api = TMCEndpointsApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

    ret = api.get_workspaces_by_request_tracker_using_get(
        request_tracker_id=q_requestTrackerId, **kwargs
    )

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))

from idem_vra.client.vra_cmx_lib.api import LimitRangesApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get_using_get1(hub, ctx, p_id, **kwargs):
    """Get a K8S LimitRange by id Get a K8S LimitRange by id Performs GET /cmx/api/resources/limit-ranges/{id}


    :param string p_id: (required in path) id
    """

    hub.log.debug("GET /cmx/api/resources/limit-ranges/{id}")

    api = LimitRangesApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

    ret = api.get_using_get1(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def list_using_get1(hub, ctx, **kwargs):
    """Get All K8S LimitRanges Get a list of all K8S LimitRanges Performs GET /cmx/api/resources/limit-ranges


    :param integer offset: (optional in query)
    :param integer pageNumber: (optional in query)
    :param integer pageSize: (optional in query)
    :param boolean paged: (optional in query)
    :param boolean sort.sorted: (optional in query)
    :param boolean sort.unsorted: (optional in query)
    :param boolean unpaged: (optional in query)
    """

    hub.log.debug("GET /cmx/api/resources/limit-ranges")

    api = LimitRangesApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

    ret = api.list_using_get1(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))

from idem_vra.client.vra_iaas_lib.api import ComputeGatewayApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def create_compute_gateway(
    hub, ctx, name, networks, natRules, projectId, **kwargs
):
    """Create a compute gateway Create a new compute gateway. Performs POST /iaas/api/compute-gateways


    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param array networks: (required in body) List of networks
    :param array natRules: (required in body) List of NAT Rules
    :param string projectId: (required in body) The id of the project the current user belongs to.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param object customProperties: (optional in body) Additional custom properties that may be used to extend this resource.
    :param string deploymentId: (optional in body) The id of the deployment that is associated with this resource
    """

    hub.log.debug("POST /iaas/api/compute-gateways")

    api = ComputeGatewayApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    body = {}
    body["name"] = name
    body["networks"] = networks
    body["natRules"] = natRules
    body["projectId"] = projectId

    if "customProperties" in kwargs:
        hub.log.debug(f"Got kwarg 'customProperties' = {kwargs['customProperties']}")
        body["customProperties"] = kwargs.get("customProperties")
        del kwargs["customProperties"]
    if "deploymentId" in kwargs:
        hub.log.debug(f"Got kwarg 'deploymentId' = {kwargs['deploymentId']}")
        body["deploymentId"] = kwargs.get("deploymentId")
        del kwargs["deploymentId"]

    ret = api.create_compute_gateway(body, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def delete_compute_gateway(hub, ctx, p_id, **kwargs):
    """Delete a compute gateway Delete compute gateway with a given id Performs DELETE /iaas/api/compute-gateways/{id}


    :param string p_id: (required in path) The ID of the compute gateway.
    :param boolean forceDelete: (optional in query) Controls whether this is a force delete operation. If true, best
      effort is made for deleting this compute gateway. Use with caution as
      force deleting may cause inconsistencies between the cloud provider
      and vRA.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    hub.log.debug("DELETE /iaas/api/compute-gateways/{id}")

    api = ComputeGatewayApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.delete_compute_gateway(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_compute_gateway(hub, ctx, p_id, **kwargs):
    """Get a compute gateway Get compute gateway with a given id Performs GET /iaas/api/compute-gateways/{id}


    :param string p_id: (required in path) The ID of the gateway.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    hub.log.debug("GET /iaas/api/compute-gateways/{id}")

    api = ComputeGatewayApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.get_compute_gateway(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_compute_gateways(hub, ctx, **kwargs):
    """Get compute gateways Get all compute gateways Performs GET /iaas/api/compute-gateways


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    hub.log.debug("GET /iaas/api/compute-gateways")

    api = ComputeGatewayApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.get_compute_gateways(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))

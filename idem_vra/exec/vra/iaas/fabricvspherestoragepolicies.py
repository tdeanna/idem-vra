from idem_vra.client.vra_iaas_lib.api import FabricVSphereStoragePoliciesApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get_fabric_v_sphere_storage_policies(hub, ctx, **kwargs):
    """Get fabric vSphere storage polices Get all fabric vSphere storage polices. Performs GET /iaas/api/fabric-vsphere-storage-policies


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param integer top: (optional in query) Number of records you want to get.
    :param integer skip: (optional in query) Number of records you want to skip.
    :param boolean count: (optional in query) Flag which when specified, regardless of the assigned value, shows the
      total number of records. If the collection has a filter it shows the
      number of records matching the filter.
    :param string select: (optional in query) Select a subset of properties to include in the response.
    :param string filter: (optional in query) Filter the results by a specified predicate expression. Operators: eq,
      ne, and, or.
    """

    hub.log.debug("GET /iaas/api/fabric-vsphere-storage-policies")

    api = FabricVSphereStoragePoliciesApi(
        hub.clients["idem_vra.client.vra_iaas_lib.api"]
    )
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.get_fabric_v_sphere_storage_policies(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_fabric_v_sphere_storage_policy(hub, ctx, p_id, **kwargs):
    """Get fabric vSphere storage policy Get fabric vSphere storage policy with a given id Performs GET /iaas/api/fabric-vsphere-storage-policies/{id}


    :param string p_id: (required in path) The ID of the Fabric vSphere Storage Policy.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string select: (optional in query) Select a subset of properties to include in the response.
    """

    hub.log.debug("GET /iaas/api/fabric-vsphere-storage-policies/{id}")

    api = FabricVSphereStoragePoliciesApi(
        hub.clients["idem_vra.client.vra_iaas_lib.api"]
    )
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.get_fabric_v_sphere_storage_policy(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))

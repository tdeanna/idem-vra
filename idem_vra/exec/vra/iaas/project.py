from idem_vra.client.vra_iaas_lib.api import ProjectApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def create_project(hub, ctx, name, **kwargs):
    """Create project Create project Performs POST /iaas/api/projects


    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param array viewers: (optional in body) List of viewer users associated with the project.
    :param string machineNamingTemplate: (optional in body) The naming template to be used for machines provisioned in this
      project
    :param object customProperties: (optional in body) The project custom properties which are added to all requests in this
      project
    :param boolean sharedResources: (optional in body) Specifies whether the resources in this projects are shared or not. If
      not set default will be used.
    :param integer operationTimeout: (optional in body) The timeout that should be used for Blueprint operations and
      Provisioning tasks. The timeout is in seconds
    :param array members: (optional in body) List of member users associated with the project.
    :param array zoneAssignmentConfigurations: (optional in body) List of configurations for zone assignment to a project.
    :param string description: (optional in body) A human-friendly description.
    :param string placementPolicy: (optional in body) Placement policy for the project. Determines how a zone will be
      selected for provisioning. DEFAULT, SPREAD or SPREAD_MEMORY.
    :param object constraints: (optional in body) List of storage, network and extensibility constraints to be applied
      when provisioning through this project.
    :param array administrators: (optional in body) List of administrator users associated with the project. Only
      administrators can manage project's configuration.
    """

    hub.log.debug("POST /iaas/api/projects")

    api = ProjectApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    body = {}
    body["name"] = name

    if "viewers" in kwargs:
        hub.log.debug(f"Got kwarg 'viewers' = {kwargs['viewers']}")
        body["viewers"] = kwargs.get("viewers")
        del kwargs["viewers"]
    if "machineNamingTemplate" in kwargs:
        hub.log.debug(
            f"Got kwarg 'machineNamingTemplate' = {kwargs['machineNamingTemplate']}"
        )
        body["machineNamingTemplate"] = kwargs.get("machineNamingTemplate")
        del kwargs["machineNamingTemplate"]
    if "customProperties" in kwargs:
        hub.log.debug(f"Got kwarg 'customProperties' = {kwargs['customProperties']}")
        body["customProperties"] = kwargs.get("customProperties")
        del kwargs["customProperties"]
    if "sharedResources" in kwargs:
        hub.log.debug(f"Got kwarg 'sharedResources' = {kwargs['sharedResources']}")
        body["sharedResources"] = kwargs.get("sharedResources")
        del kwargs["sharedResources"]
    if "operationTimeout" in kwargs:
        hub.log.debug(f"Got kwarg 'operationTimeout' = {kwargs['operationTimeout']}")
        body["operationTimeout"] = kwargs.get("operationTimeout")
        del kwargs["operationTimeout"]
    if "members" in kwargs:
        hub.log.debug(f"Got kwarg 'members' = {kwargs['members']}")
        body["members"] = kwargs.get("members")
        del kwargs["members"]
    if "zoneAssignmentConfigurations" in kwargs:
        hub.log.debug(
            f"Got kwarg 'zoneAssignmentConfigurations' = {kwargs['zoneAssignmentConfigurations']}"
        )
        body["zoneAssignmentConfigurations"] = kwargs.get(
            "zoneAssignmentConfigurations"
        )
        del kwargs["zoneAssignmentConfigurations"]
    if "description" in kwargs:
        hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
        body["description"] = kwargs.get("description")
        del kwargs["description"]
    if "placementPolicy" in kwargs:
        hub.log.debug(f"Got kwarg 'placementPolicy' = {kwargs['placementPolicy']}")
        body["placementPolicy"] = kwargs.get("placementPolicy")
        del kwargs["placementPolicy"]
    if "constraints" in kwargs:
        hub.log.debug(f"Got kwarg 'constraints' = {kwargs['constraints']}")
        body["constraints"] = kwargs.get("constraints")
        del kwargs["constraints"]
    if "administrators" in kwargs:
        hub.log.debug(f"Got kwarg 'administrators' = {kwargs['administrators']}")
        body["administrators"] = kwargs.get("administrators")
        del kwargs["administrators"]

    ret = api.create_project(body, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def delete_project(hub, ctx, p_id, **kwargs):
    """Delete project Delete project with a given id Performs DELETE /iaas/api/projects/{id}


    :param string p_id: (required in path) The ID of the project.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    hub.log.debug("DELETE /iaas/api/projects/{id}")

    api = ProjectApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.delete_project(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_project_resource_metadata(hub, ctx, p_id, **kwargs):
    """Get project resource metadata Get project resource metadata by a given project id Performs GET /iaas/api/projects/{id}/resource-metadata


    :param string p_id: (required in path) The ID of the project.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    hub.log.debug("GET /iaas/api/projects/{id}/resource-metadata")

    api = ProjectApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.get_project_resource_metadata(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_project(hub, ctx, p_id, **kwargs):
    """Get project Get project with a given id Performs GET /iaas/api/projects/{id}


    :param string p_id: (required in path) The ID of the project.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    hub.log.debug("GET /iaas/api/projects/{id}")

    api = ProjectApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.get_project(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_projects(hub, ctx, **kwargs):
    """Get projects Get all projects Performs GET /iaas/api/projects


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param integer top: (optional in query) Number of records you want to get.
    :param integer skip: (optional in query) Number of records you want to skip.
    :param string orderBy: (optional in query) Sorting criteria in the format: property (asc|desc). Default sort
      order is ascending. Multiple sort criteria are supported.
    :param boolean count: (optional in query) Flag which when specified, regardless of the assigned value, shows the
      total number of records. If the collection has a filter it shows the
      number of records matching the filter.
    :param string filter: (optional in query) Filter the results by a specified predicate expression. A set of
      operators and functions are defined for use:
      Operators: eq, ne, gt, ge, lt, le, and, or, not.
      Functions:
      bool substringof(string p0, string p1)
      bool endswith(string p0, string p1)
      bool startswith(string p0, string p1)
      int length(string p0)
      int indexof(string p0, string p1)
      string replace(string p0, string find, string replace)
      string substring(string p0, int pos)
      string substring(string p0, int pos, int length)
      string tolower(string p0)
      string toupper(string p0)
      string trim(string p0)
      string concat(string p0, string p1)
    """

    hub.log.debug("GET /iaas/api/projects")

    api = ProjectApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.get_projects(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def update_project_resource_metadata(hub, ctx, p_id, **kwargs):
    """Update project resource metadata Update project resource metadata by a given project id Performs PATCH /iaas/api/projects/{id}/resource-metadata


    :param string p_id: (required in path) The ID of the project.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param array tags: (optional in body) A list of keys and optional values to be applied to compute resources
      provisioned in a project
    """

    hub.log.debug("PATCH /iaas/api/projects/{id}/resource-metadata")

    api = ProjectApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    body = {}

    if "tags" in kwargs:
        hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
        body["tags"] = kwargs.get("tags")
        del kwargs["tags"]

    ret = api.update_project_resource_metadata(body, id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def update_project(hub, ctx, p_id, name, **kwargs):
    """Update project Update project Performs PATCH /iaas/api/projects/{id}


    :param string p_id: (required in path) The ID of the project.
    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param array viewers: (optional in body) List of viewer users associated with the project.
    :param string machineNamingTemplate: (optional in body) The naming template to be used for machines provisioned in this
      project
    :param object customProperties: (optional in body) The project custom properties which are added to all requests in this
      project
    :param boolean sharedResources: (optional in body) Specifies whether the resources in this projects are shared or not. If
      not set default will be used.
    :param integer operationTimeout: (optional in body) The timeout that should be used for Blueprint operations and
      Provisioning tasks. The timeout is in seconds
    :param array members: (optional in body) List of member users associated with the project.
    :param array zoneAssignmentConfigurations: (optional in body) List of configurations for zone assignment to a project.
    :param string description: (optional in body) A human-friendly description.
    :param string placementPolicy: (optional in body) Placement policy for the project. Determines how a zone will be
      selected for provisioning. DEFAULT, SPREAD or SPREAD_MEMORY.
    :param object constraints: (optional in body) List of storage, network and extensibility constraints to be applied
      when provisioning through this project.
    :param array administrators: (optional in body) List of administrator users associated with the project. Only
      administrators can manage project's configuration.
    """

    hub.log.debug("PATCH /iaas/api/projects/{id}")

    api = ProjectApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    body = {}
    body["name"] = name

    if "viewers" in kwargs:
        hub.log.debug(f"Got kwarg 'viewers' = {kwargs['viewers']}")
        body["viewers"] = kwargs.get("viewers")
        del kwargs["viewers"]
    if "machineNamingTemplate" in kwargs:
        hub.log.debug(
            f"Got kwarg 'machineNamingTemplate' = {kwargs['machineNamingTemplate']}"
        )
        body["machineNamingTemplate"] = kwargs.get("machineNamingTemplate")
        del kwargs["machineNamingTemplate"]
    if "customProperties" in kwargs:
        hub.log.debug(f"Got kwarg 'customProperties' = {kwargs['customProperties']}")
        body["customProperties"] = kwargs.get("customProperties")
        del kwargs["customProperties"]
    if "sharedResources" in kwargs:
        hub.log.debug(f"Got kwarg 'sharedResources' = {kwargs['sharedResources']}")
        body["sharedResources"] = kwargs.get("sharedResources")
        del kwargs["sharedResources"]
    if "operationTimeout" in kwargs:
        hub.log.debug(f"Got kwarg 'operationTimeout' = {kwargs['operationTimeout']}")
        body["operationTimeout"] = kwargs.get("operationTimeout")
        del kwargs["operationTimeout"]
    if "members" in kwargs:
        hub.log.debug(f"Got kwarg 'members' = {kwargs['members']}")
        body["members"] = kwargs.get("members")
        del kwargs["members"]
    if "zoneAssignmentConfigurations" in kwargs:
        hub.log.debug(
            f"Got kwarg 'zoneAssignmentConfigurations' = {kwargs['zoneAssignmentConfigurations']}"
        )
        body["zoneAssignmentConfigurations"] = kwargs.get(
            "zoneAssignmentConfigurations"
        )
        del kwargs["zoneAssignmentConfigurations"]
    if "description" in kwargs:
        hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
        body["description"] = kwargs.get("description")
        del kwargs["description"]
    if "placementPolicy" in kwargs:
        hub.log.debug(f"Got kwarg 'placementPolicy' = {kwargs['placementPolicy']}")
        body["placementPolicy"] = kwargs.get("placementPolicy")
        del kwargs["placementPolicy"]
    if "constraints" in kwargs:
        hub.log.debug(f"Got kwarg 'constraints' = {kwargs['constraints']}")
        body["constraints"] = kwargs.get("constraints")
        del kwargs["constraints"]
    if "administrators" in kwargs:
        hub.log.debug(f"Got kwarg 'administrators' = {kwargs['administrators']}")
        body["administrators"] = kwargs.get("administrators")
        del kwargs["administrators"]

    ret = api.update_project(body, id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))

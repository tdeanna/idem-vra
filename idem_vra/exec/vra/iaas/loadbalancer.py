from idem_vra.client.vra_iaas_lib.api import LoadBalancerApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def create_load_balancer(hub, ctx, routes, name, nics, projectId, **kwargs):
    """Create load balancer Create load balancer Performs POST /iaas/api/load-balancers


    :param array routes: (required in body) The load balancer route configuration regarding ports and protocols.
    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param array nics: (required in body) A set of network interface specifications for this load balancer.
    :param string projectId: (required in body) The id of the project the current user belongs to.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param object customProperties: (optional in body) Additional custom properties that may be used to extend this resource.
    :param string deploymentId: (optional in body) The id of the deployment that is associated with this resource
    :param array targetLinks: (optional in body) A list of links to target load balancer pool members. Links can be to
      either a machine or a machine's network interface.
    :param boolean internetFacing: (optional in body) An Internet-facing load balancer has a publicly resolvable DNS name,
      so it can route requests from clients over the Internet to the
      instances that are registered with the load balancer.
    :param string description: (optional in body) A human-friendly description.
    :param string type: (optional in body) Define the type/variant of load balancer numbers e.g.for NSX the
      number virtual servers and pool members load balancer can host
    :param string loggingLevel: (optional in body) Defines logging level for collecting load balancer traffic logs.
    :param array tags: (optional in body) A set of tag keys and optional values that should be set on any
      resource that is produced from this specification.
    """

    hub.log.debug("POST /iaas/api/load-balancers")

    api = LoadBalancerApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    body = {}
    body["routes"] = routes
    body["name"] = name
    body["nics"] = nics
    body["projectId"] = projectId

    if "customProperties" in kwargs:
        hub.log.debug(f"Got kwarg 'customProperties' = {kwargs['customProperties']}")
        body["customProperties"] = kwargs.get("customProperties")
        del kwargs["customProperties"]
    if "deploymentId" in kwargs:
        hub.log.debug(f"Got kwarg 'deploymentId' = {kwargs['deploymentId']}")
        body["deploymentId"] = kwargs.get("deploymentId")
        del kwargs["deploymentId"]
    if "targetLinks" in kwargs:
        hub.log.debug(f"Got kwarg 'targetLinks' = {kwargs['targetLinks']}")
        body["targetLinks"] = kwargs.get("targetLinks")
        del kwargs["targetLinks"]
    if "internetFacing" in kwargs:
        hub.log.debug(f"Got kwarg 'internetFacing' = {kwargs['internetFacing']}")
        body["internetFacing"] = kwargs.get("internetFacing")
        del kwargs["internetFacing"]
    if "description" in kwargs:
        hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
        body["description"] = kwargs.get("description")
        del kwargs["description"]
    if "type" in kwargs:
        hub.log.debug(f"Got kwarg 'type' = {kwargs['type']}")
        body["type"] = kwargs.get("type")
        del kwargs["type"]
    if "loggingLevel" in kwargs:
        hub.log.debug(f"Got kwarg 'loggingLevel' = {kwargs['loggingLevel']}")
        body["loggingLevel"] = kwargs.get("loggingLevel")
        del kwargs["loggingLevel"]
    if "tags" in kwargs:
        hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
        body["tags"] = kwargs.get("tags")
        del kwargs["tags"]

    ret = api.create_load_balancer(body, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def delete_load_balancer_operation(hub, ctx, p_id, **kwargs):
    """Delete operation for load balancer Second day delete operation for load balancer Performs POST /iaas/api/load-balancers/{id}/operations/delete


    :param string p_id: (required in path) The ID of the load balancer.
    :param boolean forceDelete: (optional in query) Controls whether this is a force delete operation. If true, best
      effort is made for deleting this load balancer. Use with caution as
      force deleting may cause inconsistencies between the cloud provider
      and vRA.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    hub.log.debug("POST /iaas/api/load-balancers/{id}/operations/delete")

    api = LoadBalancerApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.delete_load_balancer_operation(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def delete_load_balancer(hub, ctx, p_id, **kwargs):
    """Delete load balancer Delete load balancer with a given id Performs DELETE /iaas/api/load-balancers/{id}


    :param string p_id: (required in path) The ID of the load balancer.
    :param boolean forceDelete: (optional in query) Controls whether this is a force delete operation. If true, best
      effort is made for deleting this load balancer. Use with caution as
      force deleting may cause inconsistencies between the cloud provider
      and vRA.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    hub.log.debug("DELETE /iaas/api/load-balancers/{id}")

    api = LoadBalancerApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.delete_load_balancer(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_load_balancer_network_interface(hub, ctx, p_id, p_id1, **kwargs):
    """Get load balancer network interface Get network interface with a given id for specific load balancer Performs GET /iaas/api/load-balancers/{id}/network-interfaces/{id1}


    :param string p_id: (required in path) The ID of the load balancer.
    :param string p_id1: (required in path) The ID of the network interface.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    hub.log.debug("GET /iaas/api/load-balancers/{id}/network-interfaces/{id1}")

    api = LoadBalancerApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.get_load_balancer_network_interface(id=p_id, id1=p_id1, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_load_balancer(hub, ctx, p_id, **kwargs):
    """Get load balancer Get load balancer with a given id Performs GET /iaas/api/load-balancers/{id}


    :param string p_id: (required in path) The ID of the load balancer.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    hub.log.debug("GET /iaas/api/load-balancers/{id}")

    api = LoadBalancerApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.get_load_balancer(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_load_balancers(hub, ctx, **kwargs):
    """Get load balancers Get all load balancers Performs GET /iaas/api/load-balancers


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    hub.log.debug("GET /iaas/api/load-balancers")

    api = LoadBalancerApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.get_load_balancers(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def scale_load_balancer(hub, ctx, p_id, routes, name, nics, projectId, **kwargs):
    """Scale operation for load balancer Second day scale operation for load balancer Performs POST /iaas/api/load-balancers/{id}/operations/scale


    :param string p_id: (required in path) The ID of the load balancer.
    :param array routes: (required in body) The load balancer route configuration regarding ports and protocols.
    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param array nics: (required in body) A set of network interface specifications for this load balancer.
    :param string projectId: (required in body) The id of the project the current user belongs to.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param object customProperties: (optional in body) Additional custom properties that may be used to extend this resource.
    :param string deploymentId: (optional in body) The id of the deployment that is associated with this resource
    :param array targetLinks: (optional in body) A list of links to target load balancer pool members. Links can be to
      either a machine or a machine's network interface.
    :param boolean internetFacing: (optional in body) An Internet-facing load balancer has a publicly resolvable DNS name,
      so it can route requests from clients over the Internet to the
      instances that are registered with the load balancer.
    :param string description: (optional in body) A human-friendly description.
    :param string type: (optional in body) Define the type/variant of load balancer numbers e.g.for NSX the
      number virtual servers and pool members load balancer can host
    :param string loggingLevel: (optional in body) Defines logging level for collecting load balancer traffic logs.
    :param array tags: (optional in body) A set of tag keys and optional values that should be set on any
      resource that is produced from this specification.
    """

    hub.log.debug("POST /iaas/api/load-balancers/{id}/operations/scale")

    api = LoadBalancerApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    body = {}
    body["routes"] = routes
    body["name"] = name
    body["nics"] = nics
    body["projectId"] = projectId

    if "customProperties" in kwargs:
        hub.log.debug(f"Got kwarg 'customProperties' = {kwargs['customProperties']}")
        body["customProperties"] = kwargs.get("customProperties")
        del kwargs["customProperties"]
    if "deploymentId" in kwargs:
        hub.log.debug(f"Got kwarg 'deploymentId' = {kwargs['deploymentId']}")
        body["deploymentId"] = kwargs.get("deploymentId")
        del kwargs["deploymentId"]
    if "targetLinks" in kwargs:
        hub.log.debug(f"Got kwarg 'targetLinks' = {kwargs['targetLinks']}")
        body["targetLinks"] = kwargs.get("targetLinks")
        del kwargs["targetLinks"]
    if "internetFacing" in kwargs:
        hub.log.debug(f"Got kwarg 'internetFacing' = {kwargs['internetFacing']}")
        body["internetFacing"] = kwargs.get("internetFacing")
        del kwargs["internetFacing"]
    if "description" in kwargs:
        hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
        body["description"] = kwargs.get("description")
        del kwargs["description"]
    if "type" in kwargs:
        hub.log.debug(f"Got kwarg 'type' = {kwargs['type']}")
        body["type"] = kwargs.get("type")
        del kwargs["type"]
    if "loggingLevel" in kwargs:
        hub.log.debug(f"Got kwarg 'loggingLevel' = {kwargs['loggingLevel']}")
        body["loggingLevel"] = kwargs.get("loggingLevel")
        del kwargs["loggingLevel"]
    if "tags" in kwargs:
        hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
        body["tags"] = kwargs.get("tags")
        del kwargs["tags"]

    ret = api.scale_load_balancer(body, id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))

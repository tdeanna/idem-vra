from idem_vra.client.vra_iaas_lib.api import StorageProfileApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def create_aws_storage_profile(hub, ctx, deviceType, regionId, name, **kwargs):
    """Create AWS storage profile Create AWS storage profile Performs POST /iaas/api/storage-profiles-aws


    :param string deviceType: (required in body) Indicates the type of storage.
    :param string regionId: (required in body) A link to the region that is associated with the storage profile.
    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string volumeType: (optional in body) Indicates the type of volume associated with type of storage.
    :param boolean supportsEncryption: (optional in body) Indicates whether this storage profile supports encryption or not.
    :param string iops: (optional in body) Indicates maximum I/O operations per second in range(1-20,000).
    :param string description: (optional in body) A human-friendly description.
    :param boolean defaultItem: (optional in body) Indicates if a storage profile is default or not.
    :param array tags: (optional in body) A list of tags that represent the capabilities of this storage profile
    """

    hub.log.debug("POST /iaas/api/storage-profiles-aws")

    api = StorageProfileApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    body = {}
    body["deviceType"] = deviceType
    body["regionId"] = regionId
    body["name"] = name

    if "volumeType" in kwargs:
        hub.log.debug(f"Got kwarg 'volumeType' = {kwargs['volumeType']}")
        body["volumeType"] = kwargs.get("volumeType")
        del kwargs["volumeType"]
    if "supportsEncryption" in kwargs:
        hub.log.debug(
            f"Got kwarg 'supportsEncryption' = {kwargs['supportsEncryption']}"
        )
        body["supportsEncryption"] = kwargs.get("supportsEncryption")
        del kwargs["supportsEncryption"]
    if "iops" in kwargs:
        hub.log.debug(f"Got kwarg 'iops' = {kwargs['iops']}")
        body["iops"] = kwargs.get("iops")
        del kwargs["iops"]
    if "description" in kwargs:
        hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
        body["description"] = kwargs.get("description")
        del kwargs["description"]
    if "defaultItem" in kwargs:
        hub.log.debug(f"Got kwarg 'defaultItem' = {kwargs['defaultItem']}")
        body["defaultItem"] = kwargs.get("defaultItem")
        del kwargs["defaultItem"]
    if "tags" in kwargs:
        hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
        body["tags"] = kwargs.get("tags")
        del kwargs["tags"]

    ret = api.create_aws_storage_profile(body, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def create_azure_storage_profile(hub, ctx, regionId, name, **kwargs):
    """Create Azure storage profile Create Azure storage profile Performs POST /iaas/api/storage-profiles-azure


    :param string regionId: (required in body) The If of the region that is associated with the storage profile.
    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string storageAccountId: (optional in body) Id of a storage account where in the disk is placed.
    :param boolean supportsEncryption: (optional in body) Indicates whether this storage policy should support encryption or
      not.
    :param string description: (optional in body) A human-friendly description.
    :param boolean defaultItem: (optional in body) Indicates if a storage policy contains default storage properties.
    :param string diskType: (optional in body) Indicates the performance tier for the storage type. Premium disks are
      SSD backed and Standard disks are HDD backed.
    :param string dataDiskCaching: (optional in body) Indicates the caching mechanism for additional disk.
    :param string osDiskCaching: (optional in body) Indicates the caching mechanism for OS disk. Default policy for OS
      disks is Read/Write.
    :param string diskEncryptionSetId: (optional in body) Indicates the id of disk encryption set.
    :param array tags: (optional in body) A set of tag keys and optional values for a storage policy which
      define set of specifications for creating a disk.
    """

    hub.log.debug("POST /iaas/api/storage-profiles-azure")

    api = StorageProfileApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    body = {}
    body["regionId"] = regionId
    body["name"] = name

    if "storageAccountId" in kwargs:
        hub.log.debug(f"Got kwarg 'storageAccountId' = {kwargs['storageAccountId']}")
        body["storageAccountId"] = kwargs.get("storageAccountId")
        del kwargs["storageAccountId"]
    if "supportsEncryption" in kwargs:
        hub.log.debug(
            f"Got kwarg 'supportsEncryption' = {kwargs['supportsEncryption']}"
        )
        body["supportsEncryption"] = kwargs.get("supportsEncryption")
        del kwargs["supportsEncryption"]
    if "description" in kwargs:
        hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
        body["description"] = kwargs.get("description")
        del kwargs["description"]
    if "defaultItem" in kwargs:
        hub.log.debug(f"Got kwarg 'defaultItem' = {kwargs['defaultItem']}")
        body["defaultItem"] = kwargs.get("defaultItem")
        del kwargs["defaultItem"]
    if "diskType" in kwargs:
        hub.log.debug(f"Got kwarg 'diskType' = {kwargs['diskType']}")
        body["diskType"] = kwargs.get("diskType")
        del kwargs["diskType"]
    if "dataDiskCaching" in kwargs:
        hub.log.debug(f"Got kwarg 'dataDiskCaching' = {kwargs['dataDiskCaching']}")
        body["dataDiskCaching"] = kwargs.get("dataDiskCaching")
        del kwargs["dataDiskCaching"]
    if "osDiskCaching" in kwargs:
        hub.log.debug(f"Got kwarg 'osDiskCaching' = {kwargs['osDiskCaching']}")
        body["osDiskCaching"] = kwargs.get("osDiskCaching")
        del kwargs["osDiskCaching"]
    if "diskEncryptionSetId" in kwargs:
        hub.log.debug(
            f"Got kwarg 'diskEncryptionSetId' = {kwargs['diskEncryptionSetId']}"
        )
        body["diskEncryptionSetId"] = kwargs.get("diskEncryptionSetId")
        del kwargs["diskEncryptionSetId"]
    if "tags" in kwargs:
        hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
        body["tags"] = kwargs.get("tags")
        del kwargs["tags"]

    ret = api.create_azure_storage_profile(body, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def create_gcp_storage_profile(
    hub, ctx, regionId, name, persistentDiskType, **kwargs
):
    """Create GCP storage profile Create GCP storage profile Performs POST /iaas/api/storage-profiles-gcp


    :param string regionId: (required in body) A link to the region that is associated with the storage profile.
    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param string persistentDiskType: (required in body) Indicates the type of disk.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param boolean supportsEncryption: (optional in body) Indicates whether this storage profile supports encryption or not.
    :param string description: (optional in body) A human-friendly description.
    :param boolean defaultItem: (optional in body) Indicates if a storage profile is default or not.
    :param array tags: (optional in body) A list of tags that represent the capabilities of this storage profile
    """

    hub.log.debug("POST /iaas/api/storage-profiles-gcp")

    api = StorageProfileApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    body = {}
    body["regionId"] = regionId
    body["name"] = name
    body["persistentDiskType"] = persistentDiskType

    if "supportsEncryption" in kwargs:
        hub.log.debug(
            f"Got kwarg 'supportsEncryption' = {kwargs['supportsEncryption']}"
        )
        body["supportsEncryption"] = kwargs.get("supportsEncryption")
        del kwargs["supportsEncryption"]
    if "description" in kwargs:
        hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
        body["description"] = kwargs.get("description")
        del kwargs["description"]
    if "defaultItem" in kwargs:
        hub.log.debug(f"Got kwarg 'defaultItem' = {kwargs['defaultItem']}")
        body["defaultItem"] = kwargs.get("defaultItem")
        del kwargs["defaultItem"]
    if "tags" in kwargs:
        hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
        body["tags"] = kwargs.get("tags")
        del kwargs["tags"]

    ret = api.create_gcp_storage_profile(body, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def create_storage_profile(hub, ctx, regionId, name, defaultItem, **kwargs):
    """Create storage profile Create storage profile Performs POST /iaas/api/storage-profiles


    :param string regionId: (required in body) The Id of the region that is associated with the storage profile.
    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param boolean defaultItem: (required in body) Indicates if a storage profile is a default profile.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param object diskProperties: (optional in body) Map of storage properties that are to be applied on disk while
      provisioning.
    :param boolean supportsEncryption: (optional in body) Indicates whether this storage profile supports encryption or not.
    :param object diskTargetProperties: (optional in body) Map of storage placements to know where the disk is provisioned.
    :param string description: (optional in body) A human-friendly description.
    :param array tags: (optional in body) A list of tags that represent the capabilities of this storage profile
    """

    hub.log.debug("POST /iaas/api/storage-profiles")

    api = StorageProfileApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    body = {}
    body["regionId"] = regionId
    body["name"] = name
    body["defaultItem"] = defaultItem

    if "diskProperties" in kwargs:
        hub.log.debug(f"Got kwarg 'diskProperties' = {kwargs['diskProperties']}")
        body["diskProperties"] = kwargs.get("diskProperties")
        del kwargs["diskProperties"]
    if "supportsEncryption" in kwargs:
        hub.log.debug(
            f"Got kwarg 'supportsEncryption' = {kwargs['supportsEncryption']}"
        )
        body["supportsEncryption"] = kwargs.get("supportsEncryption")
        del kwargs["supportsEncryption"]
    if "diskTargetProperties" in kwargs:
        hub.log.debug(
            f"Got kwarg 'diskTargetProperties' = {kwargs['diskTargetProperties']}"
        )
        body["diskTargetProperties"] = kwargs.get("diskTargetProperties")
        del kwargs["diskTargetProperties"]
    if "description" in kwargs:
        hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
        body["description"] = kwargs.get("description")
        del kwargs["description"]
    if "tags" in kwargs:
        hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
        body["tags"] = kwargs.get("tags")
        del kwargs["tags"]

    ret = api.create_storage_profile(body, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def create_v_sphere_storage_profile(
    hub, ctx, regionId, name, defaultItem, **kwargs
):
    """Create vSphere storage profile Create vSphere storage profile Performs POST /iaas/api/storage-profiles-vsphere


    :param string regionId: (required in body) The Id of the region that is associated with the storage profile.
    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param boolean defaultItem: (required in body) Indicates if a storage profile acts as a default storage profile for a
      disk.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param boolean supportsEncryption: (optional in body) Indicates whether this storage profile supports encryption or not.
    :param string sharesLevel: (optional in body) Shares are specified as High, Normal, Low or Custom and these values
      specify share values with a 4:2:1 ratio, respectively.
    :param string description: (optional in body) A human-friendly description.
    :param string diskMode: (optional in body) Type of mode for the disk
    :param string storagePolicyId: (optional in body) Id of the vSphere Storage Policy to be applied.
    :param array tags: (optional in body) A list of tags that represent the capabilities of this storage
      profile.
    :param string shares: (optional in body) A specific number of shares assigned to each virtual machine.
    :param string provisioningType: (optional in body) Type of provisioning policy for the disk.
    :param string limitIops: (optional in body) The upper bound for the I/O operations per second allocated for each
      virtual disk.
    :param string diskType: (optional in body) Disk types are specified as
      Standard - Simple vSphere virtual disks which cannot be managed
      independently without an attached VM.
      First Class - Improved version of standard virtual disks, designed to
      be fully mananged independent storage objects.
      Empty value is considered as Standard
    :param string datastoreId: (optional in body) Id of the vSphere Datastore for placing disk and VM.
    """

    hub.log.debug("POST /iaas/api/storage-profiles-vsphere")

    api = StorageProfileApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    body = {}
    body["regionId"] = regionId
    body["name"] = name
    body["defaultItem"] = defaultItem

    if "supportsEncryption" in kwargs:
        hub.log.debug(
            f"Got kwarg 'supportsEncryption' = {kwargs['supportsEncryption']}"
        )
        body["supportsEncryption"] = kwargs.get("supportsEncryption")
        del kwargs["supportsEncryption"]
    if "sharesLevel" in kwargs:
        hub.log.debug(f"Got kwarg 'sharesLevel' = {kwargs['sharesLevel']}")
        body["sharesLevel"] = kwargs.get("sharesLevel")
        del kwargs["sharesLevel"]
    if "description" in kwargs:
        hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
        body["description"] = kwargs.get("description")
        del kwargs["description"]
    if "diskMode" in kwargs:
        hub.log.debug(f"Got kwarg 'diskMode' = {kwargs['diskMode']}")
        body["diskMode"] = kwargs.get("diskMode")
        del kwargs["diskMode"]
    if "storagePolicyId" in kwargs:
        hub.log.debug(f"Got kwarg 'storagePolicyId' = {kwargs['storagePolicyId']}")
        body["storagePolicyId"] = kwargs.get("storagePolicyId")
        del kwargs["storagePolicyId"]
    if "tags" in kwargs:
        hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
        body["tags"] = kwargs.get("tags")
        del kwargs["tags"]
    if "shares" in kwargs:
        hub.log.debug(f"Got kwarg 'shares' = {kwargs['shares']}")
        body["shares"] = kwargs.get("shares")
        del kwargs["shares"]
    if "provisioningType" in kwargs:
        hub.log.debug(f"Got kwarg 'provisioningType' = {kwargs['provisioningType']}")
        body["provisioningType"] = kwargs.get("provisioningType")
        del kwargs["provisioningType"]
    if "limitIops" in kwargs:
        hub.log.debug(f"Got kwarg 'limitIops' = {kwargs['limitIops']}")
        body["limitIops"] = kwargs.get("limitIops")
        del kwargs["limitIops"]
    if "diskType" in kwargs:
        hub.log.debug(f"Got kwarg 'diskType' = {kwargs['diskType']}")
        body["diskType"] = kwargs.get("diskType")
        del kwargs["diskType"]
    if "datastoreId" in kwargs:
        hub.log.debug(f"Got kwarg 'datastoreId' = {kwargs['datastoreId']}")
        body["datastoreId"] = kwargs.get("datastoreId")
        del kwargs["datastoreId"]

    ret = api.create_v_sphere_storage_profile(body, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def delete_aws_storage_profile(hub, ctx, p_id, **kwargs):
    """Delete AWS storage profile Delete AWS storage profile with a given id Performs DELETE /iaas/api/storage-profiles-aws/{id}


    :param string p_id: (required in path) The ID of the storage profile.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    hub.log.debug("DELETE /iaas/api/storage-profiles-aws/{id}")

    api = StorageProfileApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.delete_aws_storage_profile(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def delete_azure_storage_profile(hub, ctx, p_id, **kwargs):
    """Delete Azure storage profile Delete Azure storage profile with a given id Performs DELETE /iaas/api/storage-profiles-azure/{id}


    :param string p_id: (required in path) The ID of the storage profile.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    hub.log.debug("DELETE /iaas/api/storage-profiles-azure/{id}")

    api = StorageProfileApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.delete_azure_storage_profile(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def delete_gcp_storage_profile(hub, ctx, p_id, **kwargs):
    """Delete GCP storage profile Delete GCP storage profile with a given id Performs DELETE /iaas/api/storage-profiles-gcp/{id}


    :param string p_id: (required in path) The ID of the storage profile.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    hub.log.debug("DELETE /iaas/api/storage-profiles-gcp/{id}")

    api = StorageProfileApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.delete_gcp_storage_profile(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def delete_storage_profile(hub, ctx, p_id, **kwargs):
    """Delete storage profile Delete storage profile with a given id Performs DELETE /iaas/api/storage-profiles/{id}


    :param string p_id: (required in path) The ID of the storage profile.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    hub.log.debug("DELETE /iaas/api/storage-profiles/{id}")

    api = StorageProfileApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.delete_storage_profile(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def delete_v_sphere_storage_profile(hub, ctx, p_id, **kwargs):
    """Delete vSphere storage profile Delete vSphere storage profile with a given id Performs DELETE /iaas/api/storage-profiles-vsphere/{id}


    :param string p_id: (required in path) The ID of the storage profile.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    hub.log.debug("DELETE /iaas/api/storage-profiles-vsphere/{id}")

    api = StorageProfileApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.delete_v_sphere_storage_profile(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_aws_storage_profile(hub, ctx, p_id, **kwargs):
    """Get AWS storage profile Get AWS storage profile with a given id Performs GET /iaas/api/storage-profiles-aws/{id}


    :param string p_id: (required in path) The ID of storage profile.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    hub.log.debug("GET /iaas/api/storage-profiles-aws/{id}")

    api = StorageProfileApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.get_aws_storage_profile(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_aws_storage_profiles(hub, ctx, **kwargs):
    """Get AWS storage profiles Get all AWS storage profiles Performs GET /iaas/api/storage-profiles-aws


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    hub.log.debug("GET /iaas/api/storage-profiles-aws")

    api = StorageProfileApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.get_aws_storage_profiles(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_azure_storage_profile(hub, ctx, p_id, **kwargs):
    """Get Azure storage profile Get Azure storage profile with a given id Performs GET /iaas/api/storage-profiles-azure/{id}


    :param string p_id: (required in path) The ID of storage profile.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    hub.log.debug("GET /iaas/api/storage-profiles-azure/{id}")

    api = StorageProfileApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.get_azure_storage_profile(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_azure_storage_profiles(hub, ctx, **kwargs):
    """Get Azure storage profiles Get all Azure storage profiles Performs GET /iaas/api/storage-profiles-azure


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    hub.log.debug("GET /iaas/api/storage-profiles-azure")

    api = StorageProfileApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.get_azure_storage_profiles(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_gcp_storage_profile(hub, ctx, p_id, **kwargs):
    """Get GCP storage profile Get GCP storage profile with a given id Performs GET /iaas/api/storage-profiles-gcp/{id}


    :param string p_id: (required in path) The ID of storage profile.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    hub.log.debug("GET /iaas/api/storage-profiles-gcp/{id}")

    api = StorageProfileApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.get_gcp_storage_profile(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_gcp_storage_profiles(hub, ctx, **kwargs):
    """Get GCP storage profiles Get all GCP storage profiles Performs GET /iaas/api/storage-profiles-gcp


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    hub.log.debug("GET /iaas/api/storage-profiles-gcp")

    api = StorageProfileApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.get_gcp_storage_profiles(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_storage_profile(hub, ctx, p_id, **kwargs):
    """Get storage profile Get storage profile with a given id Performs GET /iaas/api/storage-profiles/{id}


    :param string p_id: (required in path) The ID of storage profile.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    hub.log.debug("GET /iaas/api/storage-profiles/{id}")

    api = StorageProfileApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.get_storage_profile(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_storage_profiles(hub, ctx, **kwargs):
    """Get storage profiles Get all storage profiles Performs GET /iaas/api/storage-profiles


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    hub.log.debug("GET /iaas/api/storage-profiles")

    api = StorageProfileApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.get_storage_profiles(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_v_sphere_storage_profile(hub, ctx, p_id, **kwargs):
    """Get vSphere storage profile Get vSphere storage profile with a given id Performs GET /iaas/api/storage-profiles-vsphere/{id}


    :param string p_id: (required in path) The ID of storage profile.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    hub.log.debug("GET /iaas/api/storage-profiles-vsphere/{id}")

    api = StorageProfileApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.get_v_sphere_storage_profile(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_v_sphere_storage_profiles(hub, ctx, **kwargs):
    """Get vSphere storage profiles Get all vSphere storage profiles Performs GET /iaas/api/storage-profiles-vsphere


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    hub.log.debug("GET /iaas/api/storage-profiles-vsphere")

    api = StorageProfileApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.get_v_sphere_storage_profiles(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def replace_storage_profile(
    hub, ctx, p_id, regionId, name, defaultItem, **kwargs
):
    """Replace storage profile Replace storage profile with a given id Performs PUT /iaas/api/storage-profiles/{id}


    :param string p_id: (required in path) The ID of the storage profile.
    :param string regionId: (required in body) The Id of the region that is associated with the storage profile.
    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param boolean defaultItem: (required in body) Indicates if a storage profile is a default profile.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param object diskProperties: (optional in body) Map of storage properties that are to be applied on disk while
      provisioning.
    :param boolean supportsEncryption: (optional in body) Indicates whether this storage profile supports encryption or not.
    :param object diskTargetProperties: (optional in body) Map of storage placements to know where the disk is provisioned.
    :param string description: (optional in body) A human-friendly description.
    :param array tags: (optional in body) A list of tags that represent the capabilities of this storage profile
    """

    hub.log.debug("PUT /iaas/api/storage-profiles/{id}")

    api = StorageProfileApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    body = {}
    body["regionId"] = regionId
    body["name"] = name
    body["defaultItem"] = defaultItem

    if "diskProperties" in kwargs:
        hub.log.debug(f"Got kwarg 'diskProperties' = {kwargs['diskProperties']}")
        body["diskProperties"] = kwargs.get("diskProperties")
        del kwargs["diskProperties"]
    if "supportsEncryption" in kwargs:
        hub.log.debug(
            f"Got kwarg 'supportsEncryption' = {kwargs['supportsEncryption']}"
        )
        body["supportsEncryption"] = kwargs.get("supportsEncryption")
        del kwargs["supportsEncryption"]
    if "diskTargetProperties" in kwargs:
        hub.log.debug(
            f"Got kwarg 'diskTargetProperties' = {kwargs['diskTargetProperties']}"
        )
        body["diskTargetProperties"] = kwargs.get("diskTargetProperties")
        del kwargs["diskTargetProperties"]
    if "description" in kwargs:
        hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
        body["description"] = kwargs.get("description")
        del kwargs["description"]
    if "tags" in kwargs:
        hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
        body["tags"] = kwargs.get("tags")
        del kwargs["tags"]

    ret = api.replace_storage_profile(body, id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def update_aws_storage_profile(
    hub, ctx, p_id, deviceType, regionId, name, **kwargs
):
    """Update AWS storage profile Update AWS storage profile Performs PATCH /iaas/api/storage-profiles-aws/{id}


    :param string p_id: (required in path) The ID of the storage profile.
    :param string deviceType: (required in body) Indicates the type of storage.
    :param string regionId: (required in body) A link to the region that is associated with the storage profile.
    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string volumeType: (optional in body) Indicates the type of volume associated with type of storage.
    :param boolean supportsEncryption: (optional in body) Indicates whether this storage profile supports encryption or not.
    :param string iops: (optional in body) Indicates maximum I/O operations per second in range(1-20,000).
    :param string description: (optional in body) A human-friendly description.
    :param boolean defaultItem: (optional in body) Indicates if a storage profile is default or not.
    :param array tags: (optional in body) A list of tags that represent the capabilities of this storage profile
    """

    hub.log.debug("PATCH /iaas/api/storage-profiles-aws/{id}")

    api = StorageProfileApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    body = {}
    body["deviceType"] = deviceType
    body["regionId"] = regionId
    body["name"] = name

    if "volumeType" in kwargs:
        hub.log.debug(f"Got kwarg 'volumeType' = {kwargs['volumeType']}")
        body["volumeType"] = kwargs.get("volumeType")
        del kwargs["volumeType"]
    if "supportsEncryption" in kwargs:
        hub.log.debug(
            f"Got kwarg 'supportsEncryption' = {kwargs['supportsEncryption']}"
        )
        body["supportsEncryption"] = kwargs.get("supportsEncryption")
        del kwargs["supportsEncryption"]
    if "iops" in kwargs:
        hub.log.debug(f"Got kwarg 'iops' = {kwargs['iops']}")
        body["iops"] = kwargs.get("iops")
        del kwargs["iops"]
    if "description" in kwargs:
        hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
        body["description"] = kwargs.get("description")
        del kwargs["description"]
    if "defaultItem" in kwargs:
        hub.log.debug(f"Got kwarg 'defaultItem' = {kwargs['defaultItem']}")
        body["defaultItem"] = kwargs.get("defaultItem")
        del kwargs["defaultItem"]
    if "tags" in kwargs:
        hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
        body["tags"] = kwargs.get("tags")
        del kwargs["tags"]

    ret = api.update_aws_storage_profile(body, id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def update_azure_storage_profile(hub, ctx, p_id, regionId, name, **kwargs):
    """Update Azure storage profile Update Azure storage profile Performs PATCH /iaas/api/storage-profiles-azure/{id}


    :param string p_id: (required in path) The ID of the storage profile.
    :param string regionId: (required in body) The If of the region that is associated with the storage profile.
    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string storageAccountId: (optional in body) Id of a storage account where in the disk is placed.
    :param boolean supportsEncryption: (optional in body) Indicates whether this storage policy should support encryption or
      not.
    :param string description: (optional in body) A human-friendly description.
    :param boolean defaultItem: (optional in body) Indicates if a storage policy contains default storage properties.
    :param string diskType: (optional in body) Indicates the performance tier for the storage type. Premium disks are
      SSD backed and Standard disks are HDD backed.
    :param string dataDiskCaching: (optional in body) Indicates the caching mechanism for additional disk.
    :param string osDiskCaching: (optional in body) Indicates the caching mechanism for OS disk. Default policy for OS
      disks is Read/Write.
    :param string diskEncryptionSetId: (optional in body) Indicates the id of disk encryption set.
    :param array tags: (optional in body) A set of tag keys and optional values for a storage policy which
      define set of specifications for creating a disk.
    """

    hub.log.debug("PATCH /iaas/api/storage-profiles-azure/{id}")

    api = StorageProfileApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    body = {}
    body["regionId"] = regionId
    body["name"] = name

    if "storageAccountId" in kwargs:
        hub.log.debug(f"Got kwarg 'storageAccountId' = {kwargs['storageAccountId']}")
        body["storageAccountId"] = kwargs.get("storageAccountId")
        del kwargs["storageAccountId"]
    if "supportsEncryption" in kwargs:
        hub.log.debug(
            f"Got kwarg 'supportsEncryption' = {kwargs['supportsEncryption']}"
        )
        body["supportsEncryption"] = kwargs.get("supportsEncryption")
        del kwargs["supportsEncryption"]
    if "description" in kwargs:
        hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
        body["description"] = kwargs.get("description")
        del kwargs["description"]
    if "defaultItem" in kwargs:
        hub.log.debug(f"Got kwarg 'defaultItem' = {kwargs['defaultItem']}")
        body["defaultItem"] = kwargs.get("defaultItem")
        del kwargs["defaultItem"]
    if "diskType" in kwargs:
        hub.log.debug(f"Got kwarg 'diskType' = {kwargs['diskType']}")
        body["diskType"] = kwargs.get("diskType")
        del kwargs["diskType"]
    if "dataDiskCaching" in kwargs:
        hub.log.debug(f"Got kwarg 'dataDiskCaching' = {kwargs['dataDiskCaching']}")
        body["dataDiskCaching"] = kwargs.get("dataDiskCaching")
        del kwargs["dataDiskCaching"]
    if "osDiskCaching" in kwargs:
        hub.log.debug(f"Got kwarg 'osDiskCaching' = {kwargs['osDiskCaching']}")
        body["osDiskCaching"] = kwargs.get("osDiskCaching")
        del kwargs["osDiskCaching"]
    if "diskEncryptionSetId" in kwargs:
        hub.log.debug(
            f"Got kwarg 'diskEncryptionSetId' = {kwargs['diskEncryptionSetId']}"
        )
        body["diskEncryptionSetId"] = kwargs.get("diskEncryptionSetId")
        del kwargs["diskEncryptionSetId"]
    if "tags" in kwargs:
        hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
        body["tags"] = kwargs.get("tags")
        del kwargs["tags"]

    ret = api.update_azure_storage_profile(body, id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def update_gcp_storage_profile(
    hub, ctx, p_id, regionId, name, persistentDiskType, **kwargs
):
    """Update GCP storage profile Update GCP storage profile Performs PATCH /iaas/api/storage-profiles-gcp/{id}


    :param string p_id: (required in path) The ID of the storage profile.
    :param string regionId: (required in body) A link to the region that is associated with the storage profile.
    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param string persistentDiskType: (required in body) Indicates the type of disk.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param boolean supportsEncryption: (optional in body) Indicates whether this storage profile supports encryption or not.
    :param string description: (optional in body) A human-friendly description.
    :param boolean defaultItem: (optional in body) Indicates if a storage profile is default or not.
    :param array tags: (optional in body) A list of tags that represent the capabilities of this storage profile
    """

    hub.log.debug("PATCH /iaas/api/storage-profiles-gcp/{id}")

    api = StorageProfileApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    body = {}
    body["regionId"] = regionId
    body["name"] = name
    body["persistentDiskType"] = persistentDiskType

    if "supportsEncryption" in kwargs:
        hub.log.debug(
            f"Got kwarg 'supportsEncryption' = {kwargs['supportsEncryption']}"
        )
        body["supportsEncryption"] = kwargs.get("supportsEncryption")
        del kwargs["supportsEncryption"]
    if "description" in kwargs:
        hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
        body["description"] = kwargs.get("description")
        del kwargs["description"]
    if "defaultItem" in kwargs:
        hub.log.debug(f"Got kwarg 'defaultItem' = {kwargs['defaultItem']}")
        body["defaultItem"] = kwargs.get("defaultItem")
        del kwargs["defaultItem"]
    if "tags" in kwargs:
        hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
        body["tags"] = kwargs.get("tags")
        del kwargs["tags"]

    ret = api.update_gcp_storage_profile(body, id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def update_v_sphere_storage_profile(
    hub, ctx, p_id, regionId, name, defaultItem, **kwargs
):
    """Update vSphere storage profile Update vSphere storage profile Performs PATCH /iaas/api/storage-profiles-vsphere/{id}


    :param string p_id: (required in path) The ID of the storage profile.
    :param string regionId: (required in body) The Id of the region that is associated with the storage profile.
    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param boolean defaultItem: (required in body) Indicates if a storage profile acts as a default storage profile for a
      disk.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param boolean supportsEncryption: (optional in body) Indicates whether this storage profile supports encryption or not.
    :param string sharesLevel: (optional in body) Shares are specified as High, Normal, Low or Custom and these values
      specify share values with a 4:2:1 ratio, respectively.
    :param string description: (optional in body) A human-friendly description.
    :param string diskMode: (optional in body) Type of mode for the disk
    :param string storagePolicyId: (optional in body) Id of the vSphere Storage Policy to be applied.
    :param array tags: (optional in body) A list of tags that represent the capabilities of this storage
      profile.
    :param string shares: (optional in body) A specific number of shares assigned to each virtual machine.
    :param string provisioningType: (optional in body) Type of provisioning policy for the disk.
    :param string limitIops: (optional in body) The upper bound for the I/O operations per second allocated for each
      virtual disk.
    :param string diskType: (optional in body) Disk types are specified as
      Standard - Simple vSphere virtual disks which cannot be managed
      independently without an attached VM.
      First Class - Improved version of standard virtual disks, designed to
      be fully mananged independent storage objects.
      Empty value is considered as Standard
    :param string datastoreId: (optional in body) Id of the vSphere Datastore for placing disk and VM.
    """

    hub.log.debug("PATCH /iaas/api/storage-profiles-vsphere/{id}")

    api = StorageProfileApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    body = {}
    body["regionId"] = regionId
    body["name"] = name
    body["defaultItem"] = defaultItem

    if "supportsEncryption" in kwargs:
        hub.log.debug(
            f"Got kwarg 'supportsEncryption' = {kwargs['supportsEncryption']}"
        )
        body["supportsEncryption"] = kwargs.get("supportsEncryption")
        del kwargs["supportsEncryption"]
    if "sharesLevel" in kwargs:
        hub.log.debug(f"Got kwarg 'sharesLevel' = {kwargs['sharesLevel']}")
        body["sharesLevel"] = kwargs.get("sharesLevel")
        del kwargs["sharesLevel"]
    if "description" in kwargs:
        hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
        body["description"] = kwargs.get("description")
        del kwargs["description"]
    if "diskMode" in kwargs:
        hub.log.debug(f"Got kwarg 'diskMode' = {kwargs['diskMode']}")
        body["diskMode"] = kwargs.get("diskMode")
        del kwargs["diskMode"]
    if "storagePolicyId" in kwargs:
        hub.log.debug(f"Got kwarg 'storagePolicyId' = {kwargs['storagePolicyId']}")
        body["storagePolicyId"] = kwargs.get("storagePolicyId")
        del kwargs["storagePolicyId"]
    if "tags" in kwargs:
        hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
        body["tags"] = kwargs.get("tags")
        del kwargs["tags"]
    if "shares" in kwargs:
        hub.log.debug(f"Got kwarg 'shares' = {kwargs['shares']}")
        body["shares"] = kwargs.get("shares")
        del kwargs["shares"]
    if "provisioningType" in kwargs:
        hub.log.debug(f"Got kwarg 'provisioningType' = {kwargs['provisioningType']}")
        body["provisioningType"] = kwargs.get("provisioningType")
        del kwargs["provisioningType"]
    if "limitIops" in kwargs:
        hub.log.debug(f"Got kwarg 'limitIops' = {kwargs['limitIops']}")
        body["limitIops"] = kwargs.get("limitIops")
        del kwargs["limitIops"]
    if "diskType" in kwargs:
        hub.log.debug(f"Got kwarg 'diskType' = {kwargs['diskType']}")
        body["diskType"] = kwargs.get("diskType")
        del kwargs["diskType"]
    if "datastoreId" in kwargs:
        hub.log.debug(f"Got kwarg 'datastoreId' = {kwargs['datastoreId']}")
        body["datastoreId"] = kwargs.get("datastoreId")
        del kwargs["datastoreId"]

    ret = api.update_v_sphere_storage_profile(body, id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))

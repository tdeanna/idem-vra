from idem_vra.client.vra_iaas_lib.api import FabricComputeApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get_fabric_compute(hub, ctx, p_id, **kwargs):
    """Get fabric compute Get fabric compute with a given id Performs GET /iaas/api/fabric-computes/{id}


    :param string p_id: (required in path) The ID of the fabric compute.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    hub.log.debug("GET /iaas/api/fabric-computes/{id}")

    api = FabricComputeApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.get_fabric_compute(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_fabric_computes(hub, ctx, **kwargs):
    """Get fabric computes Get all fabric computes. Performs GET /iaas/api/fabric-computes


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param integer top: (optional in query) Number of records you want to get.
    :param integer skip: (optional in query) Number of records you want to skip.
    :param boolean count: (optional in query) Flag which when specified, regardless of the assigned value, shows the
      total number of records. If the collection has a filter it shows the
      number of records matching the filter.
    :param string filter: (optional in query) Filter the results by a specified predicate expression. Operators: eq,
      ne, and, or.
    """

    hub.log.debug("GET /iaas/api/fabric-computes")

    api = FabricComputeApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.get_fabric_computes(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def update_fabric_compute(hub, ctx, p_id, **kwargs):
    """Update fabric compute. Update fabric compute. Only tag updates are supported. Performs PATCH /iaas/api/fabric-computes/{id}


    :param string p_id: (required in path) The ID of the Fabric Compute.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param integer maximumAllowedMemoryAllocationPercent: (optional in body) What percent of the total available memory on the compute will be used
      for VM provisioning.This value can be more than 100. e.g. If the
      compute has 100gb of memory and this value is set to80, then vRA will
      act as if this compute has only 80gb. If it is 120, then vRA will act
      as if this compute has 120gb thus allowing 20gb overallocation.
      Applies only for private cloud computes.
    :param array tags: (optional in body) A set of tag keys and optional values that were set on this resource
      instance.
    """

    hub.log.debug("PATCH /iaas/api/fabric-computes/{id}")

    api = FabricComputeApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    body = {}

    if "maximumAllowedMemoryAllocationPercent" in kwargs:
        hub.log.debug(
            f"Got kwarg 'maximumAllowedMemoryAllocationPercent' = {kwargs['maximumAllowedMemoryAllocationPercent']}"
        )
        body["maximumAllowedMemoryAllocationPercent"] = kwargs.get(
            "maximumAllowedMemoryAllocationPercent"
        )
        del kwargs["maximumAllowedMemoryAllocationPercent"]
    if "tags" in kwargs:
        hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
        body["tags"] = kwargs.get("tags")
        del kwargs["tags"]

    ret = api.update_fabric_compute(body, id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))

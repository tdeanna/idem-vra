from idem_vra.client.vra_iaas_lib.api import FabricNetworkApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get_fabric_network(hub, ctx, p_id, **kwargs):
    """Get fabric network Get fabric network with a given id Performs GET /iaas/api/fabric-networks/{id}


    :param string p_id: (required in path) The ID of the fabric network.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string select: (optional in query) Select a subset of properties to include in the response.
    """

    hub.log.debug("GET /iaas/api/fabric-networks/{id}")

    api = FabricNetworkApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.get_fabric_network(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_fabric_networks(hub, ctx, **kwargs):
    """Get fabric networks Get all fabric networks. Performs GET /iaas/api/fabric-networks


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param integer top: (optional in query) Number of records you want to get.
    :param integer skip: (optional in query) Number of records you want to skip.
    :param boolean count: (optional in query) Flag which when specified, regardless of the assigned value, shows the
      total number of records. If the collection has a filter it shows the
      number of records matching the filter.
    :param string select: (optional in query) Select a subset of properties to include in the response.
    :param string filter: (optional in query) Filter the results by a specified predicate expression. Operators: eq,
      ne, and, or.
    """

    hub.log.debug("GET /iaas/api/fabric-networks")

    api = FabricNetworkApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.get_fabric_networks(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_vsphere_fabric_network(hub, ctx, p_id, **kwargs):
    """Get vSphere fabric network Get vSphere fabric network with a given id Performs GET /iaas/api/fabric-networks-vsphere/{id}


    :param string p_id: (required in path) The ID of the fabric network.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string select: (optional in query) Select a subset of properties to include in the response.
    """

    hub.log.debug("GET /iaas/api/fabric-networks-vsphere/{id}")

    api = FabricNetworkApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.get_vsphere_fabric_network(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_vsphere_fabric_networks(hub, ctx, **kwargs):
    """Get vSphere fabric networks Get all vSphere fabric networks. Performs GET /iaas/api/fabric-networks-vsphere


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param integer top: (optional in query) Number of records you want to get.
    :param integer skip: (optional in query) Number of records you want to skip.
    :param boolean count: (optional in query) Flag which when specified, regardless of the assigned value, shows the
      total number of records. If the collection has a filter it shows the
      number of records matching the filter.
    :param string select: (optional in query) Select a subset of properties to include in the response.
    :param string filter: (optional in query) Filter the results by a specified predicate expression. Operators: eq,
      ne, and, or.
    """

    hub.log.debug("GET /iaas/api/fabric-networks-vsphere")

    api = FabricNetworkApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.get_vsphere_fabric_networks(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def update_fabric_network(hub, ctx, p_id, **kwargs):
    """Update fabric network. Update fabric network. Only tag updates are supported. Performs PATCH /iaas/api/fabric-networks/{id}


    :param string p_id: (required in path) The ID of the Fabric Network.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param array tags: (optional in body) A set of tag keys and optional values that were set on this resource
      instance.
    """

    hub.log.debug("PATCH /iaas/api/fabric-networks/{id}")

    api = FabricNetworkApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    body = {}

    if "tags" in kwargs:
        hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
        body["tags"] = kwargs.get("tags")
        del kwargs["tags"]

    ret = api.update_fabric_network(body, id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def updatev_sphere_fabric_network(hub, ctx, p_id, **kwargs):
    """Update vSphere fabric network. Update vSphere fabric network. Performs PATCH /iaas/api/fabric-networks-vsphere/{id}


    :param string p_id: (required in path) The ID of the vSphere Fabric Network.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string ipv6Cidr: (optional in body) Network IPv6 CIDR to be used.
    :param boolean isDefault: (optional in body) Indicates whether this is the default subnet for the zone.
    :param string domain: (optional in body) Domain value.
    :param string defaultIpv6Gateway: (optional in body) IPv6 default gateway to be used.
    :param array dnsServerAddresses: (optional in body) A list of DNS server addresses that were set on this resource
      instance.
    :param boolean isPublic: (optional in body) Indicates whether the sub-network supports public IP assignment.
    :param string cidr: (optional in body) Network CIDR to be used.
    :param string defaultGateway: (optional in body) IPv4 default gateway to be used.
    :param array tags: (optional in body) A set of tag keys and optional values that were set on this resource
      instance.
    :param array dnsSearchDomains: (optional in body) A list of DNS search domains that were set on this resource instance.
    """

    hub.log.debug("PATCH /iaas/api/fabric-networks-vsphere/{id}")

    api = FabricNetworkApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    body = {}

    if "ipv6Cidr" in kwargs:
        hub.log.debug(f"Got kwarg 'ipv6Cidr' = {kwargs['ipv6Cidr']}")
        body["ipv6Cidr"] = kwargs.get("ipv6Cidr")
        del kwargs["ipv6Cidr"]
    if "isDefault" in kwargs:
        hub.log.debug(f"Got kwarg 'isDefault' = {kwargs['isDefault']}")
        body["isDefault"] = kwargs.get("isDefault")
        del kwargs["isDefault"]
    if "domain" in kwargs:
        hub.log.debug(f"Got kwarg 'domain' = {kwargs['domain']}")
        body["domain"] = kwargs.get("domain")
        del kwargs["domain"]
    if "defaultIpv6Gateway" in kwargs:
        hub.log.debug(
            f"Got kwarg 'defaultIpv6Gateway' = {kwargs['defaultIpv6Gateway']}"
        )
        body["defaultIpv6Gateway"] = kwargs.get("defaultIpv6Gateway")
        del kwargs["defaultIpv6Gateway"]
    if "dnsServerAddresses" in kwargs:
        hub.log.debug(
            f"Got kwarg 'dnsServerAddresses' = {kwargs['dnsServerAddresses']}"
        )
        body["dnsServerAddresses"] = kwargs.get("dnsServerAddresses")
        del kwargs["dnsServerAddresses"]
    if "isPublic" in kwargs:
        hub.log.debug(f"Got kwarg 'isPublic' = {kwargs['isPublic']}")
        body["isPublic"] = kwargs.get("isPublic")
        del kwargs["isPublic"]
    if "cidr" in kwargs:
        hub.log.debug(f"Got kwarg 'cidr' = {kwargs['cidr']}")
        body["cidr"] = kwargs.get("cidr")
        del kwargs["cidr"]
    if "defaultGateway" in kwargs:
        hub.log.debug(f"Got kwarg 'defaultGateway' = {kwargs['defaultGateway']}")
        body["defaultGateway"] = kwargs.get("defaultGateway")
        del kwargs["defaultGateway"]
    if "tags" in kwargs:
        hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
        body["tags"] = kwargs.get("tags")
        del kwargs["tags"]
    if "dnsSearchDomains" in kwargs:
        hub.log.debug(f"Got kwarg 'dnsSearchDomains' = {kwargs['dnsSearchDomains']}")
        body["dnsSearchDomains"] = kwargs.get("dnsSearchDomains")
        del kwargs["dnsSearchDomains"]

    ret = api.updatev_sphere_fabric_network(body, id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))

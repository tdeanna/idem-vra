from idem_vra.client.vra_iaas_lib.api import AboutApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get_about_page(hub, ctx, **kwargs):
    """Get about page The page contains information about the supported API versions and the latest
    API version. The version parameter is optional but highly recommended.
    If you do not specify explicitly an exact version, you will be calling the
    latest supported General Availability API version.
    Here is an example of a call which specifies the exact version you are using:
    `GET /iaas/api/network-profiles?apiVersion=2021-07-15`
    Note that this version is deprecated: 2019-01-15. Performs GET /iaas/api/about


    """

    hub.log.debug("GET /iaas/api/about")

    api = AboutApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])

    ret = api.get_about_page(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))

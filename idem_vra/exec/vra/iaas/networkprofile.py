from idem_vra.client.vra_iaas_lib.api import NetworkProfileApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def create_network_profile(hub, ctx, regionId, name, **kwargs):
    """Create network profile Create network profile Performs POST /iaas/api/network-profiles


    :param string regionId: (required in body) The Id of the region for which this profile is created
    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string description: (optional in body) A human-friendly description.
    :param string isolationNetworkDomainCIDR: (optional in body) CIDR of the isolation network domain.
    :param string isolationNetworkDomainId: (optional in body) The Id of the network domain used for creating isolated networks.
    :param array tags: (optional in body) A set of tag keys and optional values that should be set on any
      resource that is produced from this specification.
    :param array externalIpBlockIds: (optional in body) List of external IP blocks coming from an external IPAM provider that
      can be used to create subnetworks inside them
    :param array fabricNetworkIds: (optional in body) A list of fabric network Ids which are assigned to the network
      profile.
    :param object customProperties: (optional in body) Additional properties that may be used to extend the Network Profile
      object that is produced from this specification.  For isolationType
      security group, datastoreId identifies the Compute Resource Edge
      datastore. computeCluster and resourcePoolId identify the Compute
      Resource Edge cluster. For isolationType subnet,
      distributedLogicalRouterStateLink identifies the on-demand network
      distributed local router (NSX-V only). For isolationType subnet,
      tier0LogicalRouterStateLink identifies the on-demand network tier-0
      logical router (NSX-T only). onDemandNetworkIPAssignmentType
      identifies the on-demand network IP range assignment type static,
      dynamic, or mixed.
    :param array securityGroupIds: (optional in body) A list of security group Ids which are assigned to the network
      profile.
    :param string isolationExternalFabricNetworkId: (optional in body) The Id of the fabric network used for outbound access.
    :param string isolationType: (optional in body) Specifies the isolation type e.g. none, subnet or security group
    :param integer isolatedNetworkCIDRPrefix: (optional in body) The CIDR prefix length to be used for the isolated networks that are
      created with the network profile.
    :param array loadBalancerIds: (optional in body) A list of load balancers which are assigned to the network profile.
    """

    hub.log.debug("POST /iaas/api/network-profiles")

    api = NetworkProfileApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    body = {}
    body["regionId"] = regionId
    body["name"] = name

    if "description" in kwargs:
        hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
        body["description"] = kwargs.get("description")
        del kwargs["description"]
    if "isolationNetworkDomainCIDR" in kwargs:
        hub.log.debug(
            f"Got kwarg 'isolationNetworkDomainCIDR' = {kwargs['isolationNetworkDomainCIDR']}"
        )
        body["isolationNetworkDomainCIDR"] = kwargs.get("isolationNetworkDomainCIDR")
        del kwargs["isolationNetworkDomainCIDR"]
    if "isolationNetworkDomainId" in kwargs:
        hub.log.debug(
            f"Got kwarg 'isolationNetworkDomainId' = {kwargs['isolationNetworkDomainId']}"
        )
        body["isolationNetworkDomainId"] = kwargs.get("isolationNetworkDomainId")
        del kwargs["isolationNetworkDomainId"]
    if "tags" in kwargs:
        hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
        body["tags"] = kwargs.get("tags")
        del kwargs["tags"]
    if "externalIpBlockIds" in kwargs:
        hub.log.debug(
            f"Got kwarg 'externalIpBlockIds' = {kwargs['externalIpBlockIds']}"
        )
        body["externalIpBlockIds"] = kwargs.get("externalIpBlockIds")
        del kwargs["externalIpBlockIds"]
    if "fabricNetworkIds" in kwargs:
        hub.log.debug(f"Got kwarg 'fabricNetworkIds' = {kwargs['fabricNetworkIds']}")
        body["fabricNetworkIds"] = kwargs.get("fabricNetworkIds")
        del kwargs["fabricNetworkIds"]
    if "customProperties" in kwargs:
        hub.log.debug(f"Got kwarg 'customProperties' = {kwargs['customProperties']}")
        body["customProperties"] = kwargs.get("customProperties")
        del kwargs["customProperties"]
    if "securityGroupIds" in kwargs:
        hub.log.debug(f"Got kwarg 'securityGroupIds' = {kwargs['securityGroupIds']}")
        body["securityGroupIds"] = kwargs.get("securityGroupIds")
        del kwargs["securityGroupIds"]
    if "isolationExternalFabricNetworkId" in kwargs:
        hub.log.debug(
            f"Got kwarg 'isolationExternalFabricNetworkId' = {kwargs['isolationExternalFabricNetworkId']}"
        )
        body["isolationExternalFabricNetworkId"] = kwargs.get(
            "isolationExternalFabricNetworkId"
        )
        del kwargs["isolationExternalFabricNetworkId"]
    if "isolationType" in kwargs:
        hub.log.debug(f"Got kwarg 'isolationType' = {kwargs['isolationType']}")
        body["isolationType"] = kwargs.get("isolationType")
        del kwargs["isolationType"]
    if "isolatedNetworkCIDRPrefix" in kwargs:
        hub.log.debug(
            f"Got kwarg 'isolatedNetworkCIDRPrefix' = {kwargs['isolatedNetworkCIDRPrefix']}"
        )
        body["isolatedNetworkCIDRPrefix"] = kwargs.get("isolatedNetworkCIDRPrefix")
        del kwargs["isolatedNetworkCIDRPrefix"]
    if "loadBalancerIds" in kwargs:
        hub.log.debug(f"Got kwarg 'loadBalancerIds' = {kwargs['loadBalancerIds']}")
        body["loadBalancerIds"] = kwargs.get("loadBalancerIds")
        del kwargs["loadBalancerIds"]

    ret = api.create_network_profile(body, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def delete_network_profile(hub, ctx, p_id, **kwargs):
    """Delete network profile Delete network profile with a given id Performs DELETE /iaas/api/network-profiles/{id}


    :param string p_id: (required in path) The ID of the network profile.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    hub.log.debug("DELETE /iaas/api/network-profiles/{id}")

    api = NetworkProfileApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.delete_network_profile(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_network_profile(hub, ctx, p_id, **kwargs):
    """Get network profile Get network profile with a given id Performs GET /iaas/api/network-profiles/{id}


    :param string p_id: (required in path) The ID of the network profile.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    hub.log.debug("GET /iaas/api/network-profiles/{id}")

    api = NetworkProfileApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.get_network_profile(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_network_profiles(hub, ctx, **kwargs):
    """Get network profiles Get all network profiles Performs GET /iaas/api/network-profiles


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param integer top: (optional in query) Number of records you want to get.
    :param integer skip: (optional in query) Number of records you want to skip.
    :param boolean count: (optional in query) Flag which when specified, regardless of the assigned value, shows the
      total number of records. If the collection has a filter it shows the
      number of records matching the filter.
    :param string select: (optional in query) Select a subset of properties to include in the response.
    :param string filter: (optional in query) Filter the results by a specified predicate expression. Operators: eq,
      ne, and, or.
    """

    hub.log.debug("GET /iaas/api/network-profiles")

    api = NetworkProfileApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.get_network_profiles(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def update_network_profile(hub, ctx, p_id, regionId, name, **kwargs):
    """Update network profile Update network profile Performs PATCH /iaas/api/network-profiles/{id}


    :param string p_id: (required in path) The ID of the network profile.
    :param string regionId: (required in body) The Id of the region for which this profile is created
    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string description: (optional in body) A human-friendly description.
    :param string isolationNetworkDomainCIDR: (optional in body) CIDR of the isolation network domain.
    :param string isolationNetworkDomainId: (optional in body) The Id of the network domain used for creating isolated networks.
    :param array tags: (optional in body) A set of tag keys and optional values that should be set on any
      resource that is produced from this specification.
    :param array externalIpBlockIds: (optional in body) List of external IP blocks coming from an external IPAM provider that
      can be used to create subnetworks inside them
    :param array fabricNetworkIds: (optional in body) A list of fabric network Ids which are assigned to the network
      profile.
    :param object customProperties: (optional in body) Additional properties that may be used to extend the Network Profile
      object that is produced from this specification.  For isolationType
      security group, datastoreId identifies the Compute Resource Edge
      datastore. computeCluster and resourcePoolId identify the Compute
      Resource Edge cluster. For isolationType subnet,
      distributedLogicalRouterStateLink identifies the on-demand network
      distributed local router (NSX-V only). For isolationType subnet,
      tier0LogicalRouterStateLink identifies the on-demand network tier-0
      logical router (NSX-T only). onDemandNetworkIPAssignmentType
      identifies the on-demand network IP range assignment type static,
      dynamic, or mixed.
    :param array securityGroupIds: (optional in body) A list of security group Ids which are assigned to the network
      profile.
    :param string isolationExternalFabricNetworkId: (optional in body) The Id of the fabric network used for outbound access.
    :param string isolationType: (optional in body) Specifies the isolation type e.g. none, subnet or security group
    :param integer isolatedNetworkCIDRPrefix: (optional in body) The CIDR prefix length to be used for the isolated networks that are
      created with the network profile.
    :param array loadBalancerIds: (optional in body) A list of load balancers which are assigned to the network profile.
    """

    hub.log.debug("PATCH /iaas/api/network-profiles/{id}")

    api = NetworkProfileApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    body = {}
    body["regionId"] = regionId
    body["name"] = name

    if "description" in kwargs:
        hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
        body["description"] = kwargs.get("description")
        del kwargs["description"]
    if "isolationNetworkDomainCIDR" in kwargs:
        hub.log.debug(
            f"Got kwarg 'isolationNetworkDomainCIDR' = {kwargs['isolationNetworkDomainCIDR']}"
        )
        body["isolationNetworkDomainCIDR"] = kwargs.get("isolationNetworkDomainCIDR")
        del kwargs["isolationNetworkDomainCIDR"]
    if "isolationNetworkDomainId" in kwargs:
        hub.log.debug(
            f"Got kwarg 'isolationNetworkDomainId' = {kwargs['isolationNetworkDomainId']}"
        )
        body["isolationNetworkDomainId"] = kwargs.get("isolationNetworkDomainId")
        del kwargs["isolationNetworkDomainId"]
    if "tags" in kwargs:
        hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
        body["tags"] = kwargs.get("tags")
        del kwargs["tags"]
    if "externalIpBlockIds" in kwargs:
        hub.log.debug(
            f"Got kwarg 'externalIpBlockIds' = {kwargs['externalIpBlockIds']}"
        )
        body["externalIpBlockIds"] = kwargs.get("externalIpBlockIds")
        del kwargs["externalIpBlockIds"]
    if "fabricNetworkIds" in kwargs:
        hub.log.debug(f"Got kwarg 'fabricNetworkIds' = {kwargs['fabricNetworkIds']}")
        body["fabricNetworkIds"] = kwargs.get("fabricNetworkIds")
        del kwargs["fabricNetworkIds"]
    if "customProperties" in kwargs:
        hub.log.debug(f"Got kwarg 'customProperties' = {kwargs['customProperties']}")
        body["customProperties"] = kwargs.get("customProperties")
        del kwargs["customProperties"]
    if "securityGroupIds" in kwargs:
        hub.log.debug(f"Got kwarg 'securityGroupIds' = {kwargs['securityGroupIds']}")
        body["securityGroupIds"] = kwargs.get("securityGroupIds")
        del kwargs["securityGroupIds"]
    if "isolationExternalFabricNetworkId" in kwargs:
        hub.log.debug(
            f"Got kwarg 'isolationExternalFabricNetworkId' = {kwargs['isolationExternalFabricNetworkId']}"
        )
        body["isolationExternalFabricNetworkId"] = kwargs.get(
            "isolationExternalFabricNetworkId"
        )
        del kwargs["isolationExternalFabricNetworkId"]
    if "isolationType" in kwargs:
        hub.log.debug(f"Got kwarg 'isolationType' = {kwargs['isolationType']}")
        body["isolationType"] = kwargs.get("isolationType")
        del kwargs["isolationType"]
    if "isolatedNetworkCIDRPrefix" in kwargs:
        hub.log.debug(
            f"Got kwarg 'isolatedNetworkCIDRPrefix' = {kwargs['isolatedNetworkCIDRPrefix']}"
        )
        body["isolatedNetworkCIDRPrefix"] = kwargs.get("isolatedNetworkCIDRPrefix")
        del kwargs["isolatedNetworkCIDRPrefix"]
    if "loadBalancerIds" in kwargs:
        hub.log.debug(f"Got kwarg 'loadBalancerIds' = {kwargs['loadBalancerIds']}")
        body["loadBalancerIds"] = kwargs.get("loadBalancerIds")
        del kwargs["loadBalancerIds"]

    ret = api.update_network_profile(body, id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))

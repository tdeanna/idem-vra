from idem_vra.client.vra_iaas_lib.api import TagsApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get_tags(hub, ctx, q_apiVersion, **kwargs):
    """Get tags Get all tags Performs GET /iaas/api/tags


    :param string q_apiVersion: (required in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param integer top: (optional in query) Number of records you want to get.
    :param integer skip: (optional in query) Number of records you want to skip.
    :param boolean count: (optional in query) Flag which when specified, regardless of the assigned value, shows the
      total number of records. If the collection has a filter it shows the
      number of records matching the filter.
    :param string select: (optional in query) Select a subset of properties to include in the response.
    :param string filter: (optional in query) Filter the results by a specified predicate expression. Operators: eq,
      ne, and, or.
    """

    hub.log.debug("GET /iaas/api/tags")

    api = TagsApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])

    ret = api.get_tags(api_version=q_apiVersion, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))

from idem_vra.client.vra_iaas_lib.api import FabricFlavorsApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get_fabric_flavors(hub, ctx, **kwargs):
    """Get fabric flavors Get all fabric flavors Performs GET /iaas/api/fabric-flavors


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    hub.log.debug("GET /iaas/api/fabric-flavors")

    api = FabricFlavorsApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.get_fabric_flavors(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))

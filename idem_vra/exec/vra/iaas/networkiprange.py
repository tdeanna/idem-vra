from idem_vra.client.vra_iaas_lib.api import NetworkIPRangeApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def create_internal_network_ip_range(
    hub, ctx, name, startIPAddress, endIPAddress, **kwargs
):
    """Create internal network IP range Creates an internal network IP range. Performs POST /iaas/api/network-ip-ranges


    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param string startIPAddress: (required in body) Start IP address of the range.
    :param string endIPAddress: (required in body) End IP address of the range.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param array fabricNetworkIds: (optional in body) The Ids of the fabric networks.
    :param string ipVersion: (optional in body) IP address version: IPv4 or IPv6. Default: IPv4.
    :param string description: (optional in body) A human-friendly description.
    :param array tags: (optional in body) A set of tag keys and optional values that were set on this resource
      instance.
    """

    hub.log.debug("POST /iaas/api/network-ip-ranges")

    api = NetworkIPRangeApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    body = {}
    body["name"] = name
    body["startIPAddress"] = startIPAddress
    body["endIPAddress"] = endIPAddress

    if "fabricNetworkIds" in kwargs:
        hub.log.debug(f"Got kwarg 'fabricNetworkIds' = {kwargs['fabricNetworkIds']}")
        body["fabricNetworkIds"] = kwargs.get("fabricNetworkIds")
        del kwargs["fabricNetworkIds"]
    if "ipVersion" in kwargs:
        hub.log.debug(f"Got kwarg 'ipVersion' = {kwargs['ipVersion']}")
        body["ipVersion"] = kwargs.get("ipVersion")
        del kwargs["ipVersion"]
    if "description" in kwargs:
        hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
        body["description"] = kwargs.get("description")
        del kwargs["description"]
    if "tags" in kwargs:
        hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
        body["tags"] = kwargs.get("tags")
        del kwargs["tags"]

    ret = api.create_internal_network_ip_range(body, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def delete_internal_network_ip_range(hub, ctx, p_id, **kwargs):
    """Delete internal network IP range Delete internal network IP range with a given id Performs DELETE /iaas/api/network-ip-ranges/{id}


    :param string p_id: (required in path) The ID of the network IP range.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    hub.log.debug("DELETE /iaas/api/network-ip-ranges/{id}")

    api = NetworkIPRangeApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.delete_internal_network_ip_range(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_external_ip_block(hub, ctx, p_id, **kwargs):
    """Get specific external IP block by id An external IP block is network coming from external IPAM provider that can be
      used to create subnetworks inside it Performs GET /iaas/api/external-ip-blocks/{id}


    :param string p_id: (required in path) The ID of the external IP block
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    hub.log.debug("GET /iaas/api/external-ip-blocks/{id}")

    api = NetworkIPRangeApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.get_external_ip_block(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_external_ip_blocks(hub, ctx, **kwargs):
    """Get all external IP blocks An external IP block is network coming from external IPAM provider that can be
      used to create subnetworks inside it Performs GET /iaas/api/external-ip-blocks


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    hub.log.debug("GET /iaas/api/external-ip-blocks")

    api = NetworkIPRangeApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.get_external_ip_blocks(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_external_network_ip_range(hub, ctx, p_id, **kwargs):
    """Get external IPAM network IP range Get external IPAM network IP range with a given id Performs GET /iaas/api/external-network-ip-ranges/{id}


    :param string p_id: (required in path) The ID of the external IPAM network IP range.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    hub.log.debug("GET /iaas/api/external-network-ip-ranges/{id}")

    api = NetworkIPRangeApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.get_external_network_ip_range(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_external_network_ip_ranges(hub, ctx, **kwargs):
    """Get external IPAM network IP ranges Get all external IPAM network IP ranges Performs GET /iaas/api/external-network-ip-ranges


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    hub.log.debug("GET /iaas/api/external-network-ip-ranges")

    api = NetworkIPRangeApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.get_external_network_ip_ranges(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_internal_network_ip_range(hub, ctx, p_id, **kwargs):
    """Get internal IPAM network IP range Get internal IPAM network IP range with a given id Performs GET /iaas/api/network-ip-ranges/{id}


    :param string p_id: (required in path) The ID of the network IP range.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    hub.log.debug("GET /iaas/api/network-ip-ranges/{id}")

    api = NetworkIPRangeApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.get_internal_network_ip_range(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_internal_network_ip_ranges(hub, ctx, **kwargs):
    """Get internal IPAM network IP ranges Get all internal IPAM network IP ranges Performs GET /iaas/api/network-ip-ranges


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    hub.log.debug("GET /iaas/api/network-ip-ranges")

    api = NetworkIPRangeApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.get_internal_network_ip_ranges(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def update_external_network_ip_range(hub, ctx, p_id, **kwargs):
    """Update external IPAM network IP range. Assign the external IPAM network IP range to a different network and/or change
      the tags of the external IPAM network IP range. Performs PATCH /iaas/api/external-network-ip-ranges/{id}


    :param string p_id: (required in path) The ID of the external IPAM network IP range.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param array fabricNetworkIds: (optional in body) A list of fabric network Ids that this IP range should be associated
      with.
    """

    hub.log.debug("PATCH /iaas/api/external-network-ip-ranges/{id}")

    api = NetworkIPRangeApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    body = {}

    if "fabricNetworkIds" in kwargs:
        hub.log.debug(f"Got kwarg 'fabricNetworkIds' = {kwargs['fabricNetworkIds']}")
        body["fabricNetworkIds"] = kwargs.get("fabricNetworkIds")
        del kwargs["fabricNetworkIds"]

    ret = api.update_external_network_ip_range(body, id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def update_internal_network_ip_range(
    hub, ctx, p_id, name, startIPAddress, endIPAddress, **kwargs
):
    """Update internal network IP range. Update internal network IP range. Performs PATCH /iaas/api/network-ip-ranges/{id}


    :param string p_id: (required in path) The ID of the network IP range.
    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param string startIPAddress: (required in body) Start IP address of the range.
    :param string endIPAddress: (required in body) End IP address of the range.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param array fabricNetworkIds: (optional in body) The Ids of the fabric networks.
    :param string ipVersion: (optional in body) IP address version: IPv4 or IPv6. Default: IPv4.
    :param string description: (optional in body) A human-friendly description.
    :param array tags: (optional in body) A set of tag keys and optional values that were set on this resource
      instance.
    """

    hub.log.debug("PATCH /iaas/api/network-ip-ranges/{id}")

    api = NetworkIPRangeApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    body = {}
    body["name"] = name
    body["startIPAddress"] = startIPAddress
    body["endIPAddress"] = endIPAddress

    if "fabricNetworkIds" in kwargs:
        hub.log.debug(f"Got kwarg 'fabricNetworkIds' = {kwargs['fabricNetworkIds']}")
        body["fabricNetworkIds"] = kwargs.get("fabricNetworkIds")
        del kwargs["fabricNetworkIds"]
    if "ipVersion" in kwargs:
        hub.log.debug(f"Got kwarg 'ipVersion' = {kwargs['ipVersion']}")
        body["ipVersion"] = kwargs.get("ipVersion")
        del kwargs["ipVersion"]
    if "description" in kwargs:
        hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
        body["description"] = kwargs.get("description")
        del kwargs["description"]
    if "tags" in kwargs:
        hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
        body["tags"] = kwargs.get("tags")
        del kwargs["tags"]

    ret = api.update_internal_network_ip_range(body, id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))

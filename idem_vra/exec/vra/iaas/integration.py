from idem_vra.client.vra_iaas_lib.api import IntegrationApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def create_integration_async(
    hub, ctx, integrationType, integrationProperties, name, **kwargs
):
    """Create an integration Create an integration in the current organization asynchronously Performs POST /iaas/api/integrations


    :param string integrationType: (required in body) Integration type
    :param object integrationProperties: (required in body) Integration specific properties supplied in as name value pairs
    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string validateOnly: (optional in query) Only validate provided Integration Specification. Integration will not
      be created.
    :param string privateKey: (optional in body) Secret access key or password to be used to authenticate with the
      integration
    :param array associatedCloudAccountIds: (optional in body) Cloud accounts to associate with this integration
    :param object customProperties: (optional in body) Additional custom properties that may be used to extend the
      Integration.
    :param string description: (optional in body) A human-friendly description.
    :param Any certificateInfo: (optional in body)
    :param string privateKeyId: (optional in body) Access key id or username to be used to authenticate with the
      integration
    :param array tags: (optional in body) A set of tag keys and optional values to set on the Integration
    """

    hub.log.debug("POST /iaas/api/integrations")

    api = IntegrationApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    body = {}
    body["integrationType"] = integrationType
    body["integrationProperties"] = integrationProperties
    body["name"] = name

    if "privateKey" in kwargs:
        hub.log.debug(f"Got kwarg 'privateKey' = {kwargs['privateKey']}")
        body["privateKey"] = kwargs.get("privateKey")
        del kwargs["privateKey"]
    if "associatedCloudAccountIds" in kwargs:
        hub.log.debug(
            f"Got kwarg 'associatedCloudAccountIds' = {kwargs['associatedCloudAccountIds']}"
        )
        body["associatedCloudAccountIds"] = kwargs.get("associatedCloudAccountIds")
        del kwargs["associatedCloudAccountIds"]
    if "customProperties" in kwargs:
        hub.log.debug(f"Got kwarg 'customProperties' = {kwargs['customProperties']}")
        body["customProperties"] = kwargs.get("customProperties")
        del kwargs["customProperties"]
    if "description" in kwargs:
        hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
        body["description"] = kwargs.get("description")
        del kwargs["description"]
    if "certificateInfo" in kwargs:
        hub.log.debug(f"Got kwarg 'certificateInfo' = {kwargs['certificateInfo']}")
        body["certificateInfo"] = kwargs.get("certificateInfo")
        del kwargs["certificateInfo"]
    if "privateKeyId" in kwargs:
        hub.log.debug(f"Got kwarg 'privateKeyId' = {kwargs['privateKeyId']}")
        body["privateKeyId"] = kwargs.get("privateKeyId")
        del kwargs["privateKeyId"]
    if "tags" in kwargs:
        hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
        body["tags"] = kwargs.get("tags")
        del kwargs["tags"]

    ret = api.create_integration_async(body, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def delete_integration(hub, ctx, p_id, **kwargs):
    """Delete an integration Delete an integration with a given id asynchronously Performs DELETE /iaas/api/integrations/{id}


    :param string p_id: (required in path) The ID of the Integration
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    hub.log.debug("DELETE /iaas/api/integrations/{id}")

    api = IntegrationApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.delete_integration(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_integration(hub, ctx, p_id, **kwargs):
    """Get an integration Get an integration with a given id Performs GET /iaas/api/integrations/{id}


    :param string p_id: (required in path) The ID of the Integration
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string select: (optional in query) Select a subset of properties to include in the response.
    """

    hub.log.debug("GET /iaas/api/integrations/{id}")

    api = IntegrationApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.get_integration(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_integrations(hub, ctx, **kwargs):
    """Get integrations Get all integrations within the current organization Performs GET /iaas/api/integrations


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param integer top: (optional in query) Number of records you want to get.
    :param integer skip: (optional in query) Number of records you want to skip.
    :param boolean count: (optional in query) Flag which when specified, regardless of the assigned value, shows the
      total number of records. If the collection has a filter it shows the
      number of records matching the filter.
    :param string select: (optional in query) Select a subset of properties to include in the response.
    :param string filter: (optional in query) Filter the results by a specified predicate expression. Operators: eq,
      ne, and, or.
    """

    hub.log.debug("GET /iaas/api/integrations")

    api = IntegrationApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.get_integrations(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def update_integration_async(hub, ctx, p_id, **kwargs):
    """Update an integration Update a single integration asynchronously Performs PATCH /iaas/api/integrations/{id}


    :param string p_id: (required in path) The ID of the integration
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string privateKey: (optional in body) Secret access key or password to be used to authenticate with the
      integration
    :param array associatedCloudAccountIds: (optional in body) Cloud accounts to associate with this integration
    :param object customProperties: (optional in body) Additional custom properties that may be used to extend the
      Integration.
    :param object integrationProperties: (optional in body) Integration specific properties supplied in as name value pairs
    :param string name: (optional in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param string description: (optional in body) A human-friendly description.
    :param Any certificateInfo: (optional in body)
    :param string privateKeyId: (optional in body) Access key id or username to be used to authenticate with the
      integration
    :param array tags: (optional in body) A set of tag keys and optional values to set on the Integration
    """

    hub.log.debug("PATCH /iaas/api/integrations/{id}")

    api = IntegrationApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    body = {}

    if "privateKey" in kwargs:
        hub.log.debug(f"Got kwarg 'privateKey' = {kwargs['privateKey']}")
        body["privateKey"] = kwargs.get("privateKey")
        del kwargs["privateKey"]
    if "associatedCloudAccountIds" in kwargs:
        hub.log.debug(
            f"Got kwarg 'associatedCloudAccountIds' = {kwargs['associatedCloudAccountIds']}"
        )
        body["associatedCloudAccountIds"] = kwargs.get("associatedCloudAccountIds")
        del kwargs["associatedCloudAccountIds"]
    if "customProperties" in kwargs:
        hub.log.debug(f"Got kwarg 'customProperties' = {kwargs['customProperties']}")
        body["customProperties"] = kwargs.get("customProperties")
        del kwargs["customProperties"]
    if "integrationProperties" in kwargs:
        hub.log.debug(
            f"Got kwarg 'integrationProperties' = {kwargs['integrationProperties']}"
        )
        body["integrationProperties"] = kwargs.get("integrationProperties")
        del kwargs["integrationProperties"]
    if "name" in kwargs:
        hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
        body["name"] = kwargs.get("name")
        del kwargs["name"]
    if "description" in kwargs:
        hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
        body["description"] = kwargs.get("description")
        del kwargs["description"]
    if "certificateInfo" in kwargs:
        hub.log.debug(f"Got kwarg 'certificateInfo' = {kwargs['certificateInfo']}")
        body["certificateInfo"] = kwargs.get("certificateInfo")
        del kwargs["certificateInfo"]
    if "privateKeyId" in kwargs:
        hub.log.debug(f"Got kwarg 'privateKeyId' = {kwargs['privateKeyId']}")
        body["privateKeyId"] = kwargs.get("privateKeyId")
        del kwargs["privateKeyId"]
    if "tags" in kwargs:
        hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
        body["tags"] = kwargs.get("tags")
        del kwargs["tags"]

    ret = api.update_integration_async(body, id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))

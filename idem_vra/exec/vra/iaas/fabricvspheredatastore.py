from idem_vra.client.vra_iaas_lib.api import FabricVSphereDatastoreApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get_fabric_v_sphere_datastore(hub, ctx, p_id, **kwargs):
    """Get fabric vSphere datastore Get fabric vSphere datastore with a given id Performs GET /iaas/api/fabric-vsphere-datastores/{id}


    :param string p_id: (required in path) The ID of the Fabric vSphere Datastore.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string select: (optional in query) Select a subset of properties to include in the response.
    """

    hub.log.debug("GET /iaas/api/fabric-vsphere-datastores/{id}")

    api = FabricVSphereDatastoreApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.get_fabric_v_sphere_datastore(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_fabric_v_sphere_datastores(hub, ctx, **kwargs):
    """Get fabric vSphere datastores Get all fabric vSphere datastores. Performs GET /iaas/api/fabric-vsphere-datastores


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param integer top: (optional in query) Number of records you want to get.
    :param integer skip: (optional in query) Number of records you want to skip.
    :param boolean count: (optional in query) Flag which when specified, regardless of the assigned value, shows the
      total number of records. If the collection has a filter it shows the
      number of records matching the filter.
    :param string select: (optional in query) Select a subset of properties to include in the response.
    :param string filter: (optional in query) Filter the results by a specified predicate expression. Operators: eq,
      ne, and, or.
    """

    hub.log.debug("GET /iaas/api/fabric-vsphere-datastores")

    api = FabricVSphereDatastoreApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.get_fabric_v_sphere_datastores(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def update_fabric_vsphere_datastore(hub, ctx, p_id, **kwargs):
    """Update Fabric vSphere Datastore. Update Fabric vSphere Datastore. Only tag updates are supported. Performs PATCH /iaas/api/fabric-vsphere-datastores/{id}


    :param string p_id: (required in path) The ID of the Fabric Datastore.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param array tags: (optional in body) A set of tag keys and optional values that were set on this resource
      instance.
    """

    hub.log.debug("PATCH /iaas/api/fabric-vsphere-datastores/{id}")

    api = FabricVSphereDatastoreApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    body = {}

    if "tags" in kwargs:
        hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
        body["tags"] = kwargs.get("tags")
        del kwargs["tags"]

    ret = api.update_fabric_vsphere_datastore(body, id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))

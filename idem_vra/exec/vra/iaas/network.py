from idem_vra.client.vra_iaas_lib.api import NetworkApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def create_network(hub, ctx, name, projectId, **kwargs):
    """Create network Provision a new network based on the passed in constraints. The network should
      be destroyed after the machine is destroyed to free up resources. Performs POST /iaas/api/networks


    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param string projectId: (required in body) The id of the project the current user belongs to.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param object customProperties: (optional in body) Additional custom properties that may be used to extend this resource.
    :param string deploymentId: (optional in body) The id of the deployment that is associated with this resource
    :param boolean outboundAccess: (optional in body) Flag to indicate if the network needs to have outbound access or not.
      Default is true. This field will be ignored if there is proper input
      for networkType customProperty
    :param string description: (optional in body) A human-friendly description.
    :param boolean createGateway: (optional in body) Flag to indicate if the network creation should create a gateway.
      Default is true.
    :param array constraints: (optional in body) Constraints that are used to drive placement policies for the network
      that is produced from this specification, related with the network
      profile. Constraint expressions are matched against tags on existing
      placement targets.
    :param array tags: (optional in body) A set of tag keys and optional values that should be set on any
      resource that is produced from this specification.
    """

    hub.log.debug("POST /iaas/api/networks")

    api = NetworkApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    body = {}
    body["name"] = name
    body["projectId"] = projectId

    if "customProperties" in kwargs:
        hub.log.debug(f"Got kwarg 'customProperties' = {kwargs['customProperties']}")
        body["customProperties"] = kwargs.get("customProperties")
        del kwargs["customProperties"]
    if "deploymentId" in kwargs:
        hub.log.debug(f"Got kwarg 'deploymentId' = {kwargs['deploymentId']}")
        body["deploymentId"] = kwargs.get("deploymentId")
        del kwargs["deploymentId"]
    if "outboundAccess" in kwargs:
        hub.log.debug(f"Got kwarg 'outboundAccess' = {kwargs['outboundAccess']}")
        body["outboundAccess"] = kwargs.get("outboundAccess")
        del kwargs["outboundAccess"]
    if "description" in kwargs:
        hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
        body["description"] = kwargs.get("description")
        del kwargs["description"]
    if "createGateway" in kwargs:
        hub.log.debug(f"Got kwarg 'createGateway' = {kwargs['createGateway']}")
        body["createGateway"] = kwargs.get("createGateway")
        del kwargs["createGateway"]
    if "constraints" in kwargs:
        hub.log.debug(f"Got kwarg 'constraints' = {kwargs['constraints']}")
        body["constraints"] = kwargs.get("constraints")
        del kwargs["constraints"]
    if "tags" in kwargs:
        hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
        body["tags"] = kwargs.get("tags")
        del kwargs["tags"]

    ret = api.create_network(body, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def delete_network(hub, ctx, p_id, **kwargs):
    """Delete a network Delete a network with a given id Performs DELETE /iaas/api/networks/{id}


    :param string p_id: (required in path) The ID of the network.
    :param boolean forceDelete: (optional in query) Controls whether this is a force delete operation. If true, best
      effort is made for deleting this network. Use with caution as force
      deleting may cause inconsistencies between the cloud provider and vRA.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    hub.log.debug("DELETE /iaas/api/networks/{id}")

    api = NetworkApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.delete_network(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_machine_network_interface(hub, ctx, p_id, p_networkId, **kwargs):
    """Get machine network interface Get network interface with a given id for specific machine Performs GET /iaas/api/machines/{id}/network-interfaces/{networkId}


    :param string p_id: (required in path) The ID of the machine.
    :param string p_networkId: (required in path) The ID of the network interface.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    hub.log.debug("GET /iaas/api/machines/{id}/network-interfaces/{networkId}")

    api = NetworkApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.get_machine_network_interface(id=p_id, network_id=p_networkId, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_network_domain(hub, ctx, p_id, **kwargs):
    """Get network domain Get network domain with a given id Performs GET /iaas/api/network-domains/{id}


    :param string p_id: (required in path) The ID of the network domain.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    hub.log.debug("GET /iaas/api/network-domains/{id}")

    api = NetworkApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.get_network_domain(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_network_domains(hub, ctx, **kwargs):
    """Get network domains Get all network domains. Performs GET /iaas/api/network-domains


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    hub.log.debug("GET /iaas/api/network-domains")

    api = NetworkApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.get_network_domains(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_network(hub, ctx, p_id, **kwargs):
    """Get network Get network with a given id Performs GET /iaas/api/networks/{id}


    :param string p_id: (required in path) The ID of the network.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    hub.log.debug("GET /iaas/api/networks/{id}")

    api = NetworkApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.get_network(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_networks(hub, ctx, **kwargs):
    """Get networks Get all networks Performs GET /iaas/api/networks


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    hub.log.debug("GET /iaas/api/networks")

    api = NetworkApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.get_networks(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def patch_machine_network_interface(hub, ctx, p_id, p_networkId, **kwargs):
    """Patch machine network interface Patch network interface with a given id for specific machine. Only name,
      description, IPv4 address and custom property updates are supported. The change
      to name and IPv4 address will not propagate to cloud endpoint for provisioned
      machines. Performs PATCH /iaas/api/machines/{id}/network-interfaces/{networkId}


    :param string p_id: (required in path) The ID of the machine.
    :param string p_networkId: (required in path) The ID of the network interface.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param object customProperties: (optional in body) Additional custom properties that may be used to extend the machine.
      Internal custom properties (for example, prefixed with: "__") can not
      be updated.
    :param string address: (optional in body) Set IPv4 address for the machine network interface. The change will
      not propagate to cloud endpoint for provisioned machines.
    :param string name: (optional in body) Network interface name used during machine network interface
      provisioning. This property only takes effect if it is set before
      machine provisioning starts. The change will not propagate to cloud
      endpoint for provisioned machines.
    :param string description: (optional in body) Describes the network interface of the machine within the scope of
      your organization and is not propagated to the cloud
    """

    hub.log.debug("PATCH /iaas/api/machines/{id}/network-interfaces/{networkId}")

    api = NetworkApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    body = {}

    if "customProperties" in kwargs:
        hub.log.debug(f"Got kwarg 'customProperties' = {kwargs['customProperties']}")
        body["customProperties"] = kwargs.get("customProperties")
        del kwargs["customProperties"]
    if "address" in kwargs:
        hub.log.debug(f"Got kwarg 'address' = {kwargs['address']}")
        body["address"] = kwargs.get("address")
        del kwargs["address"]
    if "name" in kwargs:
        hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
        body["name"] = kwargs.get("name")
        del kwargs["name"]
    if "description" in kwargs:
        hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
        body["description"] = kwargs.get("description")
        del kwargs["description"]

    ret = api.patch_machine_network_interface(
        body, id=p_id, network_id=p_networkId, **kwargs
    )

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))

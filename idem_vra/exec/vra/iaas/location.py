from idem_vra.client.vra_iaas_lib.api import LocationApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def create_zone(hub, ctx, regionId, name, **kwargs):
    """Create zone Create zone Performs POST /iaas/api/zones


    :param string regionId: (required in body) The id of the region for which this profile is created
    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param object customProperties: (optional in body) A list of key value pair of properties that will  be used
    :param string folder: (optional in body) The folder relative path to the datacenter where resources are
      deployed to. (only applicable for vSphere cloud zones)
    :param array computeIds: (optional in body) The ids of the compute resources that will be explicitly assigned to
      this zone
    :param array tagsToMatch: (optional in body) A set of tag keys and optional values that will be used
    :param string description: (optional in body) A human-friendly description.
    :param string placementPolicy: (optional in body) Placement policy for the zone. One of DEFAULT, SPREAD, BINPACK or
      SPREAD_MEMORY.
    :param array tags: (optional in body) A set of tag keys and optional values that are effectively applied to
      all compute resources in this zone, but only in the context of this
      zone.
    """

    hub.log.debug("POST /iaas/api/zones")

    api = LocationApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    body = {}
    body["regionId"] = regionId
    body["name"] = name

    if "customProperties" in kwargs:
        hub.log.debug(f"Got kwarg 'customProperties' = {kwargs['customProperties']}")
        body["customProperties"] = kwargs.get("customProperties")
        del kwargs["customProperties"]
    if "folder" in kwargs:
        hub.log.debug(f"Got kwarg 'folder' = {kwargs['folder']}")
        body["folder"] = kwargs.get("folder")
        del kwargs["folder"]
    if "computeIds" in kwargs:
        hub.log.debug(f"Got kwarg 'computeIds' = {kwargs['computeIds']}")
        body["computeIds"] = kwargs.get("computeIds")
        del kwargs["computeIds"]
    if "tagsToMatch" in kwargs:
        hub.log.debug(f"Got kwarg 'tagsToMatch' = {kwargs['tagsToMatch']}")
        body["tagsToMatch"] = kwargs.get("tagsToMatch")
        del kwargs["tagsToMatch"]
    if "description" in kwargs:
        hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
        body["description"] = kwargs.get("description")
        del kwargs["description"]
    if "placementPolicy" in kwargs:
        hub.log.debug(f"Got kwarg 'placementPolicy' = {kwargs['placementPolicy']}")
        body["placementPolicy"] = kwargs.get("placementPolicy")
        del kwargs["placementPolicy"]
    if "tags" in kwargs:
        hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
        body["tags"] = kwargs.get("tags")
        del kwargs["tags"]

    ret = api.create_zone(body, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def delete_zone(hub, ctx, p_id, **kwargs):
    """Delete a zone Delete a zone Performs DELETE /iaas/api/zones/{id}


    :param string p_id: (required in path) The ID of the zone.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    hub.log.debug("DELETE /iaas/api/zones/{id}")

    api = LocationApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.delete_zone(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_computes(hub, ctx, p_id, **kwargs):
    """Get computes Get zone's computes by given zone ID Performs GET /iaas/api/zones/{id}/computes


    :param string p_id: (required in path) The ID of the zone.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    hub.log.debug("GET /iaas/api/zones/{id}/computes")

    api = LocationApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.get_computes(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_region(hub, ctx, p_id, **kwargs):
    """Get Region Get Region with a given id Performs GET /iaas/api/regions/{id}


    :param string p_id: (required in path) The ID of the region.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    hub.log.debug("GET /iaas/api/regions/{id}")

    api = LocationApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.get_region(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_regions(hub, ctx, **kwargs):
    """Get regions Get all regions Performs GET /iaas/api/regions


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param integer top: (optional in query) Number of records you want to get.
    :param integer skip: (optional in query) Number of records you want to skip.
    """

    hub.log.debug("GET /iaas/api/regions")

    api = LocationApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.get_regions(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_zone(hub, ctx, p_id, **kwargs):
    """Get zone Get zone with given id Performs GET /iaas/api/zones/{id}


    :param string p_id: (required in path) The ID of the zone.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    hub.log.debug("GET /iaas/api/zones/{id}")

    api = LocationApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.get_zone(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_zones(hub, ctx, **kwargs):
    """Get zones Get all zones Performs GET /iaas/api/zones


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param integer top: (optional in query) Number of records you want to get.
    :param integer skip: (optional in query) Number of records you want to skip.
    """

    hub.log.debug("GET /iaas/api/zones")

    api = LocationApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.get_zones(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def update_zone(hub, ctx, p_id, regionId, name, **kwargs):
    """Update zone Update zone Performs PATCH /iaas/api/zones/{id}


    :param string p_id: (required in path) The ID of the zone.
    :param string regionId: (required in body) The id of the region for which this profile is created
    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param object customProperties: (optional in body) A list of key value pair of properties that will  be used
    :param string folder: (optional in body) The folder relative path to the datacenter where resources are
      deployed to. (only applicable for vSphere cloud zones)
    :param array computeIds: (optional in body) The ids of the compute resources that will be explicitly assigned to
      this zone
    :param array tagsToMatch: (optional in body) A set of tag keys and optional values that will be used
    :param string description: (optional in body) A human-friendly description.
    :param string placementPolicy: (optional in body) Placement policy for the zone. One of DEFAULT, SPREAD, BINPACK or
      SPREAD_MEMORY.
    :param array tags: (optional in body) A set of tag keys and optional values that are effectively applied to
      all compute resources in this zone, but only in the context of this
      zone.
    """

    hub.log.debug("PATCH /iaas/api/zones/{id}")

    api = LocationApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    body = {}
    body["regionId"] = regionId
    body["name"] = name

    if "customProperties" in kwargs:
        hub.log.debug(f"Got kwarg 'customProperties' = {kwargs['customProperties']}")
        body["customProperties"] = kwargs.get("customProperties")
        del kwargs["customProperties"]
    if "folder" in kwargs:
        hub.log.debug(f"Got kwarg 'folder' = {kwargs['folder']}")
        body["folder"] = kwargs.get("folder")
        del kwargs["folder"]
    if "computeIds" in kwargs:
        hub.log.debug(f"Got kwarg 'computeIds' = {kwargs['computeIds']}")
        body["computeIds"] = kwargs.get("computeIds")
        del kwargs["computeIds"]
    if "tagsToMatch" in kwargs:
        hub.log.debug(f"Got kwarg 'tagsToMatch' = {kwargs['tagsToMatch']}")
        body["tagsToMatch"] = kwargs.get("tagsToMatch")
        del kwargs["tagsToMatch"]
    if "description" in kwargs:
        hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
        body["description"] = kwargs.get("description")
        del kwargs["description"]
    if "placementPolicy" in kwargs:
        hub.log.debug(f"Got kwarg 'placementPolicy' = {kwargs['placementPolicy']}")
        body["placementPolicy"] = kwargs.get("placementPolicy")
        del kwargs["placementPolicy"]
    if "tags" in kwargs:
        hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
        body["tags"] = kwargs.get("tags")
        del kwargs["tags"]

    ret = api.update_zone(body, id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))

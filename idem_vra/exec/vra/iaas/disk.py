from idem_vra.client.vra_iaas_lib.api import DiskApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def attach_machine_disk(hub, ctx, p_id, blockDeviceId, **kwargs):
    """Attach machine disk Attach a disk to a machine. Performs POST /iaas/api/machines/{id}/disks


    :param string p_id: (required in path) The ID of the machine.
    :param string blockDeviceId: (required in body) The id of the existing block device
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string scsiController: (optional in body) Deprecated: The SCSI controller to be assigned
    :param string name: (optional in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param string description: (optional in body) A human-friendly description.
    :param string unitNumber: (optional in body) Deprecated: The Unit Number to be assigned
    :param object diskAttachmentProperties: (optional in body) Disk Attachment specific properties
    """

    hub.log.debug("POST /iaas/api/machines/{id}/disks")

    api = DiskApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    body = {}
    body["blockDeviceId"] = blockDeviceId

    if "scsiController" in kwargs:
        hub.log.debug(f"Got kwarg 'scsiController' = {kwargs['scsiController']}")
        body["scsiController"] = kwargs.get("scsiController")
        del kwargs["scsiController"]
    if "name" in kwargs:
        hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
        body["name"] = kwargs.get("name")
        del kwargs["name"]
    if "description" in kwargs:
        hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
        body["description"] = kwargs.get("description")
        del kwargs["description"]
    if "unitNumber" in kwargs:
        hub.log.debug(f"Got kwarg 'unitNumber' = {kwargs['unitNumber']}")
        body["unitNumber"] = kwargs.get("unitNumber")
        del kwargs["unitNumber"]
    if "diskAttachmentProperties" in kwargs:
        hub.log.debug(
            f"Got kwarg 'diskAttachmentProperties' = {kwargs['diskAttachmentProperties']}"
        )
        body["diskAttachmentProperties"] = kwargs.get("diskAttachmentProperties")
        del kwargs["diskAttachmentProperties"]

    ret = api.attach_machine_disk(body, id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def create_block_device_snapshot(hub, ctx, p_id, **kwargs):
    """Create snapshot operation for Block Device Second day create snapshot operation for Block device Performs POST /iaas/api/block-devices/{id}/operations/snapshots


    :param string p_id: (required in path) The ID of the block device.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string name: (optional in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param string description: (optional in body) A human-friendly description.
    :param object snapshotProperties: (optional in body) Cloud specific snapshot properties supplied in as name value pairs
    :param array tags: (optional in body) A set of tag keys and optional values that have to be set on the
      snapshot in the cloud. Currently supported for Azure Snapshots
    """

    hub.log.debug("POST /iaas/api/block-devices/{id}/operations/snapshots")

    api = DiskApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    body = {}

    if "name" in kwargs:
        hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
        body["name"] = kwargs.get("name")
        del kwargs["name"]
    if "description" in kwargs:
        hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
        body["description"] = kwargs.get("description")
        del kwargs["description"]
    if "snapshotProperties" in kwargs:
        hub.log.debug(
            f"Got kwarg 'snapshotProperties' = {kwargs['snapshotProperties']}"
        )
        body["snapshotProperties"] = kwargs.get("snapshotProperties")
        del kwargs["snapshotProperties"]
    if "tags" in kwargs:
        hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
        body["tags"] = kwargs.get("tags")
        del kwargs["tags"]

    ret = api.create_block_device_snapshot(body, id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def create_block_device(hub, ctx, capacityInGB, name, projectId, **kwargs):
    """Create BlockDevice Following disk custom properties can be passed while creating a block device:
      . dataStore: Defines name of the datastore in which the disk has to be
      provisioned.
      . storagePolicy: Defines name of the storage policy in which the disk has
      to be provisioned. If name of the datastore is specified in the custom
      properties then, datastore takes precedence.
      . provisioningType: Defines the type of provisioning. For eg. thick/thin.
      . resourceGroupName: Defines the Azure resource group name where the disk
      needs to be provisioned. Performs POST /iaas/api/block-devices


    :param integer capacityInGB: (required in body) Capacity of the block device in GB.
    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param string projectId: (required in body) The id of the project the current user belongs to.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string sourceReference: (optional in body) Reference to URI using which the block device has to be created.
    :param object customProperties: (optional in body) Additional custom properties that may be used to extend this resource.
    :param boolean encrypted: (optional in body) Indicates whether the block device should be encrypted or not.
    :param string deploymentId: (optional in body) The id of the deployment that is associated with this resource
    :param string description: (optional in body) A human-friendly description.
    :param string diskContentBase64: (optional in body) Content of a disk, base64 encoded.
    :param boolean persistent: (optional in body) Indicates whether the block device survives a delete action.
    :param array constraints: (optional in body) Constraints that are used to drive placement policies for the block
      device that is produced from this specification. Constraint
      expressions are matched against tags on existing placement targets.
    :param array tags: (optional in body) A set of tag keys and optional values that should be set on any
      resource that is produced from this specification.
    """

    hub.log.debug("POST /iaas/api/block-devices")

    api = DiskApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    body = {}
    body["capacityInGB"] = capacityInGB
    body["name"] = name
    body["projectId"] = projectId

    if "sourceReference" in kwargs:
        hub.log.debug(f"Got kwarg 'sourceReference' = {kwargs['sourceReference']}")
        body["sourceReference"] = kwargs.get("sourceReference")
        del kwargs["sourceReference"]
    if "customProperties" in kwargs:
        hub.log.debug(f"Got kwarg 'customProperties' = {kwargs['customProperties']}")
        body["customProperties"] = kwargs.get("customProperties")
        del kwargs["customProperties"]
    if "encrypted" in kwargs:
        hub.log.debug(f"Got kwarg 'encrypted' = {kwargs['encrypted']}")
        body["encrypted"] = kwargs.get("encrypted")
        del kwargs["encrypted"]
    if "deploymentId" in kwargs:
        hub.log.debug(f"Got kwarg 'deploymentId' = {kwargs['deploymentId']}")
        body["deploymentId"] = kwargs.get("deploymentId")
        del kwargs["deploymentId"]
    if "description" in kwargs:
        hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
        body["description"] = kwargs.get("description")
        del kwargs["description"]
    if "diskContentBase64" in kwargs:
        hub.log.debug(f"Got kwarg 'diskContentBase64' = {kwargs['diskContentBase64']}")
        body["diskContentBase64"] = kwargs.get("diskContentBase64")
        del kwargs["diskContentBase64"]
    if "persistent" in kwargs:
        hub.log.debug(f"Got kwarg 'persistent' = {kwargs['persistent']}")
        body["persistent"] = kwargs.get("persistent")
        del kwargs["persistent"]
    if "constraints" in kwargs:
        hub.log.debug(f"Got kwarg 'constraints' = {kwargs['constraints']}")
        body["constraints"] = kwargs.get("constraints")
        del kwargs["constraints"]
    if "tags" in kwargs:
        hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
        body["tags"] = kwargs.get("tags")
        del kwargs["tags"]

    ret = api.create_block_device(body, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def delete_block_device_snapshot(hub, ctx, p_id, p_id1, **kwargs):
    """Delete snapshot operation for Block device Second day delete snapshot operation for Block device Performs DELETE /iaas/api/block-devices/{id}/snapshots/{id1}


    :param string p_id: (required in path) The ID of the block device.
    :param string p_id1: (required in path) Snapshot id to delete.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    hub.log.debug("DELETE /iaas/api/block-devices/{id}/snapshots/{id1}")

    api = DiskApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.delete_block_device_snapshot(id=p_id, id1=p_id1, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def delete_block_device(hub, ctx, p_id, **kwargs):
    """Delete a BlockDevice Delete a BlockDevice
      . A block device cannot be deleted when attached to a machine.
      . A block device with persistent property set to 'false' is deleted.
      . A block device with persistent property set to 'true' needs an additional
      parameter 'purge' to be set to true, for deletion. Performs DELETE /iaas/api/block-devices/{id}


    :param string p_id: (required in path) The ID of the block device.
    :param boolean purge: (optional in query) Indicates if the disk has to be completely destroyed or should be kept
      in the system. Valid only for block devices with 'persistent' set to
      true.
    :param boolean forceDelete: (optional in query) Controls whether this is a force delete operation. If true, best
      effort is made for deleting this block device. Use with caution as
      force deleting may cause inconsistencies between the cloud provider
      and vRA.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    hub.log.debug("DELETE /iaas/api/block-devices/{id}")

    api = DiskApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.delete_block_device(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def delete_machine_disk(hub, ctx, p_id, p_diskId, **kwargs):
    """Delete machine disk Remove a disk from a given machine. Performs DELETE /iaas/api/machines/{id}/disks/{diskId}


    :param string p_id: (required in path) The ID of the machine.
    :param string p_diskId: (required in path) The ID of the disk.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    hub.log.debug("DELETE /iaas/api/machines/{id}/disks/{diskId}")

    api = DiskApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.delete_machine_disk(id=p_id, disk_id=p_diskId, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_block_device(hub, ctx, p_id, **kwargs):
    """Get BlockDevice Get a single BlockDevice Performs GET /iaas/api/block-devices/{id}


    :param string p_id: (required in path) The ID of the block device.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    hub.log.debug("GET /iaas/api/block-devices/{id}")

    api = DiskApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.get_block_device(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_block_devices(hub, ctx, **kwargs):
    """Get BlockDevices Get all BlockDevices Performs GET /iaas/api/block-devices


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param integer top: (optional in query) Number of records you want to get.
    :param integer skip: (optional in query) Number of records you want to skip.
    :param boolean count: (optional in query) Flag which when specified, regardless of the assigned value, shows the
      total number of records. If the collection has a filter it shows the
      number of records matching the filter.
    :param string filter: (optional in query) Filter the results by a specified predicate expression. Operators: eq,
      ne, and, or.
    """

    hub.log.debug("GET /iaas/api/block-devices")

    api = DiskApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.get_block_devices(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_disk_snapshot(hub, ctx, p_id, p_id1, **kwargs):
    """Get disk snapshot Get snapshot with a given id for specific disk Performs GET /iaas/api/block-devices/{id}/snapshots/{id1}


    :param string p_id: (required in path) The ID of the disk.
    :param string p_id1: (required in path) The ID of the snapshot.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    hub.log.debug("GET /iaas/api/block-devices/{id}/snapshots/{id1}")

    api = DiskApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.get_disk_snapshot(id=p_id, id1=p_id1, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_disk_snapshots(hub, ctx, p_id, **kwargs):
    """Get disk snapshots information Get disk snapshots information Performs GET /iaas/api/block-devices/{id}/snapshots


    :param string p_id: (required in path) The ID of the disk.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    hub.log.debug("GET /iaas/api/block-devices/{id}/snapshots")

    api = DiskApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.get_disk_snapshots(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_machine_disk(hub, ctx, p_id, p_diskId, **kwargs):
    """Get machine disk Get disk with a given id for specific machine Performs GET /iaas/api/machines/{id}/disks/{diskId}


    :param string p_id: (required in path) The ID of the machine.
    :param string p_diskId: (required in path) The ID of the disk.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    hub.log.debug("GET /iaas/api/machines/{id}/disks/{diskId}")

    api = DiskApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.get_machine_disk(id=p_id, disk_id=p_diskId, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_machine_disks(hub, ctx, p_id, **kwargs):
    """Get machine disks Get all machine disks Performs GET /iaas/api/machines/{id}/disks


    :param string p_id: (required in path) The ID of the machine.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    hub.log.debug("GET /iaas/api/machines/{id}/disks")

    api = DiskApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.get_machine_disks(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def promote_disk(hub, ctx, p_id, **kwargs):
    """Promote operation on disk Second day promote operation on disk. Applicable for vSphere Block Devices only Performs POST /iaas/api/block-devices/{id}/operations/promote


    :param string p_id: (required in path) The id of the Disk.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    hub.log.debug("POST /iaas/api/block-devices/{id}/operations/promote")

    api = DiskApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.promote_disk(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def resize_block_device(hub, ctx, p_id, q_capacityInGB, **kwargs):
    """Resize BlockDevice Resize operation on block device. Performs POST /iaas/api/block-devices/{id}


    :param string p_id: (required in path) The ID of the block device.
    :param integer q_capacityInGB: (required in query) Resize Capacity in GB
    :param boolean useSdrs: (optional in query) Only applicable for vSphere block-devices deployed on SDRS cluster. If
      set to true, SDRS Recommendation will be used for resize operation.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    hub.log.debug("POST /iaas/api/block-devices/{id}")

    api = DiskApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.resize_block_device(id=p_id, capacity_in_g_b=q_capacityInGB, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def revert_disk_snapshot(hub, ctx, p_id, q_sid, **kwargs):
    """Revert snapshot operation for Block device Second day revert snapshot operation for Block device Performs POST /iaas/api/block-devices/{id}/operations/revert


    :param string p_id: (required in path) The id of the Disk.
    :param string q_sid: (required in query) Snapshot id to revert.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    hub.log.debug("POST /iaas/api/block-devices/{id}/operations/revert")

    api = DiskApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.revert_disk_snapshot(id=p_id, sid=q_sid, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))

from idem_vra.client.vra_iaas_lib.api import PropertyApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def delete_configuration_property(hub, ctx, p_id, **kwargs):
    """Delete a configuration property Delete a configuration property Performs DELETE /iaas/api/configuration-properties/{id}


    :param string p_id: (required in path)
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    hub.log.debug("DELETE /iaas/api/configuration-properties/{id}")

    api = PropertyApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.delete_configuration_property(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_configuration_properties(hub, ctx, **kwargs):
    """Get configuration properties Get all configuration properties Performs GET /iaas/api/configuration-properties


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    hub.log.debug("GET /iaas/api/configuration-properties")

    api = PropertyApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.get_configuration_properties(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_configuration_property(hub, ctx, p_id, **kwargs):
    """Get single configuration property Get single configuration property Performs GET /iaas/api/configuration-properties/{id}


    :param string p_id: (required in path)
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    hub.log.debug("GET /iaas/api/configuration-properties/{id}")

    api = PropertyApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    ret = api.get_configuration_property(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def patch_configuration_property(hub, ctx, value, key, **kwargs):
    """Update or create configuration property. Update or create configuration property. Performs PATCH /iaas/api/configuration-properties


    :param string value: (required in body) The value of the property.
    :param string key: (required in body) The key of the property.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    hub.log.debug("PATCH /iaas/api/configuration-properties")

    api = PropertyApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2021-07-15"

    body = {}
    body["value"] = value
    body["key"] = key

    ret = api.patch_configuration_property(body, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))

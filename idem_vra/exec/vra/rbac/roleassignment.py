from idem_vra.client.vra_rbac_lib.api import RoleAssignmentApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get_grouped_role_assignments_using_get(hub, ctx, **kwargs):
    """Get grouped role assignments Get roles assignments grouped by principal. Performs GET /rbac-service/api/grouped-role-assignments


    :param string apiVersion: (optional in query)
    """

    hub.log.debug("GET /rbac-service/api/grouped-role-assignments")

    api = RoleAssignmentApi(hub.clients["idem_vra.client.vra_rbac_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-10"

    ret = api.get_grouped_role_assignments_using_get(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_role_assignments_using_get(hub, ctx, **kwargs):
    """Get role assignments  Performs GET /rbac-service/api/role-assignments


    :param string apiVersion: (optional in query)
    :param string orgId: (optional in query) orgId
    :param string principalId: (optional in query) principalId
    :param string projectId: (optional in query) projectId
    :param string roleId: (optional in query) roleId
    """

    hub.log.debug("GET /rbac-service/api/role-assignments")

    api = RoleAssignmentApi(hub.clients["idem_vra.client.vra_rbac_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-10"

    ret = api.get_role_assignments_using_get(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def update_role_assignments_using_put(hub, ctx, **kwargs):
    """Modify role assignments for a given principalId, orgId and projectId.  Performs PUT /rbac-service/api/role-assignments


    :param string apiVersion: (optional in query)
    :param string orgId: (optional in body) The id of the org this role assignment belongs to
    :param string principalId: (optional in body) The id of the principal this role assignment will be applicable for
    :param string principalType: (optional in body) Principal type (user/group)
    :param string projectId: (optional in body) The id of the project this role assignment belongs to
    :param array rolesToAdd: (optional in body) Ids of roles to add
    :param array rolesToRemove: (optional in body) Ids of roles to remove
    """

    hub.log.debug("PUT /rbac-service/api/role-assignments")

    api = RoleAssignmentApi(hub.clients["idem_vra.client.vra_rbac_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-10"

    body = {}

    if "orgId" in kwargs:
        hub.log.debug(f"Got kwarg 'orgId' = {kwargs['orgId']}")
        body["orgId"] = kwargs.get("orgId")
        del kwargs["orgId"]
    if "principalId" in kwargs:
        hub.log.debug(f"Got kwarg 'principalId' = {kwargs['principalId']}")
        body["principalId"] = kwargs.get("principalId")
        del kwargs["principalId"]
    if "principalType" in kwargs:
        hub.log.debug(f"Got kwarg 'principalType' = {kwargs['principalType']}")
        body["principalType"] = kwargs.get("principalType")
        del kwargs["principalType"]
    if "projectId" in kwargs:
        hub.log.debug(f"Got kwarg 'projectId' = {kwargs['projectId']}")
        body["projectId"] = kwargs.get("projectId")
        del kwargs["projectId"]
    if "rolesToAdd" in kwargs:
        hub.log.debug(f"Got kwarg 'rolesToAdd' = {kwargs['rolesToAdd']}")
        body["rolesToAdd"] = kwargs.get("rolesToAdd")
        del kwargs["rolesToAdd"]
    if "rolesToRemove" in kwargs:
        hub.log.debug(f"Got kwarg 'rolesToRemove' = {kwargs['rolesToRemove']}")
        body["rolesToRemove"] = kwargs.get("rolesToRemove")
        del kwargs["rolesToRemove"]

    ret = api.update_role_assignments_using_put(body, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))

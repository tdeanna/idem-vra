from idem_vra.client.vra_rbac_lib.api import UserAuthContextApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get_auth_context_using_get(hub, ctx, **kwargs):
    """Retrieve the logged in user's auth context  Performs GET /rbac-service/api/auth-context


    :param string apiVersion: (optional in query)
    :param boolean excludeSupervisorRoleProjects: (optional in query) Filters projects based on the supervisor role. When this filter is
      true it will not include the projects in which the current user is
      having only supervisor role
    :param boolean includeGroups: (optional in query) Include Group information. When the flag is true it will include all
      the groups that has been added to the project and the user is a part
      of.
    """

    hub.log.debug("GET /rbac-service/api/auth-context")

    api = UserAuthContextApi(hub.clients["idem_vra.client.vra_rbac_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-10"

    ret = api.get_auth_context_using_get(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))

from idem_vra.client.vra_rbac_lib.api import RoleApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def create_using_post1(hub, ctx, name, permissions, **kwargs):
    """Create a role  Performs POST /rbac-service/api/roles


    :param string name: (required in body) Name of the role.
    :param array permissions: (required in body) List of permissions that the role has.
    :param string apiVersion: (optional in query)
    :param string description: (optional in body) A human-friendly description.
    :param boolean hidden: (optional in body) Specifies whether the role should be hidden (true) in the UI or not
      (false). The flag is used mainly for development purposes.
    :param string id: (optional in body)
    :param string orgId: (optional in body) The id of the org this role belongs to
    :param boolean projectScope: (optional in body) Specifies whether the role is organization level role or it is project
      level.
    """

    hub.log.debug("POST /rbac-service/api/roles")

    api = RoleApi(hub.clients["idem_vra.client.vra_rbac_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-10"

    body = {}
    body["name"] = name
    body["permissions"] = permissions

    if "description" in kwargs:
        hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
        body["description"] = kwargs.get("description")
        del kwargs["description"]
    if "hidden" in kwargs:
        hub.log.debug(f"Got kwarg 'hidden' = {kwargs['hidden']}")
        body["hidden"] = kwargs.get("hidden")
        del kwargs["hidden"]
    if "id" in kwargs:
        hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
        body["id"] = kwargs.get("id")
        del kwargs["id"]
    if "orgId" in kwargs:
        hub.log.debug(f"Got kwarg 'orgId' = {kwargs['orgId']}")
        body["orgId"] = kwargs.get("orgId")
        del kwargs["orgId"]
    if "projectScope" in kwargs:
        hub.log.debug(f"Got kwarg 'projectScope' = {kwargs['projectScope']}")
        body["projectScope"] = kwargs.get("projectScope")
        del kwargs["projectScope"]

    ret = api.create_using_post1(body, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def delete_role_using_delete(hub, ctx, p_id, **kwargs):
    """Delete a role by id  Performs DELETE /rbac-service/api/roles/{id}


    :param string p_id: (required in path) id
    :param string apiVersion: (optional in query)
    """

    hub.log.debug("DELETE /rbac-service/api/roles/{id}")

    api = RoleApi(hub.clients["idem_vra.client.vra_rbac_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-10"

    ret = api.delete_role_using_delete(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_all_roles_using_get(hub, ctx, **kwargs):
    """Get all roles  Performs GET /rbac-service/api/roles


    :param string apiVersion: (optional in query)
    :param boolean excludeOrganizationScoped: (optional in query) Exclude Organization scoped roles. When the flag is true, it will not
      include the organization scoped roles.
    :param boolean excludeProjectScoped: (optional in query) Exclude Project scoped roles. When the flag is true, it will not
      include the project scoped roles.
    """

    hub.log.debug("GET /rbac-service/api/roles")

    api = RoleApi(hub.clients["idem_vra.client.vra_rbac_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-10"

    ret = api.get_all_roles_using_get(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_role_using_get(hub, ctx, p_id, **kwargs):
    """Retrieve a role by id  Performs GET /rbac-service/api/roles/{id}


    :param string p_id: (required in path) id
    :param string apiVersion: (optional in query)
    """

    hub.log.debug("GET /rbac-service/api/roles/{id}")

    api = RoleApi(hub.clients["idem_vra.client.vra_rbac_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-10"

    ret = api.get_role_using_get(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def update_using_put(hub, ctx, p_id, name, permissions, **kwargs):
    """Update a role  Performs PUT /rbac-service/api/roles/{id}


    :param string p_id: (required in path) id
    :param string name: (required in body) Name of the role.
    :param array permissions: (required in body) List of permissions that the role has.
    :param string apiVersion: (optional in query)
    :param string description: (optional in body) A human-friendly description.
    :param boolean hidden: (optional in body) Specifies whether the role should be hidden (true) in the UI or not
      (false). The flag is used mainly for development purposes.
    :param string id: (optional in body)
    :param string orgId: (optional in body) The id of the org this role belongs to
    :param boolean projectScope: (optional in body) Specifies whether the role is organization level role or it is project
      level.
    """

    hub.log.debug("PUT /rbac-service/api/roles/{id}")

    api = RoleApi(hub.clients["idem_vra.client.vra_rbac_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-10"

    body = {}
    body["name"] = name
    body["permissions"] = permissions

    if "description" in kwargs:
        hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
        body["description"] = kwargs.get("description")
        del kwargs["description"]
    if "hidden" in kwargs:
        hub.log.debug(f"Got kwarg 'hidden' = {kwargs['hidden']}")
        body["hidden"] = kwargs.get("hidden")
        del kwargs["hidden"]
    if "id" in kwargs:
        hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
        body["id"] = kwargs.get("id")
        del kwargs["id"]
    if "orgId" in kwargs:
        hub.log.debug(f"Got kwarg 'orgId' = {kwargs['orgId']}")
        body["orgId"] = kwargs.get("orgId")
        del kwargs["orgId"]
    if "projectScope" in kwargs:
        hub.log.debug(f"Got kwarg 'projectScope' = {kwargs['projectScope']}")
        body["projectScope"] = kwargs.get("projectScope")
        del kwargs["projectScope"]

    ret = api.update_using_put(body, id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))

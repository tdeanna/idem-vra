from idem_vra.client.vra_pipeline_lib.api import CustomIntegrationsApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def create_custom_integration_using_post(hub, ctx, name, **kwargs):
    """Create a Custom Integration Create a Custom Integration to be consumed in pipelines as custom tasks Performs POST /codestream/api/custom-integrations


    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    :param string description: (optional in body) A human-friendly description.
    :param string project: (optional in body) The project this entity belongs to.
    :param string yaml: (optional in body) YAML describing Custom integration details.
    """

    hub.log.debug("POST /codestream/api/custom-integrations")

    api = CustomIntegrationsApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2019-10-17"

    body = {}
    body["name"] = name

    if "description" in kwargs:
        hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
        body["description"] = kwargs.get("description")
        del kwargs["description"]
    if "project" in kwargs:
        hub.log.debug(f"Got kwarg 'project' = {kwargs['project']}")
        body["project"] = kwargs.get("project")
        del kwargs["project"]
    if "yaml" in kwargs:
        hub.log.debug(f"Got kwarg 'yaml' = {kwargs['yaml']}")
        body["yaml"] = kwargs.get("yaml")
        del kwargs["yaml"]

    ret = api.create_custom_integration_using_post(body, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def create_version_by_id_using_post(hub, ctx, p_id, **kwargs):
    """Create a Custom Integration version Create a Custom Integration version from the current draft Performs POST /codestream/api/custom-integrations/{id}/versions


    :param string p_id: (required in path) The ID of the Custom Integration
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    :param string changeLog: (optional in body) Changelog describing the changes between this and previous versions.
    :param string description: (optional in body) Description of the version.
    :param string version: (optional in body) Version of the Custom Integration.
    """

    hub.log.debug("POST /codestream/api/custom-integrations/{id}/versions")

    api = CustomIntegrationsApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2019-10-17"

    body = {}

    if "changeLog" in kwargs:
        hub.log.debug(f"Got kwarg 'changeLog' = {kwargs['changeLog']}")
        body["changeLog"] = kwargs.get("changeLog")
        del kwargs["changeLog"]
    if "description" in kwargs:
        hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
        body["description"] = kwargs.get("description")
        del kwargs["description"]
    if "version" in kwargs:
        hub.log.debug(f"Got kwarg 'version' = {kwargs['version']}")
        body["version"] = kwargs.get("version")
        del kwargs["version"]

    ret = api.create_version_by_id_using_post(body, id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def delete_draft_by_id_using_delete(hub, ctx, p_id, q_type, **kwargs):
    """Delete a Custom Integration and its versions Delete a Custom Integration with the given id and all its versions Performs DELETE /codestream/api/custom-integrations/{id}


    :param string p_id: (required in path) The ID of the Custom Integration
    :param string q_type: (required in query) The type of the Custom Integration
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    hub.log.debug("DELETE /codestream/api/custom-integrations/{id}")

    api = CustomIntegrationsApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2019-10-17"

    ret = api.delete_draft_by_id_using_delete(id=p_id, type=q_type, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def delete_version_by_id_using_delete(hub, ctx, p_id, p_version, **kwargs):
    """Delete a Custom Integration version Delete a Custom Integration version with the given id and version Performs DELETE /codestream/api/custom-integrations/{id}/versions/{version}


    :param string p_id: (required in path) The ID of the Custom Integration
    :param string p_version: (required in path) The version of the Custom Integration
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    hub.log.debug("DELETE /codestream/api/custom-integrations/{id}/versions/{version}")

    api = CustomIntegrationsApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2019-10-17"

    ret = api.delete_version_by_id_using_delete(id=p_id, version=p_version, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def deprecate_by_id_and_version_using_post(hub, ctx, p_id, p_version, **kwargs):
    """Deprecate a Custom Integration version Deprecate a Custom Integration version Performs POST /codestream/api/custom-integrations/{id}/versions/{version}/deprecate


    :param string p_id: (required in path) The ID of the Custom Integration
    :param string p_version: (required in path) The version of the Custom Integration
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    hub.log.debug(
        "POST /codestream/api/custom-integrations/{id}/versions/{version}/deprecate"
    )

    api = CustomIntegrationsApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2019-10-17"

    ret = api.deprecate_by_id_and_version_using_post(
        id=p_id, version=p_version, **kwargs
    )

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_all_custom_integration_versions_by_id_using_get(hub, ctx, p_id, **kwargs):
    """Get all versions of a Custom Integration by id Get all versions of a Custom Integration with specified id, paging and filter
      parameters Performs GET /codestream/api/custom-integrations/{id}/versions


    :param string p_id: (required in path) The ID of the Custom Integration
    :param string filter: (optional in query) To list Custom Integrations with OData like filter
    :param string orderby: (optional in query) Order by attribute
    :param string skip: (optional in query) To skip 'n' Custom Integrations for listing
    :param string top: (optional in query) To list top 'n' Custom Integrations
    :param string page: (optional in query) To select 'n'th page for listing
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    hub.log.debug("GET /codestream/api/custom-integrations/{id}/versions")

    api = CustomIntegrationsApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2019-10-17"

    ret = api.get_all_custom_integration_versions_by_id_using_get(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_custom_integration_version_by_id_using_get(
    hub, ctx, p_id, p_version, **kwargs
):
    """Get a Custom Integration by version Get a Custom Integration with the given id and version Performs GET /codestream/api/custom-integrations/{id}/versions/{version}


    :param string p_id: (required in path) The ID of the Custom Integration
    :param string p_version: (required in path) The version of the Custom Integration
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    hub.log.debug("GET /codestream/api/custom-integrations/{id}/versions/{version}")

    api = CustomIntegrationsApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2019-10-17"

    ret = api.get_custom_integration_version_by_id_using_get(
        id=p_id, version=p_version, **kwargs
    )

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_custom_integrations_using_get(hub, ctx, **kwargs):
    """Get all Custom Integrations Get all Custom Integrations with specified paging and filter parameters. Performs GET /codestream/api/custom-integrations


    :param string filter: (optional in query) To list Custom Integrations with OData like filter
    :param string orderby: (optional in query) Order by attribute
    :param string skip: (optional in query) To skip 'n' Custom Integrations for listing
    :param string top: (optional in query) To list top 'n' Custom Integrations
    :param string page: (optional in query) To select 'n'th page for listing
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    :param string type: (optional in query) type
    """

    hub.log.debug("GET /codestream/api/custom-integrations")

    api = CustomIntegrationsApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2019-10-17"

    ret = api.get_custom_integrations_using_get(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_draft_by_id_using_get(hub, ctx, p_id, **kwargs):
    """Get a Custom Integration by id Get details of a Custom Integration with the given id Performs GET /codestream/api/custom-integrations/{id}


    :param string p_id: (required in path) The ID of the Custom Integration
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    hub.log.debug("GET /codestream/api/custom-integrations/{id}")

    api = CustomIntegrationsApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2019-10-17"

    ret = api.get_draft_by_id_using_get(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def release_by_id_and_version_using_post(hub, ctx, p_id, p_version, **kwargs):
    """Release a Custom Integration version Release a Custom Integration version to be consumable in pipelines Performs POST /codestream/api/custom-integrations/{id}/versions/{version}/release


    :param string p_id: (required in path) The ID of the Custom Integration
    :param string p_version: (required in path) The version of the Custom Integration
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    hub.log.debug(
        "POST /codestream/api/custom-integrations/{id}/versions/{version}/release"
    )

    api = CustomIntegrationsApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2019-10-17"

    ret = api.release_by_id_and_version_using_post(id=p_id, version=p_version, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def restore_by_id_and_version_using_post(hub, ctx, p_id, p_version, **kwargs):
    """Restore a Custom Integration from a version to current draft Restore a Custom Integration from the given version to the current draft Performs POST /codestream/api/custom-integrations/{id}/versions/{version}/restore


    :param string p_id: (required in path) The ID of the Custom Integration
    :param string p_version: (required in path) The version of the Custom Integration
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    hub.log.debug(
        "POST /codestream/api/custom-integrations/{id}/versions/{version}/restore"
    )

    api = CustomIntegrationsApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2019-10-17"

    ret = api.restore_by_id_and_version_using_post(id=p_id, version=p_version, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def toggle_latest_using_patch(hub, ctx, p_id, p_version, **kwargs):
    """Set/Unset a release version as 'LATEST'. Sets a released Custom Integration version as 'LATEST' or unsets a version from
      'LATEST' Performs PATCH /codestream/api/custom-integrations/{id}/versions/{version}


    :param string p_id: (required in path) The ID of the Custom Integration
    :param string p_version: (required in path) The version of the Custom Integration
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    :param boolean setLatest: (optional in body) Indicates if a version of CustomIntegration should be set as 'LATEST'
    """

    hub.log.debug("PATCH /codestream/api/custom-integrations/{id}/versions/{version}")

    api = CustomIntegrationsApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2019-10-17"

    body = {}

    if "setLatest" in kwargs:
        hub.log.debug(f"Got kwarg 'setLatest' = {kwargs['setLatest']}")
        body["setLatest"] = kwargs.get("setLatest")
        del kwargs["setLatest"]

    ret = api.toggle_latest_using_patch(body, id=p_id, version=p_version, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def update_by_id_using_put(hub, ctx, p_id, name, **kwargs):
    """Update a Custom Integration by id Update a Custom Integration with the given id Performs PUT /codestream/api/custom-integrations/{id}


    :param string p_id: (required in path) The ID of the Custom Integration
    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    :param string description: (optional in body) A human-friendly description.
    :param string project: (optional in body) The project this entity belongs to.
    :param string yaml: (optional in body) YAML describing Custom integration details.
    """

    hub.log.debug("PUT /codestream/api/custom-integrations/{id}")

    api = CustomIntegrationsApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2019-10-17"

    body = {}
    body["name"] = name

    if "description" in kwargs:
        hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
        body["description"] = kwargs.get("description")
        del kwargs["description"]
    if "project" in kwargs:
        hub.log.debug(f"Got kwarg 'project' = {kwargs['project']}")
        body["project"] = kwargs.get("project")
        del kwargs["project"]
    if "yaml" in kwargs:
        hub.log.debug(f"Got kwarg 'yaml' = {kwargs['yaml']}")
        body["yaml"] = kwargs.get("yaml")
        del kwargs["yaml"]

    ret = api.update_by_id_using_put(body, id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def withdraw_by_id_and_version_using_post(hub, ctx, p_id, p_version, **kwargs):
    """Withdraw a Custom Integration version Withdraw a released/deprecated Custom Integration version to make it un-
      consumable in pipelines Performs POST /codestream/api/custom-integrations/{id}/versions/{version}/withdraw


    :param string p_id: (required in path) The ID of the Custom Integration
    :param string p_version: (required in path) The version of the Custom Integration
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    hub.log.debug(
        "POST /codestream/api/custom-integrations/{id}/versions/{version}/withdraw"
    )

    api = CustomIntegrationsApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2019-10-17"

    ret = api.withdraw_by_id_and_version_using_post(
        id=p_id, version=p_version, **kwargs
    )

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))

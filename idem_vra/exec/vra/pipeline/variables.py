from idem_vra.client.vra_pipeline_lib.api import VariablesApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def create_variable_using_post(hub, ctx, name, type, value, **kwargs):
    """Create a Variable Creates a Variable based on project name Performs POST /codestream/api/variables


    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option
    :param string type: (required in body) The type of this Variable. Ex: REGULAR / SECRET / RESTRICTED
    :param string value: (required in body) The value for this Variable.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    :param string description: (optional in body) A human-friendly description.
    :param boolean isMultiLine: (optional in body) Check box that indicates if the value of the Variable should be saved
      in a multi line format.
    :param string project: (optional in body) The project this entity belongs to.
    """

    hub.log.debug("POST /codestream/api/variables")

    api = VariablesApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2019-10-17"

    body = {}
    body["name"] = name
    body["type"] = type
    body["value"] = value

    if "description" in kwargs:
        hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
        body["description"] = kwargs.get("description")
        del kwargs["description"]
    if "isMultiLine" in kwargs:
        hub.log.debug(f"Got kwarg 'isMultiLine' = {kwargs['isMultiLine']}")
        body["isMultiLine"] = kwargs.get("isMultiLine")
        del kwargs["isMultiLine"]
    if "project" in kwargs:
        hub.log.debug(f"Got kwarg 'project' = {kwargs['project']}")
        body["project"] = kwargs.get("project")
        del kwargs["project"]

    ret = api.create_variable_using_post(body, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def delete_variable_by_id_using_delete(hub, ctx, p_id, **kwargs):
    """Deletes a Variable by Id Deletes a Variable with the given Id Performs DELETE /codestream/api/variables/{id}


    :param string p_id: (required in path) The ID of the Variable
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    hub.log.debug("DELETE /codestream/api/variables/{id}")

    api = VariablesApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2019-10-17"

    ret = api.delete_variable_by_id_using_delete(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def delete_variable_by_name_using_delete(hub, ctx, p_name, p_project, **kwargs):
    """Deletes a Variable by project and name Deletes a Variable with the given name Performs DELETE /codestream/api/variables/{project}/{name}


    :param string p_name: (required in path) The name of the Variable
    :param string p_project: (required in path) The project the Variable belongs to
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    hub.log.debug("DELETE /codestream/api/variables/{project}/{name}")

    api = VariablesApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2019-10-17"

    ret = api.delete_variable_by_name_using_delete(
        name=p_name, project=p_project, **kwargs
    )

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_all_variables_using_get(hub, ctx, **kwargs):
    """Get all Variables Get all Variables with specified paging and filter parameters. Performs GET /codestream/api/variables


    :param string filter: (optional in query) To list Variables with OData like filter
    :param string orderby: (optional in query) Order by attribute
    :param string skip: (optional in query) To skip 'n' Variables for listing
    :param string top: (optional in query) To list top 'n' Variables for listing
    :param string page: (optional in query) To select 'n'th page for listing
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    hub.log.debug("GET /codestream/api/variables")

    api = VariablesApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2019-10-17"

    ret = api.get_all_variables_using_get(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_variable_by_id_using_get(hub, ctx, p_id, **kwargs):
    """Gets a Variable Gets a Variable with the given id Performs GET /codestream/api/variables/{id}


    :param string p_id: (required in path) The ID of the Variable
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    hub.log.debug("GET /codestream/api/variables/{id}")

    api = VariablesApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2019-10-17"

    ret = api.get_variable_by_id_using_get(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_variable_by_name_using_get(hub, ctx, p_name, p_project, **kwargs):
    """Gets a Variable by project and name Get an Variable with the given project and name Performs GET /codestream/api/variables/{project}/{name}


    :param string p_name: (required in path) The name of the Variable
    :param string p_project: (required in path) The project the Variable belongs to
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    hub.log.debug("GET /codestream/api/variables/{project}/{name}")

    api = VariablesApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2019-10-17"

    ret = api.get_variable_by_name_using_get(name=p_name, project=p_project, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def update_variable_by_id_using_put(hub, ctx, p_id, name, type, value, **kwargs):
    """Update a Variable by id Updates a Variable with the given id Performs PUT /codestream/api/variables/{id}


    :param string p_id: (required in path) The ID of the Variable
    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option
    :param string type: (required in body) The type of this Variable. Ex: REGULAR / SECRET / RESTRICTED
    :param string value: (required in body) The value for this Variable.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    :param string description: (optional in body) A human-friendly description.
    :param boolean isMultiLine: (optional in body) Check box that indicates if the value of the Variable should be saved
      in a multi line format.
    :param string project: (optional in body) The project this entity belongs to.
    """

    hub.log.debug("PUT /codestream/api/variables/{id}")

    api = VariablesApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2019-10-17"

    body = {}
    body["name"] = name
    body["type"] = type
    body["value"] = value

    if "description" in kwargs:
        hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
        body["description"] = kwargs.get("description")
        del kwargs["description"]
    if "isMultiLine" in kwargs:
        hub.log.debug(f"Got kwarg 'isMultiLine' = {kwargs['isMultiLine']}")
        body["isMultiLine"] = kwargs.get("isMultiLine")
        del kwargs["isMultiLine"]
    if "project" in kwargs:
        hub.log.debug(f"Got kwarg 'project' = {kwargs['project']}")
        body["project"] = kwargs.get("project")
        del kwargs["project"]

    ret = api.update_variable_by_id_using_put(body, id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def update_variable_by_name_using_put(
    hub, ctx, p_name, p_project, name, type, value, **kwargs
):
    """Updates a Variable by project and name Update an Variable with the given project and name Performs PUT /codestream/api/variables/{project}/{name}


    :param string p_name: (required in path) The name of the Variable
    :param string p_project: (required in path) The project the Variable belongs to
    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option
    :param string type: (required in body) The type of this Variable. Ex: REGULAR / SECRET / RESTRICTED
    :param string value: (required in body) The value for this Variable.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    :param string description: (optional in body) A human-friendly description.
    :param boolean isMultiLine: (optional in body) Check box that indicates if the value of the Variable should be saved
      in a multi line format.
    :param string project: (optional in body) The project this entity belongs to.
    """

    hub.log.debug("PUT /codestream/api/variables/{project}/{name}")

    api = VariablesApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2019-10-17"

    body = {}
    body["name"] = name
    body["type"] = type
    body["value"] = value

    if "description" in kwargs:
        hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
        body["description"] = kwargs.get("description")
        del kwargs["description"]
    if "isMultiLine" in kwargs:
        hub.log.debug(f"Got kwarg 'isMultiLine' = {kwargs['isMultiLine']}")
        body["isMultiLine"] = kwargs.get("isMultiLine")
        del kwargs["isMultiLine"]
    if "project" in kwargs:
        hub.log.debug(f"Got kwarg 'project' = {kwargs['project']}")
        body["project"] = kwargs.get("project")
        del kwargs["project"]

    ret = api.update_variable_by_name_using_put(
        body, name=p_name, project=p_project, **kwargs
    )

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))

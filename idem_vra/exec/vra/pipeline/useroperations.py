from idem_vra.client.vra_pipeline_lib.api import UserOperationsApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def delete_user_operation_by_id_using_delete(hub, ctx, p_id, **kwargs):
    """Delete a User Operation by id Delete a User Operation with the given id Performs DELETE /codestream/api/user-operations/{id}


    :param string p_id: (required in path) The ID of the User Operation
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    hub.log.debug("DELETE /codestream/api/user-operations/{id}")

    api = UserOperationsApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2019-10-17"

    ret = api.delete_user_operation_by_id_using_delete(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_all_user_operations_using_get(hub, ctx, **kwargs):
    """Get all User operations Get all User operations with specified paging and filter parameters. Performs GET /codestream/api/user-operations


    :param string filter: (optional in query) To list User Operations with OData like filter
    :param string orderby: (optional in query) Order by attribute
    :param string skip: (optional in query) To skip 'n' User Operations for listing
    :param string top: (optional in query) To list top 'n' User Operations for listing
    :param string page: (optional in query) To select 'n'th page for listing
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    hub.log.debug("GET /codestream/api/user-operations")

    api = UserOperationsApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2019-10-17"

    ret = api.get_all_user_operations_using_get(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_user_operation_by_id_using_get(hub, ctx, p_id, **kwargs):
    """Get a User Operation Get a User Operation with the given id Performs GET /codestream/api/user-operations/{id}


    :param string p_id: (required in path) The ID of the User Operation
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    hub.log.debug("GET /codestream/api/user-operations/{id}")

    api = UserOperationsApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2019-10-17"

    ret = api.get_user_operation_by_id_using_get(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def modify_patch_user_operation_using_patch(hub, ctx, p_id, **kwargs):
    """Modify a User Operation Modify a User Operation with the given id Performs PATCH /codestream/api/user-operations/{id}


    :param string p_id: (required in path) The ID of the User Operation
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    :param boolean cancelPreviousPendingUserOp: (optional in body) This property tells whether to cancel previous pending user operations
      for this pipeline.
    :param string responseMessage: (optional in body) The response message which the responder would like to give.
    :param string status: (optional in body) The status of this entity.
    """

    hub.log.debug("PATCH /codestream/api/user-operations/{id}")

    api = UserOperationsApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2019-10-17"

    body = {}

    if "cancelPreviousPendingUserOp" in kwargs:
        hub.log.debug(
            f"Got kwarg 'cancelPreviousPendingUserOp' = {kwargs['cancelPreviousPendingUserOp']}"
        )
        body["cancelPreviousPendingUserOp"] = kwargs.get("cancelPreviousPendingUserOp")
        del kwargs["cancelPreviousPendingUserOp"]
    if "responseMessage" in kwargs:
        hub.log.debug(f"Got kwarg 'responseMessage' = {kwargs['responseMessage']}")
        body["responseMessage"] = kwargs.get("responseMessage")
        del kwargs["responseMessage"]
    if "status" in kwargs:
        hub.log.debug(f"Got kwarg 'status' = {kwargs['status']}")
        body["status"] = kwargs.get("status")
        del kwargs["status"]

    ret = api.modify_patch_user_operation_using_patch(body, id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def modify_post_user_operation_using_post(hub, ctx, p_id, **kwargs):
    """Modify a User Operation Modify a User Operation with the given id Performs POST /codestream/api/user-operations/{id}


    :param string p_id: (required in path) The ID of the User Operation
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    :param boolean cancelPreviousPendingUserOp: (optional in body) This property tells whether to cancel previous pending user operations
      for this pipeline.
    :param string responseMessage: (optional in body) The response message which the responder would like to give.
    :param string status: (optional in body) The status of this entity.
    """

    hub.log.debug("POST /codestream/api/user-operations/{id}")

    api = UserOperationsApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2019-10-17"

    body = {}

    if "cancelPreviousPendingUserOp" in kwargs:
        hub.log.debug(
            f"Got kwarg 'cancelPreviousPendingUserOp' = {kwargs['cancelPreviousPendingUserOp']}"
        )
        body["cancelPreviousPendingUserOp"] = kwargs.get("cancelPreviousPendingUserOp")
        del kwargs["cancelPreviousPendingUserOp"]
    if "responseMessage" in kwargs:
        hub.log.debug(f"Got kwarg 'responseMessage' = {kwargs['responseMessage']}")
        body["responseMessage"] = kwargs.get("responseMessage")
        del kwargs["responseMessage"]
    if "status" in kwargs:
        hub.log.debug(f"Got kwarg 'status' = {kwargs['status']}")
        body["status"] = kwargs.get("status")
        del kwargs["status"]

    ret = api.modify_post_user_operation_using_post(body, id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))

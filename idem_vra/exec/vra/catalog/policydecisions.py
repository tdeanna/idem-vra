from idem_vra.client.vra_catalog_lib.api import PolicyDecisionsApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get_decision_by_id_using_get2(hub, ctx, p_id, **kwargs):
    """Returns a policy decision by id. Find a specific policy decision based on the input policy decision id. Performs GET /policy/api/policyDecisions/{id}


    :param string p_id: (required in path) Policy decision Id
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    """

    hub.log.debug("GET /policy/api/policyDecisions/{id}")

    api = PolicyDecisionsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    ret = api.get_decision_by_id_using_get2(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_decisions_using_get2(hub, ctx, **kwargs):
    """Fetch a list of policy decisions. Returns a paginated list of policy decisions. If a dryRunId is provided, the
      return value has a field indicating whether the dry run is complete. Performs GET /policy/api/policyDecisions


    :param array orderby: (optional in query) Sorting criteria in the format: property (asc|desc). Default sort
      order is ascending. Multiple sort criteria are supported.
    :param integer skip: (optional in query) Number of records you want to skip
    :param integer top: (optional in query) Number of records you want
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    :param string dryRunId: (optional in query) dryRunId
    :param string policyTypeId: (optional in query) Matches will only include policies of this type
    :param string projectId: (optional in query) Matches will only include decisions with this project ID
    :param string search: (optional in query) Matches will start with this string in their policy name or target
      name or have this string somewhere in their description.
    """

    hub.log.debug("GET /policy/api/policyDecisions")

    api = PolicyDecisionsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    ret = api.get_decisions_using_get2(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))

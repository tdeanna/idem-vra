from idem_vra.client.vra_catalog_lib.api import CatalogItemTypesApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get_type_by_id_using_get2(hub, ctx, p_id, **kwargs):
    """Fetch catalog item type associated with the specified ID. Returns the Catalog Item Type with the specified ID. Performs GET /catalog/api/types/{id}


    :param string p_id: (required in path) Catalog Type ID
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    """

    hub.log.debug("GET /catalog/api/types/{id}")

    api = CatalogItemTypesApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    ret = api.get_type_by_id_using_get2(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_types_using_get4(hub, ctx, **kwargs):
    """Find all Catalog Item Types. Returns a paginated list of all available Catalog Item Types. Performs GET /catalog/api/types


    :param array orderby: (optional in query) Sorting criteria in the format: property (asc|desc). Default sort
      order is ascending. Multiple sort criteria are supported.
    :param integer skip: (optional in query) Number of records you want to skip
    :param integer top: (optional in query) Number of records you want
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    """

    hub.log.debug("GET /catalog/api/types")

    api = CatalogItemTypesApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    ret = api.get_types_using_get4(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))

from idem_vra.client.vra_catalog_lib.api import CatalogSourcesApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def delete_using_delete4(hub, ctx, p_sourceId, **kwargs):
    """Delete catalog source. Deletes the catalog source with the supplied ID. Performs DELETE /catalog/api/admin/sources/{sourceId}


    :param string p_sourceId: (required in path) Catalog source ID
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    """

    hub.log.debug("DELETE /catalog/api/admin/sources/{sourceId}")

    api = CatalogSourcesApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    ret = api.delete_using_delete4(source_id=p_sourceId, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_page_using_get2(hub, ctx, **kwargs):
    """Fetch catalog sources. Returns a paginated list of catalog sources. Performs GET /catalog/api/admin/sources


    :param array orderby: (optional in query) Sorting criteria in the format: property (asc|desc). Default sort
      order is ascending. Multiple sort criteria are supported.
    :param integer skip: (optional in query) Number of records you want to skip
    :param integer top: (optional in query) Number of records you want
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    :param string projectId: (optional in query) Find sources which contains items that can be requested in the given
      projectId
    :param string search: (optional in query) Matches will have this string in their name or description.
    """

    hub.log.debug("GET /catalog/api/admin/sources")

    api = CatalogSourcesApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    ret = api.get_page_using_get2(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_using_get2(hub, ctx, p_sourceId, **kwargs):
    """Fetch a specific catalog source for the given ID. Returns the catalog source with the supplied ID. Performs GET /catalog/api/admin/sources/{sourceId}


    :param string p_sourceId: (required in path) Catalog source ID
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    """

    hub.log.debug("GET /catalog/api/admin/sources/{sourceId}")

    api = CatalogSourcesApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    ret = api.get_using_get2(source_id=p_sourceId, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def post_using_post2(hub, ctx, config, id, name, typeId, **kwargs):
    """Create or update a catalog source. Creating or updating also imports (or re-
      imports) the associated catalog items. Creates a new catalog source or updates an existing catalog source based on the
      request body and imports catalog items from it. Performs POST /catalog/api/admin/sources


    :param object config: (required in body) Source custom configuration
    :param string id: (required in body) Catalog Source id
    :param string name: (required in body) Catalog Source name
    :param string typeId: (required in body) Type of source, e.g. blueprint, CFT... etc
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    :param boolean validationOnly: (optional in query) If true, the source will not be created. It returns the number of
      items belonging to the source. The request will still return an error
      code if the source is invalid.
    :param string createdAt: (optional in body) Creation time
    :param string createdBy: (optional in body) Created By
    :param string description: (optional in body) Catalog Source description
    :param boolean global: (optional in body) Global flag indicating that all the items can be requested across all
      projects.
    :param string iconId: (optional in body) Default Icon Id
    :param integer itemsFound: (optional in body) Number of items found
    :param integer itemsImported: (optional in body) Number of items imported.
    :param string lastImportCompletedAt: (optional in body) Last import completion time
    :param array lastImportErrors: (optional in body) Last import error(s)
    :param string lastImportStartedAt: (optional in body) Last import start time
    :param string lastUpdatedAt: (optional in body) Update time
    :param string lastUpdatedBy: (optional in body) Updated By
    :param string projectId: (optional in body) Project id where the source belongs
    """

    hub.log.debug("POST /catalog/api/admin/sources")

    api = CatalogSourcesApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    body = {}
    body["config"] = config
    body["id"] = id
    body["name"] = name
    body["typeId"] = typeId

    if "createdAt" in kwargs:
        hub.log.debug(f"Got kwarg 'createdAt' = {kwargs['createdAt']}")
        body["createdAt"] = kwargs.get("createdAt")
        del kwargs["createdAt"]
    if "createdBy" in kwargs:
        hub.log.debug(f"Got kwarg 'createdBy' = {kwargs['createdBy']}")
        body["createdBy"] = kwargs.get("createdBy")
        del kwargs["createdBy"]
    if "description" in kwargs:
        hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
        body["description"] = kwargs.get("description")
        del kwargs["description"]
    if "global" in kwargs:
        hub.log.debug(f"Got kwarg 'global' = {kwargs['global']}")
        body["global"] = kwargs.get("global")
        del kwargs["global"]
    if "iconId" in kwargs:
        hub.log.debug(f"Got kwarg 'iconId' = {kwargs['iconId']}")
        body["iconId"] = kwargs.get("iconId")
        del kwargs["iconId"]
    if "itemsFound" in kwargs:
        hub.log.debug(f"Got kwarg 'itemsFound' = {kwargs['itemsFound']}")
        body["itemsFound"] = kwargs.get("itemsFound")
        del kwargs["itemsFound"]
    if "itemsImported" in kwargs:
        hub.log.debug(f"Got kwarg 'itemsImported' = {kwargs['itemsImported']}")
        body["itemsImported"] = kwargs.get("itemsImported")
        del kwargs["itemsImported"]
    if "lastImportCompletedAt" in kwargs:
        hub.log.debug(
            f"Got kwarg 'lastImportCompletedAt' = {kwargs['lastImportCompletedAt']}"
        )
        body["lastImportCompletedAt"] = kwargs.get("lastImportCompletedAt")
        del kwargs["lastImportCompletedAt"]
    if "lastImportErrors" in kwargs:
        hub.log.debug(f"Got kwarg 'lastImportErrors' = {kwargs['lastImportErrors']}")
        body["lastImportErrors"] = kwargs.get("lastImportErrors")
        del kwargs["lastImportErrors"]
    if "lastImportStartedAt" in kwargs:
        hub.log.debug(
            f"Got kwarg 'lastImportStartedAt' = {kwargs['lastImportStartedAt']}"
        )
        body["lastImportStartedAt"] = kwargs.get("lastImportStartedAt")
        del kwargs["lastImportStartedAt"]
    if "lastUpdatedAt" in kwargs:
        hub.log.debug(f"Got kwarg 'lastUpdatedAt' = {kwargs['lastUpdatedAt']}")
        body["lastUpdatedAt"] = kwargs.get("lastUpdatedAt")
        del kwargs["lastUpdatedAt"]
    if "lastUpdatedBy" in kwargs:
        hub.log.debug(f"Got kwarg 'lastUpdatedBy' = {kwargs['lastUpdatedBy']}")
        body["lastUpdatedBy"] = kwargs.get("lastUpdatedBy")
        del kwargs["lastUpdatedBy"]
    if "projectId" in kwargs:
        hub.log.debug(f"Got kwarg 'projectId' = {kwargs['projectId']}")
        body["projectId"] = kwargs.get("projectId")
        del kwargs["projectId"]

    ret = api.post_using_post2(body, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))

from idem_vra.client.vra_catalog_lib.api import NotificationScenarioConfigurationApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def create_or_update_using_post2(hub, ctx, enabled, scenarioId, **kwargs):
    """Creates or updates a notification scenario configuration of an organization  Performs POST /notification/api/scenario-configs


    :param boolean enabled: (required in body) Notification scenario enabled
    :param string scenarioId: (required in body) Notification scenario id
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    :param string scenarioCategory: (optional in body) Notification scenario category
    :param string scenarioDescription: (optional in body) Notification scenario description
    :param string scenarioName: (optional in body) Notification scenario name
    """

    hub.log.debug("POST /notification/api/scenario-configs")

    api = NotificationScenarioConfigurationApi(
        hub.clients["idem_vra.client.vra_catalog_lib.api"]
    )
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    body = {}
    body["enabled"] = enabled
    body["scenarioId"] = scenarioId

    if "scenarioCategory" in kwargs:
        hub.log.debug(f"Got kwarg 'scenarioCategory' = {kwargs['scenarioCategory']}")
        body["scenarioCategory"] = kwargs.get("scenarioCategory")
        del kwargs["scenarioCategory"]
    if "scenarioDescription" in kwargs:
        hub.log.debug(
            f"Got kwarg 'scenarioDescription' = {kwargs['scenarioDescription']}"
        )
        body["scenarioDescription"] = kwargs.get("scenarioDescription")
        del kwargs["scenarioDescription"]
    if "scenarioName" in kwargs:
        hub.log.debug(f"Got kwarg 'scenarioName' = {kwargs['scenarioName']}")
        body["scenarioName"] = kwargs.get("scenarioName")
        del kwargs["scenarioName"]

    ret = api.create_or_update_using_post2(body, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def delete_using_delete5(hub, ctx, p_scenarioId, **kwargs):
    """Deletes a notification scenario configuration by scenario id of an organization  Performs DELETE /notification/api/scenario-configs/{scenarioId}


    :param string p_scenarioId: (required in path) Notification scenario Id
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    """

    hub.log.debug("DELETE /notification/api/scenario-configs/{scenarioId}")

    api = NotificationScenarioConfigurationApi(
        hub.clients["idem_vra.client.vra_catalog_lib.api"]
    )
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    ret = api.delete_using_delete5(scenario_id=p_scenarioId, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_all_scenario_configs_using_get2(hub, ctx, **kwargs):
    """Retrieves all notification scenario configurations of an organization  Performs GET /notification/api/scenario-configs


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    """

    hub.log.debug("GET /notification/api/scenario-configs")

    api = NotificationScenarioConfigurationApi(
        hub.clients["idem_vra.client.vra_catalog_lib.api"]
    )
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    ret = api.get_all_scenario_configs_using_get2(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_scenario_config_using_get2(hub, ctx, p_scenarioId, **kwargs):
    """Retrieves a notification scenario configuration by scenario id of an
      organization  Performs GET /notification/api/scenario-configs/{scenarioId}


    :param string p_scenarioId: (required in path) Notification Scenario Id
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    """

    hub.log.debug("GET /notification/api/scenario-configs/{scenarioId}")

    api = NotificationScenarioConfigurationApi(
        hub.clients["idem_vra.client.vra_catalog_lib.api"]
    )
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    ret = api.get_scenario_config_using_get2(scenario_id=p_scenarioId, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))

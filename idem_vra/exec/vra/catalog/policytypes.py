from idem_vra.client.vra_catalog_lib.api import PolicyTypesApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get_policy_type_by_id_using_get2(hub, ctx, p_id, **kwargs):
    """Returns the policy type with the specified ID. Find a specific policy type based on the input policy type id. Performs GET /policy/api/policyTypes/{id}


    :param string p_id: (required in path) Policy type ID
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    """

    hub.log.debug("GET /policy/api/policyTypes/{id}")

    api = PolicyTypesApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    ret = api.get_policy_type_by_id_using_get2(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_policy_type_scope_schema_using_get2(hub, ctx, p_id, **kwargs):
    """Returns the policy scope schema for the type with the specified ID. Return the policy scope schema for the given policy type. Performs GET /policy/api/policyTypes/{id}/scopeSchema


    :param string p_id: (required in path) Policy type ID
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    """

    hub.log.debug("GET /policy/api/policyTypes/{id}/scopeSchema")

    api = PolicyTypesApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    ret = api.get_policy_type_scope_schema_using_get2(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_types_using_get5(hub, ctx, **kwargs):
    """Returns a paginated list of policy types. Find all the policy types available in the current org. Performs GET /policy/api/policyTypes


    :param array orderby: (optional in query) Sorting criteria in the format: property (asc|desc). Default sort
      order is ascending. Multiple sort criteria are supported.
    :param integer skip: (optional in query) Number of records you want to skip
    :param integer top: (optional in query) Number of records you want
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    :param boolean expandSchema: (optional in query) Retrieves the schema for this policy type
    """

    hub.log.debug("GET /policy/api/policyTypes")

    api = PolicyTypesApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    ret = api.get_types_using_get5(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))

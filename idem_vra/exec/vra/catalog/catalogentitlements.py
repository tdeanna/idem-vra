from idem_vra.client.vra_catalog_lib.api import CatalogEntitlementsApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def create_entitlement_using_post2(hub, ctx, definition, id, projectId, **kwargs):
    """Create an entitlement. Creates an entitlement for a given project. Performs POST /catalog/api/admin/entitlements


    :param Any definition: (required in body)
    :param string id: (required in body) Entitlement id
    :param string projectId: (required in body) Project id
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    :param boolean migrated: (optional in body) Migrated flag for entitlements
    """

    hub.log.debug("POST /catalog/api/admin/entitlements")

    api = CatalogEntitlementsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    body = {}
    body["definition"] = definition
    body["id"] = id
    body["projectId"] = projectId

    if "migrated" in kwargs:
        hub.log.debug(f"Got kwarg 'migrated' = {kwargs['migrated']}")
        body["migrated"] = kwargs.get("migrated")
        del kwargs["migrated"]

    ret = api.create_entitlement_using_post2(body, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def delete_entitlement_using_delete2(hub, ctx, p_id, **kwargs):
    """Delete an entitlement. Deletes the entitlement with the specified id. Performs DELETE /catalog/api/admin/entitlements/{id}


    :param string p_id: (required in path) Entitlement id
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    """

    hub.log.debug("DELETE /catalog/api/admin/entitlements/{id}")

    api = CatalogEntitlementsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    ret = api.delete_entitlement_using_delete2(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_entitlements_using_get2(hub, ctx, **kwargs):
    """Returns all entitlements filtered by projectId. Returns all entitlements (filtered by projectId). Performs GET /catalog/api/admin/entitlements


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    :param string projectId: (optional in query) The project id for which to return .
    """

    hub.log.debug("GET /catalog/api/admin/entitlements")

    api = CatalogEntitlementsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    ret = api.get_entitlements_using_get2(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))

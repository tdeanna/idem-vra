from idem_vra.client.vra_catalog_lib.api import PricingCardsApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def create_policy_using_post2(hub, ctx, **kwargs):
    """Create a new pricing card Create a new pricing card based on request body and validate its field
      according to business rules. Performs POST /price/api/private/pricing-cards


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    :param string chargeModel: (optional in body)
    :param string createdAt: (optional in body)
    :param string createdBy: (optional in body)
    :param string description: (optional in body)
    :param Any fixedPrice: (optional in body)
    :param string id: (optional in body)
    :param string lastUpdatedAt: (optional in body)
    :param array meteringItems: (optional in body)
    :param string name: (optional in body)
    :param array namedMeteringItems: (optional in body)
    :param array oneTimeMeteringItems: (optional in body)
    :param string orgId: (optional in body)
    :param Any pricingCardAssignmentInfo: (optional in body)
    :param array tagBasedMeteringItems: (optional in body)
    :param array tagBasedOneTimeMeteringItems: (optional in body)
    :param array tagBasedRateFactorItems: (optional in body)
    """

    hub.log.debug("POST /price/api/private/pricing-cards")

    api = PricingCardsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    body = {}

    if "chargeModel" in kwargs:
        hub.log.debug(f"Got kwarg 'chargeModel' = {kwargs['chargeModel']}")
        body["chargeModel"] = kwargs.get("chargeModel")
        del kwargs["chargeModel"]
    if "createdAt" in kwargs:
        hub.log.debug(f"Got kwarg 'createdAt' = {kwargs['createdAt']}")
        body["createdAt"] = kwargs.get("createdAt")
        del kwargs["createdAt"]
    if "createdBy" in kwargs:
        hub.log.debug(f"Got kwarg 'createdBy' = {kwargs['createdBy']}")
        body["createdBy"] = kwargs.get("createdBy")
        del kwargs["createdBy"]
    if "description" in kwargs:
        hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
        body["description"] = kwargs.get("description")
        del kwargs["description"]
    if "fixedPrice" in kwargs:
        hub.log.debug(f"Got kwarg 'fixedPrice' = {kwargs['fixedPrice']}")
        body["fixedPrice"] = kwargs.get("fixedPrice")
        del kwargs["fixedPrice"]
    if "id" in kwargs:
        hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
        body["id"] = kwargs.get("id")
        del kwargs["id"]
    if "lastUpdatedAt" in kwargs:
        hub.log.debug(f"Got kwarg 'lastUpdatedAt' = {kwargs['lastUpdatedAt']}")
        body["lastUpdatedAt"] = kwargs.get("lastUpdatedAt")
        del kwargs["lastUpdatedAt"]
    if "meteringItems" in kwargs:
        hub.log.debug(f"Got kwarg 'meteringItems' = {kwargs['meteringItems']}")
        body["meteringItems"] = kwargs.get("meteringItems")
        del kwargs["meteringItems"]
    if "name" in kwargs:
        hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
        body["name"] = kwargs.get("name")
        del kwargs["name"]
    if "namedMeteringItems" in kwargs:
        hub.log.debug(
            f"Got kwarg 'namedMeteringItems' = {kwargs['namedMeteringItems']}"
        )
        body["namedMeteringItems"] = kwargs.get("namedMeteringItems")
        del kwargs["namedMeteringItems"]
    if "oneTimeMeteringItems" in kwargs:
        hub.log.debug(
            f"Got kwarg 'oneTimeMeteringItems' = {kwargs['oneTimeMeteringItems']}"
        )
        body["oneTimeMeteringItems"] = kwargs.get("oneTimeMeteringItems")
        del kwargs["oneTimeMeteringItems"]
    if "orgId" in kwargs:
        hub.log.debug(f"Got kwarg 'orgId' = {kwargs['orgId']}")
        body["orgId"] = kwargs.get("orgId")
        del kwargs["orgId"]
    if "pricingCardAssignmentInfo" in kwargs:
        hub.log.debug(
            f"Got kwarg 'pricingCardAssignmentInfo' = {kwargs['pricingCardAssignmentInfo']}"
        )
        body["pricingCardAssignmentInfo"] = kwargs.get("pricingCardAssignmentInfo")
        del kwargs["pricingCardAssignmentInfo"]
    if "tagBasedMeteringItems" in kwargs:
        hub.log.debug(
            f"Got kwarg 'tagBasedMeteringItems' = {kwargs['tagBasedMeteringItems']}"
        )
        body["tagBasedMeteringItems"] = kwargs.get("tagBasedMeteringItems")
        del kwargs["tagBasedMeteringItems"]
    if "tagBasedOneTimeMeteringItems" in kwargs:
        hub.log.debug(
            f"Got kwarg 'tagBasedOneTimeMeteringItems' = {kwargs['tagBasedOneTimeMeteringItems']}"
        )
        body["tagBasedOneTimeMeteringItems"] = kwargs.get(
            "tagBasedOneTimeMeteringItems"
        )
        del kwargs["tagBasedOneTimeMeteringItems"]
    if "tagBasedRateFactorItems" in kwargs:
        hub.log.debug(
            f"Got kwarg 'tagBasedRateFactorItems' = {kwargs['tagBasedRateFactorItems']}"
        )
        body["tagBasedRateFactorItems"] = kwargs.get("tagBasedRateFactorItems")
        del kwargs["tagBasedRateFactorItems"]

    ret = api.create_policy_using_post2(body, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def delete_policy_using_delete4(hub, ctx, p_id, **kwargs):
    """Delete the pricing card with specified Id Deletes the pricing card with the specified id Performs DELETE /price/api/private/pricing-cards/{id}


    :param string p_id: (required in path) pricing card Id
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    """

    hub.log.debug("DELETE /price/api/private/pricing-cards/{id}")

    api = PricingCardsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    ret = api.delete_policy_using_delete4(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_policies_using_get4(hub, ctx, **kwargs):
    """Fetch all pricing cards for private-policy cloud Returns a paginated list of pricing cards Performs GET /price/api/private/pricing-cards


    :param array orderby: (optional in query) Sorting criteria in the format: property (asc|desc). Default sort
      order is ascending. Multiple sort criteria are supported.
    :param integer skip: (optional in query) Number of records you want to skip
    :param integer top: (optional in query) Number of records you want
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    :param boolean expandAssignmentInfo: (optional in query) Whether or not returns count of assignments.
    :param boolean expandPricingCard: (optional in query) Whether or not returns detailed pricing card for each result.
    :param string search: (optional in query) Search by name and description
    """

    hub.log.debug("GET /price/api/private/pricing-cards")

    api = PricingCardsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    ret = api.get_policies_using_get4(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_policy_using_get4(hub, ctx, p_id, **kwargs):
    """Find the pricing card with specified Id Returns the pricing card with the specified id Performs GET /price/api/private/pricing-cards/{id}


    :param string p_id: (required in path) pricing card Id
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    """

    hub.log.debug("GET /price/api/private/pricing-cards/{id}")

    api = PricingCardsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    ret = api.get_policy_using_get4(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def update_policy_using_put2(hub, ctx, p_id, **kwargs):
    """Update the pricing card Updates the pricing card with the specified Id Performs PUT /price/api/private/pricing-cards/{id}


    :param string p_id: (required in path) pricing card Id
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    :param string chargeModel: (optional in body)
    :param string createdAt: (optional in body)
    :param string createdBy: (optional in body)
    :param string description: (optional in body)
    :param Any fixedPrice: (optional in body)
    :param string id: (optional in body)
    :param string lastUpdatedAt: (optional in body)
    :param array meteringItems: (optional in body)
    :param string name: (optional in body)
    :param array namedMeteringItems: (optional in body)
    :param array oneTimeMeteringItems: (optional in body)
    :param string orgId: (optional in body)
    :param Any pricingCardAssignmentInfo: (optional in body)
    :param array tagBasedMeteringItems: (optional in body)
    :param array tagBasedOneTimeMeteringItems: (optional in body)
    :param array tagBasedRateFactorItems: (optional in body)
    """

    hub.log.debug("PUT /price/api/private/pricing-cards/{id}")

    api = PricingCardsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    body = {}

    if "chargeModel" in kwargs:
        hub.log.debug(f"Got kwarg 'chargeModel' = {kwargs['chargeModel']}")
        body["chargeModel"] = kwargs.get("chargeModel")
        del kwargs["chargeModel"]
    if "createdAt" in kwargs:
        hub.log.debug(f"Got kwarg 'createdAt' = {kwargs['createdAt']}")
        body["createdAt"] = kwargs.get("createdAt")
        del kwargs["createdAt"]
    if "createdBy" in kwargs:
        hub.log.debug(f"Got kwarg 'createdBy' = {kwargs['createdBy']}")
        body["createdBy"] = kwargs.get("createdBy")
        del kwargs["createdBy"]
    if "description" in kwargs:
        hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
        body["description"] = kwargs.get("description")
        del kwargs["description"]
    if "fixedPrice" in kwargs:
        hub.log.debug(f"Got kwarg 'fixedPrice' = {kwargs['fixedPrice']}")
        body["fixedPrice"] = kwargs.get("fixedPrice")
        del kwargs["fixedPrice"]
    if "id" in kwargs:
        hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
        body["id"] = kwargs.get("id")
        del kwargs["id"]
    if "lastUpdatedAt" in kwargs:
        hub.log.debug(f"Got kwarg 'lastUpdatedAt' = {kwargs['lastUpdatedAt']}")
        body["lastUpdatedAt"] = kwargs.get("lastUpdatedAt")
        del kwargs["lastUpdatedAt"]
    if "meteringItems" in kwargs:
        hub.log.debug(f"Got kwarg 'meteringItems' = {kwargs['meteringItems']}")
        body["meteringItems"] = kwargs.get("meteringItems")
        del kwargs["meteringItems"]
    if "name" in kwargs:
        hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
        body["name"] = kwargs.get("name")
        del kwargs["name"]
    if "namedMeteringItems" in kwargs:
        hub.log.debug(
            f"Got kwarg 'namedMeteringItems' = {kwargs['namedMeteringItems']}"
        )
        body["namedMeteringItems"] = kwargs.get("namedMeteringItems")
        del kwargs["namedMeteringItems"]
    if "oneTimeMeteringItems" in kwargs:
        hub.log.debug(
            f"Got kwarg 'oneTimeMeteringItems' = {kwargs['oneTimeMeteringItems']}"
        )
        body["oneTimeMeteringItems"] = kwargs.get("oneTimeMeteringItems")
        del kwargs["oneTimeMeteringItems"]
    if "orgId" in kwargs:
        hub.log.debug(f"Got kwarg 'orgId' = {kwargs['orgId']}")
        body["orgId"] = kwargs.get("orgId")
        del kwargs["orgId"]
    if "pricingCardAssignmentInfo" in kwargs:
        hub.log.debug(
            f"Got kwarg 'pricingCardAssignmentInfo' = {kwargs['pricingCardAssignmentInfo']}"
        )
        body["pricingCardAssignmentInfo"] = kwargs.get("pricingCardAssignmentInfo")
        del kwargs["pricingCardAssignmentInfo"]
    if "tagBasedMeteringItems" in kwargs:
        hub.log.debug(
            f"Got kwarg 'tagBasedMeteringItems' = {kwargs['tagBasedMeteringItems']}"
        )
        body["tagBasedMeteringItems"] = kwargs.get("tagBasedMeteringItems")
        del kwargs["tagBasedMeteringItems"]
    if "tagBasedOneTimeMeteringItems" in kwargs:
        hub.log.debug(
            f"Got kwarg 'tagBasedOneTimeMeteringItems' = {kwargs['tagBasedOneTimeMeteringItems']}"
        )
        body["tagBasedOneTimeMeteringItems"] = kwargs.get(
            "tagBasedOneTimeMeteringItems"
        )
        del kwargs["tagBasedOneTimeMeteringItems"]
    if "tagBasedRateFactorItems" in kwargs:
        hub.log.debug(
            f"Got kwarg 'tagBasedRateFactorItems' = {kwargs['tagBasedRateFactorItems']}"
        )
        body["tagBasedRateFactorItems"] = kwargs.get("tagBasedRateFactorItems")
        del kwargs["tagBasedRateFactorItems"]

    ret = api.update_policy_using_put2(body, id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))

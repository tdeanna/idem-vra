from idem_vra.client.vra_catalog_lib.api import DeploymentsApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def check_deployment_name_exists_using_get2(hub, ctx, **kwargs):
    """Check if a deployment exists. Returns OK if a deployment with the supplied name exists. Performs GET /deployment/api/deployments/names


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    :param string name: (optional in query) Deployment name
    """

    hub.log.debug("GET /deployment/api/deployments/names")

    api = DeploymentsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    ret = api.check_deployment_name_exists_using_get2(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def check_deployment_name_using_get2(hub, ctx, p_name, **kwargs):
    """Check if a deployment exists. Returns OK if a deployment with the supplied name exists. Performs GET /deployment/api/deployments/names/{name}


    :param string p_name: (required in path) Deployment name
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    """

    hub.log.debug("GET /deployment/api/deployments/names/{name}")

    api = DeploymentsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    ret = api.check_deployment_name_using_get2(name=p_name, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def delete_deployment_using_delete2(hub, ctx, p_deploymentId, **kwargs):
    """Delete a deployment. Effectively triggers a Delete Day2 operation Deletes the deployment with the supplied ID, cleans up the associated resources
      from the Cloud Provider. Performs DELETE /deployment/api/deployments/{deploymentId}


    :param string p_deploymentId: (required in path) Deployment ID
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    """

    hub.log.debug("DELETE /deployment/api/deployments/{deploymentId}")

    api = DeploymentsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    ret = api.delete_deployment_using_delete2(deployment_id=p_deploymentId, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def delete_resource_using_delete2(
    hub, ctx, p_deploymentId, p_resourceId, **kwargs
):
    """Delete resource associated with a deployment. Effectively triggers a Delete
      Day2 operation. Deletes the resource with the specified ID and attempts to delete resource from
      the Cloud Provider. Performs DELETE /deployment/api/deployments/{deploymentId}/resources/{resourceId}


    :param string p_deploymentId: (required in path) Deployment ID
    :param string p_resourceId: (required in path) Resource ID
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    """

    hub.log.debug(
        "DELETE /deployment/api/deployments/{deploymentId}/resources/{resourceId}"
    )

    api = DeploymentsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    ret = api.delete_resource_using_delete2(
        deployment_id=p_deploymentId, resource_id=p_resourceId, **kwargs
    )

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_deployment_by_id_v3_using_get(hub, ctx, p_deploymentId, **kwargs):
    """Fetch a specific deployment. Returns the deployment with the supplied ID. Performs GET /deployment/api/deployments/{deploymentId}


    :param string p_deploymentId: (required in path) Deployment ID
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    :param boolean deleted: (optional in query) Retrieves the deployment, includes soft-deleted deployments that have
      not yet been completely deleted.
    :param array expand: (optional in query) The expanded details of the requested comma separated objects. <br />
      'resources' option returns resources with summary properties
    """

    hub.log.debug("GET /deployment/api/deployments/{deploymentId}")

    api = DeploymentsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    ret = api.get_deployment_by_id_v3_using_get(deployment_id=p_deploymentId, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_deployment_expense_history_by_id_using_get2(
    hub, ctx, p_deploymentId, **kwargs
):
    """Fetch a specific deployment's expense history. Returns the deployment expense history with the supplied ID. Performs GET /deployment/api/deployments/{deploymentId}/expense-history


    :param string p_deploymentId: (required in path) Deployment ID
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    :param string from: (optional in query) The timestamp from when history is requested. Should be of ISO_INSTANT
      format.
    :param string interval: (optional in query) The interval of the expense history. Should be one of daily, weekly or
      monthly.
    :param string to: (optional in query) The timestamp until when history is requested. Should be of
      ISO_INSTANT format.
    """

    hub.log.debug("GET /deployment/api/deployments/{deploymentId}/expense-history")

    api = DeploymentsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    ret = api.get_deployment_expense_history_by_id_using_get2(
        deployment_id=p_deploymentId, **kwargs
    )

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_deployment_filter_by_id_using_get2(hub, ctx, p_filterId, **kwargs):
    """Returns the Deployment filter with the supplied ID.  Performs GET /deployment/api/deployments/filters/{filterId}


    :param string p_filterId: (required in path) Filter Id
    :param array orderby: (optional in query) Sorting criteria in the format: property (asc|desc). Default sort
      order is ascending. Multiple sort criteria are supported.
    :param integer skip: (optional in query) Number of records you want to skip
    :param integer top: (optional in query) Number of records you want
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    :param array projects: (optional in query) A comma-separated list. Results must be associated with one of these
      project IDs.
    :param string search: (optional in query) Search string for filters
    """

    hub.log.debug("GET /deployment/api/deployments/filters/{filterId}")

    api = DeploymentsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    ret = api.get_deployment_filter_by_id_using_get2(filter_id=p_filterId, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_deployment_filters_using_get2(hub, ctx, **kwargs):
    """Returns the Deployment filters in context of given user.  Performs GET /deployment/api/deployments/filters


    :param string ISO3Country: (optional in query)
    :param string ISO3Language: (optional in query)
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    :param string country: (optional in query)
    :param string displayCountry: (optional in query)
    :param string displayLanguage: (optional in query)
    :param string displayName: (optional in query)
    :param string displayScript: (optional in query)
    :param string displayVariant: (optional in query)
    :param string language: (optional in query)
    :param string script: (optional in query)
    :param array unicodeLocaleAttributes: (optional in query)
    :param array unicodeLocaleKeys: (optional in query)
    :param string variant: (optional in query)
    """

    hub.log.debug("GET /deployment/api/deployments/filters")

    api = DeploymentsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    ret = api.get_deployment_filters_using_get2(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_deployment_resources_using_get2(hub, ctx, p_deploymentId, **kwargs):
    """Fetch resources associated with a deployment. Returns a paginated list of resources corresponding to the deployment with the
      supplied ID. Performs GET /deployment/api/deployments/{deploymentId}/resources


    :param string p_deploymentId: (required in path) Deployment ID
    :param array orderby: (optional in query) Sorting criteria in the format: property (asc|desc). Default sort
      order is ascending. Multiple sort criteria are supported.
    :param integer skip: (optional in query) Number of records you want to skip
    :param integer top: (optional in query) Number of records you want
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    :param array expand: (optional in query) The expanded details of the requested comma separated objects. Ex.
      currentRequest
    :param array names: (optional in query) Results must have exactly these resource names.
    :param array resourceTypes: (optional in query) A comma-separated list. Results must be associated with one of these
      resourceType Names.
    :param array tags: (optional in query) A comma-separated list. Results must be associated with one of these
      tags
    """

    hub.log.debug("GET /deployment/api/deployments/{deploymentId}/resources")

    api = DeploymentsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    ret = api.get_deployment_resources_using_get2(
        deployment_id=p_deploymentId, **kwargs
    )

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_deployments_for_project_using_get2(hub, ctx, p_projectId, **kwargs):
    """Returns a count of deployments using the project.  Performs GET /deployment/api/projects/{projectId}/deployment-count


    :param string p_projectId: (required in path) Project ID
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    """

    hub.log.debug("GET /deployment/api/projects/{projectId}/deployment-count")

    api = DeploymentsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    ret = api.get_deployments_for_project_using_get2(project_id=p_projectId, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_deployments_v3_using_get(hub, ctx, **kwargs):
    """Fetch all deployments. Returns a paginated list of deployments. Performs GET /deployment/api/deployments


    :param array orderby: (optional in query) Sorting criteria in the format: property (asc|desc). Default sort
      order is ascending. Multiple sort criteria are supported.
    :param integer skip: (optional in query) Number of records you want to skip
    :param integer top: (optional in query) Number of records you want
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    :param array cloudAccounts: (optional in query) A comma-separated list. Results must be associated with one of these
      cloud accounts.
    :param array cloudTypes: (optional in query) A comma-separated list. Results must be associated with one of these
      endpoint Types
    :param string createdAt: (optional in query) Comma-separated start and end dates where start date or end date is
      optional (e.g. [2020-12-01T08:00:00.000Z,2020-12-11T23:59:00.000Z],
      [2020-11-03T08:00:00.000Z,], [,2020-11-08T08:00:00.000Z]
    :param boolean deleted: (optional in query) Retrieves only soft-deleted deployments that have not yet been
      completely deleted.
    :param array expand: (optional in query) The expanded details of the requested comma separated objects. <br
      />'resources' option returns resources with summary properties.
    :param string expiresAt: (optional in query) Comma-separated start and end dates for the interval
    :param array lastRequestStatus: (optional in query) A comma-separated list of last request statuses. Allowed values are:
      ABORTED, APPROVAL_PENDING, APPROVAL_REJECTED, FAILED, INPROGRESS,
      PENDING and SUCCESSFUL. Results must be associated with one of these
      last request statuses.
    :param string lastUpdatedAt: (optional in query) Comma-separated start and end dates where start date or end date is
      optional (e.g. [2020-12-01T08:00:00.000Z,2020-12-11T23:59:00.000Z],
      [2020-11-03T08:00:00.000Z,], [,2020-11-08T08:00:00.000Z]
    :param string name: (optional in query) Results must have exactly this name.
    :param array ownedBy: (optional in query) A comma-separated list. Results must be associated with one of these
      owners
    :param array projects: (optional in query) A comma-separated list. Results must be associated with one of these
      project IDs.
    :param array requestedBy: (optional in query) A comma-separated list. Results must be associated with one of these
      requesters
    :param array resourceTypes: (optional in query) A comma-separated list. Results must be associated with one of these
      resourceType Names.
    :param string search: (optional in query) Given string should either be part of a searchable field in a
      deployment or one of deployment's resources.
    :param array status: (optional in query) A comma-separated list. Results must be associated with one of these
      statuses.
    :param array tags: (optional in query) A comma-separated list. Results must be associated with one of these
      tags
    """

    hub.log.debug("GET /deployment/api/deployments")

    api = DeploymentsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    ret = api.get_deployments_v3_using_get(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_resource_by_id_using_get4(
    hub, ctx, p_deploymentId, p_resourceId, **kwargs
):
    """Fetch resource associated with a deployment. Returns the resource with the specified ID that is correlated with the supplied
      deployment. Performs GET /deployment/api/deployments/{deploymentId}/resources/{resourceId}


    :param string p_deploymentId: (required in path) Deployment ID
    :param string p_resourceId: (required in path) Resource ID
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    :param array expand: (optional in query) The expanded details of the requested comma separated objects. Ex.
      currentRequest
    """

    hub.log.debug(
        "GET /deployment/api/deployments/{deploymentId}/resources/{resourceId}"
    )

    api = DeploymentsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    ret = api.get_resource_by_id_using_get4(
        deployment_id=p_deploymentId, resource_id=p_resourceId, **kwargs
    )

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def patch_deployment_using_patch2(hub, ctx, p_deploymentId, **kwargs):
    """Update deployment. Updates the deployment with the supplied ID. Performs PATCH /deployment/api/deployments/{deploymentId}


    :param string p_deploymentId: (required in path) Deployment ID
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    :param string description: (optional in body) New description of the deployment
    :param string iconId: (optional in body) New iconid of the deployment
    :param string name: (optional in body) New name of the deployment
    """

    hub.log.debug("PATCH /deployment/api/deployments/{deploymentId}")

    api = DeploymentsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    body = {}

    if "description" in kwargs:
        hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
        body["description"] = kwargs.get("description")
        del kwargs["description"]
    if "iconId" in kwargs:
        hub.log.debug(f"Got kwarg 'iconId' = {kwargs['iconId']}")
        body["iconId"] = kwargs.get("iconId")
        del kwargs["iconId"]
    if "name" in kwargs:
        hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
        body["name"] = kwargs.get("name")
        del kwargs["name"]

    ret = api.patch_deployment_using_patch2(
        body, deployment_id=p_deploymentId, **kwargs
    )

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))

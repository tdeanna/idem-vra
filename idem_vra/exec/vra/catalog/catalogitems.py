from idem_vra.client.vra_catalog_lib.api import CatalogItemsApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get_catalog_item_using_get5(hub, ctx, p_id, **kwargs):
    """Find a catalog item with specified ID. Returns the catalog item with the specified ID. Performs GET /catalog/api/items/{id}


    :param string p_id: (required in path) Catalog item ID
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    :param boolean expandProjects: (optional in query) Retrieves the 'projects' field of the catalog item
    """

    hub.log.debug("GET /catalog/api/items/{id}")

    api = CatalogItemsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    ret = api.get_catalog_item_using_get5(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_catalog_items_using_get5(hub, ctx, **kwargs):
    """Fetch a list of catalog items. Returns a paginated list of catalog items. Performs GET /catalog/api/items


    :param array orderby: (optional in query) Sorting criteria in the format: property (asc|desc). Default sort
      order is ascending. Multiple sort criteria are supported.
    :param integer skip: (optional in query) Number of records you want to skip
    :param integer top: (optional in query) Number of records you want
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    :param boolean expandProjects: (optional in query) Whether or not to return detailed project data for each result.
    :param array projects: (optional in query) A list of project IDs. Results will belong to one of these projects.
    :param string search: (optional in query) Matches will have this string somewhere in their name or description.
    :param array types: (optional in query) A list of Catalog Item Type IDs. Results will be one of these types.
    """

    hub.log.debug("GET /catalog/api/items")

    api = CatalogItemsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    ret = api.get_catalog_items_using_get5(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_upfront_price_response_for_catalog_item_using_get2(
    hub, ctx, p_id, p_upfrontPriceId, **kwargs
):
    """Get a response with upfront prices for a given catalog item. Returns upfront prices of a given catalog item. Performs GET /catalog/api/items/{id}/upfront-prices/{upfrontPriceId}


    :param string p_id: (required in path) Catalog Item ID
    :param string p_upfrontPriceId: (required in path) Upfront Price Request ID
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    """

    hub.log.debug("GET /catalog/api/items/{id}/upfront-prices/{upfrontPriceId}")

    api = CatalogItemsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    ret = api.get_upfront_price_response_for_catalog_item_using_get2(
        id=p_id, upfront_price_id=p_upfrontPriceId, **kwargs
    )

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_version_by_id_using_get2(hub, ctx, p_id, p_versionId, **kwargs):
    """Fetch detailed catalog item version. Returns a detailed catalog item version. Performs GET /catalog/api/items/{id}/versions/{versionId}


    :param string p_id: (required in path) Catalog Item ID
    :param string p_versionId: (required in path) Catalog Item Version ID
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    """

    hub.log.debug("GET /catalog/api/items/{id}/versions/{versionId}")

    api = CatalogItemsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    ret = api.get_version_by_id_using_get2(id=p_id, version_id=p_versionId, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_versions_using_get2(hub, ctx, p_id, **kwargs):
    """Fetch a list of catalog items with versions. Returns a paginated list of catalog item versions. Performs GET /catalog/api/items/{id}/versions


    :param string p_id: (required in path) Catalog Item ID
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    :param integer page: (optional in query) Results page you want to retrieve (0..N)
    :param integer size: (optional in query) Number of records per page.
    """

    hub.log.debug("GET /catalog/api/items/{id}/versions")

    api = CatalogItemsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    ret = api.get_versions_using_get2(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def request_catalog_item_instances_using_post1(hub, ctx, p_id, **kwargs):
    """Create deployments. Creates deployments from a catalog item. Performs POST /catalog/api/items/{id}/request


    :param string p_id: (required in path) Catalog item ID
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    :param integer bulkRequestCount: (optional in body) Deployment request count; defaults to 1 if not specified.
    :param string deploymentName: (optional in body) Name of the requested deployment
    :param object inputs: (optional in body) Input parameters for the request. These must be compliant with the
      schema of the corresponding catalog item
    :param string projectId: (optional in body) Project to be used for the request
    :param string reason: (optional in body) Reason for request
    :param string version: (optional in body) Version of the catalog item. e.g. v2.0
    """

    hub.log.debug("POST /catalog/api/items/{id}/request")

    api = CatalogItemsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    body = {}

    if "bulkRequestCount" in kwargs:
        hub.log.debug(f"Got kwarg 'bulkRequestCount' = {kwargs['bulkRequestCount']}")
        body["bulkRequestCount"] = kwargs.get("bulkRequestCount")
        del kwargs["bulkRequestCount"]
    if "deploymentName" in kwargs:
        hub.log.debug(f"Got kwarg 'deploymentName' = {kwargs['deploymentName']}")
        body["deploymentName"] = kwargs.get("deploymentName")
        del kwargs["deploymentName"]
    if "inputs" in kwargs:
        hub.log.debug(f"Got kwarg 'inputs' = {kwargs['inputs']}")
        body["inputs"] = kwargs.get("inputs")
        del kwargs["inputs"]
    if "projectId" in kwargs:
        hub.log.debug(f"Got kwarg 'projectId' = {kwargs['projectId']}")
        body["projectId"] = kwargs.get("projectId")
        del kwargs["projectId"]
    if "reason" in kwargs:
        hub.log.debug(f"Got kwarg 'reason' = {kwargs['reason']}")
        body["reason"] = kwargs.get("reason")
        del kwargs["reason"]
    if "version" in kwargs:
        hub.log.debug(f"Got kwarg 'version' = {kwargs['version']}")
        body["version"] = kwargs.get("version")
        del kwargs["version"]

    ret = api.request_catalog_item_instances_using_post1(body, id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def submit_upfront_price_request_for_catalog_item_using_post2(
    hub, ctx, p_id, **kwargs
):
    """Create a request to calculate upfront price for a given catalog item. Returns upfront price response for a given catalog item. Performs POST /catalog/api/items/{id}/upfront-prices


    :param string p_id: (required in path) Catalog Item ID
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    :param integer bulkRequestCount: (optional in body) Deployment request count; defaults to 1 if not specified.
    :param string deploymentName: (optional in body) Name of the requested deployment
    :param object inputs: (optional in body) Input parameters for the request. These must be compliant with the
      schema of the corresponding catalog item
    :param string projectId: (optional in body) Project to be used for the request
    :param string reason: (optional in body) Reason for request
    :param string version: (optional in body) Version of the catalog item. e.g. v2.0
    """

    hub.log.debug("POST /catalog/api/items/{id}/upfront-prices")

    api = CatalogItemsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    body = {}

    if "bulkRequestCount" in kwargs:
        hub.log.debug(f"Got kwarg 'bulkRequestCount' = {kwargs['bulkRequestCount']}")
        body["bulkRequestCount"] = kwargs.get("bulkRequestCount")
        del kwargs["bulkRequestCount"]
    if "deploymentName" in kwargs:
        hub.log.debug(f"Got kwarg 'deploymentName' = {kwargs['deploymentName']}")
        body["deploymentName"] = kwargs.get("deploymentName")
        del kwargs["deploymentName"]
    if "inputs" in kwargs:
        hub.log.debug(f"Got kwarg 'inputs' = {kwargs['inputs']}")
        body["inputs"] = kwargs.get("inputs")
        del kwargs["inputs"]
    if "projectId" in kwargs:
        hub.log.debug(f"Got kwarg 'projectId' = {kwargs['projectId']}")
        body["projectId"] = kwargs.get("projectId")
        del kwargs["projectId"]
    if "reason" in kwargs:
        hub.log.debug(f"Got kwarg 'reason' = {kwargs['reason']}")
        body["reason"] = kwargs.get("reason")
        del kwargs["reason"]
    if "version" in kwargs:
        hub.log.debug(f"Got kwarg 'version' = {kwargs['version']}")
        body["version"] = kwargs.get("version")
        del kwargs["version"]

    ret = api.submit_upfront_price_request_for_catalog_item_using_post2(
        body, id=p_id, **kwargs
    )

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))

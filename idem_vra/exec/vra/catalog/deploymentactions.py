from idem_vra.client.vra_catalog_lib.api import DeploymentActionsApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get_deployment_action_using_get2(
    hub, ctx, p_actionId, p_deploymentId, **kwargs
):
    """Fetch deployment action. Returns an action for the deployment specified by its Deployment ID and Action
      ID. Performs GET /deployment/api/deployments/{deploymentId}/actions/{actionId}


    :param string p_actionId: (required in path) Action ID
    :param string p_deploymentId: (required in path) Deployment ID
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    """

    hub.log.debug("GET /deployment/api/deployments/{deploymentId}/actions/{actionId}")

    api = DeploymentActionsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    ret = api.get_deployment_action_using_get2(
        action_id=p_actionId, deployment_id=p_deploymentId, **kwargs
    )

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_deployment_actions_using_get2(hub, ctx, p_deploymentId, **kwargs):
    """Fetch deployment available actions. Returns the complete list of available actions that can be performed on a given
      deployment. Performs GET /deployment/api/deployments/{deploymentId}/actions


    :param string p_deploymentId: (required in path) Deployment ID
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    """

    hub.log.debug("GET /deployment/api/deployments/{deploymentId}/actions")

    api = DeploymentActionsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    ret = api.get_deployment_actions_using_get2(deployment_id=p_deploymentId, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_resource_action_using_get4(
    hub, ctx, p_actionId, p_deploymentId, p_resourceId, **kwargs
):
    """Fetch resource action. Returns an action for the resource specified by its Resource ID and Action ID. Performs GET /deployment/api/deployments/{deploymentId}/resources/{resourceId}/actions/{actionId}


    :param string p_actionId: (required in path) Action ID
    :param string p_deploymentId: (required in path) Deployment ID
    :param string p_resourceId: (required in path) Resource ID
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    """

    hub.log.debug(
        "GET /deployment/api/deployments/{deploymentId}/resources/{resourceId}/actions/{actionId}"
    )

    api = DeploymentActionsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    ret = api.get_resource_action_using_get4(
        action_id=p_actionId,
        deployment_id=p_deploymentId,
        resource_id=p_resourceId,
        **kwargs,
    )

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_resource_actions_using_get4(
    hub, ctx, p_deploymentId, p_resourceId, **kwargs
):
    """Fetch available resource actions. Returns the complete list of available actions that can be performed on a given
      resource. Performs GET /deployment/api/deployments/{deploymentId}/resources/{resourceId}/actions


    :param string p_deploymentId: (required in path) Deployment ID
    :param string p_resourceId: (required in path) Resource ID
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    """

    hub.log.debug(
        "GET /deployment/api/deployments/{deploymentId}/resources/{resourceId}/actions"
    )

    api = DeploymentActionsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    ret = api.get_resource_actions_using_get4(
        deployment_id=p_deploymentId, resource_id=p_resourceId, **kwargs
    )

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def submit_deployment_action_request_using_post2(
    hub, ctx, p_deploymentId, **kwargs
):
    """Deployment action request. Submit a deployment action request Performs POST /deployment/api/deployments/{deploymentId}/requests


    :param string p_deploymentId: (required in path) Deployment ID
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    :param string actionId: (optional in body) The id of the action to perform.
    :param object inputs: (optional in body) Resource action request inputs
    :param string reason: (optional in body) Reason for requesting a day2 operation
    """

    hub.log.debug("POST /deployment/api/deployments/{deploymentId}/requests")

    api = DeploymentActionsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    body = {}

    if "actionId" in kwargs:
        hub.log.debug(f"Got kwarg 'actionId' = {kwargs['actionId']}")
        body["actionId"] = kwargs.get("actionId")
        del kwargs["actionId"]
    if "inputs" in kwargs:
        hub.log.debug(f"Got kwarg 'inputs' = {kwargs['inputs']}")
        body["inputs"] = kwargs.get("inputs")
        del kwargs["inputs"]
    if "reason" in kwargs:
        hub.log.debug(f"Got kwarg 'reason' = {kwargs['reason']}")
        body["reason"] = kwargs.get("reason")
        del kwargs["reason"]

    ret = api.submit_deployment_action_request_using_post2(
        body, deployment_id=p_deploymentId, **kwargs
    )

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def submit_resource_action_request_using_post4(
    hub, ctx, p_deploymentId, p_resourceId, **kwargs
):
    """Resource action request. Submit a resource action request. Performs POST /deployment/api/deployments/{deploymentId}/resources/{resourceId}/requests


    :param string p_deploymentId: (required in path) Deployment ID
    :param string p_resourceId: (required in path) Resource ID
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    :param string actionId: (optional in body) The id of the action to perform.
    :param object inputs: (optional in body) Resource action request inputs
    :param string reason: (optional in body) Reason for requesting a day2 operation
    """

    hub.log.debug(
        "POST /deployment/api/deployments/{deploymentId}/resources/{resourceId}/requests"
    )

    api = DeploymentActionsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    body = {}

    if "actionId" in kwargs:
        hub.log.debug(f"Got kwarg 'actionId' = {kwargs['actionId']}")
        body["actionId"] = kwargs.get("actionId")
        del kwargs["actionId"]
    if "inputs" in kwargs:
        hub.log.debug(f"Got kwarg 'inputs' = {kwargs['inputs']}")
        body["inputs"] = kwargs.get("inputs")
        del kwargs["inputs"]
    if "reason" in kwargs:
        hub.log.debug(f"Got kwarg 'reason' = {kwargs['reason']}")
        body["reason"] = kwargs.get("reason")
        del kwargs["reason"]

    ret = api.submit_resource_action_request_using_post4(
        body, deployment_id=p_deploymentId, resource_id=p_resourceId, **kwargs
    )

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))

from idem_vra.client.vra_catalog_lib.api import IconsApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def delete2(hub, ctx, p_id, **kwargs):
    """Delete an icon Delete an existing icon by its unique id. Performs DELETE /icon/api/icons/{id}


    :param string p_id: (required in path) Icon id
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    """

    hub.log.debug("DELETE /icon/api/icons/{id}")

    api = IconsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    ret = api.delete2(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def download2(hub, ctx, p_id, **kwargs):
    """Download an icon Download an existing icon by its unique id. Performs GET /icon/api/icons/{id}


    :param string p_id: (required in path) Icon id
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    """

    hub.log.debug("GET /icon/api/icons/{id}")

    api = IconsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    ret = api.download2(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def upload2(hub, ctx, file, **kwargs):
    """Upload an icon Create an icon. Performs POST /icon/api/icons


    :param string file: (required in body) Icon file
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    """

    hub.log.debug("POST /icon/api/icons")

    api = IconsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    body = {}
    body["file"] = file

    ret = api.upload2(body, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))

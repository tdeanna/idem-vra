from idem_vra.client.vra_catalog_lib.api import PricingCardAssignmentsApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def change_metering_assignment_strategy_using_patch2(hub, ctx, **kwargs):
    """Updates the pricing card assignment strategy for the Org.  Performs PATCH /price/api/private/pricing-card-assignments/strategy


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    :param string entityType: (optional in body) Metering Policy Assignment entityType(Strategy)
    """

    hub.log.debug("PATCH /price/api/private/pricing-card-assignments/strategy")

    api = PricingCardAssignmentsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    body = {}

    if "entityType" in kwargs:
        hub.log.debug(f"Got kwarg 'entityType' = {kwargs['entityType']}")
        body["entityType"] = kwargs.get("entityType")
        del kwargs["entityType"]

    ret = api.change_metering_assignment_strategy_using_patch2(body, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def create_metering_assignment_strategy_using_post2(hub, ctx, **kwargs):
    """Selecting the new pricing card assignment strategy,PROJECT or CLOUDZONE are
      possible values can be used while creating strategy. Also there can be only one
      strategy for a given org at a given point of time Create a new pricing card assignment strategy based on request body and
      validate its field according to business rules. Performs POST /price/api/private/pricing-card-assignments/strategy


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    :param string entityType: (optional in body) Metering Policy Assignment entityType(Strategy)
    """

    hub.log.debug("POST /price/api/private/pricing-card-assignments/strategy")

    api = PricingCardAssignmentsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    body = {}

    if "entityType" in kwargs:
        hub.log.debug(f"Got kwarg 'entityType' = {kwargs['entityType']}")
        body["entityType"] = kwargs.get("entityType")
        del kwargs["entityType"]

    ret = api.create_metering_assignment_strategy_using_post2(body, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def create_metering_policy_assignment_using_post2(hub, ctx, **kwargs):
    """Create a new pricing card assignment Create a new pricing card policy assignment based on request body and validate
      its field according to business rules. Request body with ALL entityType will
      delete the older assignments for the given pricingCardId Performs POST /price/api/private/pricing-card-assignments


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    :param string createdAt: (optional in body) Creation time
    :param string entityId: (optional in body) Pricing card assigned entity id
    :param string entityName: (optional in body) Pricing card assigned entity name
    :param string entityType: (optional in body) Pricing card assigned entity type
    :param string id: (optional in body) Id of the pricingCardAssignment
    :param string lastUpdatedAt: (optional in body) Updated time
    :param string orgId: (optional in body) OrgId of the pricingCardAssignment
    :param string pricingCardId: (optional in body) Pricing card id
    :param string pricingCardName: (optional in body) Pricing card name
    """

    hub.log.debug("POST /price/api/private/pricing-card-assignments")

    api = PricingCardAssignmentsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    body = {}

    if "createdAt" in kwargs:
        hub.log.debug(f"Got kwarg 'createdAt' = {kwargs['createdAt']}")
        body["createdAt"] = kwargs.get("createdAt")
        del kwargs["createdAt"]
    if "entityId" in kwargs:
        hub.log.debug(f"Got kwarg 'entityId' = {kwargs['entityId']}")
        body["entityId"] = kwargs.get("entityId")
        del kwargs["entityId"]
    if "entityName" in kwargs:
        hub.log.debug(f"Got kwarg 'entityName' = {kwargs['entityName']}")
        body["entityName"] = kwargs.get("entityName")
        del kwargs["entityName"]
    if "entityType" in kwargs:
        hub.log.debug(f"Got kwarg 'entityType' = {kwargs['entityType']}")
        body["entityType"] = kwargs.get("entityType")
        del kwargs["entityType"]
    if "id" in kwargs:
        hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
        body["id"] = kwargs.get("id")
        del kwargs["id"]
    if "lastUpdatedAt" in kwargs:
        hub.log.debug(f"Got kwarg 'lastUpdatedAt' = {kwargs['lastUpdatedAt']}")
        body["lastUpdatedAt"] = kwargs.get("lastUpdatedAt")
        del kwargs["lastUpdatedAt"]
    if "orgId" in kwargs:
        hub.log.debug(f"Got kwarg 'orgId' = {kwargs['orgId']}")
        body["orgId"] = kwargs.get("orgId")
        del kwargs["orgId"]
    if "pricingCardId" in kwargs:
        hub.log.debug(f"Got kwarg 'pricingCardId' = {kwargs['pricingCardId']}")
        body["pricingCardId"] = kwargs.get("pricingCardId")
        del kwargs["pricingCardId"]
    if "pricingCardName" in kwargs:
        hub.log.debug(f"Got kwarg 'pricingCardName' = {kwargs['pricingCardName']}")
        body["pricingCardName"] = kwargs.get("pricingCardName")
        del kwargs["pricingCardName"]

    ret = api.create_metering_policy_assignment_using_post2(body, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def delete_metering_policy_assignment_using_delete2(hub, ctx, p_id, **kwargs):
    """Delete the pricing card assignment with specified id Deletes the pricing card assignment with the specified id Performs DELETE /price/api/private/pricing-card-assignments/{id}


    :param string p_id: (required in path) pricing card Assignment Id
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    """

    hub.log.debug("DELETE /price/api/private/pricing-card-assignments/{id}")

    api = PricingCardAssignmentsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    ret = api.delete_metering_policy_assignment_using_delete2(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_all_metering_policy_assignments_using_get2(hub, ctx, **kwargs):
    """Fetch all pricing card assignment for private cloud Returns a paginated list of pricing card assignments Performs GET /price/api/private/pricing-card-assignments


    :param array orderby: (optional in query) Sorting criteria in the format: property (asc|desc). Default sort
      order is ascending. Multiple sort criteria are supported.
    :param integer skip: (optional in query) Number of records you want to skip
    :param integer top: (optional in query) Number of records you want
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    :param boolean refreshEntities: (optional in query) Search by name and description
    """

    hub.log.debug("GET /price/api/private/pricing-card-assignments")

    api = PricingCardAssignmentsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    ret = api.get_all_metering_policy_assignments_using_get2(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_metering_assignment_strategy_using_get2(hub, ctx, **kwargs):
    """Fetch pricing card assignment strategy for the Org Returns a pricing card assignment strategy for the Org Performs GET /price/api/private/pricing-card-assignments/strategy


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    """

    hub.log.debug("GET /price/api/private/pricing-card-assignments/strategy")

    api = PricingCardAssignmentsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    ret = api.get_metering_assignment_strategy_using_get2(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_metering_policy_assignment_using_get2(hub, ctx, p_id, **kwargs):
    """Fetch pricing card assignment for private cloud by id Returns a pricing card assignments by id Performs GET /price/api/private/pricing-card-assignments/{id}


    :param string p_id: (required in path) pricing card assignment id
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    """

    hub.log.debug("GET /price/api/private/pricing-card-assignments/{id}")

    api = PricingCardAssignmentsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    ret = api.get_metering_policy_assignment_using_get2(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def patch_metering_policy_assignment_using_patch2(hub, ctx, p_id, **kwargs):
    """Updates the pricing card assignment id with the supplied id. Request body with
      ALL entityType will delete the older assignments for the given pricingCardId  Performs PATCH /price/api/private/pricing-card-assignments/{id}


    :param string p_id: (required in path) pricing card Assignment Id
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    :param string createdAt: (optional in body) Creation time
    :param string entityId: (optional in body) Pricing card assigned entity id
    :param string entityName: (optional in body) Pricing card assigned entity name
    :param string entityType: (optional in body) Pricing card assigned entity type
    :param string id: (optional in body) Id of the pricingCardAssignment
    :param string lastUpdatedAt: (optional in body) Updated time
    :param string orgId: (optional in body) OrgId of the pricingCardAssignment
    :param string pricingCardId: (optional in body) Pricing card id
    :param string pricingCardName: (optional in body) Pricing card name
    """

    hub.log.debug("PATCH /price/api/private/pricing-card-assignments/{id}")

    api = PricingCardAssignmentsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    body = {}

    if "createdAt" in kwargs:
        hub.log.debug(f"Got kwarg 'createdAt' = {kwargs['createdAt']}")
        body["createdAt"] = kwargs.get("createdAt")
        del kwargs["createdAt"]
    if "entityId" in kwargs:
        hub.log.debug(f"Got kwarg 'entityId' = {kwargs['entityId']}")
        body["entityId"] = kwargs.get("entityId")
        del kwargs["entityId"]
    if "entityName" in kwargs:
        hub.log.debug(f"Got kwarg 'entityName' = {kwargs['entityName']}")
        body["entityName"] = kwargs.get("entityName")
        del kwargs["entityName"]
    if "entityType" in kwargs:
        hub.log.debug(f"Got kwarg 'entityType' = {kwargs['entityType']}")
        body["entityType"] = kwargs.get("entityType")
        del kwargs["entityType"]
    if "id" in kwargs:
        hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
        body["id"] = kwargs.get("id")
        del kwargs["id"]
    if "lastUpdatedAt" in kwargs:
        hub.log.debug(f"Got kwarg 'lastUpdatedAt' = {kwargs['lastUpdatedAt']}")
        body["lastUpdatedAt"] = kwargs.get("lastUpdatedAt")
        del kwargs["lastUpdatedAt"]
    if "orgId" in kwargs:
        hub.log.debug(f"Got kwarg 'orgId' = {kwargs['orgId']}")
        body["orgId"] = kwargs.get("orgId")
        del kwargs["orgId"]
    if "pricingCardId" in kwargs:
        hub.log.debug(f"Got kwarg 'pricingCardId' = {kwargs['pricingCardId']}")
        body["pricingCardId"] = kwargs.get("pricingCardId")
        del kwargs["pricingCardId"]
    if "pricingCardName" in kwargs:
        hub.log.debug(f"Got kwarg 'pricingCardName' = {kwargs['pricingCardName']}")
        body["pricingCardName"] = kwargs.get("pricingCardName")
        del kwargs["pricingCardName"]

    ret = api.patch_metering_policy_assignment_using_patch2(body, id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))

from idem_vra.client.vra_catalog_lib.api import ResourceTypesApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get_resource_type_using_get2(hub, ctx, p_resourceTypeId, **kwargs):
    """Fetch a specific Resource type. Returns the Resource type with the supplied ID. Performs GET /deployment/api/resource-types/{resourceTypeId}


    :param string p_resourceTypeId: (required in path) resourceTypeId
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    """

    hub.log.debug("GET /deployment/api/resource-types/{resourceTypeId}")

    api = ResourceTypesApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    ret = api.get_resource_type_using_get2(resource_type_id=p_resourceTypeId, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def list_resource_types_using_get2(hub, ctx, **kwargs):
    """Fetch all Resource Types. Returns a paginated list of Resource Types. Performs GET /deployment/api/resource-types


    :param array orderby: (optional in query) Sorting criteria in the format: property (asc|desc). Default sort
      order is ascending. Multiple sort criteria are supported.
    :param integer skip: (optional in query) Number of records you want to skip
    :param integer top: (optional in query) Number of records you want
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    :param boolean composableOnly: (optional in query) Include only composable resource types
    :param array ids: (optional in query) Filter by list of resource type ids
    :param array projectIds: (optional in query) Filter by list of project ids
    :param string providerId: (optional in query) Filter by provider ID
    :param string search: (optional in query) Search by name and description
    """

    hub.log.debug("GET /deployment/api/resource-types")

    api = ResourceTypesApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    ret = api.list_resource_types_using_get2(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))

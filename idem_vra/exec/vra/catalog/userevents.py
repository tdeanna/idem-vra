from idem_vra.client.vra_catalog_lib.api import UserEventsApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get_user_events_using_get2(hub, ctx, p_deploymentId, **kwargs):
    """Get user events for deployment. Returns paged user events for given deployment ID. User events represent:
      Create, Day2, Approval or vRO User Interaction Performs GET /deployment/api/deployments/{deploymentId}/userEvents


    :param string p_deploymentId: (required in path) Deployment ID
    :param array orderby: (optional in query) Sorting criteria in the format: property (asc|desc). Default sort
      order is ascending. Multiple sort criteria are supported.
    :param integer skip: (optional in query) Number of records you want to skip
    :param integer top: (optional in query) Number of records you want
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    """

    hub.log.debug("GET /deployment/api/deployments/{deploymentId}/userEvents")

    api = UserEventsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    ret = api.get_user_events_using_get2(deployment_id=p_deploymentId, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))

from idem_vra.client.vra_catalog_lib.api import ResourcesApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def create_resource_using_post2(hub, ctx, name, type, **kwargs):
    """Create a new resource. Returns the resource request response. Performs POST /deployment/api/resources


    :param string name: (required in body) Name of the resource
    :param string type: (required in body) Type of the resource
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    :param string deploymentId: (optional in body) Resource deployment id
    :param string projectId: (optional in body) Resource project id
    :param object properties: (optional in body) Resource properties
    """

    hub.log.debug("POST /deployment/api/resources")

    api = ResourcesApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    body = {}
    body["name"] = name
    body["type"] = type

    if "deploymentId" in kwargs:
        hub.log.debug(f"Got kwarg 'deploymentId' = {kwargs['deploymentId']}")
        body["deploymentId"] = kwargs.get("deploymentId")
        del kwargs["deploymentId"]
    if "projectId" in kwargs:
        hub.log.debug(f"Got kwarg 'projectId' = {kwargs['projectId']}")
        body["projectId"] = kwargs.get("projectId")
        del kwargs["projectId"]
    if "properties" in kwargs:
        hub.log.debug(f"Got kwarg 'properties' = {kwargs['properties']}")
        body["properties"] = kwargs.get("properties")
        del kwargs["properties"]

    ret = api.create_resource_using_post2(body, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_resource_by_id_using_get5(hub, ctx, p_resourceId, **kwargs):
    """Fetch a specific resource. Returns the resource with the supplied ID. Performs GET /deployment/api/resources/{resourceId}


    :param string p_resourceId: (required in path) Resource ID
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    :param array expand: (optional in query) The expanded details of the requested comma separated objects. Ex.
      project, deployment, currentRequest
    """

    hub.log.debug("GET /deployment/api/resources/{resourceId}")

    api = ResourcesApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    ret = api.get_resource_by_id_using_get5(resource_id=p_resourceId, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_resource_filter_by_id_using_get2(hub, ctx, p_filterId, **kwargs):
    """Returns the Resource filter with the supplied ID.  Performs GET /deployment/api/resources/filters/{filterId}


    :param string p_filterId: (required in path) Filter Id
    :param array orderby: (optional in query) Sorting criteria in the format: property (asc|desc). Default sort
      order is ascending. Multiple sort criteria are supported.
    :param integer skip: (optional in query) Number of records you want to skip
    :param integer top: (optional in query) Number of records you want
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    :param array projects: (optional in query) A comma-separated list. Results must be associated with one of these
      project IDs.
    :param array resourceTypes: (optional in query) A comma-separated list. Only Resources with these types will be
      included in the results.
    :param string search: (optional in query) Search string for filters
    """

    hub.log.debug("GET /deployment/api/resources/filters/{filterId}")

    api = ResourcesApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    ret = api.get_resource_filter_by_id_using_get2(filter_id=p_filterId, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_resource_filters_using_get2(hub, ctx, **kwargs):
    """Returns the Resource filters in context of given user.  Performs GET /deployment/api/resources/filters


    :param string ISO3Country: (optional in query)
    :param string ISO3Language: (optional in query)
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    :param string country: (optional in query)
    :param string displayCountry: (optional in query)
    :param string displayLanguage: (optional in query)
    :param string displayName: (optional in query)
    :param string displayScript: (optional in query)
    :param string displayVariant: (optional in query)
    :param string language: (optional in query)
    :param array resourceTypes: (optional in query) A comma-separated list. Only Resources with these types will be
      included in the results.
    :param string script: (optional in query)
    :param array unicodeLocaleAttributes: (optional in query)
    :param array unicodeLocaleKeys: (optional in query)
    :param string variant: (optional in query)
    """

    hub.log.debug("GET /deployment/api/resources/filters")

    api = ResourcesApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    ret = api.get_resource_filters_using_get2(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_resources_using_get2(hub, ctx, **kwargs):
    """Fetch all resources. Returns a paginated list of resources. Performs GET /deployment/api/resources


    :param array orderby: (optional in query) Sorting criteria in the format: property (asc|desc). Default sort
      order is ascending. Multiple sort criteria are supported.
    :param integer skip: (optional in query) Number of records you want to skip
    :param integer top: (optional in query) Number of records you want
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    :param array cloudAccounts: (optional in query) A comma-separated list. Results must be associated with one of these
      cloud accounts.
    :param array cloudTypes: (optional in query) A comma-separated list. Results must be associated with one of these
      cloud Types
    :param array expand: (optional in query) The expanded details of the requested comma separated objects. Ex.
      project, deployment, currentRequest
    :param array projects: (optional in query) A comma-separated list. Results must be associated with one of these
      project IDs.
    :param array resourceTypes: (optional in query) A comma-separated list. Results must be associated with one of these
      resourceType Names.
    :param string search: (optional in query) Given string should be part of a searchable field in one of the
      resources.
    :param array tags: (optional in query) A comma-separated list. Results must be associated with one of these
      tags
    """

    hub.log.debug("GET /deployment/api/resources")

    api = ResourcesApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    ret = api.get_resources_using_get2(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))

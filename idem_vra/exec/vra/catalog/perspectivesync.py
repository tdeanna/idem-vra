from idem_vra.client.vra_catalog_lib.api import PerspectiveSyncApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def sync_perspective_group_using_post2(hub, ctx, **kwargs):
    """On demand Perspective Sync To do on demand perspective sync for within the given org Performs POST /price/api/cloudhealth/perspective-sync


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    """

    hub.log.debug("POST /price/api/cloudhealth/perspective-sync")

    api = PerspectiveSyncApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    ret = api.sync_perspective_group_using_post2(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))

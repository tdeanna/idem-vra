from idem_vra.client.vra_catalog_lib.api import PoliciesApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def delete_policy_using_delete5(hub, ctx, p_id, **kwargs):
    """Delete a policy Delete a specified policy corresponding to its unique id. Performs DELETE /policy/api/policies/{id}


    :param string p_id: (required in path) Policy ID
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    """

    hub.log.debug("DELETE /policy/api/policies/{id}")

    api = PoliciesApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    ret = api.delete_policy_using_delete5(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def dry_run_policy_using_post2(hub, ctx, q_dryRun, typeId, **kwargs):
    """Trigger a policy dry-run. Dry-run an existing policy to rehearse actual policy effect on application. Performs POST /policy/api/policies


    :param string q_dryRun: (required in query)
    :param string typeId: (required in body) The policy type ID.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    :param string createdAt: (optional in body) Policy creation timestamp.
    :param string createdBy: (optional in body) Policy author.
    :param Any criteria: (optional in body)
    :param object definition: (optional in body) Policy-type-specific settings such as lease limits for lease policies.
    :param object definitionLegend: (optional in body)
    :param string description: (optional in body) The policy description.
    :param string enforcementType: (optional in body) Defines enforcement type for policy. Default enforcement type is HARD.
    :param string id: (optional in body) The policy ID.
    :param string lastUpdatedAt: (optional in body) Most recent policy update timestamp.
    :param string lastUpdatedBy: (optional in body) Most recent policy editor.
    :param string name: (optional in body) The policy name.
    :param string orgId: (optional in body) The ID of the organization to which the policy belongs.
    :param string projectId: (optional in body) For project-scoped policies, the ID of the project to which the policy
      belongs.
    :param Any scopeCriteria: (optional in body)
    :param Any statistics: (optional in body)
    """

    hub.log.debug("POST /policy/api/policies")

    api = PoliciesApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    body = {}
    body["typeId"] = typeId

    if "createdAt" in kwargs:
        hub.log.debug(f"Got kwarg 'createdAt' = {kwargs['createdAt']}")
        body["createdAt"] = kwargs.get("createdAt")
        del kwargs["createdAt"]
    if "createdBy" in kwargs:
        hub.log.debug(f"Got kwarg 'createdBy' = {kwargs['createdBy']}")
        body["createdBy"] = kwargs.get("createdBy")
        del kwargs["createdBy"]
    if "criteria" in kwargs:
        hub.log.debug(f"Got kwarg 'criteria' = {kwargs['criteria']}")
        body["criteria"] = kwargs.get("criteria")
        del kwargs["criteria"]
    if "definition" in kwargs:
        hub.log.debug(f"Got kwarg 'definition' = {kwargs['definition']}")
        body["definition"] = kwargs.get("definition")
        del kwargs["definition"]
    if "definitionLegend" in kwargs:
        hub.log.debug(f"Got kwarg 'definitionLegend' = {kwargs['definitionLegend']}")
        body["definitionLegend"] = kwargs.get("definitionLegend")
        del kwargs["definitionLegend"]
    if "description" in kwargs:
        hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
        body["description"] = kwargs.get("description")
        del kwargs["description"]
    if "enforcementType" in kwargs:
        hub.log.debug(f"Got kwarg 'enforcementType' = {kwargs['enforcementType']}")
        body["enforcementType"] = kwargs.get("enforcementType")
        del kwargs["enforcementType"]
    if "id" in kwargs:
        hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
        body["id"] = kwargs.get("id")
        del kwargs["id"]
    if "lastUpdatedAt" in kwargs:
        hub.log.debug(f"Got kwarg 'lastUpdatedAt' = {kwargs['lastUpdatedAt']}")
        body["lastUpdatedAt"] = kwargs.get("lastUpdatedAt")
        del kwargs["lastUpdatedAt"]
    if "lastUpdatedBy" in kwargs:
        hub.log.debug(f"Got kwarg 'lastUpdatedBy' = {kwargs['lastUpdatedBy']}")
        body["lastUpdatedBy"] = kwargs.get("lastUpdatedBy")
        del kwargs["lastUpdatedBy"]
    if "name" in kwargs:
        hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
        body["name"] = kwargs.get("name")
        del kwargs["name"]
    if "orgId" in kwargs:
        hub.log.debug(f"Got kwarg 'orgId' = {kwargs['orgId']}")
        body["orgId"] = kwargs.get("orgId")
        del kwargs["orgId"]
    if "projectId" in kwargs:
        hub.log.debug(f"Got kwarg 'projectId' = {kwargs['projectId']}")
        body["projectId"] = kwargs.get("projectId")
        del kwargs["projectId"]
    if "scopeCriteria" in kwargs:
        hub.log.debug(f"Got kwarg 'scopeCriteria' = {kwargs['scopeCriteria']}")
        body["scopeCriteria"] = kwargs.get("scopeCriteria")
        del kwargs["scopeCriteria"]
    if "statistics" in kwargs:
        hub.log.debug(f"Got kwarg 'statistics' = {kwargs['statistics']}")
        body["statistics"] = kwargs.get("statistics")
        del kwargs["statistics"]

    ret = api.dry_run_policy_using_post2(body, dry_run=q_dryRun, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_policies_using_get5(hub, ctx, **kwargs):
    """Returns a paginated list of policies. Find all the policies associated with current org. Performs GET /policy/api/policies


    :param array orderby: (optional in query) Sorting criteria in the format: property (asc|desc). Default sort
      order is ascending. Multiple sort criteria are supported.
    :param integer skip: (optional in query) Number of records you want to skip
    :param integer top: (optional in query) Number of records you want
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    :param boolean computeStats: (optional in query) computeStats
    :param boolean expandDefinition: (optional in query) Retrieves policy definition information for each returned policy.
    :param string search: (optional in query) Matches will start with this string in their name or have this string
      somewhere in their description.
    """

    hub.log.debug("GET /policy/api/policies")

    api = PoliciesApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    ret = api.get_policies_using_get5(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_policy_using_get5(hub, ctx, p_id, **kwargs):
    """Returns a specified policy. Find a specific policy based on the input policy id. Performs GET /policy/api/policies/{id}


    :param string p_id: (required in path) Policy ID
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    :param boolean computeStats: (optional in query) computeStats
    """

    hub.log.debug("GET /policy/api/policies/{id}")

    api = PoliciesApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    ret = api.get_policy_using_get5(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))

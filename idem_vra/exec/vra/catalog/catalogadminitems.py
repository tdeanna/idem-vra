from idem_vra.client.vra_catalog_lib.api import CatalogAdminItemsApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get_catalog_item_using_get4(hub, ctx, p_id, **kwargs):
    """Find a catalog item with specified id. Returns the catalog item with the specified id. Performs GET /catalog/api/admin/items/{id}


    :param string p_id: (required in path) Catalog item id
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    :param boolean includeSource: (optional in query) Includes the Content Source metadata for the Catalog Item
    """

    hub.log.debug("GET /catalog/api/admin/items/{id}")

    api = CatalogAdminItemsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    ret = api.get_catalog_item_using_get4(id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_catalog_items_using_get4(hub, ctx, **kwargs):
    """Fetch a list of catalog items. Returns a paginated list of catalog items. Performs GET /catalog/api/admin/items


    :param array orderby: (optional in query) Sorting criteria in the format: property (asc|desc). Default sort
      order is ascending. Multiple sort criteria are supported.
    :param integer skip: (optional in query) Number of records you want to skip
    :param integer top: (optional in query) Number of records you want
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    :param string projectId: (optional in query) Matches which can be requested within project with the given projectId
    :param string search: (optional in query) Matches will have this string somewhere in their name or description.
    :param array sourceIds: (optional in query) A list of Content Source IDs. Results will be from one of these
      sources.
    :param array types: (optional in query) A list of Catalog Item Type IDs. Results will be one of these types.
    """

    hub.log.debug("GET /catalog/api/admin/items")

    api = CatalogAdminItemsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    ret = api.get_catalog_items_using_get4(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def update_catalog_item_using_patch2(hub, ctx, p_id, **kwargs):
    """Set an icon or request limit to a catalog item. Updates a catalog item with specified icon id or request limit. Performs PATCH /catalog/api/admin/items/{id}


    :param string p_id: (required in path) The unique id of item to update.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). If you do not
      specify explicitly an exact version, you will be calling the latest
      supported API version.
    :param integer bulkRequestLimit: (optional in body) Max number of instances that can be requested at a time
    :param string formId: (optional in body) form id
    :param string iconId: (optional in body) icon id
    """

    hub.log.debug("PATCH /catalog/api/admin/items/{id}")

    api = CatalogAdminItemsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2020-08-25"

    body = {}

    if "bulkRequestLimit" in kwargs:
        hub.log.debug(f"Got kwarg 'bulkRequestLimit' = {kwargs['bulkRequestLimit']}")
        body["bulkRequestLimit"] = kwargs.get("bulkRequestLimit")
        del kwargs["bulkRequestLimit"]
    if "formId" in kwargs:
        hub.log.debug(f"Got kwarg 'formId' = {kwargs['formId']}")
        body["formId"] = kwargs.get("formId")
        del kwargs["formId"]
    if "iconId" in kwargs:
        hub.log.debug(f"Got kwarg 'iconId' = {kwargs['iconId']}")
        body["iconId"] = kwargs.get("iconId")
        del kwargs["iconId"]

    ret = api.update_catalog_item_using_patch2(body, id=p_id, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))

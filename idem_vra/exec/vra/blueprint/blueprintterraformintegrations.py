from idem_vra.client.vra_blueprint_lib.api import BlueprintTerraformIntegrationsApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def create_blueprint_from_mapping_using_post(hub, ctx, **kwargs):
    """Creates a blueprint from a Terraform blueprint configuration (obtained from
      /create-blueprint-mapping).  Performs POST /blueprint/api/blueprint-integrations/terraform/create-blueprint-from-mapping


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about
    :param string createdAt: (optional in body) Created time
    :param string createdBy: (optional in body) Created by
    :param string description: (optional in body) Blueprint description.
    :param string id: (optional in body) Object ID
    :param string name: (optional in body) Blueprint name.
    :param string orgId: (optional in body) Org ID
    :param string projectId: (optional in body) Project ID
    :param string projectName: (optional in body) Project Name
    :param Any terraformToBlueprintMapping: (optional in body)
    :param string updatedAt: (optional in body) Updated time
    :param string updatedBy: (optional in body) Updated by
    :param string version: (optional in body) Terraform runtime version.
    """

    hub.log.debug(
        "POST /blueprint/api/blueprint-integrations/terraform/create-blueprint-from-mapping"
    )

    api = BlueprintTerraformIntegrationsApi(
        hub.clients["idem_vra.client.vra_blueprint_lib.api"]
    )
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2019-09-12"

    body = {}

    if "createdAt" in kwargs:
        hub.log.debug(f"Got kwarg 'createdAt' = {kwargs['createdAt']}")
        body["createdAt"] = kwargs.get("createdAt")
        del kwargs["createdAt"]
    if "createdBy" in kwargs:
        hub.log.debug(f"Got kwarg 'createdBy' = {kwargs['createdBy']}")
        body["createdBy"] = kwargs.get("createdBy")
        del kwargs["createdBy"]
    if "description" in kwargs:
        hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
        body["description"] = kwargs.get("description")
        del kwargs["description"]
    if "id" in kwargs:
        hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
        body["id"] = kwargs.get("id")
        del kwargs["id"]
    if "name" in kwargs:
        hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
        body["name"] = kwargs.get("name")
        del kwargs["name"]
    if "orgId" in kwargs:
        hub.log.debug(f"Got kwarg 'orgId' = {kwargs['orgId']}")
        body["orgId"] = kwargs.get("orgId")
        del kwargs["orgId"]
    if "projectId" in kwargs:
        hub.log.debug(f"Got kwarg 'projectId' = {kwargs['projectId']}")
        body["projectId"] = kwargs.get("projectId")
        del kwargs["projectId"]
    if "projectName" in kwargs:
        hub.log.debug(f"Got kwarg 'projectName' = {kwargs['projectName']}")
        body["projectName"] = kwargs.get("projectName")
        del kwargs["projectName"]
    if "terraformToBlueprintMapping" in kwargs:
        hub.log.debug(
            f"Got kwarg 'terraformToBlueprintMapping' = {kwargs['terraformToBlueprintMapping']}"
        )
        body["terraformToBlueprintMapping"] = kwargs.get("terraformToBlueprintMapping")
        del kwargs["terraformToBlueprintMapping"]
    if "updatedAt" in kwargs:
        hub.log.debug(f"Got kwarg 'updatedAt' = {kwargs['updatedAt']}")
        body["updatedAt"] = kwargs.get("updatedAt")
        del kwargs["updatedAt"]
    if "updatedBy" in kwargs:
        hub.log.debug(f"Got kwarg 'updatedBy' = {kwargs['updatedBy']}")
        body["updatedBy"] = kwargs.get("updatedBy")
        del kwargs["updatedBy"]
    if "version" in kwargs:
        hub.log.debug(f"Got kwarg 'version' = {kwargs['version']}")
        body["version"] = kwargs.get("version")
        del kwargs["version"]

    ret = api.create_blueprint_from_mapping_using_post(body, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def create_blueprint_mapping_using_post(hub, ctx, **kwargs):
    """Retrieves and parses the specified Terraform configuration file(s) and returns
      relevant information for blueprint construction.  Performs POST /blueprint/api/blueprint-integrations/terraform/create-blueprint-mapping


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about
    :param string commitId: (optional in body) ID that identifies the commit that corresponds to the desired version
      of the remote file(s).
    :param string repositoryId: (optional in body) The ID of the relevant configuration source repository.
    :param string sourceDirectory: (optional in body) A path to the desired Terraform directory.
    """

    hub.log.debug(
        "POST /blueprint/api/blueprint-integrations/terraform/create-blueprint-mapping"
    )

    api = BlueprintTerraformIntegrationsApi(
        hub.clients["idem_vra.client.vra_blueprint_lib.api"]
    )
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2019-09-12"

    body = {}

    if "commitId" in kwargs:
        hub.log.debug(f"Got kwarg 'commitId' = {kwargs['commitId']}")
        body["commitId"] = kwargs.get("commitId")
        del kwargs["commitId"]
    if "repositoryId" in kwargs:
        hub.log.debug(f"Got kwarg 'repositoryId' = {kwargs['repositoryId']}")
        body["repositoryId"] = kwargs.get("repositoryId")
        del kwargs["repositoryId"]
    if "sourceDirectory" in kwargs:
        hub.log.debug(f"Got kwarg 'sourceDirectory' = {kwargs['sourceDirectory']}")
        body["sourceDirectory"] = kwargs.get("sourceDirectory")
        del kwargs["sourceDirectory"]

    ret = api.create_blueprint_mapping_using_post(body, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def create_terraform_version_using_post(hub, ctx, **kwargs):
    """Creates a version  Performs POST /blueprint/api/blueprint-integrations/terraform/versions


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about
    :param string authenticationType: (optional in body) The type of authentication for the download url
    :param string createdAt: (optional in body) Created time
    :param string createdBy: (optional in body) Created by
    :param string description: (optional in body) Version description
    :param boolean enabled: (optional in body) Version status
    :param string id: (optional in body) Version ID
    :param string orgId: (optional in body) Org ID
    :param string password: (optional in body) The password for basic authentication
    :param string sha256Checksum: (optional in body) The sha256 checksum of the terraform binary
    :param string updatedAt: (optional in body) Updated time
    :param string updatedBy: (optional in body) Updated by
    :param string url: (optional in body) Download url
    :param string username: (optional in body) The user name for basic authentication
    :param string version: (optional in body) The numeric version of terraform release
    """

    hub.log.debug("POST /blueprint/api/blueprint-integrations/terraform/versions")

    api = BlueprintTerraformIntegrationsApi(
        hub.clients["idem_vra.client.vra_blueprint_lib.api"]
    )
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2019-09-12"

    body = {}

    if "authenticationType" in kwargs:
        hub.log.debug(
            f"Got kwarg 'authenticationType' = {kwargs['authenticationType']}"
        )
        body["authenticationType"] = kwargs.get("authenticationType")
        del kwargs["authenticationType"]
    if "createdAt" in kwargs:
        hub.log.debug(f"Got kwarg 'createdAt' = {kwargs['createdAt']}")
        body["createdAt"] = kwargs.get("createdAt")
        del kwargs["createdAt"]
    if "createdBy" in kwargs:
        hub.log.debug(f"Got kwarg 'createdBy' = {kwargs['createdBy']}")
        body["createdBy"] = kwargs.get("createdBy")
        del kwargs["createdBy"]
    if "description" in kwargs:
        hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
        body["description"] = kwargs.get("description")
        del kwargs["description"]
    if "enabled" in kwargs:
        hub.log.debug(f"Got kwarg 'enabled' = {kwargs['enabled']}")
        body["enabled"] = kwargs.get("enabled")
        del kwargs["enabled"]
    if "id" in kwargs:
        hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
        body["id"] = kwargs.get("id")
        del kwargs["id"]
    if "orgId" in kwargs:
        hub.log.debug(f"Got kwarg 'orgId' = {kwargs['orgId']}")
        body["orgId"] = kwargs.get("orgId")
        del kwargs["orgId"]
    if "password" in kwargs:
        hub.log.debug(f"Got kwarg 'password' = {kwargs['password']}")
        body["password"] = kwargs.get("password")
        del kwargs["password"]
    if "sha256Checksum" in kwargs:
        hub.log.debug(f"Got kwarg 'sha256Checksum' = {kwargs['sha256Checksum']}")
        body["sha256Checksum"] = kwargs.get("sha256Checksum")
        del kwargs["sha256Checksum"]
    if "updatedAt" in kwargs:
        hub.log.debug(f"Got kwarg 'updatedAt' = {kwargs['updatedAt']}")
        body["updatedAt"] = kwargs.get("updatedAt")
        del kwargs["updatedAt"]
    if "updatedBy" in kwargs:
        hub.log.debug(f"Got kwarg 'updatedBy' = {kwargs['updatedBy']}")
        body["updatedBy"] = kwargs.get("updatedBy")
        del kwargs["updatedBy"]
    if "url" in kwargs:
        hub.log.debug(f"Got kwarg 'url' = {kwargs['url']}")
        body["url"] = kwargs.get("url")
        del kwargs["url"]
    if "username" in kwargs:
        hub.log.debug(f"Got kwarg 'username' = {kwargs['username']}")
        body["username"] = kwargs.get("username")
        del kwargs["username"]
    if "version" in kwargs:
        hub.log.debug(f"Got kwarg 'version' = {kwargs['version']}")
        body["version"] = kwargs.get("version")
        del kwargs["version"]

    ret = api.create_terraform_version_using_post(body, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def delete_terraform_version_using_delete(hub, ctx, p_versionId, **kwargs):
    """Deletes a terraform version  Performs DELETE /blueprint/api/blueprint-integrations/terraform/versions/{versionId}


    :param string p_versionId: (required in path) versionId
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about
    """

    hub.log.debug(
        "DELETE /blueprint/api/blueprint-integrations/terraform/versions/{versionId}"
    )

    api = BlueprintTerraformIntegrationsApi(
        hub.clients["idem_vra.client.vra_blueprint_lib.api"]
    )
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2019-09-12"

    ret = api.delete_terraform_version_using_delete(version_id=p_versionId, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_configuration_source_tree_using_get(
    hub, ctx, q_configurationSourceId, **kwargs
):
    """Shows directories of the configuration source repository that correspond to
      Terraform configurations.  Performs GET /blueprint/api/blueprint-integrations/terraform/get-configuration-source-tree


    :param string q_configurationSourceId: (required in query) The ID of the configuration source to inspect.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about
    :param string commitId: (optional in query) The commit ID corresponding to the version of the configuration
      source.
    :param string path: (optional in query) A file path prefix. The prefix is interpreted in the context of the
      configuration source's path prefix. Results will only include
      directories under this path.
    """

    hub.log.debug(
        "GET /blueprint/api/blueprint-integrations/terraform/get-configuration-source-tree"
    )

    api = BlueprintTerraformIntegrationsApi(
        hub.clients["idem_vra.client.vra_blueprint_lib.api"]
    )
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2019-09-12"

    ret = api.get_configuration_source_tree_using_get(
        configuration_source_id=q_configurationSourceId, **kwargs
    )

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_terraform_configuration_source_commit_list_using_get(
    hub, ctx, q_configurationSourceId, **kwargs
):
    """Returns a paginated list of commits for a specified configuration source.  Performs GET /blueprint/api/blueprint-integrations/terraform/get-configuration-source-commits


    :param string q_configurationSourceId: (required in query) A Configuration Source ID.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about
    """

    hub.log.debug(
        "GET /blueprint/api/blueprint-integrations/terraform/get-configuration-source-commits"
    )

    api = BlueprintTerraformIntegrationsApi(
        hub.clients["idem_vra.client.vra_blueprint_lib.api"]
    )
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2019-09-12"

    ret = api.get_terraform_configuration_source_commit_list_using_get(
        configuration_source_id=q_configurationSourceId, **kwargs
    )

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_terraform_configuration_sources_using_get(hub, ctx, q_projects, **kwargs):
    """Returns a paginated list of configuration sources configured as storage for
      Terraform configurations.  Performs GET /blueprint/api/blueprint-integrations/terraform/get-configuration-sources


    :param array q_projects: (required in query) A comma-separated list of project IDs. Results will be filtered so
      only configuration sources accessible from one or more of these
      projects will be returned.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about
    :param string search: (optional in query) A search parameter to search based on configuration source name or
      repository.
    """

    hub.log.debug(
        "GET /blueprint/api/blueprint-integrations/terraform/get-configuration-sources"
    )

    api = BlueprintTerraformIntegrationsApi(
        hub.clients["idem_vra.client.vra_blueprint_lib.api"]
    )
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2019-09-12"

    ret = api.get_terraform_configuration_sources_using_get(
        projects=q_projects, **kwargs
    )

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_terraform_version_using_get(hub, ctx, p_versionId, **kwargs):
    """Returns Terraform Version details  Performs GET /blueprint/api/blueprint-integrations/terraform/versions/{versionId}


    :param string p_versionId: (required in path) versionId
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about
    """

    hub.log.debug(
        "GET /blueprint/api/blueprint-integrations/terraform/versions/{versionId}"
    )

    api = BlueprintTerraformIntegrationsApi(
        hub.clients["idem_vra.client.vra_blueprint_lib.api"]
    )
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2019-09-12"

    ret = api.get_terraform_version_using_get(version_id=p_versionId, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def list_terraform_versions_using_get(hub, ctx, **kwargs):
    """Lists terraform versions  Performs GET /blueprint/api/blueprint-integrations/terraform/versions


    :param array orderby: (optional in query) Sorting criteria in the format: property (asc|desc). Default sort
      order is descending on updatedAt. Sorting is supported on fields
      createdAt, updatedAt, createdBy, updatedBy, version.
    :param integer skip: (optional in query) Number of records you want to skip
    :param integer top: (optional in query) Number of records you want
    :param boolean onlyEnabled: (optional in query) Include only enabled versions
    """

    hub.log.debug("GET /blueprint/api/blueprint-integrations/terraform/versions")

    api = BlueprintTerraformIntegrationsApi(
        hub.clients["idem_vra.client.vra_blueprint_lib.api"]
    )

    ret = api.list_terraform_versions_using_get(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def update_terraform_version_using_patch(hub, ctx, p_versionId, **kwargs):
    """Updates a terraform version  Performs PATCH /blueprint/api/blueprint-integrations/terraform/versions/{versionId}


    :param string p_versionId: (required in path) versionId
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about
    :param string authenticationType: (optional in body) The type of authentication for the download url
    :param string createdAt: (optional in body) Created time
    :param string createdBy: (optional in body) Created by
    :param string description: (optional in body) Version description
    :param boolean enabled: (optional in body) Version status
    :param string id: (optional in body) Version ID
    :param string orgId: (optional in body) Org ID
    :param string password: (optional in body) The password for basic authentication
    :param string sha256Checksum: (optional in body) The sha256 checksum of the terraform binary
    :param string updatedAt: (optional in body) Updated time
    :param string updatedBy: (optional in body) Updated by
    :param string url: (optional in body) Download url
    :param string username: (optional in body) The user name for basic authentication
    :param string version: (optional in body) The numeric version of terraform release
    """

    hub.log.debug(
        "PATCH /blueprint/api/blueprint-integrations/terraform/versions/{versionId}"
    )

    api = BlueprintTerraformIntegrationsApi(
        hub.clients["idem_vra.client.vra_blueprint_lib.api"]
    )
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2019-09-12"

    body = {}

    if "authenticationType" in kwargs:
        hub.log.debug(
            f"Got kwarg 'authenticationType' = {kwargs['authenticationType']}"
        )
        body["authenticationType"] = kwargs.get("authenticationType")
        del kwargs["authenticationType"]
    if "createdAt" in kwargs:
        hub.log.debug(f"Got kwarg 'createdAt' = {kwargs['createdAt']}")
        body["createdAt"] = kwargs.get("createdAt")
        del kwargs["createdAt"]
    if "createdBy" in kwargs:
        hub.log.debug(f"Got kwarg 'createdBy' = {kwargs['createdBy']}")
        body["createdBy"] = kwargs.get("createdBy")
        del kwargs["createdBy"]
    if "description" in kwargs:
        hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
        body["description"] = kwargs.get("description")
        del kwargs["description"]
    if "enabled" in kwargs:
        hub.log.debug(f"Got kwarg 'enabled' = {kwargs['enabled']}")
        body["enabled"] = kwargs.get("enabled")
        del kwargs["enabled"]
    if "id" in kwargs:
        hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
        body["id"] = kwargs.get("id")
        del kwargs["id"]
    if "orgId" in kwargs:
        hub.log.debug(f"Got kwarg 'orgId' = {kwargs['orgId']}")
        body["orgId"] = kwargs.get("orgId")
        del kwargs["orgId"]
    if "password" in kwargs:
        hub.log.debug(f"Got kwarg 'password' = {kwargs['password']}")
        body["password"] = kwargs.get("password")
        del kwargs["password"]
    if "sha256Checksum" in kwargs:
        hub.log.debug(f"Got kwarg 'sha256Checksum' = {kwargs['sha256Checksum']}")
        body["sha256Checksum"] = kwargs.get("sha256Checksum")
        del kwargs["sha256Checksum"]
    if "updatedAt" in kwargs:
        hub.log.debug(f"Got kwarg 'updatedAt' = {kwargs['updatedAt']}")
        body["updatedAt"] = kwargs.get("updatedAt")
        del kwargs["updatedAt"]
    if "updatedBy" in kwargs:
        hub.log.debug(f"Got kwarg 'updatedBy' = {kwargs['updatedBy']}")
        body["updatedBy"] = kwargs.get("updatedBy")
        del kwargs["updatedBy"]
    if "url" in kwargs:
        hub.log.debug(f"Got kwarg 'url' = {kwargs['url']}")
        body["url"] = kwargs.get("url")
        del kwargs["url"]
    if "username" in kwargs:
        hub.log.debug(f"Got kwarg 'username' = {kwargs['username']}")
        body["username"] = kwargs.get("username")
        del kwargs["username"]
    if "version" in kwargs:
        hub.log.debug(f"Got kwarg 'version' = {kwargs['version']}")
        body["version"] = kwargs.get("version")
        del kwargs["version"]

    ret = api.update_terraform_version_using_patch(
        body, version_id=p_versionId, **kwargs
    )

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))

from idem_vra.client.vra_blueprint_lib.api import BlueprintRequestsApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def cancel_blueprint_request_using_post(hub, ctx, p_requestId, **kwargs):
    """Cancel request  Performs POST /blueprint/api/blueprint-requests/{requestId}/actions/cancel


    :param string p_requestId: (required in path) requestId
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about
    """

    hub.log.debug("POST /blueprint/api/blueprint-requests/{requestId}/actions/cancel")

    api = BlueprintRequestsApi(hub.clients["idem_vra.client.vra_blueprint_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2019-09-12"

    ret = api.cancel_blueprint_request_using_post(request_id=p_requestId, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def create_blueprint_request_using_post1(hub, ctx, **kwargs):
    """Creates a blueprint request  Performs POST /blueprint/api/blueprint-requests


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about
    :param string blueprintId: (optional in body) Blueprint Id
    :param string blueprintVersion: (optional in body) Blueprint version
    :param string cancelRequestedAt: (optional in body) Cancel request time
    :param string cancelRequestedBy: (optional in body) Cancel requested by
    :param string content: (optional in body) Blueprint YAML content
    :param string createdAt: (optional in body) Created time
    :param string createdBy: (optional in body) Created by
    :param string deploymentId: (optional in body) Existing deployment Id
    :param string deploymentName: (optional in body) Name for the new deployment
    :param string description: (optional in body) Description for the new request
    :param boolean destroy: (optional in body) Destroy existing deployment
    :param string failureMessage: (optional in body) Failure message
    :param string flowExecutionId: (optional in body) Flow execution Id
    :param string flowId: (optional in body) Flow Id
    :param string id: (optional in body) Object ID
    :param boolean ignoreDeleteFailures: (optional in body) Ignore delete failures in blueprint request
    :param object inputs: (optional in body) Blueprint request inputs
    :param string orgId: (optional in body) Org ID
    :param boolean plan: (optional in body) Plan only without affecting existing deployment
    :param string projectId: (optional in body) Project ID
    :param string projectName: (optional in body) Project Name
    :param string reason: (optional in body) Reason for requesting a blueprint
    :param string requestTrackerId: (optional in body) Request tracker Id
    :param boolean simulate: (optional in body) Simulate blueprint request with providers
    :param string status: (optional in body) Status
    :param array targetResources: (optional in body) Target resources
    :param string updatedAt: (optional in body) Updated time
    :param string updatedBy: (optional in body) Updated by
    :param array validationMessages: (optional in body) Validation messages
    """

    hub.log.debug("POST /blueprint/api/blueprint-requests")

    api = BlueprintRequestsApi(hub.clients["idem_vra.client.vra_blueprint_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2019-09-12"

    body = {}

    if "blueprintId" in kwargs:
        hub.log.debug(f"Got kwarg 'blueprintId' = {kwargs['blueprintId']}")
        body["blueprintId"] = kwargs.get("blueprintId")
        del kwargs["blueprintId"]
    if "blueprintVersion" in kwargs:
        hub.log.debug(f"Got kwarg 'blueprintVersion' = {kwargs['blueprintVersion']}")
        body["blueprintVersion"] = kwargs.get("blueprintVersion")
        del kwargs["blueprintVersion"]
    if "cancelRequestedAt" in kwargs:
        hub.log.debug(f"Got kwarg 'cancelRequestedAt' = {kwargs['cancelRequestedAt']}")
        body["cancelRequestedAt"] = kwargs.get("cancelRequestedAt")
        del kwargs["cancelRequestedAt"]
    if "cancelRequestedBy" in kwargs:
        hub.log.debug(f"Got kwarg 'cancelRequestedBy' = {kwargs['cancelRequestedBy']}")
        body["cancelRequestedBy"] = kwargs.get("cancelRequestedBy")
        del kwargs["cancelRequestedBy"]
    if "content" in kwargs:
        hub.log.debug(f"Got kwarg 'content' = {kwargs['content']}")
        body["content"] = kwargs.get("content")
        del kwargs["content"]
    if "createdAt" in kwargs:
        hub.log.debug(f"Got kwarg 'createdAt' = {kwargs['createdAt']}")
        body["createdAt"] = kwargs.get("createdAt")
        del kwargs["createdAt"]
    if "createdBy" in kwargs:
        hub.log.debug(f"Got kwarg 'createdBy' = {kwargs['createdBy']}")
        body["createdBy"] = kwargs.get("createdBy")
        del kwargs["createdBy"]
    if "deploymentId" in kwargs:
        hub.log.debug(f"Got kwarg 'deploymentId' = {kwargs['deploymentId']}")
        body["deploymentId"] = kwargs.get("deploymentId")
        del kwargs["deploymentId"]
    if "deploymentName" in kwargs:
        hub.log.debug(f"Got kwarg 'deploymentName' = {kwargs['deploymentName']}")
        body["deploymentName"] = kwargs.get("deploymentName")
        del kwargs["deploymentName"]
    if "description" in kwargs:
        hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
        body["description"] = kwargs.get("description")
        del kwargs["description"]
    if "destroy" in kwargs:
        hub.log.debug(f"Got kwarg 'destroy' = {kwargs['destroy']}")
        body["destroy"] = kwargs.get("destroy")
        del kwargs["destroy"]
    if "failureMessage" in kwargs:
        hub.log.debug(f"Got kwarg 'failureMessage' = {kwargs['failureMessage']}")
        body["failureMessage"] = kwargs.get("failureMessage")
        del kwargs["failureMessage"]
    if "flowExecutionId" in kwargs:
        hub.log.debug(f"Got kwarg 'flowExecutionId' = {kwargs['flowExecutionId']}")
        body["flowExecutionId"] = kwargs.get("flowExecutionId")
        del kwargs["flowExecutionId"]
    if "flowId" in kwargs:
        hub.log.debug(f"Got kwarg 'flowId' = {kwargs['flowId']}")
        body["flowId"] = kwargs.get("flowId")
        del kwargs["flowId"]
    if "id" in kwargs:
        hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
        body["id"] = kwargs.get("id")
        del kwargs["id"]
    if "ignoreDeleteFailures" in kwargs:
        hub.log.debug(
            f"Got kwarg 'ignoreDeleteFailures' = {kwargs['ignoreDeleteFailures']}"
        )
        body["ignoreDeleteFailures"] = kwargs.get("ignoreDeleteFailures")
        del kwargs["ignoreDeleteFailures"]
    if "inputs" in kwargs:
        hub.log.debug(f"Got kwarg 'inputs' = {kwargs['inputs']}")
        body["inputs"] = kwargs.get("inputs")
        del kwargs["inputs"]
    if "orgId" in kwargs:
        hub.log.debug(f"Got kwarg 'orgId' = {kwargs['orgId']}")
        body["orgId"] = kwargs.get("orgId")
        del kwargs["orgId"]
    if "plan" in kwargs:
        hub.log.debug(f"Got kwarg 'plan' = {kwargs['plan']}")
        body["plan"] = kwargs.get("plan")
        del kwargs["plan"]
    if "projectId" in kwargs:
        hub.log.debug(f"Got kwarg 'projectId' = {kwargs['projectId']}")
        body["projectId"] = kwargs.get("projectId")
        del kwargs["projectId"]
    if "projectName" in kwargs:
        hub.log.debug(f"Got kwarg 'projectName' = {kwargs['projectName']}")
        body["projectName"] = kwargs.get("projectName")
        del kwargs["projectName"]
    if "reason" in kwargs:
        hub.log.debug(f"Got kwarg 'reason' = {kwargs['reason']}")
        body["reason"] = kwargs.get("reason")
        del kwargs["reason"]
    if "requestTrackerId" in kwargs:
        hub.log.debug(f"Got kwarg 'requestTrackerId' = {kwargs['requestTrackerId']}")
        body["requestTrackerId"] = kwargs.get("requestTrackerId")
        del kwargs["requestTrackerId"]
    if "simulate" in kwargs:
        hub.log.debug(f"Got kwarg 'simulate' = {kwargs['simulate']}")
        body["simulate"] = kwargs.get("simulate")
        del kwargs["simulate"]
    if "status" in kwargs:
        hub.log.debug(f"Got kwarg 'status' = {kwargs['status']}")
        body["status"] = kwargs.get("status")
        del kwargs["status"]
    if "targetResources" in kwargs:
        hub.log.debug(f"Got kwarg 'targetResources' = {kwargs['targetResources']}")
        body["targetResources"] = kwargs.get("targetResources")
        del kwargs["targetResources"]
    if "updatedAt" in kwargs:
        hub.log.debug(f"Got kwarg 'updatedAt' = {kwargs['updatedAt']}")
        body["updatedAt"] = kwargs.get("updatedAt")
        del kwargs["updatedAt"]
    if "updatedBy" in kwargs:
        hub.log.debug(f"Got kwarg 'updatedBy' = {kwargs['updatedBy']}")
        body["updatedBy"] = kwargs.get("updatedBy")
        del kwargs["updatedBy"]
    if "validationMessages" in kwargs:
        hub.log.debug(
            f"Got kwarg 'validationMessages' = {kwargs['validationMessages']}"
        )
        body["validationMessages"] = kwargs.get("validationMessages")
        del kwargs["validationMessages"]

    ret = api.create_blueprint_request_using_post1(body, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def delete_blueprint_request_using_delete1(hub, ctx, p_requestId, **kwargs):
    """Deletes request  Performs DELETE /blueprint/api/blueprint-requests/{requestId}


    :param string p_requestId: (required in path) requestId
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about
    :param boolean force: (optional in query) Force delete if not in final stage
    """

    hub.log.debug("DELETE /blueprint/api/blueprint-requests/{requestId}")

    api = BlueprintRequestsApi(hub.clients["idem_vra.client.vra_blueprint_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2019-09-12"

    ret = api.delete_blueprint_request_using_delete1(request_id=p_requestId, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_blueprint_plan_using_get1(hub, ctx, p_requestId, **kwargs):
    """Returns request plan  Performs GET /blueprint/api/blueprint-requests/{requestId}/plan


    :param string p_requestId: (required in path) requestId
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about
    :param boolean expand: (optional in query) Detailed plan
    """

    hub.log.debug("GET /blueprint/api/blueprint-requests/{requestId}/plan")

    api = BlueprintRequestsApi(hub.clients["idem_vra.client.vra_blueprint_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2019-09-12"

    ret = api.get_blueprint_plan_using_get1(request_id=p_requestId, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_blueprint_request_using_get1(hub, ctx, p_requestId, **kwargs):
    """Returns blueprint request details  Performs GET /blueprint/api/blueprint-requests/{requestId}


    :param string p_requestId: (required in path) requestId
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about
    """

    hub.log.debug("GET /blueprint/api/blueprint-requests/{requestId}")

    api = BlueprintRequestsApi(hub.clients["idem_vra.client.vra_blueprint_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2019-09-12"

    ret = api.get_blueprint_request_using_get1(request_id=p_requestId, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_blueprint_resources_plan_using_get1(hub, ctx, p_requestId, **kwargs):
    """Returns request resources plan  Performs GET /blueprint/api/blueprint-requests/{requestId}/resources-plan


    :param string p_requestId: (required in path) requestId
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about
    :param boolean expand: (optional in query) Detailed plan
    """

    hub.log.debug("GET /blueprint/api/blueprint-requests/{requestId}/resources-plan")

    api = BlueprintRequestsApi(hub.clients["idem_vra.client.vra_blueprint_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2019-09-12"

    ret = api.get_blueprint_resources_plan_using_get1(request_id=p_requestId, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def list_blueprint_requests_using_get1(hub, ctx, **kwargs):
    """Lists blueprint requests  Performs GET /blueprint/api/blueprint-requests


    :param array orderby: (optional in query) Sorting criteria in the format: property (asc|desc). Default sort
      order is descending on updatedAt. Sorting is supported on fields
      createdAt, updatedAt, createdBy, updatedBy.
    :param array select: (optional in query) Fields to include in content.
    :param integer skip: (optional in query) Number of records you want to skip
    :param integer top: (optional in query) Number of records you want
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about
    :param string deploymentId: (optional in query) Deployment Id filter
    :param boolean includePlan: (optional in query) Plan Requests filter
    """

    hub.log.debug("GET /blueprint/api/blueprint-requests")

    api = BlueprintRequestsApi(hub.clients["idem_vra.client.vra_blueprint_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2019-09-12"

    ret = api.list_blueprint_requests_using_get1(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))

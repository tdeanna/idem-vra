from idem_vra.client.vra_blueprint_lib.api import AboutApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get_about_using_get1(hub, ctx, **kwargs):
    """Returns blueprint API information The page contains information about the supported API versions and the latest
    API version. The version parameter is optional but highly recommended.
    If you do not specify explicitly an exact version, you will be calling the
    latest supported API version.
    Here is an example of a call which specifies the exact version you are using:
    GET /blueprint/api/blueprints?apiVersion=2019-09-12 Performs GET /blueprint/api/about


    """

    hub.log.debug("GET /blueprint/api/about")

    api = AboutApi(hub.clients["idem_vra.client.vra_blueprint_lib.api"])

    ret = api.get_about_using_get1(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))

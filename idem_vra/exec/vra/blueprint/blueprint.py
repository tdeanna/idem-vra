from idem_vra.client.vra_blueprint_lib.api import BlueprintApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def create_blueprint_using_post1(hub, ctx, name, projectId, **kwargs):
    """Creates a blueprint  Performs POST /blueprint/api/blueprints


    :param string name: (required in body) Blueprint name
    :param string projectId: (required in body) Project ID
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about
    :param string content: (optional in body) Blueprint YAML content
    :param string contentSourceId: (optional in body) Content source id
    :param string contentSourcePath: (optional in body) Content source path
    :param string contentSourceSyncAt: (optional in body) Content source last sync time
    :param array contentSourceSyncMessages: (optional in body) Content source last sync messages
    :param string contentSourceSyncStatus: (optional in body) Content source last sync status
    :param string contentSourceType: (optional in body) Content source type
    :param string createdAt: (optional in body) Created time
    :param string createdBy: (optional in body) Created by
    :param string description: (optional in body) Blueprint description
    :param string id: (optional in body) Object ID
    :param string orgId: (optional in body) Org ID
    :param string projectName: (optional in body) Project Name
    :param boolean requestScopeOrg: (optional in body) Flag to indicate blueprint can be requested from any project in org
    :param string selfLink: (optional in body) Blueprint self link
    :param string status: (optional in body) Blueprint status
    :param integer totalReleasedVersions: (optional in body) Total released versions
    :param integer totalVersions: (optional in body) Total versions
    :param string updatedAt: (optional in body) Updated time
    :param string updatedBy: (optional in body) Updated by
    :param boolean valid: (optional in body) Validation result on update
    :param array validationMessages: (optional in body) Validation messages
    """

    hub.log.debug("POST /blueprint/api/blueprints")

    api = BlueprintApi(hub.clients["idem_vra.client.vra_blueprint_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2019-09-12"

    body = {}
    body["name"] = name
    body["projectId"] = projectId

    if "content" in kwargs:
        hub.log.debug(f"Got kwarg 'content' = {kwargs['content']}")
        body["content"] = kwargs.get("content")
        del kwargs["content"]
    if "contentSourceId" in kwargs:
        hub.log.debug(f"Got kwarg 'contentSourceId' = {kwargs['contentSourceId']}")
        body["contentSourceId"] = kwargs.get("contentSourceId")
        del kwargs["contentSourceId"]
    if "contentSourcePath" in kwargs:
        hub.log.debug(f"Got kwarg 'contentSourcePath' = {kwargs['contentSourcePath']}")
        body["contentSourcePath"] = kwargs.get("contentSourcePath")
        del kwargs["contentSourcePath"]
    if "contentSourceSyncAt" in kwargs:
        hub.log.debug(
            f"Got kwarg 'contentSourceSyncAt' = {kwargs['contentSourceSyncAt']}"
        )
        body["contentSourceSyncAt"] = kwargs.get("contentSourceSyncAt")
        del kwargs["contentSourceSyncAt"]
    if "contentSourceSyncMessages" in kwargs:
        hub.log.debug(
            f"Got kwarg 'contentSourceSyncMessages' = {kwargs['contentSourceSyncMessages']}"
        )
        body["contentSourceSyncMessages"] = kwargs.get("contentSourceSyncMessages")
        del kwargs["contentSourceSyncMessages"]
    if "contentSourceSyncStatus" in kwargs:
        hub.log.debug(
            f"Got kwarg 'contentSourceSyncStatus' = {kwargs['contentSourceSyncStatus']}"
        )
        body["contentSourceSyncStatus"] = kwargs.get("contentSourceSyncStatus")
        del kwargs["contentSourceSyncStatus"]
    if "contentSourceType" in kwargs:
        hub.log.debug(f"Got kwarg 'contentSourceType' = {kwargs['contentSourceType']}")
        body["contentSourceType"] = kwargs.get("contentSourceType")
        del kwargs["contentSourceType"]
    if "createdAt" in kwargs:
        hub.log.debug(f"Got kwarg 'createdAt' = {kwargs['createdAt']}")
        body["createdAt"] = kwargs.get("createdAt")
        del kwargs["createdAt"]
    if "createdBy" in kwargs:
        hub.log.debug(f"Got kwarg 'createdBy' = {kwargs['createdBy']}")
        body["createdBy"] = kwargs.get("createdBy")
        del kwargs["createdBy"]
    if "description" in kwargs:
        hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
        body["description"] = kwargs.get("description")
        del kwargs["description"]
    if "id" in kwargs:
        hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
        body["id"] = kwargs.get("id")
        del kwargs["id"]
    if "orgId" in kwargs:
        hub.log.debug(f"Got kwarg 'orgId' = {kwargs['orgId']}")
        body["orgId"] = kwargs.get("orgId")
        del kwargs["orgId"]
    if "projectName" in kwargs:
        hub.log.debug(f"Got kwarg 'projectName' = {kwargs['projectName']}")
        body["projectName"] = kwargs.get("projectName")
        del kwargs["projectName"]
    if "requestScopeOrg" in kwargs:
        hub.log.debug(f"Got kwarg 'requestScopeOrg' = {kwargs['requestScopeOrg']}")
        body["requestScopeOrg"] = kwargs.get("requestScopeOrg")
        del kwargs["requestScopeOrg"]
    if "selfLink" in kwargs:
        hub.log.debug(f"Got kwarg 'selfLink' = {kwargs['selfLink']}")
        body["selfLink"] = kwargs.get("selfLink")
        del kwargs["selfLink"]
    if "status" in kwargs:
        hub.log.debug(f"Got kwarg 'status' = {kwargs['status']}")
        body["status"] = kwargs.get("status")
        del kwargs["status"]
    if "totalReleasedVersions" in kwargs:
        hub.log.debug(
            f"Got kwarg 'totalReleasedVersions' = {kwargs['totalReleasedVersions']}"
        )
        body["totalReleasedVersions"] = kwargs.get("totalReleasedVersions")
        del kwargs["totalReleasedVersions"]
    if "totalVersions" in kwargs:
        hub.log.debug(f"Got kwarg 'totalVersions' = {kwargs['totalVersions']}")
        body["totalVersions"] = kwargs.get("totalVersions")
        del kwargs["totalVersions"]
    if "updatedAt" in kwargs:
        hub.log.debug(f"Got kwarg 'updatedAt' = {kwargs['updatedAt']}")
        body["updatedAt"] = kwargs.get("updatedAt")
        del kwargs["updatedAt"]
    if "updatedBy" in kwargs:
        hub.log.debug(f"Got kwarg 'updatedBy' = {kwargs['updatedBy']}")
        body["updatedBy"] = kwargs.get("updatedBy")
        del kwargs["updatedBy"]
    if "valid" in kwargs:
        hub.log.debug(f"Got kwarg 'valid' = {kwargs['valid']}")
        body["valid"] = kwargs.get("valid")
        del kwargs["valid"]
    if "validationMessages" in kwargs:
        hub.log.debug(
            f"Got kwarg 'validationMessages' = {kwargs['validationMessages']}"
        )
        body["validationMessages"] = kwargs.get("validationMessages")
        del kwargs["validationMessages"]

    ret = api.create_blueprint_using_post1(body, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def create_blueprint_version_using_post1(
    hub, ctx, p_blueprintId, version, **kwargs
):
    """Creates version for the given blueprint ID  Performs POST /blueprint/api/blueprints/{blueprintId}/versions


    :param string p_blueprintId: (required in path) blueprintId
    :param string version: (required in body) Blueprint version
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about
    :param string changeLog: (optional in body) Blueprint version change log
    :param string description: (optional in body) Blueprint version description
    :param boolean release: (optional in body) Flag indicating to release version
    """

    hub.log.debug("POST /blueprint/api/blueprints/{blueprintId}/versions")

    api = BlueprintApi(hub.clients["idem_vra.client.vra_blueprint_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2019-09-12"

    body = {}
    body["version"] = version

    if "changeLog" in kwargs:
        hub.log.debug(f"Got kwarg 'changeLog' = {kwargs['changeLog']}")
        body["changeLog"] = kwargs.get("changeLog")
        del kwargs["changeLog"]
    if "description" in kwargs:
        hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
        body["description"] = kwargs.get("description")
        del kwargs["description"]
    if "release" in kwargs:
        hub.log.debug(f"Got kwarg 'release' = {kwargs['release']}")
        body["release"] = kwargs.get("release")
        del kwargs["release"]

    ret = api.create_blueprint_version_using_post1(
        body, blueprint_id=p_blueprintId, **kwargs
    )

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def delete_blueprint_using_delete1(hub, ctx, p_blueprintId, **kwargs):
    """Deletes a blueprint  Performs DELETE /blueprint/api/blueprints/{blueprintId}


    :param string p_blueprintId: (required in path) blueprintId
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about
    """

    hub.log.debug("DELETE /blueprint/api/blueprints/{blueprintId}")

    api = BlueprintApi(hub.clients["idem_vra.client.vra_blueprint_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2019-09-12"

    ret = api.delete_blueprint_using_delete1(blueprint_id=p_blueprintId, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_blueprint_inputs_schema_using_get1(hub, ctx, p_blueprintId, **kwargs):
    """Returns blueprint inputs schema  Performs GET /blueprint/api/blueprints/{blueprintId}/inputs-schema


    :param string p_blueprintId: (required in path) blueprintId
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about
    """

    hub.log.debug("GET /blueprint/api/blueprints/{blueprintId}/inputs-schema")

    api = BlueprintApi(hub.clients["idem_vra.client.vra_blueprint_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2019-09-12"

    ret = api.get_blueprint_inputs_schema_using_get1(
        blueprint_id=p_blueprintId, **kwargs
    )

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_blueprint_using_get1(hub, ctx, p_blueprintId, **kwargs):
    """Returns blueprint details  Performs GET /blueprint/api/blueprints/{blueprintId}


    :param string p_blueprintId: (required in path) blueprintId
    :param array select: (optional in query) Fields to include in content.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about
    """

    hub.log.debug("GET /blueprint/api/blueprints/{blueprintId}")

    api = BlueprintApi(hub.clients["idem_vra.client.vra_blueprint_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2019-09-12"

    ret = api.get_blueprint_using_get1(blueprint_id=p_blueprintId, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_blueprint_version_inputs_schema_using_get1(
    hub, ctx, p_blueprintId, p_version, **kwargs
):
    """Returns blueprint version inputs schema  Performs GET /blueprint/api/blueprints/{blueprintId}/versions/{version}/inputs-schema


    :param string p_blueprintId: (required in path) blueprintId
    :param string p_version: (required in path) version
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about
    """

    hub.log.debug(
        "GET /blueprint/api/blueprints/{blueprintId}/versions/{version}/inputs-schema"
    )

    api = BlueprintApi(hub.clients["idem_vra.client.vra_blueprint_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2019-09-12"

    ret = api.get_blueprint_version_inputs_schema_using_get1(
        blueprint_id=p_blueprintId, version=p_version, **kwargs
    )

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def get_blueprint_version_using_get1(
    hub, ctx, p_blueprintId, p_version, **kwargs
):
    """Returns versioned blueprint details  Performs GET /blueprint/api/blueprints/{blueprintId}/versions/{version}


    :param string p_blueprintId: (required in path) blueprintId
    :param string p_version: (required in path) version
    :param array select: (optional in query) Fields to include in content.
    """

    hub.log.debug("GET /blueprint/api/blueprints/{blueprintId}/versions/{version}")

    api = BlueprintApi(hub.clients["idem_vra.client.vra_blueprint_lib.api"])

    ret = api.get_blueprint_version_using_get1(
        blueprint_id=p_blueprintId, version=p_version, **kwargs
    )

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def list_blueprint_versions_using_get(hub, ctx, p_blueprintId, **kwargs):
    """Lists blueprint versions  Performs GET /blueprint/api/blueprints/{blueprintId}/versions


    :param string p_blueprintId: (required in path) blueprintId
    :param array orderby: (optional in query) Sorting criteria in the format: property (asc|desc). Default sort
      order is descending on updatedAt. Sorting is supported on fields
      createdAt, updatedAt, createdBy, updatedBy, name, version.
    :param array select: (optional in query) Fields to include in content.
    :param integer skip: (optional in query) Number of records you want to skip
    :param integer top: (optional in query) Number of records you want
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about
    :param array propertyGroups: (optional in query) Filter versions with any of the specified property groups
    :param string status: (optional in query) Filter by blueprint status: versioned / released
    :param string version: (optional in query) Filter by version
    """

    hub.log.debug("GET /blueprint/api/blueprints/{blueprintId}/versions")

    api = BlueprintApi(hub.clients["idem_vra.client.vra_blueprint_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2019-09-12"

    ret = api.list_blueprint_versions_using_get(blueprint_id=p_blueprintId, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def list_blueprints_using_get1(hub, ctx, **kwargs):
    """Lists draft blueprint  Performs GET /blueprint/api/blueprints


    :param array orderby: (optional in query) Sorting criteria in the format: property (asc|desc). Default sort
      order is descending on updatedAt. Sorting is supported on fields
      createdAt, updatedAt, createdBy, updatedBy, name.
    :param array select: (optional in query) Fields to include in content.
    :param integer skip: (optional in query) Number of records you want to skip
    :param integer top: (optional in query) Number of records you want
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about
    :param string name: (optional in query) Filter by name
    :param array projects: (optional in query) A comma-separated list. Results must be associated with one of these
      project IDs.
    :param array propertyGroups: (optional in query) Filter blueprints with any of the specified property groups
    :param boolean released: (optional in query) Filter blueprints with at least one released version
    :param string search: (optional in query) Search by name and description
    :param boolean versioned: (optional in query) Filter blueprints with at least one version
    """

    hub.log.debug("GET /blueprint/api/blueprints")

    api = BlueprintApi(hub.clients["idem_vra.client.vra_blueprint_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2019-09-12"

    ret = api.list_blueprints_using_get1(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def release_blueprint_version_using_post1(
    hub, ctx, p_blueprintId, p_version, **kwargs
):
    """Release versioned blueprint to catalog  Performs POST /blueprint/api/blueprints/{blueprintId}/versions/{version}/actions/release


    :param string p_blueprintId: (required in path) blueprintId
    :param string p_version: (required in path) version
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about
    """

    hub.log.debug(
        "POST /blueprint/api/blueprints/{blueprintId}/versions/{version}/actions/release"
    )

    api = BlueprintApi(hub.clients["idem_vra.client.vra_blueprint_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2019-09-12"

    ret = api.release_blueprint_version_using_post1(
        blueprint_id=p_blueprintId, version=p_version, **kwargs
    )

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def restore_blueprint_version_using_post1(
    hub, ctx, p_blueprintId, p_version, **kwargs
):
    """Restores content of draft from versioned blueprint  Performs POST /blueprint/api/blueprints/{blueprintId}/versions/{version}/actions/restore


    :param string p_blueprintId: (required in path) blueprintId
    :param string p_version: (required in path) version
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about
    """

    hub.log.debug(
        "POST /blueprint/api/blueprints/{blueprintId}/versions/{version}/actions/restore"
    )

    api = BlueprintApi(hub.clients["idem_vra.client.vra_blueprint_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2019-09-12"

    ret = api.restore_blueprint_version_using_post1(
        blueprint_id=p_blueprintId, version=p_version, **kwargs
    )

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def un_release_blueprint_version_using_post1(
    hub, ctx, p_blueprintId, p_version, **kwargs
):
    """UnRelease versioned blueprint from catalog  Performs POST /blueprint/api/blueprints/{blueprintId}/versions/{version}/actions/unrelease


    :param string p_blueprintId: (required in path) blueprintId
    :param string p_version: (required in path) version
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about
    """

    hub.log.debug(
        "POST /blueprint/api/blueprints/{blueprintId}/versions/{version}/actions/unrelease"
    )

    api = BlueprintApi(hub.clients["idem_vra.client.vra_blueprint_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2019-09-12"

    ret = api.un_release_blueprint_version_using_post1(
        blueprint_id=p_blueprintId, version=p_version, **kwargs
    )

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def update_blueprint_using_put1(
    hub, ctx, p_blueprintId, name, projectId, **kwargs
):
    """Updates a blueprint  Performs PUT /blueprint/api/blueprints/{blueprintId}


    :param string p_blueprintId: (required in path) blueprintId
    :param string name: (required in body) Blueprint name
    :param string projectId: (required in body) Project ID
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about
    :param string content: (optional in body) Blueprint YAML content
    :param string contentSourceId: (optional in body) Content source id
    :param string contentSourcePath: (optional in body) Content source path
    :param string contentSourceSyncAt: (optional in body) Content source last sync time
    :param array contentSourceSyncMessages: (optional in body) Content source last sync messages
    :param string contentSourceSyncStatus: (optional in body) Content source last sync status
    :param string contentSourceType: (optional in body) Content source type
    :param string createdAt: (optional in body) Created time
    :param string createdBy: (optional in body) Created by
    :param string description: (optional in body) Blueprint description
    :param string id: (optional in body) Object ID
    :param string orgId: (optional in body) Org ID
    :param string projectName: (optional in body) Project Name
    :param boolean requestScopeOrg: (optional in body) Flag to indicate blueprint can be requested from any project in org
    :param string selfLink: (optional in body) Blueprint self link
    :param string status: (optional in body) Blueprint status
    :param integer totalReleasedVersions: (optional in body) Total released versions
    :param integer totalVersions: (optional in body) Total versions
    :param string updatedAt: (optional in body) Updated time
    :param string updatedBy: (optional in body) Updated by
    :param boolean valid: (optional in body) Validation result on update
    :param array validationMessages: (optional in body) Validation messages
    """

    hub.log.debug("PUT /blueprint/api/blueprints/{blueprintId}")

    api = BlueprintApi(hub.clients["idem_vra.client.vra_blueprint_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2019-09-12"

    body = {}
    body["name"] = name
    body["projectId"] = projectId

    if "content" in kwargs:
        hub.log.debug(f"Got kwarg 'content' = {kwargs['content']}")
        body["content"] = kwargs.get("content")
        del kwargs["content"]
    if "contentSourceId" in kwargs:
        hub.log.debug(f"Got kwarg 'contentSourceId' = {kwargs['contentSourceId']}")
        body["contentSourceId"] = kwargs.get("contentSourceId")
        del kwargs["contentSourceId"]
    if "contentSourcePath" in kwargs:
        hub.log.debug(f"Got kwarg 'contentSourcePath' = {kwargs['contentSourcePath']}")
        body["contentSourcePath"] = kwargs.get("contentSourcePath")
        del kwargs["contentSourcePath"]
    if "contentSourceSyncAt" in kwargs:
        hub.log.debug(
            f"Got kwarg 'contentSourceSyncAt' = {kwargs['contentSourceSyncAt']}"
        )
        body["contentSourceSyncAt"] = kwargs.get("contentSourceSyncAt")
        del kwargs["contentSourceSyncAt"]
    if "contentSourceSyncMessages" in kwargs:
        hub.log.debug(
            f"Got kwarg 'contentSourceSyncMessages' = {kwargs['contentSourceSyncMessages']}"
        )
        body["contentSourceSyncMessages"] = kwargs.get("contentSourceSyncMessages")
        del kwargs["contentSourceSyncMessages"]
    if "contentSourceSyncStatus" in kwargs:
        hub.log.debug(
            f"Got kwarg 'contentSourceSyncStatus' = {kwargs['contentSourceSyncStatus']}"
        )
        body["contentSourceSyncStatus"] = kwargs.get("contentSourceSyncStatus")
        del kwargs["contentSourceSyncStatus"]
    if "contentSourceType" in kwargs:
        hub.log.debug(f"Got kwarg 'contentSourceType' = {kwargs['contentSourceType']}")
        body["contentSourceType"] = kwargs.get("contentSourceType")
        del kwargs["contentSourceType"]
    if "createdAt" in kwargs:
        hub.log.debug(f"Got kwarg 'createdAt' = {kwargs['createdAt']}")
        body["createdAt"] = kwargs.get("createdAt")
        del kwargs["createdAt"]
    if "createdBy" in kwargs:
        hub.log.debug(f"Got kwarg 'createdBy' = {kwargs['createdBy']}")
        body["createdBy"] = kwargs.get("createdBy")
        del kwargs["createdBy"]
    if "description" in kwargs:
        hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
        body["description"] = kwargs.get("description")
        del kwargs["description"]
    if "id" in kwargs:
        hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
        body["id"] = kwargs.get("id")
        del kwargs["id"]
    if "orgId" in kwargs:
        hub.log.debug(f"Got kwarg 'orgId' = {kwargs['orgId']}")
        body["orgId"] = kwargs.get("orgId")
        del kwargs["orgId"]
    if "projectName" in kwargs:
        hub.log.debug(f"Got kwarg 'projectName' = {kwargs['projectName']}")
        body["projectName"] = kwargs.get("projectName")
        del kwargs["projectName"]
    if "requestScopeOrg" in kwargs:
        hub.log.debug(f"Got kwarg 'requestScopeOrg' = {kwargs['requestScopeOrg']}")
        body["requestScopeOrg"] = kwargs.get("requestScopeOrg")
        del kwargs["requestScopeOrg"]
    if "selfLink" in kwargs:
        hub.log.debug(f"Got kwarg 'selfLink' = {kwargs['selfLink']}")
        body["selfLink"] = kwargs.get("selfLink")
        del kwargs["selfLink"]
    if "status" in kwargs:
        hub.log.debug(f"Got kwarg 'status' = {kwargs['status']}")
        body["status"] = kwargs.get("status")
        del kwargs["status"]
    if "totalReleasedVersions" in kwargs:
        hub.log.debug(
            f"Got kwarg 'totalReleasedVersions' = {kwargs['totalReleasedVersions']}"
        )
        body["totalReleasedVersions"] = kwargs.get("totalReleasedVersions")
        del kwargs["totalReleasedVersions"]
    if "totalVersions" in kwargs:
        hub.log.debug(f"Got kwarg 'totalVersions' = {kwargs['totalVersions']}")
        body["totalVersions"] = kwargs.get("totalVersions")
        del kwargs["totalVersions"]
    if "updatedAt" in kwargs:
        hub.log.debug(f"Got kwarg 'updatedAt' = {kwargs['updatedAt']}")
        body["updatedAt"] = kwargs.get("updatedAt")
        del kwargs["updatedAt"]
    if "updatedBy" in kwargs:
        hub.log.debug(f"Got kwarg 'updatedBy' = {kwargs['updatedBy']}")
        body["updatedBy"] = kwargs.get("updatedBy")
        del kwargs["updatedBy"]
    if "valid" in kwargs:
        hub.log.debug(f"Got kwarg 'valid' = {kwargs['valid']}")
        body["valid"] = kwargs.get("valid")
        del kwargs["valid"]
    if "validationMessages" in kwargs:
        hub.log.debug(
            f"Got kwarg 'validationMessages' = {kwargs['validationMessages']}"
        )
        body["validationMessages"] = kwargs.get("validationMessages")
        del kwargs["validationMessages"]

    ret = api.update_blueprint_using_put1(body, blueprint_id=p_blueprintId, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))

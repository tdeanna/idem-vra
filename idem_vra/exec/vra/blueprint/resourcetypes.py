from idem_vra.client.vra_blueprint_lib.api import ResourceTypesApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get_resource_type_using_get(hub, ctx, p_resourceTypeId, **kwargs):
    """Returns resource type details  Performs GET /blueprint/api/resource-types/{resourceTypeId}


    :param string p_resourceTypeId: (required in path) resourceTypeId
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about
    """

    hub.log.debug("GET /blueprint/api/resource-types/{resourceTypeId}")

    api = ResourceTypesApi(hub.clients["idem_vra.client.vra_blueprint_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2019-09-12"

    ret = api.get_resource_type_using_get(resource_type_id=p_resourceTypeId, **kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))


async def list_resource_types_using_get(hub, ctx, **kwargs):
    """Lists resource types  Performs GET /blueprint/api/resource-types


    :param array orderby: (optional in query) Sorting criteria in the format: property (asc|desc). Default sort
      order is ascending on name. Sorting is supported on fields createdAt,
      updatedAt, createdBy, updatedBy, name.
    :param integer skip: (optional in query) Number of records you want to skip
    :param integer top: (optional in query) Number of records you want
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about
    :param boolean expand: (optional in query) Expand with content
    :param string name: (optional in query) Filter by name
    :param string providerId: (optional in query) Filter by provider ID
    :param string search: (optional in query) Search by name and description
    """

    hub.log.debug("GET /blueprint/api/resource-types")

    api = ResourceTypesApi(hub.clients["idem_vra.client.vra_blueprint_lib.api"])
    if "api_version" not in kwargs:
        kwargs["api_version"] = "2019-09-12"

    ret = api.list_resource_types_using_get(**kwargs)

    # hub.log.debug(ret)

    return ExecReturn(result=True, ret=remap_response(ret))

from urllib.parse import urlencode

from idem_vra.helpers.models import ExecReturn


async def request(hub, ctx, path, method, **kwargs):

    # Form the full path with url-encoded parameters
    full_path = path
    if "query_params" in kwargs:
        full_path += "?" + urlencode(kwargs.get("query_params", {}))
        kwargs.pop("query_params")

    # Pass along any kwargs
    res = await hub.tool.vra.authed[method.lower()](ctx, full_path, **kwargs)

    return ExecReturn(result=True, ret=res)

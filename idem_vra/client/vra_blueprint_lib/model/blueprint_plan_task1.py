"""
    VMware Cloud Assembly Blueprint API

    A multi-cloud Blueprint API for Cloud Automation Services  # noqa: E501

    OpenAPI spec version: 2019-09-12

    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""
import pprint
import re  # noqa: F401


class BlueprintPlanTask1:
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        "depends_on_tasks": "list[str]",
        "input_properties": "object",
        "name": "str",
        "resource_name": "str",
        "resource_reason": "str",
        "resource_type": "str",
    }

    attribute_map = {
        "depends_on_tasks": "dependsOnTasks",
        "input_properties": "inputProperties",
        "name": "name",
        "resource_name": "resourceName",
        "resource_reason": "resourceReason",
        "resource_type": "resourceType",
    }

    def __init__(
        self,
        depends_on_tasks=None,
        input_properties=None,
        name=None,
        resource_name=None,
        resource_reason=None,
        resource_type=None,
    ):  # noqa: E501
        """BlueprintPlanTask1 - a model defined in Swagger"""  # noqa: E501
        self._depends_on_tasks = None
        self._input_properties = None
        self._name = None
        self._resource_name = None
        self._resource_reason = None
        self._resource_type = None
        self.discriminator = None
        if depends_on_tasks is not None:
            self.depends_on_tasks = depends_on_tasks
        if input_properties is not None:
            self.input_properties = input_properties
        if name is not None:
            self.name = name
        if resource_name is not None:
            self.resource_name = resource_name
        if resource_reason is not None:
            self.resource_reason = resource_reason
        if resource_type is not None:
            self.resource_type = resource_type

    @property
    def depends_on_tasks(self):
        """Gets the depends_on_tasks of this BlueprintPlanTask1.  # noqa: E501

        Tasks depends on other tasks  # noqa: E501

        :return: The depends_on_tasks of this BlueprintPlanTask1.  # noqa: E501
        :rtype: list[str]
        """
        return self._depends_on_tasks

    @depends_on_tasks.setter
    def depends_on_tasks(self, depends_on_tasks):
        """Sets the depends_on_tasks of this BlueprintPlanTask1.

        Tasks depends on other tasks  # noqa: E501

        :param depends_on_tasks: The depends_on_tasks of this BlueprintPlanTask1.  # noqa: E501
        :type: list[str]
        """

        self._depends_on_tasks = depends_on_tasks

    @property
    def input_properties(self):
        """Gets the input_properties of this BlueprintPlanTask1.  # noqa: E501

        Task input properties  # noqa: E501

        :return: The input_properties of this BlueprintPlanTask1.  # noqa: E501
        :rtype: object
        """
        return self._input_properties

    @input_properties.setter
    def input_properties(self, input_properties):
        """Sets the input_properties of this BlueprintPlanTask1.

        Task input properties  # noqa: E501

        :param input_properties: The input_properties of this BlueprintPlanTask1.  # noqa: E501
        :type: object
        """

        self._input_properties = input_properties

    @property
    def name(self):
        """Gets the name of this BlueprintPlanTask1.  # noqa: E501

        Task name  # noqa: E501

        :return: The name of this BlueprintPlanTask1.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this BlueprintPlanTask1.

        Task name  # noqa: E501

        :param name: The name of this BlueprintPlanTask1.  # noqa: E501
        :type: str
        """

        self._name = name

    @property
    def resource_name(self):
        """Gets the resource_name of this BlueprintPlanTask1.  # noqa: E501

        Resource name  # noqa: E501

        :return: The resource_name of this BlueprintPlanTask1.  # noqa: E501
        :rtype: str
        """
        return self._resource_name

    @resource_name.setter
    def resource_name(self, resource_name):
        """Sets the resource_name of this BlueprintPlanTask1.

        Resource name  # noqa: E501

        :param resource_name: The resource_name of this BlueprintPlanTask1.  # noqa: E501
        :type: str
        """

        self._resource_name = resource_name

    @property
    def resource_reason(self):
        """Gets the resource_reason of this BlueprintPlanTask1.  # noqa: E501

        Resource reason  # noqa: E501

        :return: The resource_reason of this BlueprintPlanTask1.  # noqa: E501
        :rtype: str
        """
        return self._resource_reason

    @resource_reason.setter
    def resource_reason(self, resource_reason):
        """Sets the resource_reason of this BlueprintPlanTask1.

        Resource reason  # noqa: E501

        :param resource_reason: The resource_reason of this BlueprintPlanTask1.  # noqa: E501
        :type: str
        """
        allowed_values = [
            "CREATE",
            "RECREATE",
            "UPDATE",
            "DELETE",
            "ACTION",
            "READ",
        ]  # noqa: E501
        if resource_reason not in allowed_values:
            raise ValueError(
                "Invalid value for `resource_reason` ({}), must be one of {}".format(  # noqa: E501
                    resource_reason, allowed_values
                )
            )

        self._resource_reason = resource_reason

    @property
    def resource_type(self):
        """Gets the resource_type of this BlueprintPlanTask1.  # noqa: E501

        Resource type  # noqa: E501

        :return: The resource_type of this BlueprintPlanTask1.  # noqa: E501
        :rtype: str
        """
        return self._resource_type

    @resource_type.setter
    def resource_type(self, resource_type):
        """Sets the resource_type of this BlueprintPlanTask1.

        Resource type  # noqa: E501

        :param resource_type: The resource_type of this BlueprintPlanTask1.  # noqa: E501
        :type: str
        """

        self._resource_type = resource_type

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in self.swagger_types.items():
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(
                    map(lambda x: x.to_dict() if hasattr(x, "to_dict") else x, value)
                )
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(
                    map(
                        lambda item: (item[0], item[1].to_dict())
                        if hasattr(item[1], "to_dict")
                        else item,
                        value.items(),
                    )
                )
            else:
                result[attr] = value
        if issubclass(BlueprintPlanTask1, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, BlueprintPlanTask1):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other

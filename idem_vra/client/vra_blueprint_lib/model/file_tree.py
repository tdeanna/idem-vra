"""
    VMware Cloud Assembly Blueprint API

    A multi-cloud Blueprint API for Cloud Automation Services  # noqa: E501

    OpenAPI spec version: 2019-09-12

    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""
import pprint
import re  # noqa: F401


class FileTree:
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {"repository_id": "str", "tree": "list[FileTreeDirectory1]"}

    attribute_map = {"repository_id": "repositoryId", "tree": "tree"}

    def __init__(self, repository_id=None, tree=None):  # noqa: E501
        """FileTree - a model defined in Swagger"""  # noqa: E501
        self._repository_id = None
        self._tree = None
        self.discriminator = None
        if repository_id is not None:
            self.repository_id = repository_id
        if tree is not None:
            self.tree = tree

    @property
    def repository_id(self):
        """Gets the repository_id of this FileTree.  # noqa: E501

        The ID of a content source  # noqa: E501

        :return: The repository_id of this FileTree.  # noqa: E501
        :rtype: str
        """
        return self._repository_id

    @repository_id.setter
    def repository_id(self, repository_id):
        """Sets the repository_id of this FileTree.

        The ID of a content source  # noqa: E501

        :param repository_id: The repository_id of this FileTree.  # noqa: E501
        :type: str
        """

        self._repository_id = repository_id

    @property
    def tree(self):
        """Gets the tree of this FileTree.  # noqa: E501

        A list of directories  # noqa: E501

        :return: The tree of this FileTree.  # noqa: E501
        :rtype: list[FileTreeDirectory1]
        """
        return self._tree

    @tree.setter
    def tree(self, tree):
        """Sets the tree of this FileTree.

        A list of directories  # noqa: E501

        :param tree: The tree of this FileTree.  # noqa: E501
        :type: list[FileTreeDirectory1]
        """

        self._tree = tree

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in self.swagger_types.items():
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(
                    map(lambda x: x.to_dict() if hasattr(x, "to_dict") else x, value)
                )
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(
                    map(
                        lambda item: (item[0], item[1].to_dict())
                        if hasattr(item[1], "to_dict")
                        else item,
                        value.items(),
                    )
                )
            else:
                result[attr] = value
        if issubclass(FileTree, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, FileTree):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other

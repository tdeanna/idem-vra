"""
    CMX Service APIs

    When using Kubernetes integration, deploy and manage Kubernetes clusters and namespaces. The APIs that list collections of resources also support OData like implementation. Below query params can be used across CMX entities.  Example:  1. `$orderby` - returns a result with the rows being sorted by the values of provided attribute.      ```/cmx/api/resources/tags?$orderby=name%20desc```  2. `$top`, `$skip` - `$top` returns the requested number of resources. Used with `$skip`, the client can skip a given number of resources.      ```/cmx/api/resources/tags?$expand=true&$top=10&$skip=2```  3. `page` and `$size` - page used in conjunction with `$size` helps in pagination of resources.      ```/cmx/api/resources/tags?$expand=true&page=0&$size=5```  4. `$filter` - `$filter` returns a subset of resources that satisfy the given predicate expression.      ```     /cmx/api/resources/tags?$filter=startswith(name, 'K8S')   # noqa: E501

    OpenAPI spec version: 2019-09-12

    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""
import re  # noqa: F401

from idem_vra.client.vra_cmx_lib.api_client import ApiClient

# python 2 and python 3 compatibility library


class TMCEndpointsApi:
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    Ref: https://github.com/swagger-api/swagger-codegen
    """

    def __init__(self, api_client=None):
        if api_client is None:
            api_client = ApiClient()
        self.api_client = api_client

    def create_request_tracker_using_post(self, body, **kwargs):  # noqa: E501
        """Create a request tracker for endpoint creation  # noqa: E501

        Create a request tracker for a TMC endpoint by providing the endpoint configuration  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.create_request_tracker_using_post(body, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param TMCEndpointConfigRequest1 body: request (required)
        :return: TMCRequestTracker1
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs["_return_http_data_only"] = True
        if kwargs.get("async_req"):
            return self.create_request_tracker_using_post_with_http_info(
                body, **kwargs
            )  # noqa: E501
        else:
            (data) = self.create_request_tracker_using_post_with_http_info(
                body, **kwargs
            )  # noqa: E501
            return data

    def create_request_tracker_using_post_with_http_info(
        self, body, **kwargs
    ):  # noqa: E501
        """Create a request tracker for endpoint creation  # noqa: E501

        Create a request tracker for a TMC endpoint by providing the endpoint configuration  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.create_request_tracker_using_post_with_http_info(body, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param TMCEndpointConfigRequest1 body: request (required)
        :return: TMCRequestTracker1
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ["body"]  # noqa: E501
        all_params.append("async_req")
        all_params.append("_return_http_data_only")
        all_params.append("_preload_content")
        all_params.append("_request_timeout")

        params = locals()
        for key, val in params["kwargs"].items():
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method create_request_tracker_using_post" % key
                )
            params[key] = val
        del params["kwargs"]
        # verify the required parameter 'body' is set
        if "body" not in params or params["body"] is None:
            raise ValueError(
                "Missing the required parameter `body` when calling `create_request_tracker_using_post`"
            )  # noqa: E501

        collection_formats = {}

        path_params = {}

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        if "body" in params:
            body_params = params["body"]
        # HTTP header `Accept`
        header_params["Accept"] = self.api_client.select_header_accept(
            ["application/json"]
        )  # noqa: E501

        # HTTP header `Content-Type`
        header_params[
            "Content-Type"
        ] = self.api_client.select_header_content_type(  # noqa: E501
            ["application/json"]
        )  # noqa: E501

        # Authentication setting
        auth_settings = ["Bearer"]  # noqa: E501

        return self.api_client.call_api(
            "/cmx/api/resources/tmc/endpoints/request-tracker",
            "POST",
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type="TMCRequestTracker1",  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get("async_req"),
            _return_http_data_only=params.get("_return_http_data_only"),
            _preload_content=params.get("_preload_content", True),
            _request_timeout=params.get("_request_timeout"),
            collection_formats=collection_formats,
        )

    def get_cluster_groups_by_endpoint_using_get(self, id, **kwargs):  # noqa: E501
        """Get all cluster groups for TMC endpoint  # noqa: E501

        Get cluster groups for a TMC endpoint by provided TMC endpoint id and starting with the provided cluster group name. If cluster group name is not provided, then all cluster groups are returned  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_cluster_groups_by_endpoint_using_get(id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str id: id (required)
        :param bool include_total_count: Include total count.
        :param str pagination_offset: Offset at which to start returning records.
        :param str pagination_size: Number of records to return.
        :param str query: TQL query string.
        :param str search_scope_name: Scope search to the specified name; supports globbing; default (*).
        :param str sort_by: Sort Order.
        :return: PageOfClusterGroup1
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs["_return_http_data_only"] = True
        if kwargs.get("async_req"):
            return self.get_cluster_groups_by_endpoint_using_get_with_http_info(
                id, **kwargs
            )  # noqa: E501
        else:
            (data) = self.get_cluster_groups_by_endpoint_using_get_with_http_info(
                id, **kwargs
            )  # noqa: E501
            return data

    def get_cluster_groups_by_endpoint_using_get_with_http_info(
        self, id, **kwargs
    ):  # noqa: E501
        """Get all cluster groups for TMC endpoint  # noqa: E501

        Get cluster groups for a TMC endpoint by provided TMC endpoint id and starting with the provided cluster group name. If cluster group name is not provided, then all cluster groups are returned  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_cluster_groups_by_endpoint_using_get_with_http_info(id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str id: id (required)
        :param bool include_total_count: Include total count.
        :param str pagination_offset: Offset at which to start returning records.
        :param str pagination_size: Number of records to return.
        :param str query: TQL query string.
        :param str search_scope_name: Scope search to the specified name; supports globbing; default (*).
        :param str sort_by: Sort Order.
        :return: PageOfClusterGroup1
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = [
            "id",
            "include_total_count",
            "pagination_offset",
            "pagination_size",
            "query",
            "search_scope_name",
            "sort_by",
        ]  # noqa: E501
        all_params.append("async_req")
        all_params.append("_return_http_data_only")
        all_params.append("_preload_content")
        all_params.append("_request_timeout")

        params = locals()
        for key, val in params["kwargs"].items():
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_cluster_groups_by_endpoint_using_get" % key
                )
            params[key] = val
        del params["kwargs"]
        # verify the required parameter 'id' is set
        if "id" not in params or params["id"] is None:
            raise ValueError(
                "Missing the required parameter `id` when calling `get_cluster_groups_by_endpoint_using_get`"
            )  # noqa: E501

        collection_formats = {}

        path_params = {}
        if "id" in params:
            path_params["id"] = params["id"]  # noqa: E501

        query_params = []
        if "include_total_count" in params:
            query_params.append(
                ("includeTotalCount", params["include_total_count"])
            )  # noqa: E501
        if "pagination_offset" in params:
            query_params.append(
                ("pagination.offset", params["pagination_offset"])
            )  # noqa: E501
        if "pagination_size" in params:
            query_params.append(
                ("pagination.size", params["pagination_size"])
            )  # noqa: E501
        if "query" in params:
            query_params.append(("query", params["query"]))  # noqa: E501
        if "search_scope_name" in params:
            query_params.append(
                ("searchScope.name", params["search_scope_name"])
            )  # noqa: E501
        if "sort_by" in params:
            query_params.append(("sortBy", params["sort_by"]))  # noqa: E501

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params["Accept"] = self.api_client.select_header_accept(
            ["application/json"]
        )  # noqa: E501

        # Authentication setting
        auth_settings = ["Bearer"]  # noqa: E501

        return self.api_client.call_api(
            "/cmx/api/resources/tmc/endpoints/{id}/clustergroups",
            "GET",
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type="PageOfClusterGroup1",  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get("async_req"),
            _return_http_data_only=params.get("_return_http_data_only"),
            _preload_content=params.get("_preload_content", True),
            _request_timeout=params.get("_request_timeout"),
            collection_formats=collection_formats,
        )

    def get_cluster_groups_by_request_tracker_using_get(
        self, request_tracker_id, **kwargs
    ):  # noqa: E501
        """Get all cluster groups for TMC endpoint  # noqa: E501

        Get cluster groups for a TMC endpoint by provided TMC endpoint id and starting with the provided cluster group name. If cluster group name is not provided, then all cluster groups are returned  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_cluster_groups_by_request_tracker_using_get(request_tracker_id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str request_tracker_id: (required)
        :param bool include_total_count: Include total count.
        :param str pagination_offset: Offset at which to start returning records.
        :param str pagination_size: Number of records to return.
        :param str query: TQL query string.
        :param str search_scope_name: Scope search to the specified name; supports globbing; default (*).
        :param str sort_by: Sort Order.
        :return: PageOfClusterGroup1
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs["_return_http_data_only"] = True
        if kwargs.get("async_req"):
            return self.get_cluster_groups_by_request_tracker_using_get_with_http_info(
                request_tracker_id, **kwargs
            )  # noqa: E501
        else:
            (
                data
            ) = self.get_cluster_groups_by_request_tracker_using_get_with_http_info(
                request_tracker_id, **kwargs
            )  # noqa: E501
            return data

    def get_cluster_groups_by_request_tracker_using_get_with_http_info(
        self, request_tracker_id, **kwargs
    ):  # noqa: E501
        """Get all cluster groups for TMC endpoint  # noqa: E501

        Get cluster groups for a TMC endpoint by provided TMC endpoint id and starting with the provided cluster group name. If cluster group name is not provided, then all cluster groups are returned  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_cluster_groups_by_request_tracker_using_get_with_http_info(request_tracker_id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str request_tracker_id: (required)
        :param bool include_total_count: Include total count.
        :param str pagination_offset: Offset at which to start returning records.
        :param str pagination_size: Number of records to return.
        :param str query: TQL query string.
        :param str search_scope_name: Scope search to the specified name; supports globbing; default (*).
        :param str sort_by: Sort Order.
        :return: PageOfClusterGroup1
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = [
            "request_tracker_id",
            "include_total_count",
            "pagination_offset",
            "pagination_size",
            "query",
            "search_scope_name",
            "sort_by",
        ]  # noqa: E501
        all_params.append("async_req")
        all_params.append("_return_http_data_only")
        all_params.append("_preload_content")
        all_params.append("_request_timeout")

        params = locals()
        for key, val in params["kwargs"].items():
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_cluster_groups_by_request_tracker_using_get" % key
                )
            params[key] = val
        del params["kwargs"]
        # verify the required parameter 'request_tracker_id' is set
        if "request_tracker_id" not in params or params["request_tracker_id"] is None:
            raise ValueError(
                "Missing the required parameter `request_tracker_id` when calling `get_cluster_groups_by_request_tracker_using_get`"
            )  # noqa: E501

        collection_formats = {}

        path_params = {}

        query_params = []
        if "include_total_count" in params:
            query_params.append(
                ("includeTotalCount", params["include_total_count"])
            )  # noqa: E501
        if "pagination_offset" in params:
            query_params.append(
                ("pagination.offset", params["pagination_offset"])
            )  # noqa: E501
        if "pagination_size" in params:
            query_params.append(
                ("pagination.size", params["pagination_size"])
            )  # noqa: E501
        if "query" in params:
            query_params.append(("query", params["query"]))  # noqa: E501
        if "request_tracker_id" in params:
            query_params.append(
                ("requestTrackerId", params["request_tracker_id"])
            )  # noqa: E501
        if "search_scope_name" in params:
            query_params.append(
                ("searchScope.name", params["search_scope_name"])
            )  # noqa: E501
        if "sort_by" in params:
            query_params.append(("sortBy", params["sort_by"]))  # noqa: E501

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params["Accept"] = self.api_client.select_header_accept(
            ["application/json"]
        )  # noqa: E501

        # Authentication setting
        auth_settings = ["Bearer"]  # noqa: E501

        return self.api_client.call_api(
            "/cmx/api/resources/tmc/endpoints/clustergroups",
            "GET",
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type="PageOfClusterGroup1",  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get("async_req"),
            _return_http_data_only=params.get("_return_http_data_only"),
            _preload_content=params.get("_preload_content", True),
            _request_timeout=params.get("_request_timeout"),
            collection_formats=collection_formats,
        )

    def get_tmc_cli_binaries_using_get(self, **kwargs):  # noqa: E501
        """Get Tmc CLI binaries  # noqa: E501

        Get Tanzu CLI binaries for Mac, Linux, Windows  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_tmc_cli_binaries_using_get(async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :return: BinariesResponse1
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs["_return_http_data_only"] = True
        if kwargs.get("async_req"):
            return self.get_tmc_cli_binaries_using_get_with_http_info(
                **kwargs
            )  # noqa: E501
        else:
            (data) = self.get_tmc_cli_binaries_using_get_with_http_info(
                **kwargs
            )  # noqa: E501
            return data

    def get_tmc_cli_binaries_using_get_with_http_info(self, **kwargs):  # noqa: E501
        """Get Tmc CLI binaries  # noqa: E501

        Get Tanzu CLI binaries for Mac, Linux, Windows  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_tmc_cli_binaries_using_get_with_http_info(async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :return: BinariesResponse1
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = []  # noqa: E501
        all_params.append("async_req")
        all_params.append("_return_http_data_only")
        all_params.append("_preload_content")
        all_params.append("_request_timeout")

        params = locals()
        for key, val in params["kwargs"].items():
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_tmc_cli_binaries_using_get" % key
                )
            params[key] = val
        del params["kwargs"]

        collection_formats = {}

        path_params = {}

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params["Accept"] = self.api_client.select_header_accept(
            ["application/json"]
        )  # noqa: E501

        # Authentication setting
        auth_settings = ["Bearer"]  # noqa: E501

        return self.api_client.call_api(
            "/cmx/api/resources/tmc/endpoints/tanzu-cli-binaries",
            "GET",
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type="BinariesResponse1",  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get("async_req"),
            _return_http_data_only=params.get("_return_http_data_only"),
            _preload_content=params.get("_preload_content", True),
            _request_timeout=params.get("_request_timeout"),
            collection_formats=collection_formats,
        )

    def get_workspaces_by_endpoint_using_get(self, id, **kwargs):  # noqa: E501
        """Get all workspace names list for TMC endpoint  # noqa: E501

        Get workspace names list for a TMC endpoint by provided TMC endpoint id and starting with the default workspace name. If workspace name is not provided, then all workspaces are returned  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_workspaces_by_endpoint_using_get(id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str id: id (required)
        :param bool include_total_count: Include total count.
        :param str pagination_offset: Offset at which to start returning records.
        :param str pagination_size: Number of records to return.
        :param str query: TQL query string.
        :param str search_scope_name: Scope search to the specified name; supports globbing; default (*).
        :param str sort_by: Sort Order.
        :return: PageOfWorkspace1
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs["_return_http_data_only"] = True
        if kwargs.get("async_req"):
            return self.get_workspaces_by_endpoint_using_get_with_http_info(
                id, **kwargs
            )  # noqa: E501
        else:
            (data) = self.get_workspaces_by_endpoint_using_get_with_http_info(
                id, **kwargs
            )  # noqa: E501
            return data

    def get_workspaces_by_endpoint_using_get_with_http_info(
        self, id, **kwargs
    ):  # noqa: E501
        """Get all workspace names list for TMC endpoint  # noqa: E501

        Get workspace names list for a TMC endpoint by provided TMC endpoint id and starting with the default workspace name. If workspace name is not provided, then all workspaces are returned  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_workspaces_by_endpoint_using_get_with_http_info(id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str id: id (required)
        :param bool include_total_count: Include total count.
        :param str pagination_offset: Offset at which to start returning records.
        :param str pagination_size: Number of records to return.
        :param str query: TQL query string.
        :param str search_scope_name: Scope search to the specified name; supports globbing; default (*).
        :param str sort_by: Sort Order.
        :return: PageOfWorkspace1
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = [
            "id",
            "include_total_count",
            "pagination_offset",
            "pagination_size",
            "query",
            "search_scope_name",
            "sort_by",
        ]  # noqa: E501
        all_params.append("async_req")
        all_params.append("_return_http_data_only")
        all_params.append("_preload_content")
        all_params.append("_request_timeout")

        params = locals()
        for key, val in params["kwargs"].items():
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_workspaces_by_endpoint_using_get" % key
                )
            params[key] = val
        del params["kwargs"]
        # verify the required parameter 'id' is set
        if "id" not in params or params["id"] is None:
            raise ValueError(
                "Missing the required parameter `id` when calling `get_workspaces_by_endpoint_using_get`"
            )  # noqa: E501

        collection_formats = {}

        path_params = {}
        if "id" in params:
            path_params["id"] = params["id"]  # noqa: E501

        query_params = []
        if "include_total_count" in params:
            query_params.append(
                ("includeTotalCount", params["include_total_count"])
            )  # noqa: E501
        if "pagination_offset" in params:
            query_params.append(
                ("pagination.offset", params["pagination_offset"])
            )  # noqa: E501
        if "pagination_size" in params:
            query_params.append(
                ("pagination.size", params["pagination_size"])
            )  # noqa: E501
        if "query" in params:
            query_params.append(("query", params["query"]))  # noqa: E501
        if "search_scope_name" in params:
            query_params.append(
                ("searchScope.name", params["search_scope_name"])
            )  # noqa: E501
        if "sort_by" in params:
            query_params.append(("sortBy", params["sort_by"]))  # noqa: E501

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params["Accept"] = self.api_client.select_header_accept(
            ["application/json"]
        )  # noqa: E501

        # Authentication setting
        auth_settings = ["Bearer"]  # noqa: E501

        return self.api_client.call_api(
            "/cmx/api/resources/tmc/endpoints/{id}/workspaces",
            "GET",
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type="PageOfWorkspace1",  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get("async_req"),
            _return_http_data_only=params.get("_return_http_data_only"),
            _preload_content=params.get("_preload_content", True),
            _request_timeout=params.get("_request_timeout"),
            collection_formats=collection_formats,
        )

    def get_workspaces_by_request_tracker_using_get(
        self, request_tracker_id, **kwargs
    ):  # noqa: E501
        """Get all workspace names list for TMC endpoint  # noqa: E501

        Get workspace names list for a TMC endpoint by provided TMC endpoint id and starting with the default workspace name. If workspace name is not provided, then all workspaces are returned  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_workspaces_by_request_tracker_using_get(request_tracker_id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str request_tracker_id: (required)
        :param bool include_total_count: Include total count.
        :param str pagination_offset: Offset at which to start returning records.
        :param str pagination_size: Number of records to return.
        :param str query: TQL query string.
        :param str search_scope_name: Scope search to the specified name; supports globbing; default (*).
        :param str sort_by: Sort Order.
        :return: PageOfWorkspace1
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs["_return_http_data_only"] = True
        if kwargs.get("async_req"):
            return self.get_workspaces_by_request_tracker_using_get_with_http_info(
                request_tracker_id, **kwargs
            )  # noqa: E501
        else:
            (data) = self.get_workspaces_by_request_tracker_using_get_with_http_info(
                request_tracker_id, **kwargs
            )  # noqa: E501
            return data

    def get_workspaces_by_request_tracker_using_get_with_http_info(
        self, request_tracker_id, **kwargs
    ):  # noqa: E501
        """Get all workspace names list for TMC endpoint  # noqa: E501

        Get workspace names list for a TMC endpoint by provided TMC endpoint id and starting with the default workspace name. If workspace name is not provided, then all workspaces are returned  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_workspaces_by_request_tracker_using_get_with_http_info(request_tracker_id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str request_tracker_id: (required)
        :param bool include_total_count: Include total count.
        :param str pagination_offset: Offset at which to start returning records.
        :param str pagination_size: Number of records to return.
        :param str query: TQL query string.
        :param str search_scope_name: Scope search to the specified name; supports globbing; default (*).
        :param str sort_by: Sort Order.
        :return: PageOfWorkspace1
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = [
            "request_tracker_id",
            "include_total_count",
            "pagination_offset",
            "pagination_size",
            "query",
            "search_scope_name",
            "sort_by",
        ]  # noqa: E501
        all_params.append("async_req")
        all_params.append("_return_http_data_only")
        all_params.append("_preload_content")
        all_params.append("_request_timeout")

        params = locals()
        for key, val in params["kwargs"].items():
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_workspaces_by_request_tracker_using_get" % key
                )
            params[key] = val
        del params["kwargs"]
        # verify the required parameter 'request_tracker_id' is set
        if "request_tracker_id" not in params or params["request_tracker_id"] is None:
            raise ValueError(
                "Missing the required parameter `request_tracker_id` when calling `get_workspaces_by_request_tracker_using_get`"
            )  # noqa: E501

        collection_formats = {}

        path_params = {}

        query_params = []
        if "include_total_count" in params:
            query_params.append(
                ("includeTotalCount", params["include_total_count"])
            )  # noqa: E501
        if "pagination_offset" in params:
            query_params.append(
                ("pagination.offset", params["pagination_offset"])
            )  # noqa: E501
        if "pagination_size" in params:
            query_params.append(
                ("pagination.size", params["pagination_size"])
            )  # noqa: E501
        if "query" in params:
            query_params.append(("query", params["query"]))  # noqa: E501
        if "request_tracker_id" in params:
            query_params.append(
                ("requestTrackerId", params["request_tracker_id"])
            )  # noqa: E501
        if "search_scope_name" in params:
            query_params.append(
                ("searchScope.name", params["search_scope_name"])
            )  # noqa: E501
        if "sort_by" in params:
            query_params.append(("sortBy", params["sort_by"]))  # noqa: E501

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params["Accept"] = self.api_client.select_header_accept(
            ["application/json"]
        )  # noqa: E501

        # Authentication setting
        auth_settings = ["Bearer"]  # noqa: E501

        return self.api_client.call_api(
            "/cmx/api/resources/tmc/endpoints/workspaces",
            "GET",
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type="PageOfWorkspace1",  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get("async_req"),
            _return_http_data_only=params.get("_return_http_data_only"),
            _preload_content=params.get("_preload_content", True),
            _request_timeout=params.get("_request_timeout"),
            collection_formats=collection_formats,
        )

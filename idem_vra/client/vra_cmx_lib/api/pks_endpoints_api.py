"""
    CMX Service APIs

    When using Kubernetes integration, deploy and manage Kubernetes clusters and namespaces. The APIs that list collections of resources also support OData like implementation. Below query params can be used across CMX entities.  Example:  1. `$orderby` - returns a result with the rows being sorted by the values of provided attribute.      ```/cmx/api/resources/tags?$orderby=name%20desc```  2. `$top`, `$skip` - `$top` returns the requested number of resources. Used with `$skip`, the client can skip a given number of resources.      ```/cmx/api/resources/tags?$expand=true&$top=10&$skip=2```  3. `page` and `$size` - page used in conjunction with `$size` helps in pagination of resources.      ```/cmx/api/resources/tags?$expand=true&page=0&$size=5```  4. `$filter` - `$filter` returns a subset of resources that satisfy the given predicate expression.      ```     /cmx/api/resources/tags?$filter=startswith(name, 'K8S')   # noqa: E501

    OpenAPI spec version: 2019-09-12

    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""
import re  # noqa: F401

from idem_vra.client.vra_cmx_lib.api_client import ApiClient

# python 2 and python 3 compatibility library


class PKSEndpointsApi:
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    Ref: https://github.com/swagger-api/swagger-codegen
    """

    def __init__(self, api_client=None):
        if api_client is None:
            api_client = ApiClient()
        self.api_client = api_client

    def create_cluster_using_post(self, body, id, **kwargs):  # noqa: E501
        """Create a K8S cluster  # noqa: E501

        Create a K8S cluster on PKS endpoint specified by endpoint id  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.create_cluster_using_post(body, id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param PKSCluster2 body: pksCluster (required)
        :param str id: id (required)
        :return: K8SCluster3
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs["_return_http_data_only"] = True
        if kwargs.get("async_req"):
            return self.create_cluster_using_post_with_http_info(
                body, id, **kwargs
            )  # noqa: E501
        else:
            (data) = self.create_cluster_using_post_with_http_info(
                body, id, **kwargs
            )  # noqa: E501
            return data

    def create_cluster_using_post_with_http_info(
        self, body, id, **kwargs
    ):  # noqa: E501
        """Create a K8S cluster  # noqa: E501

        Create a K8S cluster on PKS endpoint specified by endpoint id  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.create_cluster_using_post_with_http_info(body, id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param PKSCluster2 body: pksCluster (required)
        :param str id: id (required)
        :return: K8SCluster3
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ["body", "id"]  # noqa: E501
        all_params.append("async_req")
        all_params.append("_return_http_data_only")
        all_params.append("_preload_content")
        all_params.append("_request_timeout")

        params = locals()
        for key, val in params["kwargs"].items():
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method create_cluster_using_post" % key
                )
            params[key] = val
        del params["kwargs"]
        # verify the required parameter 'body' is set
        if "body" not in params or params["body"] is None:
            raise ValueError(
                "Missing the required parameter `body` when calling `create_cluster_using_post`"
            )  # noqa: E501
        # verify the required parameter 'id' is set
        if "id" not in params or params["id"] is None:
            raise ValueError(
                "Missing the required parameter `id` when calling `create_cluster_using_post`"
            )  # noqa: E501

        collection_formats = {}

        path_params = {}
        if "id" in params:
            path_params["id"] = params["id"]  # noqa: E501

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        if "body" in params:
            body_params = params["body"]
        # HTTP header `Accept`
        header_params["Accept"] = self.api_client.select_header_accept(
            ["application/json"]
        )  # noqa: E501

        # HTTP header `Content-Type`
        header_params[
            "Content-Type"
        ] = self.api_client.select_header_content_type(  # noqa: E501
            ["application/json"]
        )  # noqa: E501

        # Authentication setting
        auth_settings = ["Bearer"]  # noqa: E501

        return self.api_client.call_api(
            "/cmx/api/resources/pks/endpoints/{id}/clusters",
            "POST",
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type="K8SCluster3",  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get("async_req"),
            _return_http_data_only=params.get("_return_http_data_only"),
            _preload_content=params.get("_preload_content", True),
            _request_timeout=params.get("_request_timeout"),
            collection_formats=collection_formats,
        )

    def destroy_cluster_using_delete1(self, cluster_id, id, **kwargs):  # noqa: E501
        """Destroy a K8S cluster on a specific PKS endpoint  # noqa: E501

        Destroy and unregister a K8S cluster on PKS endpoint  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.destroy_cluster_using_delete1(cluster_id, id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str cluster_id: clusterId (required)
        :param str id: id (required)
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs["_return_http_data_only"] = True
        if kwargs.get("async_req"):
            return self.destroy_cluster_using_delete1_with_http_info(
                cluster_id, id, **kwargs
            )  # noqa: E501
        else:
            (data) = self.destroy_cluster_using_delete1_with_http_info(
                cluster_id, id, **kwargs
            )  # noqa: E501
            return data

    def destroy_cluster_using_delete1_with_http_info(
        self, cluster_id, id, **kwargs
    ):  # noqa: E501
        """Destroy a K8S cluster on a specific PKS endpoint  # noqa: E501

        Destroy and unregister a K8S cluster on PKS endpoint  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.destroy_cluster_using_delete1_with_http_info(cluster_id, id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str cluster_id: clusterId (required)
        :param str id: id (required)
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ["cluster_id", "id"]  # noqa: E501
        all_params.append("async_req")
        all_params.append("_return_http_data_only")
        all_params.append("_preload_content")
        all_params.append("_request_timeout")

        params = locals()
        for key, val in params["kwargs"].items():
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method destroy_cluster_using_delete1" % key
                )
            params[key] = val
        del params["kwargs"]
        # verify the required parameter 'cluster_id' is set
        if "cluster_id" not in params or params["cluster_id"] is None:
            raise ValueError(
                "Missing the required parameter `cluster_id` when calling `destroy_cluster_using_delete1`"
            )  # noqa: E501
        # verify the required parameter 'id' is set
        if "id" not in params or params["id"] is None:
            raise ValueError(
                "Missing the required parameter `id` when calling `destroy_cluster_using_delete1`"
            )  # noqa: E501

        collection_formats = {}

        path_params = {}
        if "cluster_id" in params:
            path_params["clusterId"] = params["cluster_id"]  # noqa: E501
        if "id" in params:
            path_params["id"] = params["id"]  # noqa: E501

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # Authentication setting
        auth_settings = ["Bearer"]  # noqa: E501

        return self.api_client.call_api(
            "/cmx/api/resources/pks/endpoints/{id}/clusters/{clusterId}",
            "DELETE",
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type=None,  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get("async_req"),
            _return_http_data_only=params.get("_return_http_data_only"),
            _preload_content=params.get("_preload_content", True),
            _request_timeout=params.get("_request_timeout"),
            collection_formats=collection_formats,
        )

    def get_clusters_using_get(self, id, **kwargs):  # noqa: E501
        """Get all K8S clusters for a PKS endpoint  # noqa: E501

        Get all K8S clusters for a PKS endpoint by provided PKS endpoint id  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_clusters_using_get(id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str id: id (required)
        :return: list[PKSCluster1]
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs["_return_http_data_only"] = True
        if kwargs.get("async_req"):
            return self.get_clusters_using_get_with_http_info(
                id, **kwargs
            )  # noqa: E501
        else:
            (data) = self.get_clusters_using_get_with_http_info(
                id, **kwargs
            )  # noqa: E501
            return data

    def get_clusters_using_get_with_http_info(self, id, **kwargs):  # noqa: E501
        """Get all K8S clusters for a PKS endpoint  # noqa: E501

        Get all K8S clusters for a PKS endpoint by provided PKS endpoint id  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_clusters_using_get_with_http_info(id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str id: id (required)
        :return: list[PKSCluster1]
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ["id"]  # noqa: E501
        all_params.append("async_req")
        all_params.append("_return_http_data_only")
        all_params.append("_preload_content")
        all_params.append("_request_timeout")

        params = locals()
        for key, val in params["kwargs"].items():
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_clusters_using_get" % key
                )
            params[key] = val
        del params["kwargs"]
        # verify the required parameter 'id' is set
        if "id" not in params or params["id"] is None:
            raise ValueError(
                "Missing the required parameter `id` when calling `get_clusters_using_get`"
            )  # noqa: E501

        collection_formats = {}

        path_params = {}
        if "id" in params:
            path_params["id"] = params["id"]  # noqa: E501

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params["Accept"] = self.api_client.select_header_accept(
            ["application/json"]
        )  # noqa: E501

        # Authentication setting
        auth_settings = ["Bearer"]  # noqa: E501

        return self.api_client.call_api(
            "/cmx/api/resources/pks/endpoints/{id}/clusters",
            "GET",
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type="list[PKSCluster1]",  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get("async_req"),
            _return_http_data_only=params.get("_return_http_data_only"),
            _preload_content=params.get("_preload_content", True),
            _request_timeout=params.get("_request_timeout"),
            collection_formats=collection_formats,
        )

    def get_plans_using_get(self, id, **kwargs):  # noqa: E501
        """Get supported plans for a PKS endpoint  # noqa: E501

        Get supported plans by providing PKS endpoint id  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_plans_using_get(id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str id: id (required)
        :return: list[PKSPlan1]
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs["_return_http_data_only"] = True
        if kwargs.get("async_req"):
            return self.get_plans_using_get_with_http_info(id, **kwargs)  # noqa: E501
        else:
            (data) = self.get_plans_using_get_with_http_info(id, **kwargs)  # noqa: E501
            return data

    def get_plans_using_get_with_http_info(self, id, **kwargs):  # noqa: E501
        """Get supported plans for a PKS endpoint  # noqa: E501

        Get supported plans by providing PKS endpoint id  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_plans_using_get_with_http_info(id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str id: id (required)
        :return: list[PKSPlan1]
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ["id"]  # noqa: E501
        all_params.append("async_req")
        all_params.append("_return_http_data_only")
        all_params.append("_preload_content")
        all_params.append("_request_timeout")

        params = locals()
        for key, val in params["kwargs"].items():
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_plans_using_get" % key
                )
            params[key] = val
        del params["kwargs"]
        # verify the required parameter 'id' is set
        if "id" not in params or params["id"] is None:
            raise ValueError(
                "Missing the required parameter `id` when calling `get_plans_using_get`"
            )  # noqa: E501

        collection_formats = {}

        path_params = {}
        if "id" in params:
            path_params["id"] = params["id"]  # noqa: E501

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params["Accept"] = self.api_client.select_header_accept(
            ["application/json"]
        )  # noqa: E501

        # Authentication setting
        auth_settings = ["Bearer"]  # noqa: E501

        return self.api_client.call_api(
            "/cmx/api/resources/pks/endpoints/{id}/plans",
            "GET",
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type="list[PKSPlan1]",  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get("async_req"),
            _return_http_data_only=params.get("_return_http_data_only"),
            _preload_content=params.get("_preload_content", True),
            _request_timeout=params.get("_request_timeout"),
            collection_formats=collection_formats,
        )

    def update_cluster_using_put(self, body, cluster_id, id, **kwargs):  # noqa: E501
        """Update a K8S cluster on a specific endpoint id  # noqa: E501

        Update a K8S cluster on a PKS endpoint identified by id  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.update_cluster_using_put(body, cluster_id, id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param PKSCluster3 body: pksCluster (required)
        :param str cluster_id: clusterId (required)
        :param str id: id (required)
        :return: K8SCluster3
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs["_return_http_data_only"] = True
        if kwargs.get("async_req"):
            return self.update_cluster_using_put_with_http_info(
                body, cluster_id, id, **kwargs
            )  # noqa: E501
        else:
            (data) = self.update_cluster_using_put_with_http_info(
                body, cluster_id, id, **kwargs
            )  # noqa: E501
            return data

    def update_cluster_using_put_with_http_info(
        self, body, cluster_id, id, **kwargs
    ):  # noqa: E501
        """Update a K8S cluster on a specific endpoint id  # noqa: E501

        Update a K8S cluster on a PKS endpoint identified by id  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.update_cluster_using_put_with_http_info(body, cluster_id, id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param PKSCluster3 body: pksCluster (required)
        :param str cluster_id: clusterId (required)
        :param str id: id (required)
        :return: K8SCluster3
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ["body", "cluster_id", "id"]  # noqa: E501
        all_params.append("async_req")
        all_params.append("_return_http_data_only")
        all_params.append("_preload_content")
        all_params.append("_request_timeout")

        params = locals()
        for key, val in params["kwargs"].items():
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method update_cluster_using_put" % key
                )
            params[key] = val
        del params["kwargs"]
        # verify the required parameter 'body' is set
        if "body" not in params or params["body"] is None:
            raise ValueError(
                "Missing the required parameter `body` when calling `update_cluster_using_put`"
            )  # noqa: E501
        # verify the required parameter 'cluster_id' is set
        if "cluster_id" not in params or params["cluster_id"] is None:
            raise ValueError(
                "Missing the required parameter `cluster_id` when calling `update_cluster_using_put`"
            )  # noqa: E501
        # verify the required parameter 'id' is set
        if "id" not in params or params["id"] is None:
            raise ValueError(
                "Missing the required parameter `id` when calling `update_cluster_using_put`"
            )  # noqa: E501

        collection_formats = {}

        path_params = {}
        if "cluster_id" in params:
            path_params["clusterId"] = params["cluster_id"]  # noqa: E501
        if "id" in params:
            path_params["id"] = params["id"]  # noqa: E501

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        if "body" in params:
            body_params = params["body"]
        # HTTP header `Accept`
        header_params["Accept"] = self.api_client.select_header_accept(
            ["application/json"]
        )  # noqa: E501

        # HTTP header `Content-Type`
        header_params[
            "Content-Type"
        ] = self.api_client.select_header_content_type(  # noqa: E501
            ["application/json"]
        )  # noqa: E501

        # Authentication setting
        auth_settings = ["Bearer"]  # noqa: E501

        return self.api_client.call_api(
            "/cmx/api/resources/pks/endpoints/{id}/clusters/{clusterId}",
            "PUT",
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type="K8SCluster3",  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get("async_req"),
            _return_http_data_only=params.get("_return_http_data_only"),
            _preload_content=params.get("_preload_content", True),
            _request_timeout=params.get("_request_timeout"),
            collection_formats=collection_formats,
        )

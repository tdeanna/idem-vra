"""
    CMX Service APIs

    When using Kubernetes integration, deploy and manage Kubernetes clusters and namespaces. The APIs that list collections of resources also support OData like implementation. Below query params can be used across CMX entities.  Example:  1. `$orderby` - returns a result with the rows being sorted by the values of provided attribute.      ```/cmx/api/resources/tags?$orderby=name%20desc```  2. `$top`, `$skip` - `$top` returns the requested number of resources. Used with `$skip`, the client can skip a given number of resources.      ```/cmx/api/resources/tags?$expand=true&$top=10&$skip=2```  3. `page` and `$size` - page used in conjunction with `$size` helps in pagination of resources.      ```/cmx/api/resources/tags?$expand=true&page=0&$size=5```  4. `$filter` - `$filter` returns a subset of resources that satisfy the given predicate expression.      ```     /cmx/api/resources/tags?$filter=startswith(name, 'K8S')   # noqa: E501

    OpenAPI spec version: 2019-09-12

    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""
import pprint
import re  # noqa: F401


class TagState:
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        "deleted": "bool",
        "document_auth_principal_link": "str",
        "document_description": "ServiceDocumentDescription1",
        "document_epoch": "int",
        "document_expiration_time_micros": "int",
        "document_kind": "str",
        "document_owner": "str",
        "document_self_link": "str",
        "document_source_link": "str",
        "document_transaction_id": "str",
        "document_update_action": "str",
        "document_update_time_micros": "int",
        "document_version": "int",
        "external": "bool",
        "key": "str",
        "msp_auth_link": "str",
        "org_auth_link": "str",
        "origins": "list[str]",
        "owner_auth_link": "str",
        "project_auth_link": "str",
        "tenant_links": "list[str]",
        "value": "str",
    }

    attribute_map = {
        "deleted": "deleted",
        "document_auth_principal_link": "documentAuthPrincipalLink",
        "document_description": "documentDescription",
        "document_epoch": "documentEpoch",
        "document_expiration_time_micros": "documentExpirationTimeMicros",
        "document_kind": "documentKind",
        "document_owner": "documentOwner",
        "document_self_link": "documentSelfLink",
        "document_source_link": "documentSourceLink",
        "document_transaction_id": "documentTransactionId",
        "document_update_action": "documentUpdateAction",
        "document_update_time_micros": "documentUpdateTimeMicros",
        "document_version": "documentVersion",
        "external": "external",
        "key": "key",
        "msp_auth_link": "mspAuthLink",
        "org_auth_link": "orgAuthLink",
        "origins": "origins",
        "owner_auth_link": "ownerAuthLink",
        "project_auth_link": "projectAuthLink",
        "tenant_links": "tenantLinks",
        "value": "value",
    }

    def __init__(
        self,
        deleted=None,
        document_auth_principal_link=None,
        document_description=None,
        document_epoch=None,
        document_expiration_time_micros=None,
        document_kind=None,
        document_owner=None,
        document_self_link=None,
        document_source_link=None,
        document_transaction_id=None,
        document_update_action=None,
        document_update_time_micros=None,
        document_version=None,
        external=None,
        key=None,
        msp_auth_link=None,
        org_auth_link=None,
        origins=None,
        owner_auth_link=None,
        project_auth_link=None,
        tenant_links=None,
        value=None,
    ):  # noqa: E501
        """TagState - a model defined in Swagger"""  # noqa: E501
        self._deleted = None
        self._document_auth_principal_link = None
        self._document_description = None
        self._document_epoch = None
        self._document_expiration_time_micros = None
        self._document_kind = None
        self._document_owner = None
        self._document_self_link = None
        self._document_source_link = None
        self._document_transaction_id = None
        self._document_update_action = None
        self._document_update_time_micros = None
        self._document_version = None
        self._external = None
        self._key = None
        self._msp_auth_link = None
        self._org_auth_link = None
        self._origins = None
        self._owner_auth_link = None
        self._project_auth_link = None
        self._tenant_links = None
        self._value = None
        self.discriminator = None
        if deleted is not None:
            self.deleted = deleted
        if document_auth_principal_link is not None:
            self.document_auth_principal_link = document_auth_principal_link
        if document_description is not None:
            self.document_description = document_description
        if document_epoch is not None:
            self.document_epoch = document_epoch
        if document_expiration_time_micros is not None:
            self.document_expiration_time_micros = document_expiration_time_micros
        if document_kind is not None:
            self.document_kind = document_kind
        if document_owner is not None:
            self.document_owner = document_owner
        if document_self_link is not None:
            self.document_self_link = document_self_link
        if document_source_link is not None:
            self.document_source_link = document_source_link
        if document_transaction_id is not None:
            self.document_transaction_id = document_transaction_id
        if document_update_action is not None:
            self.document_update_action = document_update_action
        if document_update_time_micros is not None:
            self.document_update_time_micros = document_update_time_micros
        if document_version is not None:
            self.document_version = document_version
        if external is not None:
            self.external = external
        if key is not None:
            self.key = key
        if msp_auth_link is not None:
            self.msp_auth_link = msp_auth_link
        if org_auth_link is not None:
            self.org_auth_link = org_auth_link
        if origins is not None:
            self.origins = origins
        if owner_auth_link is not None:
            self.owner_auth_link = owner_auth_link
        if project_auth_link is not None:
            self.project_auth_link = project_auth_link
        if tenant_links is not None:
            self.tenant_links = tenant_links
        if value is not None:
            self.value = value

    @property
    def deleted(self):
        """Gets the deleted of this TagState.  # noqa: E501


        :return: The deleted of this TagState.  # noqa: E501
        :rtype: bool
        """
        return self._deleted

    @deleted.setter
    def deleted(self, deleted):
        """Sets the deleted of this TagState.


        :param deleted: The deleted of this TagState.  # noqa: E501
        :type: bool
        """

        self._deleted = deleted

    @property
    def document_auth_principal_link(self):
        """Gets the document_auth_principal_link of this TagState.  # noqa: E501


        :return: The document_auth_principal_link of this TagState.  # noqa: E501
        :rtype: str
        """
        return self._document_auth_principal_link

    @document_auth_principal_link.setter
    def document_auth_principal_link(self, document_auth_principal_link):
        """Sets the document_auth_principal_link of this TagState.


        :param document_auth_principal_link: The document_auth_principal_link of this TagState.  # noqa: E501
        :type: str
        """

        self._document_auth_principal_link = document_auth_principal_link

    @property
    def document_description(self):
        """Gets the document_description of this TagState.  # noqa: E501


        :return: The document_description of this TagState.  # noqa: E501
        :rtype: ServiceDocumentDescription1
        """
        return self._document_description

    @document_description.setter
    def document_description(self, document_description):
        """Sets the document_description of this TagState.


        :param document_description: The document_description of this TagState.  # noqa: E501
        :type: ServiceDocumentDescription1
        """

        self._document_description = document_description

    @property
    def document_epoch(self):
        """Gets the document_epoch of this TagState.  # noqa: E501


        :return: The document_epoch of this TagState.  # noqa: E501
        :rtype: int
        """
        return self._document_epoch

    @document_epoch.setter
    def document_epoch(self, document_epoch):
        """Sets the document_epoch of this TagState.


        :param document_epoch: The document_epoch of this TagState.  # noqa: E501
        :type: int
        """

        self._document_epoch = document_epoch

    @property
    def document_expiration_time_micros(self):
        """Gets the document_expiration_time_micros of this TagState.  # noqa: E501


        :return: The document_expiration_time_micros of this TagState.  # noqa: E501
        :rtype: int
        """
        return self._document_expiration_time_micros

    @document_expiration_time_micros.setter
    def document_expiration_time_micros(self, document_expiration_time_micros):
        """Sets the document_expiration_time_micros of this TagState.


        :param document_expiration_time_micros: The document_expiration_time_micros of this TagState.  # noqa: E501
        :type: int
        """

        self._document_expiration_time_micros = document_expiration_time_micros

    @property
    def document_kind(self):
        """Gets the document_kind of this TagState.  # noqa: E501


        :return: The document_kind of this TagState.  # noqa: E501
        :rtype: str
        """
        return self._document_kind

    @document_kind.setter
    def document_kind(self, document_kind):
        """Sets the document_kind of this TagState.


        :param document_kind: The document_kind of this TagState.  # noqa: E501
        :type: str
        """

        self._document_kind = document_kind

    @property
    def document_owner(self):
        """Gets the document_owner of this TagState.  # noqa: E501


        :return: The document_owner of this TagState.  # noqa: E501
        :rtype: str
        """
        return self._document_owner

    @document_owner.setter
    def document_owner(self, document_owner):
        """Sets the document_owner of this TagState.


        :param document_owner: The document_owner of this TagState.  # noqa: E501
        :type: str
        """

        self._document_owner = document_owner

    @property
    def document_self_link(self):
        """Gets the document_self_link of this TagState.  # noqa: E501


        :return: The document_self_link of this TagState.  # noqa: E501
        :rtype: str
        """
        return self._document_self_link

    @document_self_link.setter
    def document_self_link(self, document_self_link):
        """Sets the document_self_link of this TagState.


        :param document_self_link: The document_self_link of this TagState.  # noqa: E501
        :type: str
        """

        self._document_self_link = document_self_link

    @property
    def document_source_link(self):
        """Gets the document_source_link of this TagState.  # noqa: E501


        :return: The document_source_link of this TagState.  # noqa: E501
        :rtype: str
        """
        return self._document_source_link

    @document_source_link.setter
    def document_source_link(self, document_source_link):
        """Sets the document_source_link of this TagState.


        :param document_source_link: The document_source_link of this TagState.  # noqa: E501
        :type: str
        """

        self._document_source_link = document_source_link

    @property
    def document_transaction_id(self):
        """Gets the document_transaction_id of this TagState.  # noqa: E501


        :return: The document_transaction_id of this TagState.  # noqa: E501
        :rtype: str
        """
        return self._document_transaction_id

    @document_transaction_id.setter
    def document_transaction_id(self, document_transaction_id):
        """Sets the document_transaction_id of this TagState.


        :param document_transaction_id: The document_transaction_id of this TagState.  # noqa: E501
        :type: str
        """

        self._document_transaction_id = document_transaction_id

    @property
    def document_update_action(self):
        """Gets the document_update_action of this TagState.  # noqa: E501


        :return: The document_update_action of this TagState.  # noqa: E501
        :rtype: str
        """
        return self._document_update_action

    @document_update_action.setter
    def document_update_action(self, document_update_action):
        """Sets the document_update_action of this TagState.


        :param document_update_action: The document_update_action of this TagState.  # noqa: E501
        :type: str
        """

        self._document_update_action = document_update_action

    @property
    def document_update_time_micros(self):
        """Gets the document_update_time_micros of this TagState.  # noqa: E501


        :return: The document_update_time_micros of this TagState.  # noqa: E501
        :rtype: int
        """
        return self._document_update_time_micros

    @document_update_time_micros.setter
    def document_update_time_micros(self, document_update_time_micros):
        """Sets the document_update_time_micros of this TagState.


        :param document_update_time_micros: The document_update_time_micros of this TagState.  # noqa: E501
        :type: int
        """

        self._document_update_time_micros = document_update_time_micros

    @property
    def document_version(self):
        """Gets the document_version of this TagState.  # noqa: E501


        :return: The document_version of this TagState.  # noqa: E501
        :rtype: int
        """
        return self._document_version

    @document_version.setter
    def document_version(self, document_version):
        """Sets the document_version of this TagState.


        :param document_version: The document_version of this TagState.  # noqa: E501
        :type: int
        """

        self._document_version = document_version

    @property
    def external(self):
        """Gets the external of this TagState.  # noqa: E501


        :return: The external of this TagState.  # noqa: E501
        :rtype: bool
        """
        return self._external

    @external.setter
    def external(self, external):
        """Sets the external of this TagState.


        :param external: The external of this TagState.  # noqa: E501
        :type: bool
        """

        self._external = external

    @property
    def key(self):
        """Gets the key of this TagState.  # noqa: E501


        :return: The key of this TagState.  # noqa: E501
        :rtype: str
        """
        return self._key

    @key.setter
    def key(self, key):
        """Sets the key of this TagState.


        :param key: The key of this TagState.  # noqa: E501
        :type: str
        """

        self._key = key

    @property
    def msp_auth_link(self):
        """Gets the msp_auth_link of this TagState.  # noqa: E501


        :return: The msp_auth_link of this TagState.  # noqa: E501
        :rtype: str
        """
        return self._msp_auth_link

    @msp_auth_link.setter
    def msp_auth_link(self, msp_auth_link):
        """Sets the msp_auth_link of this TagState.


        :param msp_auth_link: The msp_auth_link of this TagState.  # noqa: E501
        :type: str
        """

        self._msp_auth_link = msp_auth_link

    @property
    def org_auth_link(self):
        """Gets the org_auth_link of this TagState.  # noqa: E501


        :return: The org_auth_link of this TagState.  # noqa: E501
        :rtype: str
        """
        return self._org_auth_link

    @org_auth_link.setter
    def org_auth_link(self, org_auth_link):
        """Sets the org_auth_link of this TagState.


        :param org_auth_link: The org_auth_link of this TagState.  # noqa: E501
        :type: str
        """

        self._org_auth_link = org_auth_link

    @property
    def origins(self):
        """Gets the origins of this TagState.  # noqa: E501


        :return: The origins of this TagState.  # noqa: E501
        :rtype: list[str]
        """
        return self._origins

    @origins.setter
    def origins(self, origins):
        """Sets the origins of this TagState.


        :param origins: The origins of this TagState.  # noqa: E501
        :type: list[str]
        """
        allowed_values = ["SYSTEM", "USER_DEFINED", "DISCOVERED"]  # noqa: E501
        if not set(origins).issubset(set(allowed_values)):
            raise ValueError(
                "Invalid values for `origins` [{}], must be a subset of [{}]".format(  # noqa: E501
                    ", ".join(
                        map(str, set(origins) - set(allowed_values))
                    ),  # noqa: E501
                    ", ".join(map(str, allowed_values)),
                )
            )

        self._origins = origins

    @property
    def owner_auth_link(self):
        """Gets the owner_auth_link of this TagState.  # noqa: E501


        :return: The owner_auth_link of this TagState.  # noqa: E501
        :rtype: str
        """
        return self._owner_auth_link

    @owner_auth_link.setter
    def owner_auth_link(self, owner_auth_link):
        """Sets the owner_auth_link of this TagState.


        :param owner_auth_link: The owner_auth_link of this TagState.  # noqa: E501
        :type: str
        """

        self._owner_auth_link = owner_auth_link

    @property
    def project_auth_link(self):
        """Gets the project_auth_link of this TagState.  # noqa: E501


        :return: The project_auth_link of this TagState.  # noqa: E501
        :rtype: str
        """
        return self._project_auth_link

    @project_auth_link.setter
    def project_auth_link(self, project_auth_link):
        """Sets the project_auth_link of this TagState.


        :param project_auth_link: The project_auth_link of this TagState.  # noqa: E501
        :type: str
        """

        self._project_auth_link = project_auth_link

    @property
    def tenant_links(self):
        """Gets the tenant_links of this TagState.  # noqa: E501


        :return: The tenant_links of this TagState.  # noqa: E501
        :rtype: list[str]
        """
        return self._tenant_links

    @tenant_links.setter
    def tenant_links(self, tenant_links):
        """Sets the tenant_links of this TagState.


        :param tenant_links: The tenant_links of this TagState.  # noqa: E501
        :type: list[str]
        """

        self._tenant_links = tenant_links

    @property
    def value(self):
        """Gets the value of this TagState.  # noqa: E501


        :return: The value of this TagState.  # noqa: E501
        :rtype: str
        """
        return self._value

    @value.setter
    def value(self, value):
        """Sets the value of this TagState.


        :param value: The value of this TagState.  # noqa: E501
        :type: str
        """

        self._value = value

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in self.swagger_types.items():
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(
                    map(lambda x: x.to_dict() if hasattr(x, "to_dict") else x, value)
                )
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(
                    map(
                        lambda item: (item[0], item[1].to_dict())
                        if hasattr(item[1], "to_dict")
                        else item,
                        value.items(),
                    )
                )
            else:
                result[attr] = value
        if issubclass(TagState, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, TagState):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other

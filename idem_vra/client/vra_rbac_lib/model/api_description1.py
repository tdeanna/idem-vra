"""
    RBAC Service API

    Exposed API, providing access to user and role operations  # noqa: E501

    OpenAPI spec version: 2020-08-10

    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""
import pprint
import re  # noqa: F401


class ApiDescription1:
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        "api_version": "str",
        "deprecation_policy": "DeprecationPolicy1",
        "documentation_link": "str",
    }

    attribute_map = {
        "api_version": "apiVersion",
        "deprecation_policy": "deprecationPolicy",
        "documentation_link": "documentationLink",
    }

    def __init__(
        self, api_version=None, deprecation_policy=None, documentation_link=None
    ):  # noqa: E501
        """ApiDescription1 - a model defined in Swagger"""  # noqa: E501
        self._api_version = None
        self._deprecation_policy = None
        self._documentation_link = None
        self.discriminator = None
        self.api_version = api_version
        if deprecation_policy is not None:
            self.deprecation_policy = deprecation_policy
        self.documentation_link = documentation_link

    @property
    def api_version(self):
        """Gets the api_version of this ApiDescription1.  # noqa: E501

        The version of the API in yyyy-MM-dd format (UTC).  # noqa: E501

        :return: The api_version of this ApiDescription1.  # noqa: E501
        :rtype: str
        """
        return self._api_version

    @api_version.setter
    def api_version(self, api_version):
        """Sets the api_version of this ApiDescription1.

        The version of the API in yyyy-MM-dd format (UTC).  # noqa: E501

        :param api_version: The api_version of this ApiDescription1.  # noqa: E501
        :type: str
        """
        if api_version is None:
            raise ValueError(
                "Invalid value for `api_version`, must not be `None`"
            )  # noqa: E501

        self._api_version = api_version

    @property
    def deprecation_policy(self):
        """Gets the deprecation_policy of this ApiDescription1.  # noqa: E501


        :return: The deprecation_policy of this ApiDescription1.  # noqa: E501
        :rtype: DeprecationPolicy1
        """
        return self._deprecation_policy

    @deprecation_policy.setter
    def deprecation_policy(self, deprecation_policy):
        """Sets the deprecation_policy of this ApiDescription1.


        :param deprecation_policy: The deprecation_policy of this ApiDescription1.  # noqa: E501
        :type: DeprecationPolicy1
        """

        self._deprecation_policy = deprecation_policy

    @property
    def documentation_link(self):
        """Gets the documentation_link of this ApiDescription1.  # noqa: E501

        The link to the documentation of this api version.  # noqa: E501

        :return: The documentation_link of this ApiDescription1.  # noqa: E501
        :rtype: str
        """
        return self._documentation_link

    @documentation_link.setter
    def documentation_link(self, documentation_link):
        """Sets the documentation_link of this ApiDescription1.

        The link to the documentation of this api version.  # noqa: E501

        :param documentation_link: The documentation_link of this ApiDescription1.  # noqa: E501
        :type: str
        """
        if documentation_link is None:
            raise ValueError(
                "Invalid value for `documentation_link`, must not be `None`"
            )  # noqa: E501

        self._documentation_link = documentation_link

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in self.swagger_types.items():
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(
                    map(lambda x: x.to_dict() if hasattr(x, "to_dict") else x, value)
                )
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(
                    map(
                        lambda item: (item[0], item[1].to_dict())
                        if hasattr(item[1], "to_dict")
                        else item,
                        value.items(),
                    )
                )
            else:
                result[attr] = value
        if issubclass(ApiDescription1, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, ApiDescription1):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other

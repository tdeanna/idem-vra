"""
    VMware Aria Automation Pipelines APIs

    Aria Automation Pipelines is a continuous integration and continuous delivery (CICD) tool that you use to build Pipelines that model the software release process in your DevOps lifecycle. By creating Pipelines, you build the code infrastructure that delivers your software rapidly and continuously.  This page describes the RESTful APIs for Aria Automation Pipelines. The APIs facilitate CRUD operations on the various resources and entities used throughout Aria Automation Pipelines (Pipelines, Endpoints, Variables, etc.) and allow operations on them (executing a Pipeline, validating an Endpoint connection, etc.).  The APIs that list collections of resources  also support OData like implementation. Below query params can be used across Aria Automation Pipelines entities.  Example:  1. `$orderby` - returns a result with the rows being sorted by the values of provided attribute.      ```/codestream/api/endpoints?$orderby=name%20desc```  2. `$top`, `$skip` - `$top` returns the requested number of resources. Used with `$skip`, the client can skip a given number of resources.      ```/codestream/api/endpoints?$expand=true&$top=10&$skip=2```  3. `page` and `$size` - page used in conjunction with `$size` helps in pagination of resources.      ```/codestream/api/endpoints?$expand=true&page=0&$size=5```  4. `$filter` - `$filter` returns a subset of resources that satisfy the given predicate expression.      ```     /codestream/api/endpoints?$filter=startswith(name, 'ABC')     /codestream/api/endpoints?$filter=toupper(name) eq 'ABCD-JENKINS'     /codestream/api/endpoints?$filter=substringof(%27bc%27,tolower(name))     /codestream/api/endpoints?$filter=name eq 'ABCD' and project eq 'demo'   # noqa: E501

    OpenAPI spec version: 2019-10-17

    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""
import pprint
import re  # noqa: F401


class ValidatorResponse:
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        "class_type": "str",
        "message": "ValidationMessage1",
        "success": "bool",
    }

    attribute_map = {
        "class_type": "classType",
        "message": "message",
        "success": "success",
    }

    def __init__(self, class_type=None, message=None, success=None):  # noqa: E501
        """ValidatorResponse - a model defined in Swagger"""  # noqa: E501
        self._class_type = None
        self._message = None
        self._success = None
        self.discriminator = None
        if class_type is not None:
            self.class_type = class_type
        if message is not None:
            self.message = message
        if success is not None:
            self.success = success

    @property
    def class_type(self):
        """Gets the class_type of this ValidatorResponse.  # noqa: E501

        Class which will be validated.  # noqa: E501

        :return: The class_type of this ValidatorResponse.  # noqa: E501
        :rtype: str
        """
        return self._class_type

    @class_type.setter
    def class_type(self, class_type):
        """Sets the class_type of this ValidatorResponse.

        Class which will be validated.  # noqa: E501

        :param class_type: The class_type of this ValidatorResponse.  # noqa: E501
        :type: str
        """

        self._class_type = class_type

    @property
    def message(self):
        """Gets the message of this ValidatorResponse.  # noqa: E501


        :return: The message of this ValidatorResponse.  # noqa: E501
        :rtype: ValidationMessage1
        """
        return self._message

    @message.setter
    def message(self, message):
        """Sets the message of this ValidatorResponse.


        :param message: The message of this ValidatorResponse.  # noqa: E501
        :type: ValidationMessage1
        """

        self._message = message

    @property
    def success(self):
        """Gets the success of this ValidatorResponse.  # noqa: E501

        Indicates whether validation was successful or not.  # noqa: E501

        :return: The success of this ValidatorResponse.  # noqa: E501
        :rtype: bool
        """
        return self._success

    @success.setter
    def success(self, success):
        """Sets the success of this ValidatorResponse.

        Indicates whether validation was successful or not.  # noqa: E501

        :param success: The success of this ValidatorResponse.  # noqa: E501
        :type: bool
        """

        self._success = success

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in self.swagger_types.items():
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(
                    map(lambda x: x.to_dict() if hasattr(x, "to_dict") else x, value)
                )
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(
                    map(
                        lambda item: (item[0], item[1].to_dict())
                        if hasattr(item[1], "to_dict")
                        else item,
                        value.items(),
                    )
                )
            else:
                result[attr] = value
        if issubclass(ValidatorResponse, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, ValidatorResponse):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other

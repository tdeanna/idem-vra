"""
    VMware Aria Automation Pipelines APIs

    Aria Automation Pipelines is a continuous integration and continuous delivery (CICD) tool that you use to build Pipelines that model the software release process in your DevOps lifecycle. By creating Pipelines, you build the code infrastructure that delivers your software rapidly and continuously.  This page describes the RESTful APIs for Aria Automation Pipelines. The APIs facilitate CRUD operations on the various resources and entities used throughout Aria Automation Pipelines (Pipelines, Endpoints, Variables, etc.) and allow operations on them (executing a Pipeline, validating an Endpoint connection, etc.).  The APIs that list collections of resources  also support OData like implementation. Below query params can be used across Aria Automation Pipelines entities.  Example:  1. `$orderby` - returns a result with the rows being sorted by the values of provided attribute.      ```/codestream/api/endpoints?$orderby=name%20desc```  2. `$top`, `$skip` - `$top` returns the requested number of resources. Used with `$skip`, the client can skip a given number of resources.      ```/codestream/api/endpoints?$expand=true&$top=10&$skip=2```  3. `page` and `$size` - page used in conjunction with `$size` helps in pagination of resources.      ```/codestream/api/endpoints?$expand=true&page=0&$size=5```  4. `$filter` - `$filter` returns a subset of resources that satisfy the given predicate expression.      ```     /codestream/api/endpoints?$filter=startswith(name, 'ABC')     /codestream/api/endpoints?$filter=toupper(name) eq 'ABCD-JENKINS'     /codestream/api/endpoints?$filter=substringof(%27bc%27,tolower(name))     /codestream/api/endpoints?$filter=name eq 'ABCD' and project eq 'demo'   # noqa: E501

    OpenAPI spec version: 2019-10-17

    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""
import pprint
import re  # noqa: F401


class GerritTriggerSpecGerritEventConfiguration:
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        "event_type": "str",
        "failure_comment": "str",
        "input": "dict(str, str)",
        "pipeline": "str",
        "success_comment": "str",
        "verified_label": "str",
    }

    attribute_map = {
        "event_type": "eventType",
        "failure_comment": "failureComment",
        "input": "input",
        "pipeline": "pipeline",
        "success_comment": "successComment",
        "verified_label": "verifiedLabel",
    }

    def __init__(
        self,
        event_type=None,
        failure_comment=None,
        input=None,
        pipeline=None,
        success_comment=None,
        verified_label=None,
    ):  # noqa: E501
        """GerritTriggerSpecGerritEventConfiguration - a model defined in Swagger"""  # noqa: E501
        self._event_type = None
        self._failure_comment = None
        self._input = None
        self._pipeline = None
        self._success_comment = None
        self._verified_label = None
        self.discriminator = None
        self.event_type = event_type
        if failure_comment is not None:
            self.failure_comment = failure_comment
        if input is not None:
            self.input = input
        self.pipeline = pipeline
        if success_comment is not None:
            self.success_comment = success_comment
        if verified_label is not None:
            self.verified_label = verified_label

    @property
    def event_type(self):
        """Gets the event_type of this GerritTriggerSpecGerritEventConfiguration.  # noqa: E501

        Type of the gerrit event.  # noqa: E501

        :return: The event_type of this GerritTriggerSpecGerritEventConfiguration.  # noqa: E501
        :rtype: str
        """
        return self._event_type

    @event_type.setter
    def event_type(self, event_type):
        """Sets the event_type of this GerritTriggerSpecGerritEventConfiguration.

        Type of the gerrit event.  # noqa: E501

        :param event_type: The event_type of this GerritTriggerSpecGerritEventConfiguration.  # noqa: E501
        :type: str
        """
        if event_type is None:
            raise ValueError(
                "Invalid value for `event_type`, must not be `None`"
            )  # noqa: E501

        self._event_type = event_type

    @property
    def failure_comment(self):
        """Gets the failure_comment of this GerritTriggerSpecGerritEventConfiguration.  # noqa: E501

        Comment to be posted to the ChangeSet on execution termination.  # noqa: E501

        :return: The failure_comment of this GerritTriggerSpecGerritEventConfiguration.  # noqa: E501
        :rtype: str
        """
        return self._failure_comment

    @failure_comment.setter
    def failure_comment(self, failure_comment):
        """Sets the failure_comment of this GerritTriggerSpecGerritEventConfiguration.

        Comment to be posted to the ChangeSet on execution termination.  # noqa: E501

        :param failure_comment: The failure_comment of this GerritTriggerSpecGerritEventConfiguration.  # noqa: E501
        :type: str
        """

        self._failure_comment = failure_comment

    @property
    def input(self):
        """Gets the input of this GerritTriggerSpecGerritEventConfiguration.  # noqa: E501

        Map representing the Input properties for the Pipeline.  # noqa: E501

        :return: The input of this GerritTriggerSpecGerritEventConfiguration.  # noqa: E501
        :rtype: dict(str, str)
        """
        return self._input

    @input.setter
    def input(self, input):
        """Sets the input of this GerritTriggerSpecGerritEventConfiguration.

        Map representing the Input properties for the Pipeline.  # noqa: E501

        :param input: The input of this GerritTriggerSpecGerritEventConfiguration.  # noqa: E501
        :type: dict(str, str)
        """

        self._input = input

    @property
    def pipeline(self):
        """Gets the pipeline of this GerritTriggerSpecGerritEventConfiguration.  # noqa: E501

        Pipeline that needs to be triggered on receiving this event.  # noqa: E501

        :return: The pipeline of this GerritTriggerSpecGerritEventConfiguration.  # noqa: E501
        :rtype: str
        """
        return self._pipeline

    @pipeline.setter
    def pipeline(self, pipeline):
        """Sets the pipeline of this GerritTriggerSpecGerritEventConfiguration.

        Pipeline that needs to be triggered on receiving this event.  # noqa: E501

        :param pipeline: The pipeline of this GerritTriggerSpecGerritEventConfiguration.  # noqa: E501
        :type: str
        """
        if pipeline is None:
            raise ValueError(
                "Invalid value for `pipeline`, must not be `None`"
            )  # noqa: E501

        self._pipeline = pipeline

    @property
    def success_comment(self):
        """Gets the success_comment of this GerritTriggerSpecGerritEventConfiguration.  # noqa: E501

        Comment to be posted to the ChangeSet on execution termination.  # noqa: E501

        :return: The success_comment of this GerritTriggerSpecGerritEventConfiguration.  # noqa: E501
        :rtype: str
        """
        return self._success_comment

    @success_comment.setter
    def success_comment(self, success_comment):
        """Sets the success_comment of this GerritTriggerSpecGerritEventConfiguration.

        Comment to be posted to the ChangeSet on execution termination.  # noqa: E501

        :param success_comment: The success_comment of this GerritTriggerSpecGerritEventConfiguration.  # noqa: E501
        :type: str
        """

        self._success_comment = success_comment

    @property
    def verified_label(self):
        """Gets the verified_label of this GerritTriggerSpecGerritEventConfiguration.  # noqa: E501

        The label to be posted on Gerrit Server to perform actions.  # noqa: E501

        :return: The verified_label of this GerritTriggerSpecGerritEventConfiguration.  # noqa: E501
        :rtype: str
        """
        return self._verified_label

    @verified_label.setter
    def verified_label(self, verified_label):
        """Sets the verified_label of this GerritTriggerSpecGerritEventConfiguration.

        The label to be posted on Gerrit Server to perform actions.  # noqa: E501

        :param verified_label: The verified_label of this GerritTriggerSpecGerritEventConfiguration.  # noqa: E501
        :type: str
        """

        self._verified_label = verified_label

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in self.swagger_types.items():
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(
                    map(lambda x: x.to_dict() if hasattr(x, "to_dict") else x, value)
                )
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(
                    map(
                        lambda item: (item[0], item[1].to_dict())
                        if hasattr(item[1], "to_dict")
                        else item,
                        value.items(),
                    )
                )
            else:
                result[attr] = value
        if issubclass(GerritTriggerSpecGerritEventConfiguration, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, GerritTriggerSpecGerritEventConfiguration):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other

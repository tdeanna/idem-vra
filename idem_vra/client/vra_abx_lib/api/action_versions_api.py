"""
    ABX Service - APIs

    Create or manage actions and their versions. Execute actions and flows.  This page describes the RESTful APIs for ABX Service. The APIs facilitate CRUD operations on creation and management of Actions and their Versions and also allow Execution of Actions and Flows.  The APIs that list collections of resources  also support OData like implementation. Below query params can be used across ABX Service APIs.  Example:  1. `$orderby` - returns a result with the rows being sorted by the values of provided attribute.      ```/abx/api/resources/actions?$orderby=name%20desc```  2. `$top`, `$skip` - `$top` returns the requested number of resources. Used with `$skip`, the client can skip a given number of resources.      ```/abx/api/resources/actions?$expand=true&$top=10&$skip=2```  3. `page` and `$size` - page used in conjunction with `$size` helps in pagination of resources.      ```/abx/api/resources/actions?$expand=true&page=0&$size=5```  4. `$filter` - `$filter` returns a subset of resources that satisfy the given predicate expression.      ```     /abx/api/resources/actions?$filter=startswith(name, 'ABC')     /abx/api/resources/actions?$filter=toupper(name) eq 'ABCD-ACTION'     /abx/api/resources/actions?$filter=substringof(%27bc%27,tolower(name))     /abx/api/resources/actions?$filter=name eq 'ABCD' and actionType eq 'SCRIPT'   # noqa: E501

    OpenAPI spec version: 2019-09-12

    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""
import re  # noqa: F401

from idem_vra.client.vra_abx_lib.api_client import ApiClient

# python 2 and python 3 compatibility library


class ActionVersionsApi:
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    Ref: https://github.com/swagger-api/swagger-codegen
    """

    def __init__(self, api_client=None):
        if api_client is None:
            api_client = ApiClient()
        self.api_client = api_client

    def create_action_version_using_post(self, body, id, **kwargs):  # noqa: E501
        """Create a version for an action  # noqa: E501

        Creates a new version for the specified action  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.create_action_version_using_post(body, id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param ActionVersion2 body: Details of the action version (required)
        :param str id: ID of the action (required)
        :param str project_id: Project ID of action (required for non-system actions)
        :return: ActionVersion2
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs["_return_http_data_only"] = True
        if kwargs.get("async_req"):
            return self.create_action_version_using_post_with_http_info(
                body, id, **kwargs
            )  # noqa: E501
        else:
            (data) = self.create_action_version_using_post_with_http_info(
                body, id, **kwargs
            )  # noqa: E501
            return data

    def create_action_version_using_post_with_http_info(
        self, body, id, **kwargs
    ):  # noqa: E501
        """Create a version for an action  # noqa: E501

        Creates a new version for the specified action  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.create_action_version_using_post_with_http_info(body, id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param ActionVersion2 body: Details of the action version (required)
        :param str id: ID of the action (required)
        :param str project_id: Project ID of action (required for non-system actions)
        :return: ActionVersion2
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ["body", "id", "project_id"]  # noqa: E501
        all_params.append("async_req")
        all_params.append("_return_http_data_only")
        all_params.append("_preload_content")
        all_params.append("_request_timeout")

        params = locals()
        for key, val in params["kwargs"].items():
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method create_action_version_using_post" % key
                )
            params[key] = val
        del params["kwargs"]
        # verify the required parameter 'body' is set
        if "body" not in params or params["body"] is None:
            raise ValueError(
                "Missing the required parameter `body` when calling `create_action_version_using_post`"
            )  # noqa: E501
        # verify the required parameter 'id' is set
        if "id" not in params or params["id"] is None:
            raise ValueError(
                "Missing the required parameter `id` when calling `create_action_version_using_post`"
            )  # noqa: E501

        collection_formats = {}

        path_params = {}
        if "id" in params:
            path_params["id"] = params["id"]  # noqa: E501

        query_params = []
        if "project_id" in params:
            query_params.append(("projectId", params["project_id"]))  # noqa: E501

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        if "body" in params:
            body_params = params["body"]
        # HTTP header `Accept`
        header_params["Accept"] = self.api_client.select_header_accept(
            ["application/json"]
        )  # noqa: E501

        # HTTP header `Content-Type`
        header_params[
            "Content-Type"
        ] = self.api_client.select_header_content_type(  # noqa: E501
            ["application/json"]
        )  # noqa: E501

        # Authentication setting
        auth_settings = ["Authorization"]  # noqa: E501

        return self.api_client.call_api(
            "/abx/api/resources/actions/{id}/versions",
            "POST",
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type="ActionVersion2",  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get("async_req"),
            _return_http_data_only=params.get("_return_http_data_only"),
            _preload_content=params.get("_preload_content", True),
            _request_timeout=params.get("_request_timeout"),
            collection_formats=collection_formats,
        )

    def delete_action_version_using_delete(
        self, id, version_id, **kwargs
    ):  # noqa: E501
        """Delete an action version  # noqa: E501

        Deletes an action version  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.delete_action_version_using_delete(id, version_id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str id: ID of the action (required)
        :param str version_id: ID of the action version (required)
        :param str project_id: Project ID of action (required for non-system actions)
        :return: ActionVersion2
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs["_return_http_data_only"] = True
        if kwargs.get("async_req"):
            return self.delete_action_version_using_delete_with_http_info(
                id, version_id, **kwargs
            )  # noqa: E501
        else:
            (data) = self.delete_action_version_using_delete_with_http_info(
                id, version_id, **kwargs
            )  # noqa: E501
            return data

    def delete_action_version_using_delete_with_http_info(
        self, id, version_id, **kwargs
    ):  # noqa: E501
        """Delete an action version  # noqa: E501

        Deletes an action version  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.delete_action_version_using_delete_with_http_info(id, version_id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str id: ID of the action (required)
        :param str version_id: ID of the action version (required)
        :param str project_id: Project ID of action (required for non-system actions)
        :return: ActionVersion2
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ["id", "version_id", "project_id"]  # noqa: E501
        all_params.append("async_req")
        all_params.append("_return_http_data_only")
        all_params.append("_preload_content")
        all_params.append("_request_timeout")

        params = locals()
        for key, val in params["kwargs"].items():
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method delete_action_version_using_delete" % key
                )
            params[key] = val
        del params["kwargs"]
        # verify the required parameter 'id' is set
        if "id" not in params or params["id"] is None:
            raise ValueError(
                "Missing the required parameter `id` when calling `delete_action_version_using_delete`"
            )  # noqa: E501
        # verify the required parameter 'version_id' is set
        if "version_id" not in params or params["version_id"] is None:
            raise ValueError(
                "Missing the required parameter `version_id` when calling `delete_action_version_using_delete`"
            )  # noqa: E501

        collection_formats = {}

        path_params = {}
        if "id" in params:
            path_params["id"] = params["id"]  # noqa: E501
        if "version_id" in params:
            path_params["versionId"] = params["version_id"]  # noqa: E501

        query_params = []
        if "project_id" in params:
            query_params.append(("projectId", params["project_id"]))  # noqa: E501

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params["Accept"] = self.api_client.select_header_accept(
            ["application/json"]
        )  # noqa: E501

        # Authentication setting
        auth_settings = ["Authorization"]  # noqa: E501

        return self.api_client.call_api(
            "/abx/api/resources/actions/{id}/versions/{versionId}",
            "DELETE",
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type="ActionVersion2",  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get("async_req"),
            _return_http_data_only=params.get("_return_http_data_only"),
            _preload_content=params.get("_preload_content", True),
            _request_timeout=params.get("_request_timeout"),
            collection_formats=collection_formats,
        )

    def get_all_versions_using_get(self, id, **kwargs):  # noqa: E501
        """Fetch all versions of an action  # noqa: E501

        Retrieves all created versions for an action  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_all_versions_using_get(id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str id: ID of the action (required)
        :param str page: Page to fetch (starting from 0)
        :param str project_id: Project ID of action (required for non-system actions)
        :param str size: Amount of entities per page
        :return: ActionVersionsPage1
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs["_return_http_data_only"] = True
        if kwargs.get("async_req"):
            return self.get_all_versions_using_get_with_http_info(
                id, **kwargs
            )  # noqa: E501
        else:
            (data) = self.get_all_versions_using_get_with_http_info(
                id, **kwargs
            )  # noqa: E501
            return data

    def get_all_versions_using_get_with_http_info(self, id, **kwargs):  # noqa: E501
        """Fetch all versions of an action  # noqa: E501

        Retrieves all created versions for an action  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_all_versions_using_get_with_http_info(id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str id: ID of the action (required)
        :param str page: Page to fetch (starting from 0)
        :param str project_id: Project ID of action (required for non-system actions)
        :param str size: Amount of entities per page
        :return: ActionVersionsPage1
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ["id", "page", "project_id", "size"]  # noqa: E501
        all_params.append("async_req")
        all_params.append("_return_http_data_only")
        all_params.append("_preload_content")
        all_params.append("_request_timeout")

        params = locals()
        for key, val in params["kwargs"].items():
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_all_versions_using_get" % key
                )
            params[key] = val
        del params["kwargs"]
        # verify the required parameter 'id' is set
        if "id" not in params or params["id"] is None:
            raise ValueError(
                "Missing the required parameter `id` when calling `get_all_versions_using_get`"
            )  # noqa: E501

        collection_formats = {}

        path_params = {}
        if "id" in params:
            path_params["id"] = params["id"]  # noqa: E501

        query_params = []
        if "page" in params:
            query_params.append(("page", params["page"]))  # noqa: E501
        if "project_id" in params:
            query_params.append(("projectId", params["project_id"]))  # noqa: E501
        if "size" in params:
            query_params.append(("size", params["size"]))  # noqa: E501

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params["Accept"] = self.api_client.select_header_accept(
            ["application/json"]
        )  # noqa: E501

        # Authentication setting
        auth_settings = ["Authorization"]  # noqa: E501

        return self.api_client.call_api(
            "/abx/api/resources/actions/{id}/versions",
            "GET",
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type="ActionVersionsPage1",  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get("async_req"),
            _return_http_data_only=params.get("_return_http_data_only"),
            _preload_content=params.get("_preload_content", True),
            _request_timeout=params.get("_request_timeout"),
            collection_formats=collection_formats,
        )

    def release_action_version_using_put(self, body, id, **kwargs):  # noqa: E501
        """Mark an action version as released  # noqa: E501

        Marks an exisiting version of an action as released  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.release_action_version_using_put(body, id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param ActionVersionReleaseRequest1 body: Name of the version to release (required)
        :param str id: ID of the action (required)
        :param str project_id: Project ID of action (required for non-system actions)
        :return: ActionVersion1
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs["_return_http_data_only"] = True
        if kwargs.get("async_req"):
            return self.release_action_version_using_put_with_http_info(
                body, id, **kwargs
            )  # noqa: E501
        else:
            (data) = self.release_action_version_using_put_with_http_info(
                body, id, **kwargs
            )  # noqa: E501
            return data

    def release_action_version_using_put_with_http_info(
        self, body, id, **kwargs
    ):  # noqa: E501
        """Mark an action version as released  # noqa: E501

        Marks an exisiting version of an action as released  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.release_action_version_using_put_with_http_info(body, id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param ActionVersionReleaseRequest1 body: Name of the version to release (required)
        :param str id: ID of the action (required)
        :param str project_id: Project ID of action (required for non-system actions)
        :return: ActionVersion1
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ["body", "id", "project_id"]  # noqa: E501
        all_params.append("async_req")
        all_params.append("_return_http_data_only")
        all_params.append("_preload_content")
        all_params.append("_request_timeout")

        params = locals()
        for key, val in params["kwargs"].items():
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method release_action_version_using_put" % key
                )
            params[key] = val
        del params["kwargs"]
        # verify the required parameter 'body' is set
        if "body" not in params or params["body"] is None:
            raise ValueError(
                "Missing the required parameter `body` when calling `release_action_version_using_put`"
            )  # noqa: E501
        # verify the required parameter 'id' is set
        if "id" not in params or params["id"] is None:
            raise ValueError(
                "Missing the required parameter `id` when calling `release_action_version_using_put`"
            )  # noqa: E501

        collection_formats = {}

        path_params = {}
        if "id" in params:
            path_params["id"] = params["id"]  # noqa: E501

        query_params = []
        if "project_id" in params:
            query_params.append(("projectId", params["project_id"]))  # noqa: E501

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        if "body" in params:
            body_params = params["body"]
        # HTTP header `Accept`
        header_params["Accept"] = self.api_client.select_header_accept(
            ["application/json"]
        )  # noqa: E501

        # HTTP header `Content-Type`
        header_params[
            "Content-Type"
        ] = self.api_client.select_header_content_type(  # noqa: E501
            ["application/json"]
        )  # noqa: E501

        # Authentication setting
        auth_settings = ["Authorization"]  # noqa: E501

        return self.api_client.call_api(
            "/abx/api/resources/actions/{id}/release",
            "PUT",
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type="ActionVersion1",  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get("async_req"),
            _return_http_data_only=params.get("_return_http_data_only"),
            _preload_content=params.get("_preload_content", True),
            _request_timeout=params.get("_request_timeout"),
            collection_formats=collection_formats,
        )

    def restore_action_version_using_put(self, id, version_id, **kwargs):  # noqa: E501
        """Restore the action state based on a specified version  # noqa: E501

        Change the current action state to the state specified in the version  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.restore_action_version_using_put(id, version_id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str id: ID of the action (required)
        :param str version_id: ID of the action version (required)
        :param str project_id: Project ID of action (required for non-system actions)
        :return: Action4
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs["_return_http_data_only"] = True
        if kwargs.get("async_req"):
            return self.restore_action_version_using_put_with_http_info(
                id, version_id, **kwargs
            )  # noqa: E501
        else:
            (data) = self.restore_action_version_using_put_with_http_info(
                id, version_id, **kwargs
            )  # noqa: E501
            return data

    def restore_action_version_using_put_with_http_info(
        self, id, version_id, **kwargs
    ):  # noqa: E501
        """Restore the action state based on a specified version  # noqa: E501

        Change the current action state to the state specified in the version  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.restore_action_version_using_put_with_http_info(id, version_id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str id: ID of the action (required)
        :param str version_id: ID of the action version (required)
        :param str project_id: Project ID of action (required for non-system actions)
        :return: Action4
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ["id", "version_id", "project_id"]  # noqa: E501
        all_params.append("async_req")
        all_params.append("_return_http_data_only")
        all_params.append("_preload_content")
        all_params.append("_request_timeout")

        params = locals()
        for key, val in params["kwargs"].items():
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method restore_action_version_using_put" % key
                )
            params[key] = val
        del params["kwargs"]
        # verify the required parameter 'id' is set
        if "id" not in params or params["id"] is None:
            raise ValueError(
                "Missing the required parameter `id` when calling `restore_action_version_using_put`"
            )  # noqa: E501
        # verify the required parameter 'version_id' is set
        if "version_id" not in params or params["version_id"] is None:
            raise ValueError(
                "Missing the required parameter `version_id` when calling `restore_action_version_using_put`"
            )  # noqa: E501

        collection_formats = {}

        path_params = {}
        if "id" in params:
            path_params["id"] = params["id"]  # noqa: E501
        if "version_id" in params:
            path_params["versionId"] = params["version_id"]  # noqa: E501

        query_params = []
        if "project_id" in params:
            query_params.append(("projectId", params["project_id"]))  # noqa: E501

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params["Accept"] = self.api_client.select_header_accept(
            ["application/json"]
        )  # noqa: E501

        # Authentication setting
        auth_settings = ["Authorization"]  # noqa: E501

        return self.api_client.call_api(
            "/abx/api/resources/actions/{id}/versions/{versionId}/restore",
            "PUT",
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type="Action4",  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get("async_req"),
            _return_http_data_only=params.get("_return_http_data_only"),
            _preload_content=params.get("_preload_content", True),
            _request_timeout=params.get("_request_timeout"),
            collection_formats=collection_formats,
        )

    def unrelease_action_version_using_delete(self, id, **kwargs):  # noqa: E501
        """Mark an action version as not released  # noqa: E501

        Marks an actions released version as not released  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.unrelease_action_version_using_delete(id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str id: ID of the action (required)
        :param str project_id: Project ID of action (required for non-system actions)
        :return: Action4
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs["_return_http_data_only"] = True
        if kwargs.get("async_req"):
            return self.unrelease_action_version_using_delete_with_http_info(
                id, **kwargs
            )  # noqa: E501
        else:
            (data) = self.unrelease_action_version_using_delete_with_http_info(
                id, **kwargs
            )  # noqa: E501
            return data

    def unrelease_action_version_using_delete_with_http_info(
        self, id, **kwargs
    ):  # noqa: E501
        """Mark an action version as not released  # noqa: E501

        Marks an actions released version as not released  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.unrelease_action_version_using_delete_with_http_info(id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str id: ID of the action (required)
        :param str project_id: Project ID of action (required for non-system actions)
        :return: Action4
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ["id", "project_id"]  # noqa: E501
        all_params.append("async_req")
        all_params.append("_return_http_data_only")
        all_params.append("_preload_content")
        all_params.append("_request_timeout")

        params = locals()
        for key, val in params["kwargs"].items():
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method unrelease_action_version_using_delete" % key
                )
            params[key] = val
        del params["kwargs"]
        # verify the required parameter 'id' is set
        if "id" not in params or params["id"] is None:
            raise ValueError(
                "Missing the required parameter `id` when calling `unrelease_action_version_using_delete`"
            )  # noqa: E501

        collection_formats = {}

        path_params = {}
        if "id" in params:
            path_params["id"] = params["id"]  # noqa: E501

        query_params = []
        if "project_id" in params:
            query_params.append(("projectId", params["project_id"]))  # noqa: E501

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params["Accept"] = self.api_client.select_header_accept(
            ["application/json"]
        )  # noqa: E501

        # Authentication setting
        auth_settings = ["Authorization"]  # noqa: E501

        return self.api_client.call_api(
            "/abx/api/resources/actions/{id}/release",
            "DELETE",
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type="Action4",  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get("async_req"),
            _return_http_data_only=params.get("_return_http_data_only"),
            _preload_content=params.get("_preload_content", True),
            _request_timeout=params.get("_request_timeout"),
            collection_formats=collection_formats,
        )

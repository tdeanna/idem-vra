"""
    VMware Cloud Assembly IaaS API

    IAAS API is a RESTful service, which allows users to execute provisioning related UI actions via an API. <br><br>This page describes the RESTful APIs for IAAS API. The APIs facilitate CRUD operations on the various resources and entities used throughout Cloud Assembly(Cloud Accounts, Cloud zones, Computes, Mappings, etc.) and allows operations on them (creating a cloud account, patching a machine, creating image progile, etc.).<br><br>The APIs that list collections of resources  also support OData like implementation. Below query params can be used across different IAAS API endpoints<br><br><ol><li>`$orderby` -  returns a result with the rows being sorted by the values of provided attribute.<br>`/iaas/api/cloud-accounts?$orderby=name%20desc`</li><br><li>`$top` - number of records you want to get.<br>`/iaas/api/cloud-accounts?$top=20`</li><br><li>`$skip` - number of records you want to skip.<br>`/iaas/api/cloud-accounts?$skip=10`</li><br><li>`$select` - select a subset of properties to include in the response.<br>`/iaas/api/cloud-accounts?$select=id`</li><br><li>`$filter` - filter the results by a specified predicate expression. Operators: eq, ne, and, or.<br>`/iaas/api/cloud-accounts?$filter=name%20eq%20'ABC*'` - name starts with 'ABC'<br>`/iaas/api/cloud-accounts?$filter=name%20eq%20'*ABC*'` - name contains 'ABC'<br>`/iaas/api/cloud-accounts?$filter=name%20eq%20'*ABC'` - name ends with 'ABC'<br><b>/iaas/api/projects</b> and <b>/iaas/api/deployments</b> support different format for partial match: <br>`/iaas/api/projects?$filter=startswith(name, 'ABC')` - name starts with 'ABC'<br>`/iaas/api/projects?$filter=substringof('ABC', name)` - name contains 'ABC'<br>`/iaas/api/projects?$filter=endswith(name, 'ABC')` - name ends with 'ABC'<br>`/iaas/api/cloud-accounts?$filter=name%20ne%20'example-cloud-account'%20or%20customProperties.isExternal%20eq%20'false'`</li><br><li>`$count` - flag which when specified, regardless of the assigned value, shows the total number of records. If the collection has a filter it shows the number of records matching the filter.<br>`/iaas/api/cloud-accounts?$count=true`<br></li></ol>  # noqa: E501

    OpenAPI spec version: 2021-07-15

    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""
import pprint
import re  # noqa: F401


class IaasapimachinesSaltConfiguration:
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        "installer_file_name": "str",
        "master_id": "str",
        "variables": "dict(str, str)",
        "salt_environment": "str",
        "pillar_environment": "str",
        "additional_auth_params": "dict(str, str)",
        "state_files": "list[str]",
        "additional_minion_params": "dict(str, str)",
        "minion_id": "str",
    }

    attribute_map = {
        "installer_file_name": "installerFileName",
        "master_id": "masterId",
        "variables": "variables",
        "salt_environment": "saltEnvironment",
        "pillar_environment": "pillarEnvironment",
        "additional_auth_params": "additionalAuthParams",
        "state_files": "stateFiles",
        "additional_minion_params": "additionalMinionParams",
        "minion_id": "minionId",
    }

    def __init__(
        self,
        installer_file_name=None,
        master_id=None,
        variables=None,
        salt_environment=None,
        pillar_environment=None,
        additional_auth_params=None,
        state_files=None,
        additional_minion_params=None,
        minion_id=None,
    ):  # noqa: E501
        """IaasapimachinesSaltConfiguration - a model defined in Swagger"""  # noqa: E501
        self._installer_file_name = None
        self._master_id = None
        self._variables = None
        self._salt_environment = None
        self._pillar_environment = None
        self._additional_auth_params = None
        self._state_files = None
        self._additional_minion_params = None
        self._minion_id = None
        self.discriminator = None
        if installer_file_name is not None:
            self.installer_file_name = installer_file_name
        if master_id is not None:
            self.master_id = master_id
        if variables is not None:
            self.variables = variables
        if salt_environment is not None:
            self.salt_environment = salt_environment
        if pillar_environment is not None:
            self.pillar_environment = pillar_environment
        if additional_auth_params is not None:
            self.additional_auth_params = additional_auth_params
        if state_files is not None:
            self.state_files = state_files
        if additional_minion_params is not None:
            self.additional_minion_params = additional_minion_params
        if minion_id is not None:
            self.minion_id = minion_id

    @property
    def installer_file_name(self):
        """Gets the installer_file_name of this IaasapimachinesSaltConfiguration.  # noqa: E501

        Salt minion installer file name on the master. This property is currently not being used by any SaltStack operation.  # noqa: E501

        :return: The installer_file_name of this IaasapimachinesSaltConfiguration.  # noqa: E501
        :rtype: str
        """
        return self._installer_file_name

    @installer_file_name.setter
    def installer_file_name(self, installer_file_name):
        """Sets the installer_file_name of this IaasapimachinesSaltConfiguration.

        Salt minion installer file name on the master. This property is currently not being used by any SaltStack operation.  # noqa: E501

        :param installer_file_name: The installer_file_name of this IaasapimachinesSaltConfiguration.  # noqa: E501
        :type: str
        """

        self._installer_file_name = installer_file_name

    @property
    def master_id(self):
        """Gets the master_id of this IaasapimachinesSaltConfiguration.  # noqa: E501

        Salt master id to which the Salt minion will be connected to.  # noqa: E501

        :return: The master_id of this IaasapimachinesSaltConfiguration.  # noqa: E501
        :rtype: str
        """
        return self._master_id

    @master_id.setter
    def master_id(self, master_id):
        """Sets the master_id of this IaasapimachinesSaltConfiguration.

        Salt master id to which the Salt minion will be connected to.  # noqa: E501

        :param master_id: The master_id of this IaasapimachinesSaltConfiguration.  # noqa: E501
        :type: str
        """

        self._master_id = master_id

    @property
    def variables(self):
        """Gets the variables of this IaasapimachinesSaltConfiguration.  # noqa: E501

        Parameters required by the state file to run on the deployed minion.  # noqa: E501

        :return: The variables of this IaasapimachinesSaltConfiguration.  # noqa: E501
        :rtype: dict(str, str)
        """
        return self._variables

    @variables.setter
    def variables(self, variables):
        """Sets the variables of this IaasapimachinesSaltConfiguration.

        Parameters required by the state file to run on the deployed minion.  # noqa: E501

        :param variables: The variables of this IaasapimachinesSaltConfiguration.  # noqa: E501
        :type: dict(str, str)
        """

        self._variables = variables

    @property
    def salt_environment(self):
        """Gets the salt_environment of this IaasapimachinesSaltConfiguration.  # noqa: E501

        Salt environment to use when running state files.  # noqa: E501

        :return: The salt_environment of this IaasapimachinesSaltConfiguration.  # noqa: E501
        :rtype: str
        """
        return self._salt_environment

    @salt_environment.setter
    def salt_environment(self, salt_environment):
        """Sets the salt_environment of this IaasapimachinesSaltConfiguration.

        Salt environment to use when running state files.  # noqa: E501

        :param salt_environment: The salt_environment of this IaasapimachinesSaltConfiguration.  # noqa: E501
        :type: str
        """

        self._salt_environment = salt_environment

    @property
    def pillar_environment(self):
        """Gets the pillar_environment of this IaasapimachinesSaltConfiguration.  # noqa: E501

        Pillar environment to use when running state files. Refer: https://docs.saltproject.io/en/latest/ref/modules/all/salt.modules.state.html  # noqa: E501

        :return: The pillar_environment of this IaasapimachinesSaltConfiguration.  # noqa: E501
        :rtype: str
        """
        return self._pillar_environment

    @pillar_environment.setter
    def pillar_environment(self, pillar_environment):
        """Sets the pillar_environment of this IaasapimachinesSaltConfiguration.

        Pillar environment to use when running state files. Refer: https://docs.saltproject.io/en/latest/ref/modules/all/salt.modules.state.html  # noqa: E501

        :param pillar_environment: The pillar_environment of this IaasapimachinesSaltConfiguration.  # noqa: E501
        :type: str
        """

        self._pillar_environment = pillar_environment

    @property
    def additional_auth_params(self):
        """Gets the additional_auth_params of this IaasapimachinesSaltConfiguration.  # noqa: E501

        Additional auth params that can be passed in for provisioning the salt minion. Refer: https://docs.saltproject.io/en/master/topics/cloud/profiles.html  # noqa: E501

        :return: The additional_auth_params of this IaasapimachinesSaltConfiguration.  # noqa: E501
        :rtype: dict(str, str)
        """
        return self._additional_auth_params

    @additional_auth_params.setter
    def additional_auth_params(self, additional_auth_params):
        """Sets the additional_auth_params of this IaasapimachinesSaltConfiguration.

        Additional auth params that can be passed in for provisioning the salt minion. Refer: https://docs.saltproject.io/en/master/topics/cloud/profiles.html  # noqa: E501

        :param additional_auth_params: The additional_auth_params of this IaasapimachinesSaltConfiguration.  # noqa: E501
        :type: dict(str, str)
        """

        self._additional_auth_params = additional_auth_params

    @property
    def state_files(self):
        """Gets the state_files of this IaasapimachinesSaltConfiguration.  # noqa: E501

        List of state files to run on the deployed minion.  # noqa: E501

        :return: The state_files of this IaasapimachinesSaltConfiguration.  # noqa: E501
        :rtype: list[str]
        """
        return self._state_files

    @state_files.setter
    def state_files(self, state_files):
        """Sets the state_files of this IaasapimachinesSaltConfiguration.

        List of state files to run on the deployed minion.  # noqa: E501

        :param state_files: The state_files of this IaasapimachinesSaltConfiguration.  # noqa: E501
        :type: list[str]
        """

        self._state_files = state_files

    @property
    def additional_minion_params(self):
        """Gets the additional_minion_params of this IaasapimachinesSaltConfiguration.  # noqa: E501

        Additional configuration parameters for the salt minion, to be passed in as dictionary. Refer: https://docs.saltproject.io/en/latest/ref/configuration/minion.html  # noqa: E501

        :return: The additional_minion_params of this IaasapimachinesSaltConfiguration.  # noqa: E501
        :rtype: dict(str, str)
        """
        return self._additional_minion_params

    @additional_minion_params.setter
    def additional_minion_params(self, additional_minion_params):
        """Sets the additional_minion_params of this IaasapimachinesSaltConfiguration.

        Additional configuration parameters for the salt minion, to be passed in as dictionary. Refer: https://docs.saltproject.io/en/latest/ref/configuration/minion.html  # noqa: E501

        :param additional_minion_params: The additional_minion_params of this IaasapimachinesSaltConfiguration.  # noqa: E501
        :type: dict(str, str)
        """

        self._additional_minion_params = additional_minion_params

    @property
    def minion_id(self):
        """Gets the minion_id of this IaasapimachinesSaltConfiguration.  # noqa: E501

        Salt minion ID to be assigned to the deployed minion.  # noqa: E501

        :return: The minion_id of this IaasapimachinesSaltConfiguration.  # noqa: E501
        :rtype: str
        """
        return self._minion_id

    @minion_id.setter
    def minion_id(self, minion_id):
        """Sets the minion_id of this IaasapimachinesSaltConfiguration.

        Salt minion ID to be assigned to the deployed minion.  # noqa: E501

        :param minion_id: The minion_id of this IaasapimachinesSaltConfiguration.  # noqa: E501
        :type: str
        """

        self._minion_id = minion_id

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in self.swagger_types.items():
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(
                    map(lambda x: x.to_dict() if hasattr(x, "to_dict") else x, value)
                )
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(
                    map(
                        lambda item: (item[0], item[1].to_dict())
                        if hasattr(item[1], "to_dict")
                        else item,
                        value.items(),
                    )
                )
            else:
                result[attr] = value
        if issubclass(IaasapimachinesSaltConfiguration, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, IaasapimachinesSaltConfiguration):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other

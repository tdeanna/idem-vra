"""
    VMware Cloud Assembly IaaS API

    IAAS API is a RESTful service, which allows users to execute provisioning related UI actions via an API. <br><br>This page describes the RESTful APIs for IAAS API. The APIs facilitate CRUD operations on the various resources and entities used throughout Cloud Assembly(Cloud Accounts, Cloud zones, Computes, Mappings, etc.) and allows operations on them (creating a cloud account, patching a machine, creating image progile, etc.).<br><br>The APIs that list collections of resources  also support OData like implementation. Below query params can be used across different IAAS API endpoints<br><br><ol><li>`$orderby` -  returns a result with the rows being sorted by the values of provided attribute.<br>`/iaas/api/cloud-accounts?$orderby=name%20desc`</li><br><li>`$top` - number of records you want to get.<br>`/iaas/api/cloud-accounts?$top=20`</li><br><li>`$skip` - number of records you want to skip.<br>`/iaas/api/cloud-accounts?$skip=10`</li><br><li>`$select` - select a subset of properties to include in the response.<br>`/iaas/api/cloud-accounts?$select=id`</li><br><li>`$filter` - filter the results by a specified predicate expression. Operators: eq, ne, and, or.<br>`/iaas/api/cloud-accounts?$filter=name%20eq%20'ABC*'` - name starts with 'ABC'<br>`/iaas/api/cloud-accounts?$filter=name%20eq%20'*ABC*'` - name contains 'ABC'<br>`/iaas/api/cloud-accounts?$filter=name%20eq%20'*ABC'` - name ends with 'ABC'<br><b>/iaas/api/projects</b> and <b>/iaas/api/deployments</b> support different format for partial match: <br>`/iaas/api/projects?$filter=startswith(name, 'ABC')` - name starts with 'ABC'<br>`/iaas/api/projects?$filter=substringof('ABC', name)` - name contains 'ABC'<br>`/iaas/api/projects?$filter=endswith(name, 'ABC')` - name ends with 'ABC'<br>`/iaas/api/cloud-accounts?$filter=name%20ne%20'example-cloud-account'%20or%20customProperties.isExternal%20eq%20'false'`</li><br><li>`$count` - flag which when specified, regardless of the assigned value, shows the total number of records. If the collection has a filter it shows the number of records matching the filter.<br>`/iaas/api/cloud-accounts?$count=true`<br></li></ol>  # noqa: E501

    OpenAPI spec version: 2021-07-15

    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""
import pprint
import re  # noqa: F401


class InlineResponse2017:
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        "owner": "str",
        "links": "dict(str, IaasapimachinesidoperationschangesecuritygroupsLinks)",
        "supports_encryption": "bool",
        "external_region_id": "str",
        "cloud_account_id": "str",
        "description": "str",
        "data_disk_caching": "str",
        "org_id": "str",
        "tags": "list[IaasapistorageprofilesgcpTags]",
        "created_at": "str",
        "name": "str",
        "id": "str",
        "default_item": "bool",
        "disk_type": "str",
        "os_disk_caching": "str",
        "disk_encryption_set_id": "str",
        "updated_at": "str",
    }

    attribute_map = {
        "owner": "owner",
        "links": "_links",
        "supports_encryption": "supportsEncryption",
        "external_region_id": "externalRegionId",
        "cloud_account_id": "cloudAccountId",
        "description": "description",
        "data_disk_caching": "dataDiskCaching",
        "org_id": "orgId",
        "tags": "tags",
        "created_at": "createdAt",
        "name": "name",
        "id": "id",
        "default_item": "defaultItem",
        "disk_type": "diskType",
        "os_disk_caching": "osDiskCaching",
        "disk_encryption_set_id": "diskEncryptionSetId",
        "updated_at": "updatedAt",
    }

    def __init__(
        self,
        owner=None,
        links=None,
        supports_encryption=None,
        external_region_id=None,
        cloud_account_id=None,
        description=None,
        data_disk_caching=None,
        org_id=None,
        tags=None,
        created_at=None,
        name=None,
        id=None,
        default_item=None,
        disk_type=None,
        os_disk_caching=None,
        disk_encryption_set_id=None,
        updated_at=None,
    ):  # noqa: E501
        """InlineResponse2017 - a model defined in Swagger"""  # noqa: E501
        self._owner = None
        self._links = None
        self._supports_encryption = None
        self._external_region_id = None
        self._cloud_account_id = None
        self._description = None
        self._data_disk_caching = None
        self._org_id = None
        self._tags = None
        self._created_at = None
        self._name = None
        self._id = None
        self._default_item = None
        self._disk_type = None
        self._os_disk_caching = None
        self._disk_encryption_set_id = None
        self._updated_at = None
        self.discriminator = None
        if owner is not None:
            self.owner = owner
        self.links = links
        if supports_encryption is not None:
            self.supports_encryption = supports_encryption
        if external_region_id is not None:
            self.external_region_id = external_region_id
        if cloud_account_id is not None:
            self.cloud_account_id = cloud_account_id
        if description is not None:
            self.description = description
        if data_disk_caching is not None:
            self.data_disk_caching = data_disk_caching
        if org_id is not None:
            self.org_id = org_id
        if tags is not None:
            self.tags = tags
        if created_at is not None:
            self.created_at = created_at
        if name is not None:
            self.name = name
        self.id = id
        self.default_item = default_item
        if disk_type is not None:
            self.disk_type = disk_type
        if os_disk_caching is not None:
            self.os_disk_caching = os_disk_caching
        if disk_encryption_set_id is not None:
            self.disk_encryption_set_id = disk_encryption_set_id
        if updated_at is not None:
            self.updated_at = updated_at

    @property
    def owner(self):
        """Gets the owner of this InlineResponse2017.  # noqa: E501

        Email of the user that owns the entity.  # noqa: E501

        :return: The owner of this InlineResponse2017.  # noqa: E501
        :rtype: str
        """
        return self._owner

    @owner.setter
    def owner(self, owner):
        """Sets the owner of this InlineResponse2017.

        Email of the user that owns the entity.  # noqa: E501

        :param owner: The owner of this InlineResponse2017.  # noqa: E501
        :type: str
        """

        self._owner = owner

    @property
    def links(self):
        """Gets the links of this InlineResponse2017.  # noqa: E501

        HATEOAS of the entity  # noqa: E501

        :return: The links of this InlineResponse2017.  # noqa: E501
        :rtype: dict(str, IaasapimachinesidoperationschangesecuritygroupsLinks)
        """
        return self._links

    @links.setter
    def links(self, links):
        """Sets the links of this InlineResponse2017.

        HATEOAS of the entity  # noqa: E501

        :param links: The links of this InlineResponse2017.  # noqa: E501
        :type: dict(str, IaasapimachinesidoperationschangesecuritygroupsLinks)
        """
        if links is None:
            raise ValueError(
                "Invalid value for `links`, must not be `None`"
            )  # noqa: E501

        self._links = links

    @property
    def supports_encryption(self):
        """Gets the supports_encryption of this InlineResponse2017.  # noqa: E501

        Indicates whether this storage profile should support encryption or not.  # noqa: E501

        :return: The supports_encryption of this InlineResponse2017.  # noqa: E501
        :rtype: bool
        """
        return self._supports_encryption

    @supports_encryption.setter
    def supports_encryption(self, supports_encryption):
        """Sets the supports_encryption of this InlineResponse2017.

        Indicates whether this storage profile should support encryption or not.  # noqa: E501

        :param supports_encryption: The supports_encryption of this InlineResponse2017.  # noqa: E501
        :type: bool
        """

        self._supports_encryption = supports_encryption

    @property
    def external_region_id(self):
        """Gets the external_region_id of this InlineResponse2017.  # noqa: E501

        The id of the region for which this profile is defined  # noqa: E501

        :return: The external_region_id of this InlineResponse2017.  # noqa: E501
        :rtype: str
        """
        return self._external_region_id

    @external_region_id.setter
    def external_region_id(self, external_region_id):
        """Sets the external_region_id of this InlineResponse2017.

        The id of the region for which this profile is defined  # noqa: E501

        :param external_region_id: The external_region_id of this InlineResponse2017.  # noqa: E501
        :type: str
        """

        self._external_region_id = external_region_id

    @property
    def cloud_account_id(self):
        """Gets the cloud_account_id of this InlineResponse2017.  # noqa: E501

        Id of the cloud account this storage profile belongs to.  # noqa: E501

        :return: The cloud_account_id of this InlineResponse2017.  # noqa: E501
        :rtype: str
        """
        return self._cloud_account_id

    @cloud_account_id.setter
    def cloud_account_id(self, cloud_account_id):
        """Sets the cloud_account_id of this InlineResponse2017.

        Id of the cloud account this storage profile belongs to.  # noqa: E501

        :param cloud_account_id: The cloud_account_id of this InlineResponse2017.  # noqa: E501
        :type: str
        """

        self._cloud_account_id = cloud_account_id

    @property
    def description(self):
        """Gets the description of this InlineResponse2017.  # noqa: E501

        A human-friendly description.  # noqa: E501

        :return: The description of this InlineResponse2017.  # noqa: E501
        :rtype: str
        """
        return self._description

    @description.setter
    def description(self, description):
        """Sets the description of this InlineResponse2017.

        A human-friendly description.  # noqa: E501

        :param description: The description of this InlineResponse2017.  # noqa: E501
        :type: str
        """

        self._description = description

    @property
    def data_disk_caching(self):
        """Gets the data_disk_caching of this InlineResponse2017.  # noqa: E501

        Indicates the caching mechanism for additional disk.   # noqa: E501

        :return: The data_disk_caching of this InlineResponse2017.  # noqa: E501
        :rtype: str
        """
        return self._data_disk_caching

    @data_disk_caching.setter
    def data_disk_caching(self, data_disk_caching):
        """Sets the data_disk_caching of this InlineResponse2017.

        Indicates the caching mechanism for additional disk.   # noqa: E501

        :param data_disk_caching: The data_disk_caching of this InlineResponse2017.  # noqa: E501
        :type: str
        """

        self._data_disk_caching = data_disk_caching

    @property
    def org_id(self):
        """Gets the org_id of this InlineResponse2017.  # noqa: E501

        The id of the organization this entity belongs to.  # noqa: E501

        :return: The org_id of this InlineResponse2017.  # noqa: E501
        :rtype: str
        """
        return self._org_id

    @org_id.setter
    def org_id(self, org_id):
        """Sets the org_id of this InlineResponse2017.

        The id of the organization this entity belongs to.  # noqa: E501

        :param org_id: The org_id of this InlineResponse2017.  # noqa: E501
        :type: str
        """

        self._org_id = org_id

    @property
    def tags(self):
        """Gets the tags of this InlineResponse2017.  # noqa: E501

        A list of tags that represent the capabilities of this storage profile  # noqa: E501

        :return: The tags of this InlineResponse2017.  # noqa: E501
        :rtype: list[IaasapistorageprofilesgcpTags]
        """
        return self._tags

    @tags.setter
    def tags(self, tags):
        """Sets the tags of this InlineResponse2017.

        A list of tags that represent the capabilities of this storage profile  # noqa: E501

        :param tags: The tags of this InlineResponse2017.  # noqa: E501
        :type: list[IaasapistorageprofilesgcpTags]
        """

        self._tags = tags

    @property
    def created_at(self):
        """Gets the created_at of this InlineResponse2017.  # noqa: E501

        Date when the entity was created. The date is in ISO 8601 and UTC.  # noqa: E501

        :return: The created_at of this InlineResponse2017.  # noqa: E501
        :rtype: str
        """
        return self._created_at

    @created_at.setter
    def created_at(self, created_at):
        """Sets the created_at of this InlineResponse2017.

        Date when the entity was created. The date is in ISO 8601 and UTC.  # noqa: E501

        :param created_at: The created_at of this InlineResponse2017.  # noqa: E501
        :type: str
        """

        self._created_at = created_at

    @property
    def name(self):
        """Gets the name of this InlineResponse2017.  # noqa: E501

        A human-friendly name used as an identifier in APIs that support this option.  # noqa: E501

        :return: The name of this InlineResponse2017.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this InlineResponse2017.

        A human-friendly name used as an identifier in APIs that support this option.  # noqa: E501

        :param name: The name of this InlineResponse2017.  # noqa: E501
        :type: str
        """

        self._name = name

    @property
    def id(self):
        """Gets the id of this InlineResponse2017.  # noqa: E501

        The id of this resource instance  # noqa: E501

        :return: The id of this InlineResponse2017.  # noqa: E501
        :rtype: str
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this InlineResponse2017.

        The id of this resource instance  # noqa: E501

        :param id: The id of this InlineResponse2017.  # noqa: E501
        :type: str
        """
        if id is None:
            raise ValueError("Invalid value for `id`, must not be `None`")  # noqa: E501

        self._id = id

    @property
    def default_item(self):
        """Gets the default_item of this InlineResponse2017.  # noqa: E501

        Indicates if a storage profile contains default storage properties.  # noqa: E501

        :return: The default_item of this InlineResponse2017.  # noqa: E501
        :rtype: bool
        """
        return self._default_item

    @default_item.setter
    def default_item(self, default_item):
        """Sets the default_item of this InlineResponse2017.

        Indicates if a storage profile contains default storage properties.  # noqa: E501

        :param default_item: The default_item of this InlineResponse2017.  # noqa: E501
        :type: bool
        """
        if default_item is None:
            raise ValueError(
                "Invalid value for `default_item`, must not be `None`"
            )  # noqa: E501

        self._default_item = default_item

    @property
    def disk_type(self):
        """Gets the disk_type of this InlineResponse2017.  # noqa: E501

        Indicates the performance tier for the storage type. Premium disks are SSD backed and Standard disks are HDD backed.  # noqa: E501

        :return: The disk_type of this InlineResponse2017.  # noqa: E501
        :rtype: str
        """
        return self._disk_type

    @disk_type.setter
    def disk_type(self, disk_type):
        """Sets the disk_type of this InlineResponse2017.

        Indicates the performance tier for the storage type. Premium disks are SSD backed and Standard disks are HDD backed.  # noqa: E501

        :param disk_type: The disk_type of this InlineResponse2017.  # noqa: E501
        :type: str
        """

        self._disk_type = disk_type

    @property
    def os_disk_caching(self):
        """Gets the os_disk_caching of this InlineResponse2017.  # noqa: E501

        Indicates the caching mechanism for OS disk. Default policy for OS disks is Read/Write.  # noqa: E501

        :return: The os_disk_caching of this InlineResponse2017.  # noqa: E501
        :rtype: str
        """
        return self._os_disk_caching

    @os_disk_caching.setter
    def os_disk_caching(self, os_disk_caching):
        """Sets the os_disk_caching of this InlineResponse2017.

        Indicates the caching mechanism for OS disk. Default policy for OS disks is Read/Write.  # noqa: E501

        :param os_disk_caching: The os_disk_caching of this InlineResponse2017.  # noqa: E501
        :type: str
        """

        self._os_disk_caching = os_disk_caching

    @property
    def disk_encryption_set_id(self):
        """Gets the disk_encryption_set_id of this InlineResponse2017.  # noqa: E501

        Indicates the id of disk encryption set.   # noqa: E501

        :return: The disk_encryption_set_id of this InlineResponse2017.  # noqa: E501
        :rtype: str
        """
        return self._disk_encryption_set_id

    @disk_encryption_set_id.setter
    def disk_encryption_set_id(self, disk_encryption_set_id):
        """Sets the disk_encryption_set_id of this InlineResponse2017.

        Indicates the id of disk encryption set.   # noqa: E501

        :param disk_encryption_set_id: The disk_encryption_set_id of this InlineResponse2017.  # noqa: E501
        :type: str
        """

        self._disk_encryption_set_id = disk_encryption_set_id

    @property
    def updated_at(self):
        """Gets the updated_at of this InlineResponse2017.  # noqa: E501

        Date when the entity was last updated. The date is ISO 8601 and UTC.  # noqa: E501

        :return: The updated_at of this InlineResponse2017.  # noqa: E501
        :rtype: str
        """
        return self._updated_at

    @updated_at.setter
    def updated_at(self, updated_at):
        """Sets the updated_at of this InlineResponse2017.

        Date when the entity was last updated. The date is ISO 8601 and UTC.  # noqa: E501

        :param updated_at: The updated_at of this InlineResponse2017.  # noqa: E501
        :type: str
        """

        self._updated_at = updated_at

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in self.swagger_types.items():
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(
                    map(lambda x: x.to_dict() if hasattr(x, "to_dict") else x, value)
                )
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(
                    map(
                        lambda item: (item[0], item[1].to_dict())
                        if hasattr(item[1], "to_dict")
                        else item,
                        value.items(),
                    )
                )
            else:
                result[attr] = value
        if issubclass(InlineResponse2017, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, InlineResponse2017):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other

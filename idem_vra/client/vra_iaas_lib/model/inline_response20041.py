"""
    VMware Cloud Assembly IaaS API

    IAAS API is a RESTful service, which allows users to execute provisioning related UI actions via an API. <br><br>This page describes the RESTful APIs for IAAS API. The APIs facilitate CRUD operations on the various resources and entities used throughout Cloud Assembly(Cloud Accounts, Cloud zones, Computes, Mappings, etc.) and allows operations on them (creating a cloud account, patching a machine, creating image progile, etc.).<br><br>The APIs that list collections of resources  also support OData like implementation. Below query params can be used across different IAAS API endpoints<br><br><ol><li>`$orderby` -  returns a result with the rows being sorted by the values of provided attribute.<br>`/iaas/api/cloud-accounts?$orderby=name%20desc`</li><br><li>`$top` - number of records you want to get.<br>`/iaas/api/cloud-accounts?$top=20`</li><br><li>`$skip` - number of records you want to skip.<br>`/iaas/api/cloud-accounts?$skip=10`</li><br><li>`$select` - select a subset of properties to include in the response.<br>`/iaas/api/cloud-accounts?$select=id`</li><br><li>`$filter` - filter the results by a specified predicate expression. Operators: eq, ne, and, or.<br>`/iaas/api/cloud-accounts?$filter=name%20eq%20'ABC*'` - name starts with 'ABC'<br>`/iaas/api/cloud-accounts?$filter=name%20eq%20'*ABC*'` - name contains 'ABC'<br>`/iaas/api/cloud-accounts?$filter=name%20eq%20'*ABC'` - name ends with 'ABC'<br><b>/iaas/api/projects</b> and <b>/iaas/api/deployments</b> support different format for partial match: <br>`/iaas/api/projects?$filter=startswith(name, 'ABC')` - name starts with 'ABC'<br>`/iaas/api/projects?$filter=substringof('ABC', name)` - name contains 'ABC'<br>`/iaas/api/projects?$filter=endswith(name, 'ABC')` - name ends with 'ABC'<br>`/iaas/api/cloud-accounts?$filter=name%20ne%20'example-cloud-account'%20or%20customProperties.isExternal%20eq%20'false'`</li><br><li>`$count` - flag which when specified, regardless of the assigned value, shows the total number of records. If the collection has a filter it shows the number of records matching the filter.<br>`/iaas/api/cloud-accounts?$count=true`<br></li></ol>  # noqa: E501

    OpenAPI spec version: 2021-07-15

    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""
import pprint
import re  # noqa: F401


class InlineResponse20041:
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        "owner": "str",
        "links": "dict(str, IaasapimachinesidoperationschangesecuritygroupsLinks)",
        "nsx_host_name": "str",
        "vcenter_username": "str",
        "description": "str",
        "vcf_domain_name": "str",
        "sddc_manager_id": "str",
        "org_id": "str",
        "tags": "list[IaasapistorageprofilesgcpTags]",
        "created_at": "str",
        "enabled_regions": "list[CloudAccountGcpResultEnabledRegions]",
        "vcenter_host_name": "str",
        "create_default_zones": "bool",
        "nsx_username": "str",
        "custom_properties": "dict(str, str)",
        "name": "str",
        "vcf_domain_id": "str",
        "id": "str",
        "updated_at": "str",
    }

    attribute_map = {
        "owner": "owner",
        "links": "_links",
        "nsx_host_name": "nsxHostName",
        "vcenter_username": "vcenterUsername",
        "description": "description",
        "vcf_domain_name": "vcfDomainName",
        "sddc_manager_id": "sddcManagerId",
        "org_id": "orgId",
        "tags": "tags",
        "created_at": "createdAt",
        "enabled_regions": "enabledRegions",
        "vcenter_host_name": "vcenterHostName",
        "create_default_zones": "createDefaultZones",
        "nsx_username": "nsxUsername",
        "custom_properties": "customProperties",
        "name": "name",
        "vcf_domain_id": "vcfDomainId",
        "id": "id",
        "updated_at": "updatedAt",
    }

    def __init__(
        self,
        owner=None,
        links=None,
        nsx_host_name=None,
        vcenter_username=None,
        description=None,
        vcf_domain_name=None,
        sddc_manager_id=None,
        org_id=None,
        tags=None,
        created_at=None,
        enabled_regions=None,
        vcenter_host_name=None,
        create_default_zones=None,
        nsx_username=None,
        custom_properties=None,
        name=None,
        vcf_domain_id=None,
        id=None,
        updated_at=None,
    ):  # noqa: E501
        """InlineResponse20041 - a model defined in Swagger"""  # noqa: E501
        self._owner = None
        self._links = None
        self._nsx_host_name = None
        self._vcenter_username = None
        self._description = None
        self._vcf_domain_name = None
        self._sddc_manager_id = None
        self._org_id = None
        self._tags = None
        self._created_at = None
        self._enabled_regions = None
        self._vcenter_host_name = None
        self._create_default_zones = None
        self._nsx_username = None
        self._custom_properties = None
        self._name = None
        self._vcf_domain_id = None
        self._id = None
        self._updated_at = None
        self.discriminator = None
        if owner is not None:
            self.owner = owner
        self.links = links
        self.nsx_host_name = nsx_host_name
        self.vcenter_username = vcenter_username
        if description is not None:
            self.description = description
        self.vcf_domain_name = vcf_domain_name
        self.sddc_manager_id = sddc_manager_id
        if org_id is not None:
            self.org_id = org_id
        if tags is not None:
            self.tags = tags
        if created_at is not None:
            self.created_at = created_at
        if enabled_regions is not None:
            self.enabled_regions = enabled_regions
        self.vcenter_host_name = vcenter_host_name
        if create_default_zones is not None:
            self.create_default_zones = create_default_zones
        self.nsx_username = nsx_username
        if custom_properties is not None:
            self.custom_properties = custom_properties
        if name is not None:
            self.name = name
        self.vcf_domain_id = vcf_domain_id
        self.id = id
        if updated_at is not None:
            self.updated_at = updated_at

    @property
    def owner(self):
        """Gets the owner of this InlineResponse20041.  # noqa: E501

        Email of the user that owns the entity.  # noqa: E501

        :return: The owner of this InlineResponse20041.  # noqa: E501
        :rtype: str
        """
        return self._owner

    @owner.setter
    def owner(self, owner):
        """Sets the owner of this InlineResponse20041.

        Email of the user that owns the entity.  # noqa: E501

        :param owner: The owner of this InlineResponse20041.  # noqa: E501
        :type: str
        """

        self._owner = owner

    @property
    def links(self):
        """Gets the links of this InlineResponse20041.  # noqa: E501

        HATEOAS of the entity  # noqa: E501

        :return: The links of this InlineResponse20041.  # noqa: E501
        :rtype: dict(str, IaasapimachinesidoperationschangesecuritygroupsLinks)
        """
        return self._links

    @links.setter
    def links(self, links):
        """Sets the links of this InlineResponse20041.

        HATEOAS of the entity  # noqa: E501

        :param links: The links of this InlineResponse20041.  # noqa: E501
        :type: dict(str, IaasapimachinesidoperationschangesecuritygroupsLinks)
        """
        if links is None:
            raise ValueError(
                "Invalid value for `links`, must not be `None`"
            )  # noqa: E501

        self._links = links

    @property
    def nsx_host_name(self):
        """Gets the nsx_host_name of this InlineResponse20041.  # noqa: E501

        NSX Hostname in a workload domain  # noqa: E501

        :return: The nsx_host_name of this InlineResponse20041.  # noqa: E501
        :rtype: str
        """
        return self._nsx_host_name

    @nsx_host_name.setter
    def nsx_host_name(self, nsx_host_name):
        """Sets the nsx_host_name of this InlineResponse20041.

        NSX Hostname in a workload domain  # noqa: E501

        :param nsx_host_name: The nsx_host_name of this InlineResponse20041.  # noqa: E501
        :type: str
        """
        if nsx_host_name is None:
            raise ValueError(
                "Invalid value for `nsx_host_name`, must not be `None`"
            )  # noqa: E501

        self._nsx_host_name = nsx_host_name

    @property
    def vcenter_username(self):
        """Gets the vcenter_username of this InlineResponse20041.  # noqa: E501

        Username to authenticate to vCenter Server in workload domain  # noqa: E501

        :return: The vcenter_username of this InlineResponse20041.  # noqa: E501
        :rtype: str
        """
        return self._vcenter_username

    @vcenter_username.setter
    def vcenter_username(self, vcenter_username):
        """Sets the vcenter_username of this InlineResponse20041.

        Username to authenticate to vCenter Server in workload domain  # noqa: E501

        :param vcenter_username: The vcenter_username of this InlineResponse20041.  # noqa: E501
        :type: str
        """
        if vcenter_username is None:
            raise ValueError(
                "Invalid value for `vcenter_username`, must not be `None`"
            )  # noqa: E501

        self._vcenter_username = vcenter_username

    @property
    def description(self):
        """Gets the description of this InlineResponse20041.  # noqa: E501

        A human-friendly description.  # noqa: E501

        :return: The description of this InlineResponse20041.  # noqa: E501
        :rtype: str
        """
        return self._description

    @description.setter
    def description(self, description):
        """Sets the description of this InlineResponse20041.

        A human-friendly description.  # noqa: E501

        :param description: The description of this InlineResponse20041.  # noqa: E501
        :type: str
        """

        self._description = description

    @property
    def vcf_domain_name(self):
        """Gets the vcf_domain_name of this InlineResponse20041.  # noqa: E501

        Name of the VCF worload domain.  # noqa: E501

        :return: The vcf_domain_name of this InlineResponse20041.  # noqa: E501
        :rtype: str
        """
        return self._vcf_domain_name

    @vcf_domain_name.setter
    def vcf_domain_name(self, vcf_domain_name):
        """Sets the vcf_domain_name of this InlineResponse20041.

        Name of the VCF worload domain.  # noqa: E501

        :param vcf_domain_name: The vcf_domain_name of this InlineResponse20041.  # noqa: E501
        :type: str
        """
        if vcf_domain_name is None:
            raise ValueError(
                "Invalid value for `vcf_domain_name`, must not be `None`"
            )  # noqa: E501

        self._vcf_domain_name = vcf_domain_name

    @property
    def sddc_manager_id(self):
        """Gets the sddc_manager_id of this InlineResponse20041.  # noqa: E501

        SDDC Manager ID  # noqa: E501

        :return: The sddc_manager_id of this InlineResponse20041.  # noqa: E501
        :rtype: str
        """
        return self._sddc_manager_id

    @sddc_manager_id.setter
    def sddc_manager_id(self, sddc_manager_id):
        """Sets the sddc_manager_id of this InlineResponse20041.

        SDDC Manager ID  # noqa: E501

        :param sddc_manager_id: The sddc_manager_id of this InlineResponse20041.  # noqa: E501
        :type: str
        """
        if sddc_manager_id is None:
            raise ValueError(
                "Invalid value for `sddc_manager_id`, must not be `None`"
            )  # noqa: E501

        self._sddc_manager_id = sddc_manager_id

    @property
    def org_id(self):
        """Gets the org_id of this InlineResponse20041.  # noqa: E501

        The id of the organization this entity belongs to.  # noqa: E501

        :return: The org_id of this InlineResponse20041.  # noqa: E501
        :rtype: str
        """
        return self._org_id

    @org_id.setter
    def org_id(self, org_id):
        """Sets the org_id of this InlineResponse20041.

        The id of the organization this entity belongs to.  # noqa: E501

        :param org_id: The org_id of this InlineResponse20041.  # noqa: E501
        :type: str
        """

        self._org_id = org_id

    @property
    def tags(self):
        """Gets the tags of this InlineResponse20041.  # noqa: E501

        A set of tag keys and optional values to set on the Cloud Account.Cloud account capability tags may enable different features.   # noqa: E501

        :return: The tags of this InlineResponse20041.  # noqa: E501
        :rtype: list[IaasapistorageprofilesgcpTags]
        """
        return self._tags

    @tags.setter
    def tags(self, tags):
        """Sets the tags of this InlineResponse20041.

        A set of tag keys and optional values to set on the Cloud Account.Cloud account capability tags may enable different features.   # noqa: E501

        :param tags: The tags of this InlineResponse20041.  # noqa: E501
        :type: list[IaasapistorageprofilesgcpTags]
        """

        self._tags = tags

    @property
    def created_at(self):
        """Gets the created_at of this InlineResponse20041.  # noqa: E501

        Date when the entity was created. The date is in ISO 8601 and UTC.  # noqa: E501

        :return: The created_at of this InlineResponse20041.  # noqa: E501
        :rtype: str
        """
        return self._created_at

    @created_at.setter
    def created_at(self, created_at):
        """Sets the created_at of this InlineResponse20041.

        Date when the entity was created. The date is in ISO 8601 and UTC.  # noqa: E501

        :param created_at: The created_at of this InlineResponse20041.  # noqa: E501
        :type: str
        """

        self._created_at = created_at

    @property
    def enabled_regions(self):
        """Gets the enabled_regions of this InlineResponse20041.  # noqa: E501

        A list of regions that are enabled for provisioning on this cloud account  # noqa: E501

        :return: The enabled_regions of this InlineResponse20041.  # noqa: E501
        :rtype: list[CloudAccountGcpResultEnabledRegions]
        """
        return self._enabled_regions

    @enabled_regions.setter
    def enabled_regions(self, enabled_regions):
        """Sets the enabled_regions of this InlineResponse20041.

        A list of regions that are enabled for provisioning on this cloud account  # noqa: E501

        :param enabled_regions: The enabled_regions of this InlineResponse20041.  # noqa: E501
        :type: list[CloudAccountGcpResultEnabledRegions]
        """

        self._enabled_regions = enabled_regions

    @property
    def vcenter_host_name(self):
        """Gets the vcenter_host_name of this InlineResponse20041.  # noqa: E501

        vCenter Hostname in a workload domain  # noqa: E501

        :return: The vcenter_host_name of this InlineResponse20041.  # noqa: E501
        :rtype: str
        """
        return self._vcenter_host_name

    @vcenter_host_name.setter
    def vcenter_host_name(self, vcenter_host_name):
        """Sets the vcenter_host_name of this InlineResponse20041.

        vCenter Hostname in a workload domain  # noqa: E501

        :param vcenter_host_name: The vcenter_host_name of this InlineResponse20041.  # noqa: E501
        :type: str
        """
        if vcenter_host_name is None:
            raise ValueError(
                "Invalid value for `vcenter_host_name`, must not be `None`"
            )  # noqa: E501

        self._vcenter_host_name = vcenter_host_name

    @property
    def create_default_zones(self):
        """Gets the create_default_zones of this InlineResponse20041.  # noqa: E501

        Create default cloud zones for the enabled regions.  # noqa: E501

        :return: The create_default_zones of this InlineResponse20041.  # noqa: E501
        :rtype: bool
        """
        return self._create_default_zones

    @create_default_zones.setter
    def create_default_zones(self, create_default_zones):
        """Sets the create_default_zones of this InlineResponse20041.

        Create default cloud zones for the enabled regions.  # noqa: E501

        :param create_default_zones: The create_default_zones of this InlineResponse20041.  # noqa: E501
        :type: bool
        """

        self._create_default_zones = create_default_zones

    @property
    def nsx_username(self):
        """Gets the nsx_username of this InlineResponse20041.  # noqa: E501

        Username to authenticate to NSX manager in workload domain  # noqa: E501

        :return: The nsx_username of this InlineResponse20041.  # noqa: E501
        :rtype: str
        """
        return self._nsx_username

    @nsx_username.setter
    def nsx_username(self, nsx_username):
        """Sets the nsx_username of this InlineResponse20041.

        Username to authenticate to NSX manager in workload domain  # noqa: E501

        :param nsx_username: The nsx_username of this InlineResponse20041.  # noqa: E501
        :type: str
        """
        if nsx_username is None:
            raise ValueError(
                "Invalid value for `nsx_username`, must not be `None`"
            )  # noqa: E501

        self._nsx_username = nsx_username

    @property
    def custom_properties(self):
        """Gets the custom_properties of this InlineResponse20041.  # noqa: E501

        Additional properties that may be used to extend the base type.  # noqa: E501

        :return: The custom_properties of this InlineResponse20041.  # noqa: E501
        :rtype: dict(str, str)
        """
        return self._custom_properties

    @custom_properties.setter
    def custom_properties(self, custom_properties):
        """Sets the custom_properties of this InlineResponse20041.

        Additional properties that may be used to extend the base type.  # noqa: E501

        :param custom_properties: The custom_properties of this InlineResponse20041.  # noqa: E501
        :type: dict(str, str)
        """

        self._custom_properties = custom_properties

    @property
    def name(self):
        """Gets the name of this InlineResponse20041.  # noqa: E501

        A human-friendly name used as an identifier in APIs that support this option.  # noqa: E501

        :return: The name of this InlineResponse20041.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this InlineResponse20041.

        A human-friendly name used as an identifier in APIs that support this option.  # noqa: E501

        :param name: The name of this InlineResponse20041.  # noqa: E501
        :type: str
        """

        self._name = name

    @property
    def vcf_domain_id(self):
        """Gets the vcf_domain_id of this InlineResponse20041.  # noqa: E501

        Id of the VCF worload domain.  # noqa: E501

        :return: The vcf_domain_id of this InlineResponse20041.  # noqa: E501
        :rtype: str
        """
        return self._vcf_domain_id

    @vcf_domain_id.setter
    def vcf_domain_id(self, vcf_domain_id):
        """Sets the vcf_domain_id of this InlineResponse20041.

        Id of the VCF worload domain.  # noqa: E501

        :param vcf_domain_id: The vcf_domain_id of this InlineResponse20041.  # noqa: E501
        :type: str
        """
        if vcf_domain_id is None:
            raise ValueError(
                "Invalid value for `vcf_domain_id`, must not be `None`"
            )  # noqa: E501

        self._vcf_domain_id = vcf_domain_id

    @property
    def id(self):
        """Gets the id of this InlineResponse20041.  # noqa: E501

        The id of this resource instance  # noqa: E501

        :return: The id of this InlineResponse20041.  # noqa: E501
        :rtype: str
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this InlineResponse20041.

        The id of this resource instance  # noqa: E501

        :param id: The id of this InlineResponse20041.  # noqa: E501
        :type: str
        """
        if id is None:
            raise ValueError("Invalid value for `id`, must not be `None`")  # noqa: E501

        self._id = id

    @property
    def updated_at(self):
        """Gets the updated_at of this InlineResponse20041.  # noqa: E501

        Date when the entity was last updated. The date is ISO 8601 and UTC.  # noqa: E501

        :return: The updated_at of this InlineResponse20041.  # noqa: E501
        :rtype: str
        """
        return self._updated_at

    @updated_at.setter
    def updated_at(self, updated_at):
        """Sets the updated_at of this InlineResponse20041.

        Date when the entity was last updated. The date is ISO 8601 and UTC.  # noqa: E501

        :param updated_at: The updated_at of this InlineResponse20041.  # noqa: E501
        :type: str
        """

        self._updated_at = updated_at

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in self.swagger_types.items():
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(
                    map(lambda x: x.to_dict() if hasattr(x, "to_dict") else x, value)
                )
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(
                    map(
                        lambda item: (item[0], item[1].to_dict())
                        if hasattr(item[1], "to_dict")
                        else item,
                        value.items(),
                    )
                )
            else:
                result[attr] = value
        if issubclass(InlineResponse20041, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, InlineResponse20041):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other

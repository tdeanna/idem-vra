"""
    VMware Cloud Assembly IaaS API

    IAAS API is a RESTful service, which allows users to execute provisioning related UI actions via an API. <br><br>This page describes the RESTful APIs for IAAS API. The APIs facilitate CRUD operations on the various resources and entities used throughout Cloud Assembly(Cloud Accounts, Cloud zones, Computes, Mappings, etc.) and allows operations on them (creating a cloud account, patching a machine, creating image progile, etc.).<br><br>The APIs that list collections of resources  also support OData like implementation. Below query params can be used across different IAAS API endpoints<br><br><ol><li>`$orderby` -  returns a result with the rows being sorted by the values of provided attribute.<br>`/iaas/api/cloud-accounts?$orderby=name%20desc`</li><br><li>`$top` - number of records you want to get.<br>`/iaas/api/cloud-accounts?$top=20`</li><br><li>`$skip` - number of records you want to skip.<br>`/iaas/api/cloud-accounts?$skip=10`</li><br><li>`$select` - select a subset of properties to include in the response.<br>`/iaas/api/cloud-accounts?$select=id`</li><br><li>`$filter` - filter the results by a specified predicate expression. Operators: eq, ne, and, or.<br>`/iaas/api/cloud-accounts?$filter=name%20eq%20'ABC*'` - name starts with 'ABC'<br>`/iaas/api/cloud-accounts?$filter=name%20eq%20'*ABC*'` - name contains 'ABC'<br>`/iaas/api/cloud-accounts?$filter=name%20eq%20'*ABC'` - name ends with 'ABC'<br><b>/iaas/api/projects</b> and <b>/iaas/api/deployments</b> support different format for partial match: <br>`/iaas/api/projects?$filter=startswith(name, 'ABC')` - name starts with 'ABC'<br>`/iaas/api/projects?$filter=substringof('ABC', name)` - name contains 'ABC'<br>`/iaas/api/projects?$filter=endswith(name, 'ABC')` - name ends with 'ABC'<br>`/iaas/api/cloud-accounts?$filter=name%20ne%20'example-cloud-account'%20or%20customProperties.isExternal%20eq%20'false'`</li><br><li>`$count` - flag which when specified, regardless of the assigned value, shows the total number of records. If the collection has a filter it shows the number of records matching the filter.<br>`/iaas/api/cloud-accounts?$count=true`<br></li></ol>  # noqa: E501

    OpenAPI spec version: 2021-07-15

    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""
import pprint
import re  # noqa: F401


class FabricNetworkVsphere:
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        "owner": "str",
        "links": "dict(str, IaasapimachinesidoperationschangesecuritygroupsLinks)",
        "external_region_id": "str",
        "default_ipv6_gateway": "str",
        "description": "str",
        "external_id": "str",
        "default_gateway": "str",
        "org_id": "str",
        "tags": "list[IaasapistorageprofilesgcpTags]",
        "created_at": "str",
        "ipv6_cidr": "str",
        "cloud_account_ids": "list[str]",
        "is_default": "bool",
        "custom_properties": "dict(str, str)",
        "domain": "str",
        "dns_server_addresses": "list[str]",
        "name": "str",
        "is_public": "bool",
        "cidr": "str",
        "id": "str",
        "updated_at": "str",
        "dns_search_domains": "list[str]",
    }

    attribute_map = {
        "owner": "owner",
        "links": "_links",
        "external_region_id": "externalRegionId",
        "default_ipv6_gateway": "defaultIpv6Gateway",
        "description": "description",
        "external_id": "externalId",
        "default_gateway": "defaultGateway",
        "org_id": "orgId",
        "tags": "tags",
        "created_at": "createdAt",
        "ipv6_cidr": "ipv6Cidr",
        "cloud_account_ids": "cloudAccountIds",
        "is_default": "isDefault",
        "custom_properties": "customProperties",
        "domain": "domain",
        "dns_server_addresses": "dnsServerAddresses",
        "name": "name",
        "is_public": "isPublic",
        "cidr": "cidr",
        "id": "id",
        "updated_at": "updatedAt",
        "dns_search_domains": "dnsSearchDomains",
    }

    def __init__(
        self,
        owner=None,
        links=None,
        external_region_id=None,
        default_ipv6_gateway=None,
        description=None,
        external_id=None,
        default_gateway=None,
        org_id=None,
        tags=None,
        created_at=None,
        ipv6_cidr=None,
        cloud_account_ids=None,
        is_default=None,
        custom_properties=None,
        domain=None,
        dns_server_addresses=None,
        name=None,
        is_public=None,
        cidr=None,
        id=None,
        updated_at=None,
        dns_search_domains=None,
    ):  # noqa: E501
        """FabricNetworkVsphere - a model defined in Swagger"""  # noqa: E501
        self._owner = None
        self._links = None
        self._external_region_id = None
        self._default_ipv6_gateway = None
        self._description = None
        self._external_id = None
        self._default_gateway = None
        self._org_id = None
        self._tags = None
        self._created_at = None
        self._ipv6_cidr = None
        self._cloud_account_ids = None
        self._is_default = None
        self._custom_properties = None
        self._domain = None
        self._dns_server_addresses = None
        self._name = None
        self._is_public = None
        self._cidr = None
        self._id = None
        self._updated_at = None
        self._dns_search_domains = None
        self.discriminator = None
        if owner is not None:
            self.owner = owner
        self.links = links
        if external_region_id is not None:
            self.external_region_id = external_region_id
        if default_ipv6_gateway is not None:
            self.default_ipv6_gateway = default_ipv6_gateway
        if description is not None:
            self.description = description
        if external_id is not None:
            self.external_id = external_id
        if default_gateway is not None:
            self.default_gateway = default_gateway
        if org_id is not None:
            self.org_id = org_id
        if tags is not None:
            self.tags = tags
        if created_at is not None:
            self.created_at = created_at
        if ipv6_cidr is not None:
            self.ipv6_cidr = ipv6_cidr
        if cloud_account_ids is not None:
            self.cloud_account_ids = cloud_account_ids
        if is_default is not None:
            self.is_default = is_default
        if custom_properties is not None:
            self.custom_properties = custom_properties
        if domain is not None:
            self.domain = domain
        if dns_server_addresses is not None:
            self.dns_server_addresses = dns_server_addresses
        if name is not None:
            self.name = name
        if is_public is not None:
            self.is_public = is_public
        if cidr is not None:
            self.cidr = cidr
        self.id = id
        if updated_at is not None:
            self.updated_at = updated_at
        if dns_search_domains is not None:
            self.dns_search_domains = dns_search_domains

    @property
    def owner(self):
        """Gets the owner of this FabricNetworkVsphere.  # noqa: E501

        Email of the user that owns the entity.  # noqa: E501

        :return: The owner of this FabricNetworkVsphere.  # noqa: E501
        :rtype: str
        """
        return self._owner

    @owner.setter
    def owner(self, owner):
        """Sets the owner of this FabricNetworkVsphere.

        Email of the user that owns the entity.  # noqa: E501

        :param owner: The owner of this FabricNetworkVsphere.  # noqa: E501
        :type: str
        """

        self._owner = owner

    @property
    def links(self):
        """Gets the links of this FabricNetworkVsphere.  # noqa: E501

        HATEOAS of the entity  # noqa: E501

        :return: The links of this FabricNetworkVsphere.  # noqa: E501
        :rtype: dict(str, IaasapimachinesidoperationschangesecuritygroupsLinks)
        """
        return self._links

    @links.setter
    def links(self, links):
        """Sets the links of this FabricNetworkVsphere.

        HATEOAS of the entity  # noqa: E501

        :param links: The links of this FabricNetworkVsphere.  # noqa: E501
        :type: dict(str, IaasapimachinesidoperationschangesecuritygroupsLinks)
        """
        if links is None:
            raise ValueError(
                "Invalid value for `links`, must not be `None`"
            )  # noqa: E501

        self._links = links

    @property
    def external_region_id(self):
        """Gets the external_region_id of this FabricNetworkVsphere.  # noqa: E501

        The id of the region for which this network is defined  # noqa: E501

        :return: The external_region_id of this FabricNetworkVsphere.  # noqa: E501
        :rtype: str
        """
        return self._external_region_id

    @external_region_id.setter
    def external_region_id(self, external_region_id):
        """Sets the external_region_id of this FabricNetworkVsphere.

        The id of the region for which this network is defined  # noqa: E501

        :param external_region_id: The external_region_id of this FabricNetworkVsphere.  # noqa: E501
        :type: str
        """

        self._external_region_id = external_region_id

    @property
    def default_ipv6_gateway(self):
        """Gets the default_ipv6_gateway of this FabricNetworkVsphere.  # noqa: E501

        IPv6 default gateway to be used.  # noqa: E501

        :return: The default_ipv6_gateway of this FabricNetworkVsphere.  # noqa: E501
        :rtype: str
        """
        return self._default_ipv6_gateway

    @default_ipv6_gateway.setter
    def default_ipv6_gateway(self, default_ipv6_gateway):
        """Sets the default_ipv6_gateway of this FabricNetworkVsphere.

        IPv6 default gateway to be used.  # noqa: E501

        :param default_ipv6_gateway: The default_ipv6_gateway of this FabricNetworkVsphere.  # noqa: E501
        :type: str
        """

        self._default_ipv6_gateway = default_ipv6_gateway

    @property
    def description(self):
        """Gets the description of this FabricNetworkVsphere.  # noqa: E501

        A human-friendly description.  # noqa: E501

        :return: The description of this FabricNetworkVsphere.  # noqa: E501
        :rtype: str
        """
        return self._description

    @description.setter
    def description(self, description):
        """Sets the description of this FabricNetworkVsphere.

        A human-friendly description.  # noqa: E501

        :param description: The description of this FabricNetworkVsphere.  # noqa: E501
        :type: str
        """

        self._description = description

    @property
    def external_id(self):
        """Gets the external_id of this FabricNetworkVsphere.  # noqa: E501

        External entity Id on the provider side.  # noqa: E501

        :return: The external_id of this FabricNetworkVsphere.  # noqa: E501
        :rtype: str
        """
        return self._external_id

    @external_id.setter
    def external_id(self, external_id):
        """Sets the external_id of this FabricNetworkVsphere.

        External entity Id on the provider side.  # noqa: E501

        :param external_id: The external_id of this FabricNetworkVsphere.  # noqa: E501
        :type: str
        """

        self._external_id = external_id

    @property
    def default_gateway(self):
        """Gets the default_gateway of this FabricNetworkVsphere.  # noqa: E501

        IPv4 default gateway to be used.  # noqa: E501

        :return: The default_gateway of this FabricNetworkVsphere.  # noqa: E501
        :rtype: str
        """
        return self._default_gateway

    @default_gateway.setter
    def default_gateway(self, default_gateway):
        """Sets the default_gateway of this FabricNetworkVsphere.

        IPv4 default gateway to be used.  # noqa: E501

        :param default_gateway: The default_gateway of this FabricNetworkVsphere.  # noqa: E501
        :type: str
        """

        self._default_gateway = default_gateway

    @property
    def org_id(self):
        """Gets the org_id of this FabricNetworkVsphere.  # noqa: E501

        The id of the organization this entity belongs to.  # noqa: E501

        :return: The org_id of this FabricNetworkVsphere.  # noqa: E501
        :rtype: str
        """
        return self._org_id

    @org_id.setter
    def org_id(self, org_id):
        """Sets the org_id of this FabricNetworkVsphere.

        The id of the organization this entity belongs to.  # noqa: E501

        :param org_id: The org_id of this FabricNetworkVsphere.  # noqa: E501
        :type: str
        """

        self._org_id = org_id

    @property
    def tags(self):
        """Gets the tags of this FabricNetworkVsphere.  # noqa: E501

        A set of tag keys and optional values that were set on this resource instance.  # noqa: E501

        :return: The tags of this FabricNetworkVsphere.  # noqa: E501
        :rtype: list[IaasapistorageprofilesgcpTags]
        """
        return self._tags

    @tags.setter
    def tags(self, tags):
        """Sets the tags of this FabricNetworkVsphere.

        A set of tag keys and optional values that were set on this resource instance.  # noqa: E501

        :param tags: The tags of this FabricNetworkVsphere.  # noqa: E501
        :type: list[IaasapistorageprofilesgcpTags]
        """

        self._tags = tags

    @property
    def created_at(self):
        """Gets the created_at of this FabricNetworkVsphere.  # noqa: E501

        Date when the entity was created. The date is in ISO 8601 and UTC.  # noqa: E501

        :return: The created_at of this FabricNetworkVsphere.  # noqa: E501
        :rtype: str
        """
        return self._created_at

    @created_at.setter
    def created_at(self, created_at):
        """Sets the created_at of this FabricNetworkVsphere.

        Date when the entity was created. The date is in ISO 8601 and UTC.  # noqa: E501

        :param created_at: The created_at of this FabricNetworkVsphere.  # noqa: E501
        :type: str
        """

        self._created_at = created_at

    @property
    def ipv6_cidr(self):
        """Gets the ipv6_cidr of this FabricNetworkVsphere.  # noqa: E501

        Network IPv6 CIDR to be used.  # noqa: E501

        :return: The ipv6_cidr of this FabricNetworkVsphere.  # noqa: E501
        :rtype: str
        """
        return self._ipv6_cidr

    @ipv6_cidr.setter
    def ipv6_cidr(self, ipv6_cidr):
        """Sets the ipv6_cidr of this FabricNetworkVsphere.

        Network IPv6 CIDR to be used.  # noqa: E501

        :param ipv6_cidr: The ipv6_cidr of this FabricNetworkVsphere.  # noqa: E501
        :type: str
        """

        self._ipv6_cidr = ipv6_cidr

    @property
    def cloud_account_ids(self):
        """Gets the cloud_account_ids of this FabricNetworkVsphere.  # noqa: E501

        Set of ids of the cloud accounts this entity belongs to.  # noqa: E501

        :return: The cloud_account_ids of this FabricNetworkVsphere.  # noqa: E501
        :rtype: list[str]
        """
        return self._cloud_account_ids

    @cloud_account_ids.setter
    def cloud_account_ids(self, cloud_account_ids):
        """Sets the cloud_account_ids of this FabricNetworkVsphere.

        Set of ids of the cloud accounts this entity belongs to.  # noqa: E501

        :param cloud_account_ids: The cloud_account_ids of this FabricNetworkVsphere.  # noqa: E501
        :type: list[str]
        """

        self._cloud_account_ids = cloud_account_ids

    @property
    def is_default(self):
        """Gets the is_default of this FabricNetworkVsphere.  # noqa: E501

        Indicates whether this is the default subnet for the zone.  # noqa: E501

        :return: The is_default of this FabricNetworkVsphere.  # noqa: E501
        :rtype: bool
        """
        return self._is_default

    @is_default.setter
    def is_default(self, is_default):
        """Sets the is_default of this FabricNetworkVsphere.

        Indicates whether this is the default subnet for the zone.  # noqa: E501

        :param is_default: The is_default of this FabricNetworkVsphere.  # noqa: E501
        :type: bool
        """

        self._is_default = is_default

    @property
    def custom_properties(self):
        """Gets the custom_properties of this FabricNetworkVsphere.  # noqa: E501

        Custom properties of the fabric network instance  # noqa: E501

        :return: The custom_properties of this FabricNetworkVsphere.  # noqa: E501
        :rtype: dict(str, str)
        """
        return self._custom_properties

    @custom_properties.setter
    def custom_properties(self, custom_properties):
        """Sets the custom_properties of this FabricNetworkVsphere.

        Custom properties of the fabric network instance  # noqa: E501

        :param custom_properties: The custom_properties of this FabricNetworkVsphere.  # noqa: E501
        :type: dict(str, str)
        """

        self._custom_properties = custom_properties

    @property
    def domain(self):
        """Gets the domain of this FabricNetworkVsphere.  # noqa: E501

        Domain value.  # noqa: E501

        :return: The domain of this FabricNetworkVsphere.  # noqa: E501
        :rtype: str
        """
        return self._domain

    @domain.setter
    def domain(self, domain):
        """Sets the domain of this FabricNetworkVsphere.

        Domain value.  # noqa: E501

        :param domain: The domain of this FabricNetworkVsphere.  # noqa: E501
        :type: str
        """

        self._domain = domain

    @property
    def dns_server_addresses(self):
        """Gets the dns_server_addresses of this FabricNetworkVsphere.  # noqa: E501

        A list of DNS server addresses that were set on this resource instance.  # noqa: E501

        :return: The dns_server_addresses of this FabricNetworkVsphere.  # noqa: E501
        :rtype: list[str]
        """
        return self._dns_server_addresses

    @dns_server_addresses.setter
    def dns_server_addresses(self, dns_server_addresses):
        """Sets the dns_server_addresses of this FabricNetworkVsphere.

        A list of DNS server addresses that were set on this resource instance.  # noqa: E501

        :param dns_server_addresses: The dns_server_addresses of this FabricNetworkVsphere.  # noqa: E501
        :type: list[str]
        """

        self._dns_server_addresses = dns_server_addresses

    @property
    def name(self):
        """Gets the name of this FabricNetworkVsphere.  # noqa: E501

        A human-friendly name used as an identifier in APIs that support this option.  # noqa: E501

        :return: The name of this FabricNetworkVsphere.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this FabricNetworkVsphere.

        A human-friendly name used as an identifier in APIs that support this option.  # noqa: E501

        :param name: The name of this FabricNetworkVsphere.  # noqa: E501
        :type: str
        """

        self._name = name

    @property
    def is_public(self):
        """Gets the is_public of this FabricNetworkVsphere.  # noqa: E501

        Indicates whether the sub-network supports public IP assignment.  # noqa: E501

        :return: The is_public of this FabricNetworkVsphere.  # noqa: E501
        :rtype: bool
        """
        return self._is_public

    @is_public.setter
    def is_public(self, is_public):
        """Sets the is_public of this FabricNetworkVsphere.

        Indicates whether the sub-network supports public IP assignment.  # noqa: E501

        :param is_public: The is_public of this FabricNetworkVsphere.  # noqa: E501
        :type: bool
        """

        self._is_public = is_public

    @property
    def cidr(self):
        """Gets the cidr of this FabricNetworkVsphere.  # noqa: E501

        Network CIDR to be used.  # noqa: E501

        :return: The cidr of this FabricNetworkVsphere.  # noqa: E501
        :rtype: str
        """
        return self._cidr

    @cidr.setter
    def cidr(self, cidr):
        """Sets the cidr of this FabricNetworkVsphere.

        Network CIDR to be used.  # noqa: E501

        :param cidr: The cidr of this FabricNetworkVsphere.  # noqa: E501
        :type: str
        """

        self._cidr = cidr

    @property
    def id(self):
        """Gets the id of this FabricNetworkVsphere.  # noqa: E501

        The id of this resource instance  # noqa: E501

        :return: The id of this FabricNetworkVsphere.  # noqa: E501
        :rtype: str
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this FabricNetworkVsphere.

        The id of this resource instance  # noqa: E501

        :param id: The id of this FabricNetworkVsphere.  # noqa: E501
        :type: str
        """
        if id is None:
            raise ValueError("Invalid value for `id`, must not be `None`")  # noqa: E501

        self._id = id

    @property
    def updated_at(self):
        """Gets the updated_at of this FabricNetworkVsphere.  # noqa: E501

        Date when the entity was last updated. The date is ISO 8601 and UTC.  # noqa: E501

        :return: The updated_at of this FabricNetworkVsphere.  # noqa: E501
        :rtype: str
        """
        return self._updated_at

    @updated_at.setter
    def updated_at(self, updated_at):
        """Sets the updated_at of this FabricNetworkVsphere.

        Date when the entity was last updated. The date is ISO 8601 and UTC.  # noqa: E501

        :param updated_at: The updated_at of this FabricNetworkVsphere.  # noqa: E501
        :type: str
        """

        self._updated_at = updated_at

    @property
    def dns_search_domains(self):
        """Gets the dns_search_domains of this FabricNetworkVsphere.  # noqa: E501

        A list of DNS search domains that were set on this resource instance.  # noqa: E501

        :return: The dns_search_domains of this FabricNetworkVsphere.  # noqa: E501
        :rtype: list[str]
        """
        return self._dns_search_domains

    @dns_search_domains.setter
    def dns_search_domains(self, dns_search_domains):
        """Sets the dns_search_domains of this FabricNetworkVsphere.

        A list of DNS search domains that were set on this resource instance.  # noqa: E501

        :param dns_search_domains: The dns_search_domains of this FabricNetworkVsphere.  # noqa: E501
        :type: list[str]
        """

        self._dns_search_domains = dns_search_domains

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in self.swagger_types.items():
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(
                    map(lambda x: x.to_dict() if hasattr(x, "to_dict") else x, value)
                )
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(
                    map(
                        lambda item: (item[0], item[1].to_dict())
                        if hasattr(item[1], "to_dict")
                        else item,
                        value.items(),
                    )
                )
            else:
                result[attr] = value
        if issubclass(FabricNetworkVsphere, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, FabricNetworkVsphere):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other

"""
    VMware Cloud Assembly IaaS API

    IAAS API is a RESTful service, which allows users to execute provisioning related UI actions via an API. <br><br>This page describes the RESTful APIs for IAAS API. The APIs facilitate CRUD operations on the various resources and entities used throughout Cloud Assembly(Cloud Accounts, Cloud zones, Computes, Mappings, etc.) and allows operations on them (creating a cloud account, patching a machine, creating image progile, etc.).<br><br>The APIs that list collections of resources  also support OData like implementation. Below query params can be used across different IAAS API endpoints<br><br><ol><li>`$orderby` -  returns a result with the rows being sorted by the values of provided attribute.<br>`/iaas/api/cloud-accounts?$orderby=name%20desc`</li><br><li>`$top` - number of records you want to get.<br>`/iaas/api/cloud-accounts?$top=20`</li><br><li>`$skip` - number of records you want to skip.<br>`/iaas/api/cloud-accounts?$skip=10`</li><br><li>`$select` - select a subset of properties to include in the response.<br>`/iaas/api/cloud-accounts?$select=id`</li><br><li>`$filter` - filter the results by a specified predicate expression. Operators: eq, ne, and, or.<br>`/iaas/api/cloud-accounts?$filter=name%20eq%20'ABC*'` - name starts with 'ABC'<br>`/iaas/api/cloud-accounts?$filter=name%20eq%20'*ABC*'` - name contains 'ABC'<br>`/iaas/api/cloud-accounts?$filter=name%20eq%20'*ABC'` - name ends with 'ABC'<br><b>/iaas/api/projects</b> and <b>/iaas/api/deployments</b> support different format for partial match: <br>`/iaas/api/projects?$filter=startswith(name, 'ABC')` - name starts with 'ABC'<br>`/iaas/api/projects?$filter=substringof('ABC', name)` - name contains 'ABC'<br>`/iaas/api/projects?$filter=endswith(name, 'ABC')` - name ends with 'ABC'<br>`/iaas/api/cloud-accounts?$filter=name%20ne%20'example-cloud-account'%20or%20customProperties.isExternal%20eq%20'false'`</li><br><li>`$count` - flag which when specified, regardless of the assigned value, shows the total number of records. If the collection has a filter it shows the number of records matching the filter.<br>`/iaas/api/cloud-accounts?$count=true`<br></li></ol>  # noqa: E501

    OpenAPI spec version: 2021-07-15

    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""
import pprint
import re  # noqa: F401


class ApiConfigurationpropertiesBody:
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {"value": "str", "key": "str"}

    attribute_map = {"value": "value", "key": "key"}

    def __init__(self, value=None, key=None):  # noqa: E501
        """ApiConfigurationpropertiesBody - a model defined in Swagger"""  # noqa: E501
        self._value = None
        self._key = None
        self.discriminator = None
        self.value = value
        self.key = key

    @property
    def value(self):
        """Gets the value of this ApiConfigurationpropertiesBody.  # noqa: E501

        The value of the property.  # noqa: E501

        :return: The value of this ApiConfigurationpropertiesBody.  # noqa: E501
        :rtype: str
        """
        return self._value

    @value.setter
    def value(self, value):
        """Sets the value of this ApiConfigurationpropertiesBody.

        The value of the property.  # noqa: E501

        :param value: The value of this ApiConfigurationpropertiesBody.  # noqa: E501
        :type: str
        """
        if value is None:
            raise ValueError(
                "Invalid value for `value`, must not be `None`"
            )  # noqa: E501

        self._value = value

    @property
    def key(self):
        """Gets the key of this ApiConfigurationpropertiesBody.  # noqa: E501

        The key of the property.  # noqa: E501

        :return: The key of this ApiConfigurationpropertiesBody.  # noqa: E501
        :rtype: str
        """
        return self._key

    @key.setter
    def key(self, key):
        """Sets the key of this ApiConfigurationpropertiesBody.

        The key of the property.  # noqa: E501

        :param key: The key of this ApiConfigurationpropertiesBody.  # noqa: E501
        :type: str
        """
        if key is None:
            raise ValueError(
                "Invalid value for `key`, must not be `None`"
            )  # noqa: E501
        allowed_values = [
            "SESSION_TIMEOUT_DURATION_MINUTES",
            "RELEASE_IPADDRESS_PERIOD_MINUTES",
            "NSXT_RETRY_DURATION_MINUTES",
        ]  # noqa: E501
        if key not in allowed_values:
            raise ValueError(
                "Invalid value for `key` ({}), must be one of {}".format(  # noqa: E501
                    key, allowed_values
                )
            )

        self._key = key

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in self.swagger_types.items():
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(
                    map(lambda x: x.to_dict() if hasattr(x, "to_dict") else x, value)
                )
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(
                    map(
                        lambda item: (item[0], item[1].to_dict())
                        if hasattr(item[1], "to_dict")
                        else item,
                        value.items(),
                    )
                )
            else:
                result[attr] = value
        if issubclass(ApiConfigurationpropertiesBody, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, ApiConfigurationpropertiesBody):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other

"""
    VMware Service Broker API

    A multi-cloud API for Cloud Automation Services  # noqa: E501

    OpenAPI spec version: 2020-08-25

    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""
import re  # noqa: F401

from idem_vra.client.vra_catalog_lib.api_client import ApiClient

# python 2 and python 3 compatibility library


class CatalogSourcesApi:
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    Ref: https://github.com/swagger-api/swagger-codegen
    """

    def __init__(self, api_client=None):
        if api_client is None:
            api_client = ApiClient()
        self.api_client = api_client

    def delete_using_delete4(self, source_id, **kwargs):  # noqa: E501
        """Delete catalog source.  # noqa: E501

        Deletes the catalog source with the supplied ID.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.delete_using_delete4(source_id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str source_id: Catalog source ID (required)
        :param str api_version: The version of the API in yyyy-MM-dd format (UTC). If you do not specify explicitly an exact version, you will be calling the latest supported API version.
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs["_return_http_data_only"] = True
        if kwargs.get("async_req"):
            return self.delete_using_delete4_with_http_info(
                source_id, **kwargs
            )  # noqa: E501
        else:
            (data) = self.delete_using_delete4_with_http_info(
                source_id, **kwargs
            )  # noqa: E501
            return data

    def delete_using_delete4_with_http_info(self, source_id, **kwargs):  # noqa: E501
        """Delete catalog source.  # noqa: E501

        Deletes the catalog source with the supplied ID.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.delete_using_delete4_with_http_info(source_id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str source_id: Catalog source ID (required)
        :param str api_version: The version of the API in yyyy-MM-dd format (UTC). If you do not specify explicitly an exact version, you will be calling the latest supported API version.
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ["source_id", "api_version"]  # noqa: E501
        all_params.append("async_req")
        all_params.append("_return_http_data_only")
        all_params.append("_preload_content")
        all_params.append("_request_timeout")

        params = locals()
        for key, val in params["kwargs"].items():
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method delete_using_delete4" % key
                )
            params[key] = val
        del params["kwargs"]
        # verify the required parameter 'source_id' is set
        if "source_id" not in params or params["source_id"] is None:
            raise ValueError(
                "Missing the required parameter `source_id` when calling `delete_using_delete4`"
            )  # noqa: E501

        collection_formats = {}

        path_params = {}
        if "source_id" in params:
            path_params["sourceId"] = params["source_id"]  # noqa: E501

        query_params = []
        if "api_version" in params:
            query_params.append(("apiVersion", params["api_version"]))  # noqa: E501

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # Authentication setting
        auth_settings = ["Bearer"]  # noqa: E501

        return self.api_client.call_api(
            "/catalog/api/admin/sources/{sourceId}",
            "DELETE",
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type=None,  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get("async_req"),
            _return_http_data_only=params.get("_return_http_data_only"),
            _preload_content=params.get("_preload_content", True),
            _request_timeout=params.get("_request_timeout"),
            collection_formats=collection_formats,
        )

    def get_page_using_get2(self, **kwargs):  # noqa: E501
        """Fetch catalog sources.  # noqa: E501

        Returns a paginated list of catalog sources.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_page_using_get2(async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param list[str] orderby: Sorting criteria in the format: property (asc|desc). Default sort order is ascending. Multiple sort criteria are supported.
        :param int skip: Number of records you want to skip
        :param int top: Number of records you want
        :param str api_version: The version of the API in yyyy-MM-dd format (UTC). If you do not specify explicitly an exact version, you will be calling the latest supported API version.
        :param str project_id: Find sources which contains items that can be requested in the given projectId
        :param str search: Matches will have this string in their name or description.
        :return: PageOfCatalogSource1
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs["_return_http_data_only"] = True
        if kwargs.get("async_req"):
            return self.get_page_using_get2_with_http_info(**kwargs)  # noqa: E501
        else:
            (data) = self.get_page_using_get2_with_http_info(**kwargs)  # noqa: E501
            return data

    def get_page_using_get2_with_http_info(self, **kwargs):  # noqa: E501
        """Fetch catalog sources.  # noqa: E501

        Returns a paginated list of catalog sources.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_page_using_get2_with_http_info(async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param list[str] orderby: Sorting criteria in the format: property (asc|desc). Default sort order is ascending. Multiple sort criteria are supported.
        :param int skip: Number of records you want to skip
        :param int top: Number of records you want
        :param str api_version: The version of the API in yyyy-MM-dd format (UTC). If you do not specify explicitly an exact version, you will be calling the latest supported API version.
        :param str project_id: Find sources which contains items that can be requested in the given projectId
        :param str search: Matches will have this string in their name or description.
        :return: PageOfCatalogSource1
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = [
            "orderby",
            "skip",
            "top",
            "api_version",
            "project_id",
            "search",
        ]  # noqa: E501
        all_params.append("async_req")
        all_params.append("_return_http_data_only")
        all_params.append("_preload_content")
        all_params.append("_request_timeout")

        params = locals()
        for key, val in params["kwargs"].items():
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_page_using_get2" % key
                )
            params[key] = val
        del params["kwargs"]

        collection_formats = {}

        path_params = {}

        query_params = []
        if "orderby" in params:
            query_params.append(("$orderby", params["orderby"]))  # noqa: E501
            collection_formats["$orderby"] = "multi"  # noqa: E501
        if "skip" in params:
            query_params.append(("$skip", params["skip"]))  # noqa: E501
        if "top" in params:
            query_params.append(("$top", params["top"]))  # noqa: E501
        if "api_version" in params:
            query_params.append(("apiVersion", params["api_version"]))  # noqa: E501
        if "project_id" in params:
            query_params.append(("projectId", params["project_id"]))  # noqa: E501
        if "search" in params:
            query_params.append(("search", params["search"]))  # noqa: E501

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params["Accept"] = self.api_client.select_header_accept(
            ["application/json"]
        )  # noqa: E501

        # Authentication setting
        auth_settings = ["Bearer"]  # noqa: E501

        return self.api_client.call_api(
            "/catalog/api/admin/sources",
            "GET",
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type="PageOfCatalogSource1",  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get("async_req"),
            _return_http_data_only=params.get("_return_http_data_only"),
            _preload_content=params.get("_preload_content", True),
            _request_timeout=params.get("_request_timeout"),
            collection_formats=collection_formats,
        )

    def get_using_get2(self, source_id, **kwargs):  # noqa: E501
        """Fetch a specific catalog source for the given ID.  # noqa: E501

        Returns the catalog source with the supplied ID.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_using_get2(source_id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str source_id: Catalog source ID (required)
        :param str api_version: The version of the API in yyyy-MM-dd format (UTC). If you do not specify explicitly an exact version, you will be calling the latest supported API version.
        :return: CatalogSource1
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs["_return_http_data_only"] = True
        if kwargs.get("async_req"):
            return self.get_using_get2_with_http_info(source_id, **kwargs)  # noqa: E501
        else:
            (data) = self.get_using_get2_with_http_info(
                source_id, **kwargs
            )  # noqa: E501
            return data

    def get_using_get2_with_http_info(self, source_id, **kwargs):  # noqa: E501
        """Fetch a specific catalog source for the given ID.  # noqa: E501

        Returns the catalog source with the supplied ID.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_using_get2_with_http_info(source_id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str source_id: Catalog source ID (required)
        :param str api_version: The version of the API in yyyy-MM-dd format (UTC). If you do not specify explicitly an exact version, you will be calling the latest supported API version.
        :return: CatalogSource1
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ["source_id", "api_version"]  # noqa: E501
        all_params.append("async_req")
        all_params.append("_return_http_data_only")
        all_params.append("_preload_content")
        all_params.append("_request_timeout")

        params = locals()
        for key, val in params["kwargs"].items():
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_using_get2" % key
                )
            params[key] = val
        del params["kwargs"]
        # verify the required parameter 'source_id' is set
        if "source_id" not in params or params["source_id"] is None:
            raise ValueError(
                "Missing the required parameter `source_id` when calling `get_using_get2`"
            )  # noqa: E501

        collection_formats = {}

        path_params = {}
        if "source_id" in params:
            path_params["sourceId"] = params["source_id"]  # noqa: E501

        query_params = []
        if "api_version" in params:
            query_params.append(("apiVersion", params["api_version"]))  # noqa: E501

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params["Accept"] = self.api_client.select_header_accept(
            ["application/json"]
        )  # noqa: E501

        # Authentication setting
        auth_settings = ["Bearer"]  # noqa: E501

        return self.api_client.call_api(
            "/catalog/api/admin/sources/{sourceId}",
            "GET",
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type="CatalogSource1",  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get("async_req"),
            _return_http_data_only=params.get("_return_http_data_only"),
            _preload_content=params.get("_preload_content", True),
            _request_timeout=params.get("_request_timeout"),
            collection_formats=collection_formats,
        )

    def post_using_post2(self, **kwargs):  # noqa: E501
        """Create or update a catalog source. Creating or updating also imports (or re-imports) the associated catalog items.  # noqa: E501

        Creates a new catalog source or updates an existing catalog source based on the request body and imports catalog items from it.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.post_using_post2(async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param CatalogSource1 body: The catalog source to be created
        :param str api_version: The version of the API in yyyy-MM-dd format (UTC). If you do not specify explicitly an exact version, you will be calling the latest supported API version.
        :param bool validation_only: If true, the source will not be created. It returns the number of items belonging to the source. The request will still return an error code if the source is invalid.
        :return: CatalogSource1
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs["_return_http_data_only"] = True
        if kwargs.get("async_req"):
            return self.post_using_post2_with_http_info(**kwargs)  # noqa: E501
        else:
            (data) = self.post_using_post2_with_http_info(**kwargs)  # noqa: E501
            return data

    def post_using_post2_with_http_info(self, **kwargs):  # noqa: E501
        """Create or update a catalog source. Creating or updating also imports (or re-imports) the associated catalog items.  # noqa: E501

        Creates a new catalog source or updates an existing catalog source based on the request body and imports catalog items from it.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.post_using_post2_with_http_info(async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param CatalogSource1 body: The catalog source to be created
        :param str api_version: The version of the API in yyyy-MM-dd format (UTC). If you do not specify explicitly an exact version, you will be calling the latest supported API version.
        :param bool validation_only: If true, the source will not be created. It returns the number of items belonging to the source. The request will still return an error code if the source is invalid.
        :return: CatalogSource1
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ["body", "api_version", "validation_only"]  # noqa: E501
        all_params.append("async_req")
        all_params.append("_return_http_data_only")
        all_params.append("_preload_content")
        all_params.append("_request_timeout")

        params = locals()
        for key, val in params["kwargs"].items():
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method post_using_post2" % key
                )
            params[key] = val
        del params["kwargs"]

        collection_formats = {}

        path_params = {}

        query_params = []
        if "api_version" in params:
            query_params.append(("apiVersion", params["api_version"]))  # noqa: E501
        if "validation_only" in params:
            query_params.append(
                ("validationOnly", params["validation_only"])
            )  # noqa: E501

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        if "body" in params:
            body_params = params["body"]
        # HTTP header `Accept`
        header_params["Accept"] = self.api_client.select_header_accept(
            ["application/json"]
        )  # noqa: E501

        # HTTP header `Content-Type`
        header_params[
            "Content-Type"
        ] = self.api_client.select_header_content_type(  # noqa: E501
            ["application/json"]
        )  # noqa: E501

        # Authentication setting
        auth_settings = ["Bearer"]  # noqa: E501

        return self.api_client.call_api(
            "/catalog/api/admin/sources",
            "POST",
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type="CatalogSource1",  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get("async_req"),
            _return_http_data_only=params.get("_return_http_data_only"),
            _preload_content=params.get("_preload_content", True),
            _request_timeout=params.get("_request_timeout"),
            collection_formats=collection_formats,
        )

"""
    VMware Service Broker API

    A multi-cloud API for Cloud Automation Services  # noqa: E501

    OpenAPI spec version: 2020-08-25

    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""
import pprint
import re  # noqa: F401


class ContentDefinition1:
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        "description": "str",
        "icon_id": "str",
        "id": "str",
        "name": "str",
        "num_items": "int",
        "source_name": "str",
        "source_type": "str",
        "type": "str",
    }

    attribute_map = {
        "description": "description",
        "icon_id": "iconId",
        "id": "id",
        "name": "name",
        "num_items": "numItems",
        "source_name": "sourceName",
        "source_type": "sourceType",
        "type": "type",
    }

    def __init__(
        self,
        description=None,
        icon_id=None,
        id=None,
        name=None,
        num_items=None,
        source_name=None,
        source_type=None,
        type=None,
    ):  # noqa: E501
        """ContentDefinition1 - a model defined in Swagger"""  # noqa: E501
        self._description = None
        self._icon_id = None
        self._id = None
        self._name = None
        self._num_items = None
        self._source_name = None
        self._source_type = None
        self._type = None
        self.discriminator = None
        if description is not None:
            self.description = description
        if icon_id is not None:
            self.icon_id = icon_id
        self.id = id
        if name is not None:
            self.name = name
        if num_items is not None:
            self.num_items = num_items
        if source_name is not None:
            self.source_name = source_name
        if source_type is not None:
            self.source_type = source_type
        self.type = type

    @property
    def description(self):
        """Gets the description of this ContentDefinition1.  # noqa: E501

        Description of either the catalog item or the catalog source  # noqa: E501

        :return: The description of this ContentDefinition1.  # noqa: E501
        :rtype: str
        """
        return self._description

    @description.setter
    def description(self, description):
        """Sets the description of this ContentDefinition1.

        Description of either the catalog item or the catalog source  # noqa: E501

        :param description: The description of this ContentDefinition1.  # noqa: E501
        :type: str
        """

        self._description = description

    @property
    def icon_id(self):
        """Gets the icon_id of this ContentDefinition1.  # noqa: E501

        Icon id of associated catalog item (if association is with catalog item)  # noqa: E501

        :return: The icon_id of this ContentDefinition1.  # noqa: E501
        :rtype: str
        """
        return self._icon_id

    @icon_id.setter
    def icon_id(self, icon_id):
        """Sets the icon_id of this ContentDefinition1.

        Icon id of associated catalog item (if association is with catalog item)  # noqa: E501

        :param icon_id: The icon_id of this ContentDefinition1.  # noqa: E501
        :type: str
        """

        self._icon_id = icon_id

    @property
    def id(self):
        """Gets the id of this ContentDefinition1.  # noqa: E501

        Id of either the catalog source or catalog item.  # noqa: E501

        :return: The id of this ContentDefinition1.  # noqa: E501
        :rtype: str
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this ContentDefinition1.

        Id of either the catalog source or catalog item.  # noqa: E501

        :param id: The id of this ContentDefinition1.  # noqa: E501
        :type: str
        """
        if id is None:
            raise ValueError("Invalid value for `id`, must not be `None`")  # noqa: E501

        self._id = id

    @property
    def name(self):
        """Gets the name of this ContentDefinition1.  # noqa: E501

        Name of either the catalog item or the catalog source  # noqa: E501

        :return: The name of this ContentDefinition1.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this ContentDefinition1.

        Name of either the catalog item or the catalog source  # noqa: E501

        :param name: The name of this ContentDefinition1.  # noqa: E501
        :type: str
        """

        self._name = name

    @property
    def num_items(self):
        """Gets the num_items of this ContentDefinition1.  # noqa: E501

        Number of items in the associated catalog source  # noqa: E501

        :return: The num_items of this ContentDefinition1.  # noqa: E501
        :rtype: int
        """
        return self._num_items

    @num_items.setter
    def num_items(self, num_items):
        """Sets the num_items of this ContentDefinition1.

        Number of items in the associated catalog source  # noqa: E501

        :param num_items: The num_items of this ContentDefinition1.  # noqa: E501
        :type: int
        """

        self._num_items = num_items

    @property
    def source_name(self):
        """Gets the source_name of this ContentDefinition1.  # noqa: E501

        Catalog source name  # noqa: E501

        :return: The source_name of this ContentDefinition1.  # noqa: E501
        :rtype: str
        """
        return self._source_name

    @source_name.setter
    def source_name(self, source_name):
        """Sets the source_name of this ContentDefinition1.

        Catalog source name  # noqa: E501

        :param source_name: The source_name of this ContentDefinition1.  # noqa: E501
        :type: str
        """

        self._source_name = source_name

    @property
    def source_type(self):
        """Gets the source_type of this ContentDefinition1.  # noqa: E501

        Catalog source type  # noqa: E501

        :return: The source_type of this ContentDefinition1.  # noqa: E501
        :rtype: str
        """
        return self._source_type

    @source_type.setter
    def source_type(self, source_type):
        """Sets the source_type of this ContentDefinition1.

        Catalog source type  # noqa: E501

        :param source_type: The source_type of this ContentDefinition1.  # noqa: E501
        :type: str
        """

        self._source_type = source_type

    @property
    def type(self):
        """Gets the type of this ContentDefinition1.  # noqa: E501

        Content definition type  # noqa: E501

        :return: The type of this ContentDefinition1.  # noqa: E501
        :rtype: str
        """
        return self._type

    @type.setter
    def type(self, type):
        """Sets the type of this ContentDefinition1.

        Content definition type  # noqa: E501

        :param type: The type of this ContentDefinition1.  # noqa: E501
        :type: str
        """
        if type is None:
            raise ValueError(
                "Invalid value for `type`, must not be `None`"
            )  # noqa: E501

        self._type = type

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in self.swagger_types.items():
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(
                    map(lambda x: x.to_dict() if hasattr(x, "to_dict") else x, value)
                )
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(
                    map(
                        lambda item: (item[0], item[1].to_dict())
                        if hasattr(item[1], "to_dict")
                        else item,
                        value.items(),
                    )
                )
            else:
                result[attr] = value
        if issubclass(ContentDefinition1, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, ContentDefinition1):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other

"""
    VMware Service Broker API

    A multi-cloud API for Cloud Automation Services  # noqa: E501

    OpenAPI spec version: 2020-08-25

    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""
import pprint
import re  # noqa: F401


class TagBasedRateFactorItem:
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {"item_name": "str", "rate_factors": "list[RateFactorItem1]"}

    attribute_map = {"item_name": "itemName", "rate_factors": "rateFactors"}

    def __init__(self, item_name=None, rate_factors=None):  # noqa: E501
        """TagBasedRateFactorItem - a model defined in Swagger"""  # noqa: E501
        self._item_name = None
        self._rate_factors = None
        self.discriminator = None
        if item_name is not None:
            self.item_name = item_name
        if rate_factors is not None:
            self.rate_factors = rate_factors

    @property
    def item_name(self):
        """Gets the item_name of this TagBasedRateFactorItem.  # noqa: E501


        :return: The item_name of this TagBasedRateFactorItem.  # noqa: E501
        :rtype: str
        """
        return self._item_name

    @item_name.setter
    def item_name(self, item_name):
        """Sets the item_name of this TagBasedRateFactorItem.


        :param item_name: The item_name of this TagBasedRateFactorItem.  # noqa: E501
        :type: str
        """

        self._item_name = item_name

    @property
    def rate_factors(self):
        """Gets the rate_factors of this TagBasedRateFactorItem.  # noqa: E501


        :return: The rate_factors of this TagBasedRateFactorItem.  # noqa: E501
        :rtype: list[RateFactorItem1]
        """
        return self._rate_factors

    @rate_factors.setter
    def rate_factors(self, rate_factors):
        """Sets the rate_factors of this TagBasedRateFactorItem.


        :param rate_factors: The rate_factors of this TagBasedRateFactorItem.  # noqa: E501
        :type: list[RateFactorItem1]
        """

        self._rate_factors = rate_factors

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in self.swagger_types.items():
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(
                    map(lambda x: x.to_dict() if hasattr(x, "to_dict") else x, value)
                )
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(
                    map(
                        lambda item: (item[0], item[1].to_dict())
                        if hasattr(item[1], "to_dict")
                        else item,
                        value.items(),
                    )
                )
            else:
                result[attr] = value
        if issubclass(TagBasedRateFactorItem, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, TagBasedRateFactorItem):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other

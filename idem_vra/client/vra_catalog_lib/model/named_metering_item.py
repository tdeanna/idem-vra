"""
    VMware Service Broker API

    A multi-cloud API for Cloud Automation Services  # noqa: E501

    OpenAPI spec version: 2020-08-25

    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""
import pprint
import re  # noqa: F401


class NamedMeteringItem:
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {"item_name": "str", "named_meterings": "list[NamedMetering1]"}

    attribute_map = {"item_name": "itemName", "named_meterings": "namedMeterings"}

    def __init__(self, item_name=None, named_meterings=None):  # noqa: E501
        """NamedMeteringItem - a model defined in Swagger"""  # noqa: E501
        self._item_name = None
        self._named_meterings = None
        self.discriminator = None
        if item_name is not None:
            self.item_name = item_name
        if named_meterings is not None:
            self.named_meterings = named_meterings

    @property
    def item_name(self):
        """Gets the item_name of this NamedMeteringItem.  # noqa: E501


        :return: The item_name of this NamedMeteringItem.  # noqa: E501
        :rtype: str
        """
        return self._item_name

    @item_name.setter
    def item_name(self, item_name):
        """Sets the item_name of this NamedMeteringItem.


        :param item_name: The item_name of this NamedMeteringItem.  # noqa: E501
        :type: str
        """

        self._item_name = item_name

    @property
    def named_meterings(self):
        """Gets the named_meterings of this NamedMeteringItem.  # noqa: E501


        :return: The named_meterings of this NamedMeteringItem.  # noqa: E501
        :rtype: list[NamedMetering1]
        """
        return self._named_meterings

    @named_meterings.setter
    def named_meterings(self, named_meterings):
        """Sets the named_meterings of this NamedMeteringItem.


        :param named_meterings: The named_meterings of this NamedMeteringItem.  # noqa: E501
        :type: list[NamedMetering1]
        """

        self._named_meterings = named_meterings

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in self.swagger_types.items():
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(
                    map(lambda x: x.to_dict() if hasattr(x, "to_dict") else x, value)
                )
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(
                    map(
                        lambda item: (item[0], item[1].to_dict())
                        if hasattr(item[1], "to_dict")
                        else item,
                        value.items(),
                    )
                )
            else:
                result[attr] = value
        if issubclass(NamedMeteringItem, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, NamedMeteringItem):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other

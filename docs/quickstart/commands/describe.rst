Describe
========

The *Describe* command will show you how your environment is currently
configured. The output describing your environment can also be used to
configure and update your environment.

Examples
++++++++

Environment
-----------

List and Describe your vRA Cloudaccounts

.. code-block:: bash

    idem describe vra.iaas.cloudaccount

Output:

.. code-block:: sls

    xxx.xxx.xxx.xxx-e1a6c91e99e1:
        vra.iaas.cloudaccount.present:
        - cloudAccountProperties:
        - cloudAccountType: nsxt
        - description: null
        - tags: []
        - name: xxx.xxx.xxx.xxx
        - id: cb9a30f7-xxxx-xxxx-xxxx-e1a6c91e99e1
        - associatedMobilityCloudAccountIds: null
        - certificateInfo:
        - regions: []
        - associatedCloudAccountIds: []
        - privateKeyId: admin
        - privateKey: <private key / password>

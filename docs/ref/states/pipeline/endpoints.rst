endpoints
=========

.. automodule:: idem_vra.states.vra.pipeline.endpoints
   :members:
   :undoc-members:
   :show-inheritance:

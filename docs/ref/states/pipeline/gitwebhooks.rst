gitwebhooks
===========

.. automodule:: idem_vra.states.vra.pipeline.gitwebhooks
   :members:
   :undoc-members:
   :show-inheritance:

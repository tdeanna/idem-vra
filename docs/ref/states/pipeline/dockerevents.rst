dockerevents
============

.. automodule:: idem_vra.states.vra.pipeline.dockerevents
   :members:
   :undoc-members:
   :show-inheritance:

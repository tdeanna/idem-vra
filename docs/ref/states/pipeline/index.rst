pipeline
========

.. toctree::
   :maxdepth: 4

   customintegrations
   dockerevents
   dockertriggers
   dockerwebhooks
   endpoints
   executions
   gerritevents
   gerritlisteners
   gerrittriggers
   gitevents
   gitwebhooks
   pipelines
   useroperations
   variables

dockertriggers
==============

.. automodule:: idem_vra.states.vra.pipeline.dockertriggers
   :members:
   :undoc-members:
   :show-inheritance:

gitevents
=========

.. automodule:: idem_vra.states.vra.pipeline.gitevents
   :members:
   :undoc-members:
   :show-inheritance:

propertygroups
==============

.. automodule:: idem_vra.states.vra.blueprint.propertygroups
   :members:
   :undoc-members:
   :show-inheritance:

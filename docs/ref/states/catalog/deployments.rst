deployments
===========

.. automodule:: idem_vra.states.vra.catalog.deployments
   :members:
   :undoc-members:
   :show-inheritance:

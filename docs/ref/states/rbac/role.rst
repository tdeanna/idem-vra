role
====

.. automodule:: idem_vra.states.vra.rbac.role
   :members:
   :undoc-members:
   :show-inheritance:

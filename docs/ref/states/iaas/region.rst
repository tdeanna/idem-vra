region
======

.. automodule:: idem_vra.states.vra.iaas.region
   :members:
   :undoc-members:
   :show-inheritance:

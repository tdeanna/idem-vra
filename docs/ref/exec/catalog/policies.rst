policies
========

.. automodule:: idem_vra.exec.vra.catalog.policies
   :members:
   :undoc-members:
   :show-inheritance:

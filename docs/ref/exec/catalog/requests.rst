requests
========

.. automodule:: idem_vra.exec.vra.catalog.requests
   :members:
   :undoc-members:
   :show-inheritance:

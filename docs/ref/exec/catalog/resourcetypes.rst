resourcetypes
=============

.. automodule:: idem_vra.exec.vra.catalog.resourcetypes
   :members:
   :undoc-members:
   :show-inheritance:

deployments
===========

.. automodule:: idem_vra.exec.vra.catalog.deployments
   :members:
   :undoc-members:
   :show-inheritance:

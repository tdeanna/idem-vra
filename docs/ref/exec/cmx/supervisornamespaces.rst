supervisornamespaces
====================

.. automodule:: idem_vra.exec.vra.cmx.supervisornamespaces
   :members:
   :undoc-members:
   :show-inheritance:

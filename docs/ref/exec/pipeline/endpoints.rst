endpoints
=========

.. automodule:: idem_vra.exec.vra.pipeline.endpoints
   :members:
   :undoc-members:
   :show-inheritance:

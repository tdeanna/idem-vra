resourcetypes
=============

.. automodule:: idem_vra.exec.vra.blueprint.resourcetypes
   :members:
   :undoc-members:
   :show-inheritance:

userauthcontext
===============

.. automodule:: idem_vra.exec.vra.rbac.userauthcontext
   :members:
   :undoc-members:
   :show-inheritance:

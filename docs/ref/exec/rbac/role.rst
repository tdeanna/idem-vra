role
====

.. automodule:: idem_vra.exec.vra.rbac.role
   :members:
   :undoc-members:
   :show-inheritance:

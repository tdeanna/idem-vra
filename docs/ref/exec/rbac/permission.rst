permission
==========

.. automodule:: idem_vra.exec.vra.rbac.permission
   :members:
   :undoc-members:
   :show-inheritance:

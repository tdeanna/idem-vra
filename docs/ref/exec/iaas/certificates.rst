certificates
============

.. automodule:: idem_vra.exec.vra.iaas.certificates
   :members:
   :undoc-members:
   :show-inheritance:
